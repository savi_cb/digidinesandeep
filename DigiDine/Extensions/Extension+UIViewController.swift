//
//  Extension+UIViewController.swift
//  Sneni
//
//  Created by neelam panwar on 23/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation

//MARK:- ======== ViewController Identifiers ========
enum Storyboard: String {
    
    
    case digiHome = "DigiDineHome"
    
    var storyBoard: UIStoryboard {
        return UIStoryboard(name: rawValue, bundle: Bundle.main)
    }
}


extension UIViewController {
    static func getVC(_ storyBoard: Storyboard) -> Self {
        
        func instanceFromNib<T: UIViewController>(_ storyBoard: Storyboard) -> T {
            guard let vc = controller(storyBoard: storyBoard, controller: T.identifier) as? T else {
                fatalError("'\(storyBoard.rawValue)' : '\(T.identifier)' is Not exist")
            }
            return vc
        }
        return instanceFromNib(storyBoard)
    }
    
    static func controller(storyBoard: Storyboard, controller: String) -> UIViewController {
        let storyBoard = storyBoard.storyBoard
        let vc = storyBoard.instantiateViewController(withIdentifier: controller)
        return vc
    }
}
