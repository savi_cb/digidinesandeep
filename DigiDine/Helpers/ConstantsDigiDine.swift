//
//  ConstantsDigiDine.swift
//  Sneni
//
//  Created by neelam panwar on 24/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation


enum VCLiterals: String {
    
    case userInfo = "UserInfo"
    case payment = "Payment"
    case promotions = "Vouchers"
    case orders = "Orders"
    case termsAndCondi = "Terms and conditions"
    case privacy = "Privacy policy"
    case howItWorks = "How it works"
    case contactUs = "Contact us"
    case logout = "Logout"
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

class ProfileLayout {
    
    var titleText: VCLiterals?
    var subTitleText: VCLiterals?
    
    init(title: VCLiterals?, subTitle: VCLiterals? = nil) {
        titleText = title
        subTitleText = subTitle
    }
    
    class func getProfileElements() -> [ProfileLayout] {
        var items = [ProfileLayout]()
        items = [
            ProfileLayout.init( title: VCLiterals.userInfo),
            //ProfileLayout.init(title: VCLiterals.payment),
            //ProfileLayout.init( title: VCLiterals.promotions),
            ProfileLayout.init( title: VCLiterals.orders) ,
            ProfileLayout.init( title: VCLiterals.termsAndCondi) ,
            ProfileLayout.init( title: VCLiterals.privacy),
            ProfileLayout.init( title: VCLiterals.howItWorks),
            ProfileLayout.init( title: VCLiterals.contactUs),
            ProfileLayout.init( title: VCLiterals.logout)]
        return items
    }
}

