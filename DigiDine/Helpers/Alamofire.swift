//
//  Alamofire.swift
//  Videe
//
//  Created by osvin-k01 on 1/13/20.
//  Copyright © 2020 osvin-k01. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON

func executePOST(view:UIView,path:String, parameter:Parameters , header : [String : String], completion: @escaping (JSON) -> ()) {
    if Reachability.isConnectedToNetwork() == true{
        Alamofire.request(path,method: .post, parameters: parameter, encoding: JSONEncoding.default ,headers: header).responseJSON { response in
            switch response.result {
            case .success:
                do {
                    let jsonData = try JSON(data: response.data!)
                    completion(jsonData)
                }catch{
                    
                }
                
            case .failure:
                do {
                    print(response)
                    try  completion(JSON(data: response.data!))
                }catch{
                    
                }
                
            }
        }
    }else {
        //view.showToast( Constants.commomInternetmsz.checkInternet)
    }
}



func executeGET(view:UIView, path:String , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    if Reachability.isConnectedToNetwork() == true{
        Alamofire.request(path,method: .get,  encoding: JSONEncoding.default,headers:nil).validate().responseJSON { (responseObject) -> Void in
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }else {
        //view.showToast(Constants.commomInternetmsz.checkInternet)
    }
}

func alamofire_Post_Func(urlName: String, method: String, params: [String :Any], headers : [String : String], completion: @escaping (_ success: Bool, _ object: Any, _ errorMsg: String) -> ()){
    var request = URLRequest(url: URL(string: urlName)!)
    request.httpMethod = method
    request.httpBody = try? JSONSerialization.data(withJSONObject: params)
    request.allHTTPHeaderFields = headers
    print("API Name:- \(urlName) Get body Data: \(params)")
    
    //Now use this URLRequest with Alamofire to make request
    Alamofire.request(request).responseJSON { (responseObj) in
        print(responseObj)
        if responseObj.result.isSuccess {
            let resJson = responseObj.result.value
            completion(true, resJson, "")
        }
        
        if responseObj.result.isFailure {
            let error : Error = responseObj.result.error!
            completion(false, "", "AKErrorHandler.ServerIssues.UNKNOWN_ERROR_FROM_SERVER")
        }
        
    }
}

func executePOSTHEADER(view: UIView, path: String , parameter:Parameters ,headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path,method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).validate().responseJSON  { (responseObject) -> Void in
        print(responseObject)
        
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}


func executeHeaderPOST(view: UIView, path: String , parameter:Parameters ,headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path,method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON  { (responseObject) -> Void in
        print(responseObject)
        
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}



func executePOSTHEADERArray(view: UIView, path: String , parameter:Parameters ,headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    //view.makeToastActivity(.center)
    if headers == ["" : ""] {
        Alamofire.request(path,method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { (responseObject) -> Void in
            //view.hideToastActivity()
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }else {
        Alamofire.request(path,method: .post, parameters: parameter, encoding: JSONEncoding.default,headers: headers).responseJSON { (responseObject) -> Void in
            //view.hideToastActivity()
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
}

func writeAReviewAPI(view: UIView, path: String , parameter:Parameters ,headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path,method: .post, parameters: parameter, encoding: URLEncoding.default,headers: headers).responseJSON { (responseObject) -> Void in
        print(responseObject)
        
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
    
    
}


func executePOSTHEADERS(view: UIView, path: String , parameter:Parameters ,headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON{ (responseObject) -> Void in
        //view.hideToastActivity()
        print(responseObject)
        
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}




func executeGETTHEADERS(view: UIView, path: String , headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path,method: .get , encoding: URLEncoding.default,headers: headers).responseJSON{ (responseObject) -> Void in
        print(responseObject)
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}

func executeGETWITHHEADER(view: UIView, path: String , headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    if headers == ["" : ""] {
        Alamofire.request(path,method: .get , encoding: URLEncoding.default).validate().responseJSON { (responseObject) -> Void in
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }else{
        Alamofire.request(path,method: .get , encoding: URLEncoding.default,headers: headers).validate().responseJSON { (responseObject) -> Void in
            print(responseObject)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}


func executeGETHeader(view: UIView, path: String , headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path,method: .get , encoding: URLEncoding.default,headers: headers).responseJSON { (responseObject) -> Void in
        print(responseObject)
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}

func executeDeleteHeader(view: UIView, path: String , headers : [String : String] , success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
    Alamofire.request(path, method: .delete , encoding: URLEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
        print(responseObject)
        if responseObject.result.isSuccess {
            let resJson = JSON(responseObject.result.value!)
            success(resJson)
        }
        if responseObject.result.isFailure {
            let error : Error = responseObject.result.error!
            failure(error)
        }
    }
}


func blurImage(image:UIImage) -> UIImage? {
    
    let context = CIContext(options: nil)
    let inputImage = CIImage(image: image)
    let originalOrientation = image.imageOrientation
    let originalScale = image.scale
    
    let filter = CIFilter(name: "CIGaussianBlur")
    filter?.setValue(inputImage, forKey: kCIInputImageKey)
    filter?.setValue(10.0, forKey: kCIInputRadiusKey)
    let outputImage = filter?.outputImage
    
    var cgImage:CGImage?
    
    if let asd = outputImage
    {
        cgImage = context.createCGImage(asd, from: (inputImage?.extent)!)
    }
    
    if let cgImageA = cgImage
    {
        return UIImage(cgImage: cgImageA, scale: originalScale, orientation: originalOrientation)
    }
    
    return nil
}


//MARK:- hit google geocoding api
//func hitGeoCoderApi(addres : String, component : String, view : UIView , completion : @escaping (JSON) -> ()){
//    let googleApiKey = Constants.APIs.googleAPIKey
//    let googleAPIURL = "https://maps.googleapis.com/maps/api/geocode/json?address=\(addres)&components=\(component)&key=\(googleApiKey)"
//    let urlString = googleAPIURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//
//    executeGET(view: view, path: urlString!, success: { (response) in
//        completion(response)
//    }) { (error) in
//        print(error)
//        let status_code = error.localizedDescription
//    }
//}
