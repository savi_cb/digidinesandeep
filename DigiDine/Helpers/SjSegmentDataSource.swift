//
//  SjSegmentDataSource.swift
//  AmbleGene
//
//  Created by Apple on 03/02/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

/// A block which is triggered when tabs are changed.
typealias TabChanged = (_ index : Int) -> ()
// A block which is triggered when the SJSegmentDataSource successfully adds and configures segmentVC
typealias Success = () -> ()

/// A helper class which provides customization of SJSegmentedViewController segment bar and adds the segmentVC to the containerView.

class SJSegmentDataSource: SJSegmentedViewControllerDelegate {
  
  var segmentVC: SJSegmentedViewController?
  var selectedSegment: SJSegmentTab?
  var tabChanged: TabChanged?
  var success: Success?
   
  
    /**
     Initializes SJSegmentDataSource with custom design attributes and completion blocks
     - parameters:
     - segmentVC: An instance of SJSegmentedViewController containing header and view controllers displayed in segments.
     - containerView: An instance of UIView which acts as the cotainer of segmentVC.
     - vc: An instance of UIViewController which acts as the ParentViewController of segmentVC.
     - titles: An array of strings to be displayed as titles of view controllers in segmentVC.
     - segmentViewHeight: Height of segment bar in CGFloat.
     - selectedHeight: Height of bottom separator line in CGFloat
     - headerHeight: Height of header view passed in segment VC
     - headerOffset: headerview offset height.
     - scrollingEnabled: A bool which enables or disables scrolling of segment bar. Set true when bar items could take more space than the screen width.
     - tabChanged: An instance of block which is triggered when tabs are changed.
     - success: An instance of block which is triggered when the SJSegmentDataSource successfully adds and configures segmentVC.
     - returns: Instance of SJSegmentDataSource.
     */
  init(segmentVC: SJSegmentedViewController?, containerView: UIView, vc: UIViewController, headerTitle: [String], segmentViewHeight: CGFloat = 48.0, selectedHeight: CGFloat = 1.0, headerHeight: CGFloat = 0.0, headerOffset: CGFloat = 0.0, scrollingEnabled: Bool? = true, tabChanged: TabChanged? = nil, success: Success? = nil) {
    self.segmentVC = segmentVC
    self.tabChanged = tabChanged
    self.success = success
    
    segmentVC?.headerViewOffsetHeight = headerOffset
//    segmentVC?.isScrollingEnabled = /scrollingEnabled
    segmentVC?.selectedSegmentViewHeight = selectedHeight
    segmentVC?.segmentTitleFont = UIFont.systemFont(ofSize: 14)
    segmentVC?.selectedSegmentViewColor = UIColor.appRed
    segmentVC?.headerViewHeight = headerHeight
    segmentVC?.segmentViewHeight = segmentViewHeight
    segmentVC?.delegate = self
    for (index, element) in (segmentVC?.segmentControllers ?? []).enumerated() {
        element.title = headerTitle[index]
    }
    
    vc.addChild(segmentVC!)
    containerView.addSubview((segmentVC?.view)!)
    vc.view.bringSubviewToFront(containerView)
    segmentVC?.view.frame = containerView.bounds
    segmentVC?.didMove(toParent: vc)
    if let block = success {
      block()
    }
  }
  
  // SJSegmentedViewControllerDelegate
  func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
    if selectedSegment != nil {
      selectedSegment?.titleFont(UIFont.systemFont(ofSize: 14))
    }
    if /segmentVC?.segments.count > 0 {
      selectedSegment = segmentVC?.segments[index]
      selectedSegment?.titleFont(UIFont.systemFont(ofSize: 14))
    }
    if let block = tabChanged {
      block(index)
    }
  }
    func addImageToNavigation(vc : UIViewController , image : UIImage){
        // Custom ImageView
        let view = UIImageView()
        view.frame.size.width = 40
        view.frame.size.height = 40
        view.image = image
        view.contentMode = .center
        view.backgroundColor = .white
        vc.navigationItem.titleView = view
    }
}
