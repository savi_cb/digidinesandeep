//
//  PayOptionsViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 7/11/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class PayOptionsViewController: UIViewController {
    
    var paymentType = String()
    var amount = Double()
    var orderId = String()
    var userCount = Int()
    var myOrderBill = Double()
    var onlyMyOustandingOrders = Int()
    var myAmount = String()
    var referalAmount = String()
    var userUnpaidAmount = String()
    var totalUnpaidAmount = String()
    
    var userUnpaidDiscount = String()
    var totalUnpaidDiscouunt = String()
    var outstandingProductData = [OutstandingProductData]()
    var outStandingOrderIds = [Int]()
    
    @IBOutlet weak var payOptionHeigh: NSLayoutConstraint!
    @IBOutlet weak var solitEvenlyView: UIView!
    @IBOutlet weak var selectedProductPaymentView: UIView!
    @IBOutlet weak var payForYourOrders: UIView!
    @IBOutlet weak var payEntireBill: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if onlyMyOustandingOrders == 1 {
            self.payOptionHeigh.constant = 70
            self.payForYourOrders.isHidden = false
            self.payEntireBill.isHidden = true
            self.solitEvenlyView.isHidden = true
            self.selectedProductPaymentView.isHidden = true
        }else if userCount > 1 {
            self.payOptionHeigh.constant = 250
            self.solitEvenlyView.isHidden = false
            self.selectedProductPaymentView.isHidden = false
            self.payForYourOrders.isHidden = false
            self.payEntireBill.isHidden = false
        }else{
            self.payForYourOrders.isHidden = true
            self.payEntireBill.isHidden = true
            self.solitEvenlyView.isHidden = true
            self.selectedProductPaymentView.isHidden = true
            self.hitSplitEvenly(payMode: "entire_bill")
        }
        
        for i in 0 ..< outstandingProductData.count{
            outStandingOrderIds.append(outstandingProductData[i].order_id ?? 0)
        }
    }
    
    @IBAction func payEntireBillOption(_ sender: UIButton) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "You will pay د.إ \(self.totalUnpaidAmount)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
                self.hitSplitEvenly(payMode: "entire_bill")
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func splitEvenlyAction(_ sender: UIButton) {
        //self.hitSplitEvenly(payMode: "split")
        self.sendSplitNotification(showAmount: true)
    }
    
    @IBAction func payForYourOrder(_ sender: UIButton) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "You will pay د.إ \(self.userUnpaidAmount)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
                alert.dismiss(animated: true, completion: nil)
                self.hitSplitEvenly(payMode: "my_order_bill")
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func payForYourOrdersPlusOthers(_ sender: UIButton) {
        let VC = SelectProductToPayViewController.getVC(.digiHome)
        self.pushVC(VC)
    }
    
    
    func hitSplitEvenly(payMode : String){
        //        let amo = String(amount)
        APIManager.sharedInstance.showLoader(message: "Processing Payment")
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
        let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
        //          let orderId = GDataSingleton.sharedInstance.OrderId
        let params = FormatAPIParameters.splitEvenly(table_id: tableId, amount: amount, supplier_id: supplierId, order_id : orderId , payMode : payMode, totalUnpaidDiscount: /self.totalUnpaidDiscouunt, userUnpaidDiscount: /self.userUnpaidDiscount).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.splitEvenly(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                print(object)
                APIManager.sharedInstance.hideLoader()
                if let obj = object as? [String:Any]{
                    //SKToast.makeToast("Payment made successfully")
                    // create the alert
                    let alert = UIAlertController(title: "", message: "Payment made successfully", preferredStyle: UIAlertController.Style.alert)
                    // add the actions (buttons)
                    let Ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        GDataSingleton.sharedInstance.PayOption = "paid"
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                        self.popVC()
                    })
                    alert.addAction(Ok)
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    GDataSingleton.sharedInstance.PayOption = "paid"
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                    self.popVC()
                }
            case .Failure(let error):
                SKToast.makeToast("Payment failed")
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
    
    func sendSplitNotification(showAmount : Bool){
        if showAmount == true {
            APIManager.sharedInstance.showLoader()
        }else{
            APIManager.sharedInstance.showLoader(message: "Processing Payment")
        }
        
        let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
        //          let orderId = GDataSingleton.sharedInstance.OrderId
        let params = FormatAPIParameters.sendSplitNotif(table_id: tableId, showAmount: showAmount, totalUnpaidDiscount: /self.totalUnpaidDiscouunt, userUnpaidDiscount: /self.userUnpaidDiscount,orderIds:self.outStandingOrderIds).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.sendSplitNotif(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                APIManager.sharedInstance.hideLoader()
                print(object)
                if showAmount == true {
                    if let obj = object as? Dictionary<String,AnyObject>{
                        if let amount = obj["amount"] as? Int {
                            if let noOfUser = obj["no_of_users"] as? Int {
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "", message: "After spliting each member will pay د.إ \(amount)", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
                                        alert.dismiss(animated: true, completion: nil)
                                        self.sendSplitNotification(showAmount: false)
                                    }))
                                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }else{
                    SKToast.makeToast("Payment request sent to all")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        self.popVC()
                    }
                }
            case .Failure(let error):
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
}
