//
//  VouchersViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 4/27/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

protocol voucher {
    func getVoucherData(promoCode1 : Int?)
}

class VouchersViewController: UIViewController{
    
    @IBOutlet weak var voucherTitle: UILabel!
    @IBOutlet weak var voucherSubTitleLabel: UILabel!
    @IBOutlet weak var vouchersTableView: UITableView!
    
    var voucherListing : [VouchersByUser]?
    let dateFormatter = DateFormatter()
    let formatter = DateFormatter()
    
    var supplier_id : String?
    var comingFrom : String? = "detail"
    var delegate : voucher?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.voucherListing = GDataSingleton.sharedInstance.VoucherListing
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getVoucherListing()
    }
 
    @IBAction func btnBack(_ sender: UIButton) {
        dismissVC(completion: nil)
        popVC()
     }
    
    func getVoucherListing(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.listVoucherByUser(FormatAPIParameters.listVoucherByUser(supplier_id: /self.supplier_id, sessionRecordId: /GDataSingleton.sharedInstance.tableNumber).formatParameters())) { [unowned self] (response) in
            switch response{
            case .Success(let listing):
                
                if let voucherListing = listing as? [VouchersByUser]{
                    print(voucherListing)
                    if voucherListing.count > 0 {
                        //GDataSingleton.sharedInstance.VoucherListing = voucherListing
                        self.voucherListing = voucherListing
                        self.vouchersTableView.reloadData()
                    }
                    
                }
            default :
                
                break
            }
        }
    }
     
}

//MARK::- TableView Delegate , dataSOurce
extension VouchersViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /voucherListing?.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "VouchersTableViewCell", for: indexPath) as? VouchersTableViewCell else { return UITableViewCell() }
        let data = self.voucherListing?[indexPath.row]
        cell.selectionStyle = .none
        if let date = data?.endDate {
            let endDate = self.getTimeLocalfromisoDate(date: date)
            print(endDate)
            cell.voucherDateLabel.text = "Voucher valid untill \(endDate)"
        }
       
        cell.redeemButton.tag = indexPath.row
        cell.redeemButton.addTarget(self, action: #selector(reddemAction(_:)),
        for: .touchUpInside)
        cell.promoCodeDesc.text = data?.promoDesc
        cell.redeemButton.setCornerRadius(radius: 10.0)
        if self.comingFrom == "cart"{
            if let isApplied = data?.is_applied {
                if isApplied == 0 {
                    cell.redeemButton.setTitle("REDEEM", for: .normal)
                    cell.redeemButton.setBackgroundColor(.appRed, forState: .normal)
                }else{
                    cell.redeemButton.setTitle("Voucher Redeemed", for: .normal)
                    cell.redeemButton.setBackgroundColor(.lightGray, forState: .normal)
                }
            }
        }else{
            self.voucherSubTitleLabel.text = "Vouchers can be applied at time of payment for pay later only"
            cell.redeemButton.setBackgroundColor(.lightGray, forState: .normal)
        }
        return cell
    }
    
    @objc func reddemAction(_ sender: AnyObject){
        let promoCode = /self.voucherListing?[sender.tag].id
        self.delegate?.getVoucherData(promoCode1: promoCode)
        dismissVC(completion: nil)
        popVC()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getTimeLocalfromisoDate(date:String) -> String{
           
           dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
           let dateStart = dateFormatter.date(from:date)
           formatter.dateFormat = "MMM dd, yyyy"
           dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
           if let val = dateStart{
               let str = formatter.string(from: val)
               return str
           }
           else{
               return "NA"
           }
       }
}
