//
//  AcceptRejectViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 6/23/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class AcceptRejectViewController: UIViewController {
  
  @IBOutlet weak var nameLabel: UILabel!
  var user : NewRequest?
  var userId : String?
  var firstName : String?
  var sessionRecordId : String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    self.nameLabel.text = /firstName + " wants to join your table"
  }
  
  
  @IBAction func acceptAction(_ sender: UIButton) {
    let objR = API.acceptRequest((FormatAPIParameters.acceptRequest( sessionRecordId: sessionRecordId ?? "")).formatParameters())
    
    APIManager.sharedInstance.opertationWithRequest(isLoader: true, withApi: objR) {
      [weak self] (response) in
      guard let self = self else { return }
      print(response)
      //            self.stopSkeletonAnimation(self.tableView)
      switch response {
      case APIResponse.Success(let object):
        self.popVC()
        break
      //SKToast.makeToast(error.lo)
      case .Failure(let validation):
        APIManager.sharedInstance.hideLoader()
        SKToast.makeToast("failed")
        break
      default :
        break
      }
    }
  }
  
  @IBAction func rejectAction(_ sender: UIButton) {
    let objR = API.rejectRequest((FormatAPIParameters.rejectRequest(sessionRecordId: sessionRecordId ?? "" )).formatParameters())
    
    APIManager.sharedInstance.opertationWithRequest(isLoader: true, withApi: objR) {
      [weak self] (response) in
      guard let self = self else { return }
      print(response)
      //            self.stopSkeletonAnimation(self.tableView)
      switch response {
      case APIResponse.Success(let object):
        print(object)
        self.popVC()
        break
      //SKToast.makeToast(error.lo)
      case .Failure(let validation):
        APIManager.sharedInstance.hideLoader()
        SKToast.makeToast("failed")
        break
      default :
        break
      }
    }
  }
}
