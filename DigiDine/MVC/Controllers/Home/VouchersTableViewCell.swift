//
//  VouchersTableViewCell.swift
//  Sneni
//
//  Created by osvin-k01 on 4/27/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit



class VouchersTableViewCell: UITableViewCell {

    @IBOutlet weak var voucherHeadingLabel: UILabel!
    @IBOutlet weak var promoCodeDesc: UILabel!
    @IBOutlet weak var voucherDateLabel: UILabel!
    @IBOutlet weak var redeemButton: UIButton!
    @IBOutlet weak var referalImageView: UIImageView!
    
    //var delegate : voucher?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
