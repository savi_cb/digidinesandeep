//
//  SplitPaymentViewController.swift
//  Sneni
//
//  Created by admin on 12/08/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class SplitPaymentViewController: UIViewController {

    @IBOutlet weak var notificationTextview: UITextView!
    @IBOutlet weak var nitificationLabel: UILabel!
    
    var name : String?
    var splitAmount : String?
    var sessionId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.nitificationLabel.text = /name?.capitalized + " wants to split bill payment with you of amount AED \(/splitAmount)"
    }
    
    
    @IBAction func acceptAction(_ sender: UIButton) {
      let objR = API.acceptSplit((FormatAPIParameters.acceptSplit( sessionRecordId: sessionId ?? "")).formatParameters())

      APIManager.sharedInstance.opertationWithRequest(isLoader: true, withApi: objR) {
        [weak self] (response) in
        guard let self = self else { return }
        print(response)
        //            self.stopSkeletonAnimation(self.tableView)
        switch response {
        case APIResponse.Success(let object):
          self.popVC()
          break
        //SKToast.makeToast(error.lo)
        case .Failure(let validation):
          APIManager.sharedInstance.hideLoader()
          SKToast.makeToast("failed")
          self.popVC()
          break
        default :
          break
        }
      }
    }
    

    @IBAction func rejectAction(_ sender: UIButton) {
      let objR = API.rejectSplit((FormatAPIParameters.rejectSplit( sessionRecordId: sessionId ?? "")).formatParameters())
      
      APIManager.sharedInstance.opertationWithRequest(isLoader: true, withApi: objR) {
        [weak self] (response) in
        guard let self = self else { return }
        print(response)
        //            self.stopSkeletonAnimation(self.tableView)
        switch response {
        case APIResponse.Success(let object):
          self.popVC()
          break
        //SKToast.makeToast(error.lo)
        case .Failure(let validation):
          APIManager.sharedInstance.hideLoader()
          self.popVC()
          //SKToast.makeToast("failed")
          break
        default :
          break
        }
      }
    }
}
