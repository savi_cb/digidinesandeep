//
//  Vouchers.swift
//  Sneni
//
//  Created by admin on 24/11/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper

class VoucherData: NSObject, RMMapping, NSCoding {
    
    var result: [VouchersByUser]?
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(result, forKey: APIConstants.DataKey)
    }
    
    required init?(coder aDecoder: NSCoder) {
    }
    
    init(sender : SwiftyJSONParameter){
        
        guard let rawData = sender else { return }
        
        let json = JSON(rawData)
        
        let data =  json["data"].dictionaryValue
        
        let voucherArray = data[APIConstants.voucherData]?.array ?? []
        
        self.result = []
        
        for element in voucherArray {
            let voucherDict = VouchersByUser(attributes: element.dictionaryValue)
            self.result?.append(voucherDict)
        }
    }
}
class VouchersByUser: NSObject,NSCoding  {
    
    var isDeleted : String?
    var isActive : String?
    var perUserCount : String?
    var startDate : String?
    var endDate : String?
    var promoCode : String?
    var id : Int?
    var bear_by : String?
    var promoDesc : String?
    var detailsJson : String?
    var is_applied : Int?

    init(isDeleted: String?,id: Int?,isActive:String?,perUserCount:String?,startDate:String?,endDate: String?, promoDesc : String?, promoCode : String?, is_applied : Int?){
        
        self.isDeleted = isDeleted
        self.id = id
        self.isActive = isActive
        self.perUserCount = perUserCount
        self.startDate = startDate
        self.endDate = endDate
        self.promoDesc = promoDesc
        self.promoCode = promoCode
        self.is_applied = is_applied
    }
    
    init(attributes : SwiftyJSONParameter){
        
        self.isDeleted = attributes?["isDeleted"]?.stringValue
        self.id = attributes?["id"]?.intValue
        self.isActive = attributes?["isActive"]?.stringValue
        self.perUserCount = attributes?["perUserCount"]?.stringValue
        self.id = attributes?["id"]?.intValue
        self.startDate = attributes?["startDate"]?.stringValue
        self.endDate = attributes?["endDate"]?.stringValue
        self.promoDesc = attributes?["promoDesc"]?.stringValue
        self.promoCode = attributes?["promoCode"]?.stringValue
        self.is_applied = attributes?["isApplied"]?.intValue
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(isDeleted, forKey: "isDeleted")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(perUserCount, forKey: "perUserCount")
        aCoder.encode(startDate, forKey: "startDate")
        aCoder.encode(endDate, forKey: "endDate")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(isActive, forKey: "isActive")
        aCoder.encode(promoDesc, forKey: "promoDesc")
        aCoder.encode(is_applied, forKey: "is_applied")
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        isDeleted = aDecoder.decodeObject(forKey: "isDeleted") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        isActive = aDecoder.decodeObject(forKey: "isActive") as? String
        perUserCount = aDecoder.decodeObject(forKey: "perUserCount") as? String
        startDate = aDecoder.decodeObject(forKey: "startDate") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        endDate = aDecoder.decodeObject(forKey: "endDate") as? String
        promoDesc = aDecoder.decodeObject(forKey: "promoDesc") as? String
        promoCode = aDecoder.decodeObject(forKey: "promoCode") as? String
        is_applied = aDecoder.decodeObject(forKey: "is_applied") as? Int
    }
    
}
