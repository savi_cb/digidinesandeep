    //
    //  DigiDineHeaderVC.swift
    //  Sneni
    //
    //  Created by neelam panwar on 24/04/20.
    //  Copyright © 2020 Taran. All rights reserved.
    //
    
    import UIKit
    import GSKStretchyHeaderView
    
    var callback : ((String) -> Void)?
    class DigiDineHeaderVC: CategoryFlowBaseViewController {
        
        @IBOutlet weak var enterBtn: UIButton!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    enterBtn.isHidden = true
                }else{
                    enterBtn.isHidden = false
                }
            }
        }
        
        @IBOutlet weak var tableNumberTextfield: UITextField!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    tableNumberTextfield.isHidden = true
                }else{
                    tableNumberTextfield.isHidden = false
                }
            }
        }
        @IBOutlet weak var enterTheTableNmberLabel: UILabel!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    enterTheTableNmberLabel.isHidden = true
                }else{
                    enterTheTableNmberLabel.isHidden = false
                    enterTheTableNmberLabel.text = "Please enter the table number written on the DigiDine Tent Card placed on the table"
                }
            }
        }
        @IBOutlet weak var youAreSeatedLabel: UILabel!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    youAreSeatedLabel.isHidden = false
                }else{
                    youAreSeatedLabel.isHidden = true
                }
            }
        }
        
        @IBOutlet weak var viewHeightConstant: NSLayoutConstraint!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    viewHeightConstant.constant = 320
                }else{
                    viewHeightConstant.constant = 220
                }
            }
        }
        
        @IBOutlet weak var happyHoursBtn: UIButton!
        @IBOutlet weak var vpucherBtn: UIButton!
        @IBOutlet weak var pushTextView: UITextView!
        @IBOutlet weak var resturantLogo: UIImageView!
        @IBOutlet weak var resturantBanner: UIImageView!
        @IBOutlet weak var resturantName: UILabel!
        @IBOutlet weak var categTableView: UITableView!
        @IBOutlet weak var resturantView: UIView!
        @IBOutlet weak var backBtn: UIButton!{
            didSet{
                if GDataSingleton.sharedInstance.loginType == "NFC" {
                    backBtn.isHidden = true
                }else{
                    backBtn.isHidden = false
                }
            }
        }
        
        @IBOutlet weak var tableNumberLabel: UILabel!
        @IBOutlet weak var welcomeToLabel: UILabel!
        @IBOutlet weak var categoryCollectionView: UICollectionView!
        @IBOutlet var tblHeaderView: GSKStretchyHeaderView?
        //var stretchyHeader: GSKStretchyHeaderView!
        
        var categoryArray = [ "Non-vegetarian" , "Vegetarian" , "Others"]
        var resturanName = String()
        var comingFrom = String()
        var selectedIndex = Int()
        var currentSection = IndexPath()
        var isFilterEnable : Bool = false
        var isSearchBarClicked : Bool = false
        var filterData : [ProductList]?
        var selfPickup : Int? = 0
        var continueSession : Bool? = false
        var canDismiss : Bool? = false
        var tableNumberShoe : Int? = 0
        var voucherListing : [VouchersByUser]?
        
        var sessionTimer: Timer?
        //MARK :- Variables
        var isHappyHoursSelected : Bool?
        var arrayProductList : [ProductList]? {
            didSet {
                categTableView?.reloadData()
                categoryCollectionView.reloadData()
                //            self.categoryCollectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
                //                  at: .top,
                //            animated: true)
            }
        }
        
        var supplierData: Supplier?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            //    NotificationCenter.default.addObserver(self, selector: #selector(self.showText(_:)), name: NSNotification.Name(rawValue: "push"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.enterRest), name: NSNotification.Name(rawValue: "merge"), object: nil)
          //  NotificationCenter.default.addObserver(self, selector: #selector(self.invalidateSessionTimer), name: NSNotification.Name(rawValue: "invalidateSessionTimer"), object: nil)
            categTableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell")
            tblHeaderView?.minimumContentHeight = 80
            tblHeaderView?.maximumContentHeight = 450
            tblHeaderView?.expansionMode = .topOnly
            
            tblHeaderView?.contentShrinks = true
            tblHeaderView?.contentExpands = false // useful if you want to display the
            //tblHeaderView?.contentAnchor = .top
            self.categTableView.addSubview(self.tblHeaderView ?? UIView())
            //self.categTableView.contentInset = .init(top: -10, left: 0, bottom: 0, right: 0)
            if self.continueSession == false{
                if GDataSingleton.sharedInstance.selfPickup != 1{
                    self.resturantView.isHidden = false
                    if GDataSingleton.sharedInstance.loginType == "NFC" && comingFrom != "Listing" {
                        self.welcomeToLabel.text = GDataSingleton.sharedInstance.resturantName
                    }else if comingFrom == "Listing"{
                        self.welcomeToLabel.text = "Welcome to \( resturanName)"
                    }
                    if GDataSingleton.sharedInstance.loginType == "NFC" {
                        viewHeightConstant.constant = 180
                    }else if GDataSingleton.sharedInstance.isHotel == true {
                        self.resturantView.isHidden = true
                    }else{
                        viewHeightConstant.constant = 320
                    }
                }else{
                    self.resturantView.isHidden = true
                }
            }else{
                self.resturantView.isHidden = true
            }
        }
        
        @objc func showText(_ notification: NSNotification) {
            self.pushTextView.text = ("push :- \(String(describing: notification.userInfo))")
        }
        
        @objc func enterRest() {
            self.resturantView.isHidden = true
        }
        
        
        override func viewWillAppear(_ animated: Bool) {
            happyHoursBtn.isSelected = false
            if GDataSingleton.sharedInstance.loginType == "NonTable" {
                self.backBtn.isHidden = false
            }else{
                self.backBtn.isHidden = true
            }
            getProductListing()
            categTableView.reloadData()
            categoryCollectionView.reloadData()
            self.imageGradient()
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            //            if self.isHappyHoursSelected == false{
            //                self.happyHoursBtn.backgroundColor = UIColor.appTextColor
            //                happyHoursBtn.isSelected = false
            //                self.isHappyHoursSelected = false
            //                getProductListing()
            //            }else{
            //                self.happyHoursBtn.backgroundColor = UIColor.appRed
            //                self.isHappyHoursSelected = true
            //                happyHoursBtn.isSelected = true
            //                self.webserviceHomeOffers(latitude: LocationSingleton.sharedInstance.searchedAddress?.lat ?? 0.0, longitude: LocationSingleton.sharedInstance.searchedAddress?.long ?? 0.0)
            //
            //            }
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            var touch: UITouch? = touches.first
            //location is relative to the current view
            // do something with the touched point
            if GDataSingleton.sharedInstance.loginType == "NFC" {
                if touch?.view == resturantView {
                    self.resturantView.isHidden = true
                }
            }else{
                if touch?.view == resturantView {
                    if canDismiss == true{
                        self.resturantView.isHidden = true
                        self.popVC()
                    }else{
                        if self.tableNumberTextfield.text?.count == 0 {
                            self.resturantView.isHidden = true
                            self.popVC()
                        }else{
                            self.canDismiss = false
                        }
                    }
                }
            }
        }
        
        @IBAction func btnBack(_ sender: UIButton) {
            popVC()
        }
        
        @IBAction func vouchersAction(_ sender: UIButton) {
            let VC = VouchersViewController.getVC(.digiHome)
            VC.supplier_id = self.supplierData?.id
            self.presentVC(VC)
            //self.getVoucherListing()
        }
        
        @IBAction func happyHoursAction(_ sender: UIButton) {
            if sender.isSelected{
                sender.backgroundColor = UIColor.appTextColor
                sender.isSelected = false
                self.isHappyHoursSelected = false
                getProductListing()
            }else{
                sender.backgroundColor = UIColor.appRed
                self.isHappyHoursSelected = true
                sender.isSelected = true
                self.webserviceHomeOffers(latitude: LocationSingleton.sharedInstance.searchedAddress?.lat ?? 0.0, longitude: LocationSingleton.sharedInstance.searchedAddress?.long ?? 0.0)
                
            }
            
            // NotificationCenter.default.post(name: Notification.Name("HappyHours"), object: nil)
            // popVC()
            //        callback?("Hi")
            //        popVC()
        }
        
        @IBAction func enterTableAction(_ sender: UIButton) {
            if self.tableNumberTextfield.text?.count == 0 {
                SKToast.makeToast("Please enter your table number")
            }else{
                self.hitQRCodeScanAPI()
                //            self.resturantView.isHidden = true
            }
        }
        
        func webserviceHomeOffers(latitude: Double?,longitude: Double?)  {
            let objR = API.offers(supplier_id: /Int(/supplierData?.id), latitude: latitude ?? nil, longitude: longitude ?? nil)
            APIManager.sharedInstance.showLoader()
            APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
                [weak self] (response) in
                guard let self = self else { return }
                APIManager.sharedInstance.hideLoader()
                switch response {
                    
                case APIResponse.Success(let object):
                    guard let objModel = object as? HappyHoursSection else { return }
                    self.arrayProductList = objModel.arrayProduct ?? []
                    self.supplierData = objModel.supplier
                    self.filterData = self.arrayProductList
                    GDataSingleton.sharedInstance.selfPickup = objModel.arrayProduct?.first?.productValue?.first?.self_pickup
                    ///self.tileForSection = self.arrayProductList?.compactMap({ $0.catName})
                    //
                    //                self.viewHeaderTable.supplier = objModel.supplier
                    //self.resturantName.text = /objModel.supplier?.name?.capitalized
                    self.welcomeToLabel.text = "Welcome to \(/objModel.supplier?.name?.capitalized)"
                    self.youAreSeatedLabel.text = "You are seated on table no. \(/self.tableNumberShoe)"
                    self.resturantLogo?.sd_setImage(with: URL(string: /objModel.supplier?.logo), placeholderImage: #imageLiteral(resourceName: "ic_black"))
                    self.resturantBanner?.sd_setImage(with: URL(string: /objModel.supplier?.supplierImages?.first), placeholderImage: nil)
                    
                    if let section = self.arrayProductList?.firstIndex(where: { $0.catName?.lowercased() ==  /self.passedData.categoryName?.lowercased() }) {
                        //                    self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: true)
                        
                        // self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: false)
                        if let cell = self.categTableView.cellForRow(at: IndexPath(row: 0, section: section)) {
                            //                        //self.tableView.scrollRectToVisible(CGRect(x: 0, y: cell.frame.minY-40.0-35.0-44.0, w: cell.frame.width, h: self.tableView.frame.height ), animated: false)
                        }
                    }
                    
                    break
                default :
                    break
                }
                
            }
        }
        
        
        func hitQRCodeScanAPI(){
            var supplierId = String()
            let tableName = Int(self.tableNumberTextfield.text ?? "") ?? 0
            if /passedData.supplierId == nil || /passedData.supplierId == "" {
                supplierId = GDataSingleton.sharedInstance.supplierId ?? ""
            }else{
                supplierId = /passedData.supplierId
                GDataSingleton.sharedInstance.supplierId = /passedData.supplierId
            }
            
            APIManager.sharedInstance.showLoader()
            let params = ["supplier_id" : supplierId , "scanQRcode" : false , "tableName" : tableName] as [String : Any]
            print(params)
            let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
            let url = "https://api.digidine.ae/scanQRCode"
            print(url)
            print(headers)
            executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
                APIManager.sharedInstance.hideLoader()
                if let dict = response.dictionaryObject {
                    print(dict)
                    if let data = dict["data"] as? Dictionary<String,AnyObject>{
                        if let requestSend = data["requestSend"] as? Int{
                            self.canDismiss = false
                            if requestSend == 1 {
                                SKToast.makeToast(dict["status"] as? String)
                               // self.sessionTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.runSessionTimedCode), userInfo: nil, repeats: true)

                                if let tableId = data["sessionRecordId"] as? Int{
                                    GDataSingleton.sharedInstance.orderId = nil
                                    GDataSingleton.sharedInstance.tableNumber = "\(tableId)"
                                }
                            }
                        }else  if let waiting = data["waitingForApproval"] as? Int{
                            self.canDismiss = true
                            if waiting == 1 {
                                SKToast.makeToast(dict["status"] as? String)
                            }
                        }else{
                            if let tableId = data["sessionRecordId"] as? Int{
                                GDataSingleton.sharedInstance.orderId = nil
                                GDataSingleton.sharedInstance.tableNumber = "\(tableId)"
                                self.resturantView.isHidden = true
                            }else{
                                self.canDismiss = true
                                SKToast.makeToast(dict["message"] as? String)
                            }
                        }
                    }
                }
            }){ (error) in
                print(error)
                self.canDismiss = true
                APIManager.sharedInstance.hideLoader()
                SKToast.makeToast("Can not join this table")
            }
        }
        
      /*  @objc func runSessionTimedCode(){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "checkCurrentSession"), object: nil, userInfo: nil)
        }
        
        @objc func invalidateSessionTimer(){
            sessionTimer?.invalidate()
        }*/
        
        func imageGradient(){
            let view = UIView(frame: resturantBanner.frame)
            let gradient = CAGradientLayer()
            gradient.frame = view.frame
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            gradient.locations = [0.0, 1.0]
            view.layer.insertSublayer(gradient, at: 0)
            resturantBanner.addSubview(view)
            resturantBanner.bringSubviewToFront(view)
        }
        
        func getProductListing()  {
            var lati : Double?
            var longi: Double?
            
            if let _ = LocationSingleton.sharedInstance.selectedLatitude{
                lati = LocationSingleton.sharedInstance.selectedLatitude ?? 0.0
                longi = LocationSingleton.sharedInstance.selectedLongitude ?? 0.0
                
            } else if let _ = LocationSingleton.sharedInstance.searchedAddress?.formattedAddress{
                lati = LocationSingleton.sharedInstance.searchedAddress?.lat ?? 0.0
                longi = LocationSingleton.sharedInstance.searchedAddress?.long ?? 0.0
            }
            //        if UserDefaults.standard.value(forKey: "Type") as? String == "LocationFalse" {
            //            self.getRestorents(latitude: 0.0, longitude: 0.0)
            //        }else{
            //            self.getRestorents(latitude: lati, longitude: longi)
            //        }
            var supplierId = String()
            if /passedData.supplierId == nil || /passedData.supplierId == "" {
                supplierId = /GDataSingleton.sharedInstance.supplierId
            }else{
                supplierId = /passedData.supplierId
                GDataSingleton.sharedInstance.supplierId = /passedData.supplierId
            }
            let objR = API.getProductList(supplierId: supplierId , latitude: lati ?? nil, longitude: longi ?? nil)
            APIManager.sharedInstance.showLoader()
            APIManager.sharedInstance.opertationWithRequest(isLoader: false, withApi: objR) {
                
                [weak self] (response) in
                guard let self = self else { return }
                APIManager.sharedInstance.hideLoader()
                print(response)
                //            self.stopSkeletonAnimation(self.tableView)
                switch response {
                case APIResponse.Success(let object):
                    guard let objModel = object as? MenuProductSection else { return }
                    self.arrayProductList = objModel.arrayProduct ?? []
                    self.supplierData = objModel.supplier
                    self.filterData = self.arrayProductList
                    GDataSingleton.sharedInstance.selfPickup = objModel.arrayProduct?.first?.productValue?.first?.self_pickup
                    GDataSingleton.sharedInstance.isVoucher = self.supplierData?.isVoucher
                    if self.supplierData?.isVoucher == false{
                        self.vpucherBtn.isHidden = true
                    }else{
                        self.vpucherBtn.isHidden = false
                        //self.getVoucherListing()
                    }
                    if self.supplierData?.isHappyHours == false{
                        self.happyHoursBtn.isHidden = true
                    }else{
                        self.happyHoursBtn.isHidden = false
                        //self.getVoucherListing()
                    }
                    self.resturantName.text = /objModel.supplier?.name?.capitalized
                    self.welcomeToLabel.text = "Welcome to \(/objModel.supplier?.name?.capitalized)"
                    self.youAreSeatedLabel.text = "You are seated on table no. \(/self.tableNumberShoe)"
                    self.resturantLogo?.sd_setImage(with: URL(string: /objModel.supplier?.logo), placeholderImage: #imageLiteral(resourceName: "ic_black"))
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.resturantBanner?.sd_setImage(with: URL(string: /objModel.supplier?.cover_image), placeholderImage: nil)
                    }
                    
                    if let section = self.arrayProductList?.firstIndex(where: { $0.catName?.lowercased() ==  /self.passedData.categoryName?.lowercased() }) {
                        //                    self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: true)
                        
                        // self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: false)
                        if let cell = self.categTableView.cellForRow(at: IndexPath(row: 0, section: section)) {
                            //                        //self.tableView.scrollRectToVisible(CGRect(x: 0, y: cell.frame.minY-40.0-35.0-44.0, w: cell.frame.width, h: self.tableView.frame.height ), animated: false)
                        }
                    }
                    //self.menu_button.isUserInteractionEnabled = true
                    break
                default :
                    break
                }
            }
        }
    }
    extension DigiDineHeaderVC : UITableViewDelegate, UITableViewDataSource {
        func numberOfSections(in tableView: UITableView) -> Int {
            return isFilterEnable ? (/filterData?.count) : (/arrayProductList?.count)
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return isFilterEnable ? (filterData?[section].productValue?.count ?? 0) : (arrayProductList?[section].productValue?.count ?? 0)
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell") as? ProductTableViewCell else {
                fatalError("Missing ServiceCell identifier")
            }
            cell.selectionStyle = .none
            let products = arrayProductList?[indexPath.section].productValue
            
            cell.selectedIndex = indexPath.row
            if let data = products?[indexPath.row] {
                data.supplierAddrerss = self.supplierData?.address ?? ""
                cell.product = data
                let purchased = data.purchasedQuantity
                let total = data.totalMaxQuantity
                
                if (total-purchased) == 0 {
                    cell.labelOutOfStock.isHidden = false
                    cell.stepper?.isHidden = true
                } else {
                    cell.labelOutOfStock.isHidden = true
                    cell.stepper?.isHidden = false
                }
            }
            
            //        cell.newCompletionBlock = { [weak self] data in
            //            guard let _ = self else {return}
            //            if let value = data as? Bool,value {
            //                cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
            //            } else {
            //                cell.backgroundColor = .white
            //            }
            //        }
            
            cell.addonsCompletionBlock = { [weak self] value in
                guard let self = self else {return}
                GDataSingleton.sharedInstance.fromCart = false
                if let data = value as? (Product,Bool,Double){
                    if data.1 { // data.1 == true for open customization controller
                        data.0.addOnValue?.removeAll()
                        self.openCustomizationView(cell: cell, product: data.0, cartData: nil, quantity: data.2, index: indexPath.row)
                    }
                } else if let data = value as? (Product,Cart,Bool,Double) {
                    //for open checkcustomization controller
                    self.openCheckCustomizationController(cell: cell, productData: data.0,cartData: data.1, shouldShow: data.2, index: indexPath.row)
               }
            }
            
            if  indexPath.section == 0 {
                let indxpth = IndexPath.init(item: indexPath.section , section: 0)
                self.selectedIndex = indxpth.row
                // categoryCollectionView.scrollToItem(at: indxpth, at: .right, animated: false)
                categoryCollectionView.selectItem(at: indxpth, animated: true, scrollPosition: .centeredHorizontally)
                //                let collcell = categoryCollectionView.cellForItem(at: indxpth) as? CategoryCollectionViewCell
                
                self.categoryCollectionView.reloadData()
            }else{
                if let indx = categTableView.indexPathsForVisibleRows?.last{
                    print("====section===" , indx.section)
                    let indxpth = IndexPath.init(item: indx.section , section: 0)
                    self.selectedIndex = indxpth.row
                    // categoryCollectionView.scrollToItem(at: indxpth, at: .right, animated: false)
                    categoryCollectionView.selectItem(at: indxpth, animated: true, scrollPosition: .centeredHorizontally)
                    //                let collcell = categoryCollectionView.cellForItem(at: indxpth) as? CategoryCollectionViewCell
                    
                    self.categoryCollectionView.reloadData()
                }
            }
            return cell
            
        }
        
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 50.0
        }
        
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return CGFloat.leastNormalMagnitude//0.001
        }
        
        //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //
        //        return isFilterEnable ? (filterData?[section].catName ?? "") : (arrayProductList?[section].catName ?? "")
        //    }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell") as? MenuListCell  else {
                fatalError("Missing ServiceCell identifier")
            }
            let products = arrayProductList?[section]
            cell.lblHeader.text = /products?.catName
            
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let products = arrayProductList?[indexPath.section].productValue
            guard let data = products?[indexPath.row] else { return }
            let vc = RestaurantDescVC.getVC(.options)
            vc.product = data
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            
            //self.present(vc, animated: true)
        }
        
        func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
            if scrollView == categTableView {
                
            }
        }
        
        //        func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //            if scrollView === self.categTableView {
        //                if
        //                    let topSectionIndex = self.categTableView.indexPathsForVisibleRows?.map({ $0.section }).sorted().first,
        //                    let selectedCollectionIndex = self.categoryCollectionView.indexPathsForSelectedItems?.first?.row,
        //                    selectedCollectionIndex != topSectionIndex {
        //                    let indexPath = IndexPath(item: topSectionIndex, section: 0)
        //                    self.categoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        //                }
        //            }
        //        }
    }
    
    extension DigiDineHeaderVC {
        func openCustomizationView(cell:ProductTableViewCell?,product: Product?,cartData: Cart?,quantity: Double?,shouldHide:Bool = false,index:Int?) {
            
            let vc = StoryboardScene.Options.instantiateCustomizationViewController()
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            vc.product = product
            vc.hideAddCustom = shouldHide
            vc.index = index
            vc.cartData = cartData
            
            vc.completionBlock = { [weak self] data in
                guard let self = self else {return}
                if let obj = data as? (Bool,Product) {
                    if let _ = quantity { // called when add to card button is added
                        cell?.comingFromcustomization = true
                        cell?.product = obj.1
                    }
                }
                self.removeViewAndSaveData()
            }
            
            self.present(vc, animated: true) {
                self.createTempView()
            }
            
        }
        
        func openCheckCustomizationController(cell:ProductTableViewCell?,productData: Product?,cartData: Cart?, shouldShow: Bool, index:Int?) {
            
            let vc = StoryboardScene.Options.instantiateCheckCustomizationViewController()
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            vc.cartProdcuts = cartData
            vc.product = productData
            vc.completionBlock = {[weak self] data in
                guard let self = self else {return}
                guard let productCell = cell else {return}
                if let dataValue = data as? (Bool,Product) {
                    productCell.product = dataValue.1
                    if !dataValue.0 {
                        dataValue.1.addOnValue?.removeAll()
                        self.removeViewAndSaveData()
                    }
                } else if let dataValue = data as? (Product,Cart,Bool),let productCell = cell{
                    let obj = Product(cart: dataValue.1)
                    dataValue.0.addOnValue?.removeAll()
                    let addonId = Int(obj.addOnId ?? "0")
                    self.openCustomizationView(cell: cell, product: dataValue.0,cartData: dataValue.1, quantity: productCell.stepper?.value ?? 0.0, index: addonId)
                } else if let _ = data as? Bool{
                    //productCell.stepper?.stepperState = !obj ? .ShouldDecrease : .ShouldIncrease
                    self.removeViewAndSaveData()
                } else if let obj = data as? Product {
                    productCell.product = obj
                } else if let _ = data as? Int {
                    self.removeViewAndSaveData()
                } else if let value = data as? (Bool,Double) {
                    if value.1 == 0 {
                        self.removeViewAndSaveData()
                    }
                    productCell.stepper?.stepperState = .ShouldDecrease
                }
            }
            
            self.present(vc, animated: true) {
                self.createTempView()
            }
        }
        
        func removeViewAndSaveData() {
            self.view.subviews.forEach { (view) in
                if view.tag == 10001 {
                    view.removeFromSuperview()
                }
            }
        }
        
        func createTempView(){
            let view = UIView()
            view.frame = self.view.frame
            view.backgroundColor = UIColor(white: 0.10, alpha: 0.8)
            view.tag = 10001
            self.view.addSubview(view)
        }
    }
    
    extension DigiDineHeaderVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return /arrayProductList?.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell else { return UICollectionViewCell() }
            if indexPath.row == selectedIndex {
                cell.isSelected = true
                cell.categoryTitle.textColor = UIColor.appRed
                cell.bottomView.backgroundColor = UIColor.appRed
            }else{
                cell.isSelected = false
                cell.categoryTitle.textColor = UIColor.gray
                cell.bottomView.backgroundColor = UIColor.clear
            }
            let products = arrayProductList?[indexPath.row]
            cell.categoryTitle.text = /products?.catName
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height )
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let collectionView = self.categoryCollectionView
            self.selectedIndex = indexPath.row
            DispatchQueue.main.async {
                //self.categTableView.setContentOffset(CGPoint.zero, animated: true)
                let index = IndexPath(row: NSNotFound, section: indexPath.row)
                //self.categTableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                DispatchQueue.main.async(execute: {
                    self.categTableView.scrollToRow(at: index, at: .top, animated: false)
                    var offset = self.categTableView.contentOffset.y
                    let headerHeight: CGFloat = 50
                    offset += (self.categTableView.contentInset.top - headerHeight)
                    self.categTableView.setContentOffset(CGPoint(x: 0, y: offset), animated: false)
                    //                    self.categTableView.contentOffset = CGPoint(x: 0, y: -self.categTableView.contentInset.top)
                    //                    self.categoryCollectionView.reloadData()
                })
            }
        }
        //        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        //            let collectionView = self.categoryCollectionView
        //            self.selectedIndex = indexPath.row
        //            DispatchQueue.main.async {
        //                //self.categTableView.setContentOffset(CGPoint.zero, animated: true)
        //                let index = IndexPath(row: NSNotFound, section: indexPath.row)
        //                //self.categTableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
        //                DispatchQueue.main.async(execute: {
        //                    self.categTableView.scrollToRow(at: index, at: .top, animated: true)
        ////                    self.categTableView.contentOffset = CGPoint(x: 0, y: -self.categTableView.contentInset.top)
        ////                    self.categoryCollectionView.reloadData()
        //                })
        //            }
        //        }
    }
    
    //MARK:- UIViewControllerTransitioningDelegate
    extension DigiDineHeaderVC : UIViewControllerTransitioningDelegate {
        func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
            return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
        }
    }
