//
//  ScanVC.swift
//  Electric_Scanner
//
//  Created by Apple on 15/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseDynamicLinks
//import EZSwiftExtensions
typealias ScanBlock = (_ scanedData: String) -> ()

class ScanVC: UIViewController {
    
    //MARK: -----> Outletsƒ√
    @IBOutlet weak var viewScanner: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var viewHeader: AnimatableView!
    @IBOutlet weak var interestedView: UIImageView!
    @IBOutlet weak var btnQrBottom: UIButton!
    @IBOutlet weak var btnDashBottom: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblTopTitle: UILabel!
    
    
    
    //MARK: -----> Properties
    var captureSession : AVCaptureSession?
    var videoPreviewLayer : AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    var qrScanned = false
    var scanBlock: ScanBlock?
    let dateFormatter = DateFormatter()
    let formatter = DateFormatter()
    
    var scanTableId = Int()
    
    //    static let shared : ScanVC = {
    //        let instance = ScanVC()
    //        return instance
    //    }()
    
    //MARK: -----> Lify Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("eventsLoaded"), object: nil, queue: nil, using: catchNotification)
        NotificationCenter.default.addObserver(self, selector: #selector(self.goForward), name: NSNotification.Name(rawValue: "mergeQrCode"), object: nil)
    }
    
    @objc func goForward() {
        var tableId = Int()
        tableId = /Int(/GDataSingleton.sharedInstance.tableNumber)
        let VC = DigiDineHeaderVC.getVC(.digiHome)
        VC.tableNumberShoe = self.scanTableId
        VC.continueSession = true
        self.pushVC(VC)
    }
              
    override func viewWillAppear(_ animated: Bool) {
        guard let session = self.captureSession else{return}
        session.startRunning()
        DBManager.sharedManager.cleanCart()
        //self.hitQRCodeScanAPI(suppId:  0, tableId: 0, venueId: 8)
    }
    
    func catchNotification(notification:Notification) -> Void {
        print("notification")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if (captureSession?.isRunning == true) {
            captureSession?.stopRunning()
                }
    }
    
    
    func hitQRCodeScanAPI(suppId : Int, tableId: Int, venueId : Int , hotelId : Int, roomNo : Int){
        var params = [String : Any]()
        if suppId != 0 {
            params = ["supplier_id" : suppId , "supplierTableInformationId": tableId ]
        }else if hotelId != 0 {
            params = ["hotel_id" : hotelId, "room_no" : roomNo ]//hotelId]
        }else{
            params = ["venue_id" : venueId]
        }
        APIManager.sharedInstance.showLoader()
        print(params)
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/scanQRCode"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            //APIManager.sharedInstance.hideLoader()
            if let dict = response.dictionaryObject {
                print(dict)
                let home = HomeSuppliers(sender: response.dictionaryValue)
                if home.arrayItems?.count != 0 {
                    if let supplierList = home.arrayItems{
                        print(supplierList)
                        DBManager.sharedManager.cleanCart()
                        GDataSingleton.sharedInstance.tableNumber = nil
                        if hotelId != 0 {
                            GDataSingleton.sharedInstance.orderId = nil
                            GDataSingleton.sharedInstance.isHotel = true
                            GDataSingleton.sharedInstance.hotelRoomNo = "\(roomNo)"
                            GDataSingleton.sharedInstance.hotelId = "\(hotelId)"
                        }else{
                            GDataSingleton.sharedInstance.isHotel = false
                        }
                        GDataSingleton.sharedInstance.loginType = "NonTable"
                        let VC = DigiDineViewController.getVC(.digiHome)
                        VC.supplier = supplierList
                        self.pushVC(VC)
                    }
                }
                if let data = dict["data"] as? Dictionary<String,AnyObject>{
                    if let requestSend = data["requestSend"] as? Int{
                        if requestSend == 1 {
                            SKToast.makeToast(dict["status"] as? String)
                            if let tableId = data["sessionRecordId"] as? Int{
                                GDataSingleton.sharedInstance.orderId = nil
                                GDataSingleton.sharedInstance.tableNumber = "\(tableId)"
                            }
                        }
                    }else  if let waiting = data["waitingForApproval"] as? Int{
                        if waiting == 1 {
                            SKToast.makeToast(dict["status"] as? String)
                        }
                    }else{
                        if let tableId = data["sessionRecordId"] as? Int{
                            GDataSingleton.sharedInstance.orderId = nil
                            GDataSingleton.sharedInstance.loginType = "NFC"
                            GDataSingleton.sharedInstance.supplierId = "\(suppId)"
                            GDataSingleton.sharedInstance.tableNumber = "\(tableId)"
                            if let tableNumber = data["table_name"] as? String{
                                self.scanTableId = Int(tableNumber) ?? 0
                            }
                            let VC = DigiDineHeaderVC.getVC(.digiHome)
                            VC.tableNumberShoe = self.scanTableId
                            self.pushVC(VC)
                        }else{
                            SKToast.makeToast(dict["message"] as? String)
                        }
                    }
                }
                if home.arrayItems?.count == 0 &&  self.scanTableId == 0 {
                    //SKToast.makeToast("Scan some other QR Code,data is empty")
                    //self.resetScannerToScanAgain()
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            SKToast.makeToast("Can not join this table")
        }
    }
    
   
}

extension ScanVC {
    func onViewDidLoad() {
        DispatchQueue.main.asyncAfter(deadline: .now() ) {
            self.setupCamera()
        }
    }
    
    fileprivate func setupCamera() {
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {return}
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            setMetaDataOutput()
            
        } catch {
            print(error)
            return
        }
        
        captureSession?.startRunning()
        self.videoPreviewLayer?.frame = self.view.bounds
        if (videoPreviewLayer?.connection?.isVideoOrientationSupported)! {
            self.videoPreviewLayer?.connection?.videoOrientation = self.interfaceOrientation(toVideoOrientation: UIApplication.shared.statusBarOrientation)
        }
    }
    
    func checkPermission(){
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            self.setupCamera()
            
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    self.setupCamera()
                    
                } else {
                    //access denied
                }
            })
        }
    }
    
    func setMetaDataOutput(){
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        
        //        captureMetadataOutput.rectOfInterest = self.viewLine.bounds
        //        captureMetadataOutput.rectOfInterest = interestedView.bounds
        //        captureMetadataOutput.rectOfInterest = viewScanner.bounds
        //        captureMetadataOutput.rectOfInterest = CGRect.init(x: self.interestedView.frame.origin.x / UIScreen.main.bounds.width  , y: self.interestedView.frame.origin.y / UIScreen.main.bounds.height , width: 1.0, height: 1.0)
        //        captureSession?.addOutput(captureMetadataOutput)
        
        
        //        var x = interestedView.origin.x/480
        //        var y = interestedView.origin.y/640
        //        var width = interestedView.size.width/480
        //        var height = interestedView.size.height/640
        //        var scanRectTransformed = CGRect.init(x: x, y: y, w: width, h: height)
        //        captureMetadataOutput.rectOfInterest = scanRectTransformed
        //      captureSession?.addOutput(captureMetadataOutput)
        
        
        let x = interestedView.frame.origin.x/(UIScreen.main.bounds.width)
        let y = interestedView.frame.origin.y/(UIScreen.main.bounds.height)
        //        var width = interestedView.frame.size.width/(UIScreen.main.bounds.width/2)
        //        var height = interestedView.frame.size.height/(UIScreen.main.bounds.height/2)
        
        let scanRectTransformed = CGRect.init(x: x, y: x, width: interestedView.frame.width, height: interestedView.frame.height/2)
        captureMetadataOutput.rectOfInterest = scanRectTransformed
        captureSession?.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr,AVMetadataObject.ObjectType.upce,AVMetadataObject.ObjectType.code39,AVMetadataObject.ObjectType.code39Mod43,AVMetadataObject.ObjectType.ean13,AVMetadataObject.ObjectType.ean8,AVMetadataObject.ObjectType.code93,AVMetadataObject.ObjectType.code128,AVMetadataObject.ObjectType.pdf417,AVMetadataObject.ObjectType.aztec,AVMetadataObject.ObjectType.interleaved2of5,AVMetadataObject.ObjectType.itf14,AVMetadataObject.ObjectType.dataMatrix]
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        view.layer.insertSublayer(videoPreviewLayer!, at: 0)
        captureSession?.startRunning()
        
        qrCodeFrameView = UIView(frame: viewScanner.bounds)
        view.addSubview(qrCodeFrameView!)
        view.bringSubviewToFront(qrCodeFrameView!)
        view.bringSubviewToFront(btnBack)
        view.bringSubviewToFront(btnFlash)
        view.bringSubviewToFront(viewHeader)
        
    }
    
    //MARK: -----> Flash Animation
    func flashView(flashing : Bool,view : UIView){
        
        if !flashing{
            view.alpha = 1.0
            flash(timeInterval:  0.5, delay: 0.0, options: [.curveEaseInOut, .repeat, .autoreverse, .allowUserInteraction])
        }else{
            flash(timeInterval:  0.1, delay: 0.0, options: [.curveEaseInOut, .beginFromCurrentState])
        }
    }
    
    func flash(timeInterval: TimeInterval , delay: TimeInterval , options: UIView.AnimationOptions){
        UIView.animate(withDuration: timeInterval, delay: 0.0, options: options, animations: { [weak self] () -> Void in
            self?.view.alpha = 1.0
            }, completion: { [weak self] (finished: Bool) -> Void in
        })
    }
    
    //MARK: -----> ACTIONS
    
    @IBAction func actionFlashLight(_ sender: Any) {
        
        if let device = AVCaptureDevice.default(for: .video), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOn(level: AVCaptureDevice.maxAvailableTorchLevel)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        popVC()
    }
}

//MARK: -----> QRCode Delegate Functions
extension ScanVC : AVCaptureMetadataOutputObjectsDelegate{
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            debugPrint("No QR code is detected")
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            _ = videoPreviewLayer?.transformedMetadataObject(for: metadataObj as AVMetadataMachineReadableCodeObject) as! AVMetadataMachineReadableCodeObject
            
            if metadataObj.stringValue != nil {
                if !self.qrScanned {
                    self.qrScanned = true
                    self.captureSession?.stopRunning()
                    guard let str = metadataObj.stringValue else { return }
                    self.readQrCodeText(qrText: str)
                }
            }
        }
    }
    
    
    func openRequestVC(vc:UIViewController){
        
        vc.modalPresentationStyle = .overFullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        
        // To present from any controller
        if self.navigationController?.visibleViewController != nil{
            self.navigationController?.visibleViewController?.present(vc, animated: true, completion: nil)
        }
        else{
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func interfaceOrientation(toVideoOrientation orientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch orientation {
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .landscapeLeft:
            return .landscapeLeft
        case .landscapeRight:
            return .landscapeRight
        default:
            break
        }
        return .portrait
    }
}

extension ScanVC {
    
    //MARK: Extract Data from qrcode
    func readQrCodeText(qrText: String){
        if let incomingURL = URL(string: qrText) {
            let handleLink = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamicLink, error) in
                if let dynamicLink = dynamicLink{
                    print("Your Dynamic Link parameter: \(dynamicLink)")
                    self.handleDynamicLink(dynamicLink)
                } else {
                    // self.showAlert(title: "Alert!", message: "Link is not wokring right now, please retry.")
                }
            })
        }
        //        let data = qrText.data(using: .utf8)!
        //        do {
        //            if let jsonObject = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>{
        //                print(jsonObject)
        //                if jsonObject["supplier_id"] as? Int != nil {
        //                    self.hitQRCodeScanAPI(suppId: jsonObject["supplier_id"] as? Int ?? 0, tableId: jsonObject["table_id"] as? Int ?? 0, venueId: 0, hotelId:  0, roomNo: 0)
        //                }else if jsonObject["venue_id"] as? Int != nil {
        //                    self.hitQRCodeScanAPI(suppId: 0, tableId: 0, venueId: jsonObject["venue_id"] as? Int ?? 0, hotelId:  0, roomNo: 0)
        //                }else{
        //                    self.hitQRCodeScanAPI(suppId: jsonObject["supplier_id"] as? Int ?? 0, tableId: jsonObject["table_id"] as? Int ?? 0, venueId: jsonObject["venue_id"] as? Int ?? -1, hotelId:  jsonObject["hotel_id"] as? Int ?? -1, roomNo: jsonObject["hotel_id"] as? Int ?? 0)
        //                }
        //            } else {
        //                print("wdwfew")
        //            }
        //        } catch let error as NSError {
        //            self.resetScannerToScanAgain()
        //            print(error)
        //        }
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        if let dynamicUrl = dynamicLink.url {
            if let venueId = dynamicUrl.queryParameters?["venueId"]{
                self.hitQRCodeScanAPI(suppId: 0, tableId: 0, venueId:  Int(venueId) ?? 0, hotelId: 0, roomNo: 0)
            }else if let hotel_id = dynamicUrl.queryParameters?["hotel_id"] {
                if let room_no = dynamicUrl.queryParameters?["room_no"]{
                    self.hitQRCodeScanAPI(suppId: 0, tableId: 0, venueId: 0, hotelId: Int(hotel_id) ?? 0, roomNo: Int(room_no) ?? 0)
                }
            }else{
                if let supplierId = dynamicUrl.queryParameters?["supplier_id"]{
                    if let tableId = dynamicUrl.queryParameters?["table_id"] {
                        self.hitQRCodeScanAPI(suppId: Int(supplierId) ?? 0, tableId: Int(tableId) ?? 0, venueId: 0, hotelId:  0, roomNo: 0)
                    }
                }
            }
        }
    }
    
    //MARK: Verify ticket from Server
    func resetScannerToScanAgain(){
        self.qrScanned = false
        self.captureSession?.startRunning()
        //viewBottom.isHidden = false
        interestedView.isHidden = false
        self.lblTopTitle.text = "Scan Ticket/Drink QR Code"
    }
    
    
    @IBAction func actionCloseScannedView(_ sender: Any) {
        self.resetScannerToScanAgain()
    }
    
    func getTimeLocalfromisoDate(date:String) -> String{
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateStart = dateFormatter.date(from:date)
        formatter.dateFormat = "h:mm:a"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        if let val = dateStart{
            let str = formatter.string(from: val)
            return str
        }
        else{
            return "NA"
        }
    }
    
    
}
