//
//  MenuListViewController.swift
//  Sneni
//
//  Created by neelam panwar on 24/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class MenuListViewController: UIViewController {

    //MARk::- IBOutlets
    @IBOutlet weak var tableView: UITableView?{
        didSet{
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 64
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}

//MARK::- TableView Delegate , dataSOurce
extension MenuListViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SKToast.makeToast("Coming Soon")
//        let vc = ResturantDetailDineVC.getVC(.digiHome)
//        pushVC(vc)
    }
    
}
