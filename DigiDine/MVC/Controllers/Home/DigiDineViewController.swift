//
//  DigiDineViewController.swift
//  Sneni
//
//  Created by neelam panwar on 23/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import CoreLocation
import SkeletonView
import EZSwiftExtensions

class DigiDineViewController: CategoryFlowBaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var tableView: UITableView?{
        didSet{
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 80
        }
    }
    @IBOutlet weak var txtfSearch: UITextField?
    
    var searchBool : Bool = false
    
    @IBOutlet weak var areYouatLabel: UILabel!
    @IBOutlet weak var tableBookingScreen: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if  GDataSingleton.sharedInstance.loginType == "NFC" {
            let VC = DigiDineHeaderVC.getVC(.digiHome)
            pushVC(VC)
        }else{
            self.titleLabel.text = "Showing resturants nearby"
        }
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.happyHoursSelected), name: Notification.Name("HappyHours"), object: nil)
    }
    
    //MARK:- Variables
    var supplier : [Supplier]? = []{
        didSet{
            tableViewDataSource.items = supplier
            //self.areYouatLabel.text = "Are you at \(self.supplier?.first?.name ?? "")"
            tableView?.reloadTableViewData(inView: view)
        }
    }
    var tableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = tableViewDataSource
            tableView?.delegate = tableViewDataSource
        }
    }
    
    @objc func happyHoursSelected(){
        self.titleLabel.text = "Happy Hours"
    }
    
    //variables
    var locationManager = CLLocationManager()
    var currentLoc: CLLocation!
    
    var filterSuppliers = [Supplier]()
    
    override func viewWillAppear(_ animated: Bool) {
        if  GDataSingleton.sharedInstance.loginType != "NonTable" {
            self.hitServiceListingAPI()
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //self.titleLabel.text = "Resturent Result"
    }
    
    //MARK :- get current location and hit API of resturant listing
    func hitServiceListingAPI(){
        var lati : Double?
        var longi: Double?
        
        if let _ = LocationSingleton.sharedInstance.selectedLatitude{
            lati = LocationSingleton.sharedInstance.selectedLatitude ?? 0.0
            longi = LocationSingleton.sharedInstance.selectedLongitude ?? 0.0
            let add = LocationSingleton.sharedInstance.selectedAddress
            let addres = Address(name: nil, address: String("add"), landmark: "", houseNo: "", buildingName: "", city: "", country: "", placeLink: "", area: "", lat: String(/lati), long: String(/longi),id: nil)
            let params = FormatAPIParameters.AddNewAddress(address: addres).formatParameters()
            let api = API.AddNewAddress(params)
            
            APIManager.sharedInstance.opertationWithRequest(withApi: api) { (response) in
                switch response {
                case .Success(let item): break
//                    if isEdit == true, let tempId = addressObj?.id, let selId = LocationSingleton.sharedInstance.searchedAddress?.id, tempId == selId {
//                        let newAddress = LocationSingleton.sharedInstance.tempAddAddress
//                        newAddress?.id = tempId
//                        LocationSingleton.sharedInstance.searchedAddress = newAddress
//                    }
//                    else if let addressNew = item as? Address, let addId = addressNew.id, !addId.isEmpty {
//                        let newAddress = LocationSingleton.sharedInstance.tempAddAddress
//                        newAddress?.id = addId
//                        LocationSingleton.sharedInstance.searchedAddress = newAddress
//                    }
                case .Failure(_):
                    break
                }
            }
        } else if let _ = LocationSingleton.sharedInstance.searchedAddress?.formattedAddress{
            lati = LocationSingleton.sharedInstance.searchedAddress?.lat ?? 0.0
            longi = LocationSingleton.sharedInstance.searchedAddress?.long ?? 0.0
        }
        
        self.getRestorents(latitude: lati, longitude: longi, skipLatAndLngDistance: false)
        //        if  GDataSingleton.sharedInstance.loginType != "Manual" {
        //            self.getRestorents(latitude: 0.0, longitude: 0.0, skipLatAndLngDistance: true)
        //        }else{
        //            self.getRestorents(latitude: lati, longitude: longi, skipLatAndLngDistance: false)
        //        }
    }
    
    
    func getRestorents(showSkelton: Bool = true,latitude: Double?,longitude: Double?, skipLatAndLngDistance: Bool?)  {
        
        //        if !LocationSingleton.sharedInstance.isLocationSelected() {
        //            self.home = home
        //            return
        //        }
        
        let objR = API.getRestorentList(latitude: latitude ?? nil, longitude: longitude ?? nil, skipLatAndLngDistance: skipLatAndLngDistance)
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            
            self.stopSkeletonAnimation(self.tableView)
            switch response {
            case APIResponse.Success(let object):
                print(object)
                self.supplier = object as? [Supplier]
                break
            default :
                print("Hello Nitin")
                break
            }
        }
    }
}

//MARK::- TableView Delegate , dataSOurce
extension DigiDineViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBool == true {
            return filterSuppliers.count
        }else{
            return supplier?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DigiDineHomeCell", for: indexPath) as? DigiDineHomeCell else { return UITableViewCell() }
        
        if searchBool == true {
            if filterSuppliers.count > 0 {
                let data = filterSuppliers[indexPath.row]
                if let name = data.name {
                    cell.labelResturantName?.text = name.capitalized
                }
                if let address = data.address{
                    cell.resturantAdress.text = address
                }
                if let distance = data.distance{
                    var double =  Double(distance) ?? 0.0
                    double.roundByPlaces(2)
                    cell.resturantDistance.text = "\(double) KM"
                }
                if let logo = data.logo {
                    if logo == "" {
                        cell.resturantImageWidth.constant = 0
                    }else{
                        cell.resturantImgView?.sd_setImage(with: URL(string: logo), placeholderImage: nil)
                        cell.resturantImageWidth.constant = 100
                    }
                }
            }
        }else{
            let data = supplier?[indexPath.row]
            if let name = data?.name {
                cell.labelResturantName?.text = name.capitalized
            }
            if let logo = data?.logo {
                if logo == "" {
                    cell.resturantImageWidth.constant = 0
                }else{
                    cell.resturantImgView?.sd_setImage(with: URL(string: logo), placeholderImage: nil)
                    cell.resturantImageWidth.constant = 100
                }
            }
            if let address = data?.address{
                cell.resturantAdress.text = address
            }
            if let distance = data?.distance{
                var double =  Double(distance) ?? 0.0
                double.roundByPlaces(2)
                cell.resturantDistance.text = "\(double) KM"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let VC = ResturantDetailDineVC.getVC(.digiHome)
        let VC = DigiDineHeaderVC.getVC(.digiHome)
        if searchBool == true {
            if filterSuppliers.count > 0 {
                let data = filterSuppliers[indexPath.row]
                VC.passedData.supplierId = data.id
                VC.comingFrom = "Listing"
                VC.resturanName = data.name ?? ""
                //VC.selfPickup = data.self_pickup
                GDataSingleton.sharedInstance.selfPickup = data.self_pickup
                pushVC(VC)
            }
        }else{
            let data = supplier?[indexPath.row]
            VC.passedData.supplierId = data?.id
            VC.comingFrom = "Listing"
            VC.resturanName = data?.name ?? ""
            //VC.selfPickup = data?.self_pickup
            GDataSingleton.sharedInstance.selfPickup = data?.self_pickup
            pushVC(VC)
        }
    }
}

//MARK: - SearchField handlers

extension DigiDineViewController : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        print(replacedText)
        if replacedText.count == 0 {
            self.searchBool = false
            tableView?.reloadData()
        }else{
            self.searchBool = true
            filterSuppliers = (supplier?.filter({ (supplier) -> Bool in
                guard let match = supplier.name?.contains(replacedText, compareOption: .caseInsensitive) else { return false }
                print(match)
                return match
            })) ?? []
            tableView?.reloadData()
        }
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //constraintTableViewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //constraintTableViewBottom.constant = 0
        
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
