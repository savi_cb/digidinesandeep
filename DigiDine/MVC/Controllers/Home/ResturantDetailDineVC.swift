//
//  ResturantDetailDineVC.swift
//  Sneni
//
//  Created by neelam panwar on 24/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class ResturantDetailDineVC: CategoryFlowBaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var resturantView: UIView!
    
    //MARK::- Properties
    let headerVC = DigiDineHeaderVC.getVC(.digiHome)
    let item1 = MenuListViewController.getVC(.digiHome)
    let item2 = MenuListViewController.getVC(.digiHome)
    let item3  = MenuListViewController.getVC(.digiHome)
    var sjDataSource: SJSegmentDataSource?
    var sjSegmentVC: SJSegmentedViewController?
    
    var comingFrom = String()
    
    //MARK :- Variables
    var arrayProductList : [ProductList]? {
        didSet {
            //tableView?.reloadData()
        }
    }
    var supplierData: Supplier?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sjSegmentVC = SJSegmentedViewController.init(headerViewController: headerVC, segmentControllers: [item1, item2, item3])
        self.getProductListing()
        
        if GDataSingleton.sharedInstance.loginType == "NFC" && comingFrom != "Listing" {
            self.resturantView.isHidden = false
        }else if comingFrom == "Listing"{
            self.resturantView.isHidden = true
            sjVCinit()
        }
    }
     
     override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         var touch: UITouch? = touches.first
         //location is relative to the current view
         // do something with the touched point
         if touch?.view != resturantView {
             self.resturantView.isHidden = false
             sjVCinit()
         }
     }
     
    
    func sjVCinit() {
        
        let segmentTitle = [ "Non-vegetarian" , "Vegetarian" , "Others"]
        sjDataSource = SJSegmentDataSource.init(segmentVC: sjSegmentVC, containerView: containerView, vc: self, headerTitle: segmentTitle, segmentViewHeight: 48.0, selectedHeight: 2.0, headerHeight: 480.0,headerOffset: 0, scrollingEnabled: true, tabChanged: { (selectedIndex) in
            
        }, success: {
            
        })
        
        sjDataSource?.tabChanged = { index in
            /// Perform tab change action here
            
        }
    }
    
    func getProductListing()  {
        var lati : Double?
        var longi: Double?

        if let _ = LocationSingleton.sharedInstance.selectedLatitude{
            lati = LocationSingleton.sharedInstance.selectedLatitude ?? 0.0
            longi = LocationSingleton.sharedInstance.selectedLongitude ?? 0.0

        } else if let _ = LocationSingleton.sharedInstance.searchedAddress?.formattedAddress{
            lati = LocationSingleton.sharedInstance.searchedAddress?.lat ?? 0.0
            longi = LocationSingleton.sharedInstance.searchedAddress?.long ?? 0.0
        }
//        if UserDefaults.standard.value(forKey: "Type") as? String == "LocationFalse" {
//            self.getRestorents(latitude: 0.0, longitude: 0.0)
//        }else{
//            self.getRestorents(latitude: lati, longitude: longi)
//        }
        let objR = API.getProductList(supplierId: /passedData.supplierId, latitude: lati ?? nil, longitude: longi ?? nil)

        APIManager.sharedInstance.opertationWithRequest(isLoader: false, withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            print(response)
//            self.stopSkeletonAnimation(self.tableView)
            switch response {
            case APIResponse.Success(let object):
                guard let objModel = object as? MenuProductSection else { return }
                self.arrayProductList = objModel.arrayProduct ?? []
                self.supplierData = objModel.supplier
//                self.filterData = self.arrayProductList
//                self.tileForSection = self.arrayProductList?.compactMap({ $0.catName})
//
//                self.viewHeaderTable.supplier = objModel.supplier
                self.headerVC.resturantName.text = /objModel.supplier?.name?.capitalized
                self.headerVC.resturantLogo?.sd_setImage(with: URL(string: /objModel.supplier?.logo), placeholderImage: #imageLiteral(resourceName: "ic_black"))
                self.headerVC.resturantBanner?.sd_setImage(with: URL(string: /objModel.supplier?.logo), placeholderImage: nil)

                if let section = self.arrayProductList?.firstIndex(where: { $0.catName?.lowercased() ==  /self.passedData.categoryName?.lowercased() }) {
                    //                    self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: true)

                   // self.tableView.scrollToRow(at: IndexPath(row: NSNotFound, section: section), at: .middle, animated: false)
//                    if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: section)) {
//                        //self.tableView.scrollRectToVisible(CGRect(x: 0, y: cell.frame.minY-40.0-35.0-44.0, w: cell.frame.width, h: self.tableView.frame.height ), animated: false)
//                    }
                }
               //self.menu_button.isUserInteractionEnabled = true
                break
            default :
                break
            }
        }
    }
}
