//
//  DigiDineSettingViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 4/25/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class DigiDineSettingViewController: UIViewController {
    
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var selectedImageBanner: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhoneNumber: UILabel!
    @IBOutlet weak var notificationToggle: UISwitch!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var notifyToggle: UISwitch!
    @IBOutlet weak var userNameTextfield: UITextField!
    
    var selectedImage : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.blurImage()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.doneBtn.isHidden = true
        self.userNameTextfield.isUserInteractionEnabled = false
        let userData =  GDataSingleton.sharedInstance.loggedInUser
        userNameTextfield?.text = userData?.firstName
        userPhoneNumber?.text = userData?.mobileNo
        
        if let img = self.selectedImage{
            self.profileImage.image = img
            //self.selectedImageBanner.image = img
            //self.selectedImageBanner.alpha = 0.5
        }else{
            profileImage?.loadImage(thumbnail: userData?.userImage, original: nil, placeHolder: UIImage(asset: Asset.User_placeholder))
            //selectedImageBanner?.loadImage(thumbnail: userData?.userImage, original: nil, placeHolder: UIImage(asset: Asset.User_placeholder))
            //selectedImageBanner.alpha = 0.5
        }
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func toggleAction(_ sender: UISwitch) {
        if notifyToggle.isOn {
            SKToast.makeToast("Notification Enabled")
        }else{
            SKToast.makeToast("Notification Disabled")
        }
    }
    
    
    @IBAction func editAction(_ sender: UIButton) {
        self.doneBtn.isHidden = false
        self.userNameTextfield.isUserInteractionEnabled = true
    }
    
    
    @IBAction func doneAction(_ sender: UIButton) {
        self.updateName()
    }
    
    @IBAction func goToPassword(_ sender: UIButton) {
        let VC =  ChangePassViewController.getVC(.digiHome)
        pushVC(VC)
    }
    
    @IBAction func actionAddDP(sender: UIButton) {
        
        if UtilityFunctions.isCameraPermission() {
            UtilityFunctions.showActionSheet(withTitle: nil, subTitle: L10n.SelectPicture.string, vc: self, senders: [L10n.Camera.string,L10n.PhotoLibrary.string]) { (text, index) in
                
                CameraGalleryPickerBlock.sharedInstance.pickerImage(type: text as! String, presentInVc: self, pickedListner: {[weak self] (image) in
                    self?.changeProfileWebservice(image: image)
                }) {
                    
                    //Cancelled
                }
            }
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(viewController: self)
        }
    }
    
    
    //MARK :- function to blur banner
    //     func blurImage(){
    //        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
    //         let blurEffectView = UIVisualEffectView(effect: blurEffect)
    //         blurEffectView.frame = view.bounds
    //         blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    //         selectedImageBanner.addSubview(blurEffectView)
    //     }
    
    
    func updateName(){
        let name = /self.userNameTextfield.text
        let token = /GDataSingleton.sharedInstance.loggedInUser?.token
        
        APIManager.sharedInstance.showLoader()
        
        let objR = API.RegisterLastStep(FormatAPIParameters.RegisterLastStep(accessToken : token, name: name).formatParameters())
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            APIManager.sharedInstance.hideLoader()
            guard let `self` = self else { return }
            switch response {
            case .Success(let object):
                let userNew = object as? User
                GDataSingleton.sharedInstance.loggedInUser = userNew
                self.userNameTextfield.isUserInteractionEnabled = false
                self.doneBtn.isHidden = true
            default:
                break
            }
        }
    }
}


extension DigiDineSettingViewController {
    
    func changeProfileWebservice(image : UIImage?){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ChangeProfile(FormatAPIParameters.ChangeProfile.formatParameters()), image: image?.resize(toWidth:300)) {
            [weak self] (response) in
            switch response {
            case .Success(let object):
                self?.selectedImage = image
                self?.profileImage.image = image
                self?.selectedImageBanner.image = image
                self?.selectedImageBanner.alpha = 0.5
//                self?.imageViewProfile.image = image
//                self?.imageViewCover.image = image//UIImageEffects.imageByApplyingDarkEffect(to: image)
                let user = GDataSingleton.sharedInstance.loggedInUser
                user?.userImage = object as? String
                GDataSingleton.sharedInstance.loggedInUser = user
            default:
                break
            }
        }
    }

}
