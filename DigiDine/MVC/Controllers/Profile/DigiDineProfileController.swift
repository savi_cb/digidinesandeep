//
//  DigiDineProfileController.swift
//  Sneni
//
//  Created by neelam panwar on 23/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit


class DigiDineProfileController: UIViewController {
    
    //MARK::- IBOUTLETS
    @IBOutlet weak var tableView: UITableView?{
        didSet{
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 64
        }
    }
    
    //MARK::- PROPERTIES
    var arrProfile = ProfileLayout.getProfileElements()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView?.reloadData()
    }
    
}

//MARK::- TableView Delegate , dataSOurce
extension DigiDineProfileController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProfile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifer = (indexPath.row == 0) ? "ProfileUserCell" : "ProfileTableViewCell"
        let userData =  GDataSingleton.sharedInstance.loggedInUser
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath)
        cell.selectionStyle = .none
        (cell as? ProfileUserCell)?.labelUserName?.text = userData?.firstName?.capitalizedFirst()
        (cell as? ProfileTableViewCell)?.labelTitle?.text = arrProfile[indexPath.row].titleText?.localized
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0 :
            let VC =  DigiDineSettingViewController.getVC(.digiHome)
            pushVC(VC)
//        case 1:
//            let VC = CardPayVC.getVC(.digiHome)
//            pushVC(VC)
//        case 1:
//            let VC = VouchersViewController.getVC(.digiHome)
//            pushVC(VC)
        case 1 :
            let VC = OrderHistoryViewController.getVC(.order)
            pushVC(VC)
        case 2:
            let VC =  LinksViewController.getVC(.digiHome)
            VC.link = "https://www.termsandconditionsgenerator.com/live.php?token=8W8PMEFyll2HpQt0YYkFpu4jb7XdHLSw"
            VC.titl = "Terms and Conditions"
            VC.comingFrom = "Terms"
            pushVC(VC)
        case 3:
            let VC =  LinksViewController.getVC(.digiHome)
            VC.link = "https://www.privacypolicygenerator.info/live.php?token=b9TsVo6lsWBQ5rVnvXusiFBxhX0FxEvz"
            VC.titl = "Privacy Policy"
            VC.comingFrom = "Privacy"
            pushVC(VC)
        case 4:
             let vc = OnboardingViewController.getVC(.splash)
             vc.comingFrom = "Profile"
            pushVC(vc)
        case 5 :
            let VC =  ContactUsViewController.getVC(.digiHome)
            pushVC(VC)
        case 6 :
            self.logOut()
        default :
            print("default")
        }
    }
    
    
    func logOut(){
        UtilityFunctions.showSweetAlert(title: L10n.LogOut.string, message: L10n.AreYouSureYouWantToLogout.string, success: { [weak self] in
            guard let self = self else {return}
            print(self)
            // LocationSingleton.sharedInstance.selectedAddress = nil
            // LocationSingleton.sharedInstance.searchedAddress = nil //Nitin check
            LocationSingleton.sharedInstance.tempAddAddress = nil
            LocationSingleton.sharedInstance.scheduledOrders = "0"
            GDataSingleton.sharedInstance.loggedInUser = nil
            //  GDataSingleton.sharedInstance.agentDBSecretKey = nil
            GDataSingleton.sharedInstance.autoSearchedArray = nil
            GDataSingleton.sharedInstance.showRatingPopUp = nil
            GDataSingleton.sharedInstance.pushDict = nil
            GDataSingleton.sharedInstance.deviceToken = nil
            GDataSingleton.sharedInstance.pickupAddress = nil
            GDataSingleton.sharedInstance.PayOption = nil
            //            GDataSingleton.isOnBoardingDone = false
            //            GDataSingleton.isAskLocationDone = false
            GDataSingleton.isProfilePicDone = false
            GDataSingleton.sharedInstance.fromCart = false
            GDataSingleton.sharedInstance.tableNumber = nil
            GDataSingleton.sharedInstance.hotelId = nil
            GDataSingleton.sharedInstance.hotelRoomNo = nil
            GDataSingleton.sharedInstance.isHotel = nil
            // self.resetDefaults()
            DBManager.sharedManager.cleanCart()
            let cache = YYWebImageManager.shared().cache
            cache?.diskCache.removeAllObjects()
            cache?.memoryCache.removeAllObjects()
            let loginManager = LoginManager()
            loginManager.logOut()
            let address = LocationSingleton.sharedInstance.searchedAddress
            address?.id = ""
            LocationSingleton.sharedInstance.searchedAddress = address
            
            // (UIApplication.shared.delegate as? AppDelegate)?.moveHere()
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            appDelegate.switchViewControllers()
            
            //            let navigationVc = StoryboardScene.Main.instantiateLeftNavigationViewController()
            //            window.rootViewController = navigationVc
            
            //            if let vc = ez.topMostVC as? MoreViewController {
            //                guard let parentVc = vc.parent as? MainTabBarViewController else { return }
            //                parentVc.selectedIndex = 0
            //            }
            
        }) {
        }
    }
}


