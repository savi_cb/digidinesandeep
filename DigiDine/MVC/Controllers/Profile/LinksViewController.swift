//
//  LinksViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 4/27/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import WebKit

class LinksViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    var activityIndicator: UIActivityIndicatorView!
    lazy var link = String()
    lazy var titl = String()
    
    var text = String()
    var comingFrom = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if comingFrom == "Privacy" {
            text = """
             <div class="WordSection1"><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:6.0pt"><o:p>&nbsp;</o:p></span><b style="background-color: transparent;"><span lang="EN-US" style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Privacy Policy</span></b></p><p class="MsoNormal" style="margin-top:.05pt;line-height:8.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:8.0pt"><o:p>&nbsp;</o:p></span></p><p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:7.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Last Modified on 27/04/2020</span></b><spanArial"><o:p></o:p></span></p><p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p><p class="MsoNormal" style="margin-top:0cm;margin-right:3.85pt;margin-bottom:0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">When</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> you use DigiDine’s
                   website, the DigiDine mobile application
                   (“the App”),
                   you are trusting Digidine Portal L.L.C
                   with your
                   information. In this Privacy Policy,
                   “Users” means you and
                   other users
                   of the
                   App.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:4.55pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">By</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> accessing,
                   signing into and/or registering to
                   use the
                   App and
                   by providing
                   your personal information, you confirm that you
                   consent to the collection, use,
                   storage and disclosure of
                   your personal
                   information by us in accordance
                   with this
                   Privacy Policy. The terms
                   of this
                   Privacy Policy apply to you
                   and all
                   other Users,
                   and they should be read together
                   with our
                   End User
                   License Agreement (EULA) (which apply to
                   you and
                   all users
                   of the
                   App to
                   the extent
                   that the
                   App is
                   downloaded through the Apple
                   App Store
                   and/or Google Play Store).<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:8.85pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">Our</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Privacy
                   Policy does not apply to
                   services offered by, and information
                   practices of, other companies
                   or individuals
                   that may
                   be displayed
                   to you
                   when you use the App (such
                   as advertisers)
                   or other
                   sites linked
                   from the
                   App, including in relation to the
                   use of
                   cookies and other technologies. We encourage you to read
                   the privacy
                   statements of each and every
                   external application, website and or service
                   that collects
                   personal information from or about
                   you.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:10.3pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> reserve
                   the right
                   to change,
                   add or
                   remove any portion of this
                   Privacy Policy at any time by
                   updating this page. You are
                   responsible for periodically visiting this page to review any
                   changes to this Privacy Policy
                   since your
                   last visit.
                   You can easily tell whether there
                   have been
                   any changes
                   since your
                   last visit
                   by viewing the “Last Modified” date
                   at the
                   top of
                   the Privacy
                   Policy, so please visit<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;margin-right:6.6pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">this</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> page frequently. Your
                   use of
                   the App
                   following the posting of any
                   changes to this Privacy Policy constitutes
                   your express
                   acceptance of, and consent to,
                   its terms.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">The information we collect and how we collect
                   and hold it</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:
                   &quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:5.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> collect
                   the following
                   information about you:<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.4pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:15.45pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Registration:
                   Information provided when you create
                   an account
                   with us
                   or otherwise subscribe to the App
                   or join
                   our mailing
                   list, which
                   may include
                   your name, email address(es), telephone number(s),
                   user identification,
                   passwords, answers to security
                   questions, text messaging information and
                   other contact
                   information associated with electronic
                   or digital
                   messaging methods or vehicles and payment information;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:3.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Use of the
                   App: Information
                   you provide
                   through your use of the
                   App, including
                   ratings, restaurants visited, interaction
                   with third
                   party apps,
                   communications with<o:p></o:p></span></p>

                   </div>

                   <span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
                   EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
                   mso-break-type:section-break">
                   </span>

                   <div class="WordSection2">

                   <p class="MsoNormal" style="margin-top:3.05pt;margin-right:12.75pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">us,</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> as well as
                   your preferential
                   and behavioural
                   information. This also includes information received when you: (a)
                   order goods
                   or services
                   through the App; (b) complete a transaction with us;
                   or (c)
                   participate in any rewards program
                   or competitions we offer;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:12.75pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Communicating
                   with us:
                   Information you provide us when
                   you contact
                   us for
                   support or with questions,
                   or when
                   you seek
                   to apply
                   for a
                   job with
                   us or
                   seek to
                   become a service provider or seek
                   us to
                   provide you services;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:15.65pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Third parties: Information
                   obtained (a) when you visit
                   one of
                   our partners’
                   websites as part of
                   the App
                   we provide;
                   (b) when
                   you provide
                   information via social media pages; (c)
                   when you
                   contact our partners, suppliers or
                   associates; (d) from information service providers,
                   public sources, credit reporting agencies
                   or anyone authorised to
                   act on
                   your behalf;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:22.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Device Information: Device-specific information
                   (including, for example, your hardware model, operating system version,
                   unique device identifiers, mobile network information, and phone number),
                   which associate
                   with your
                   account;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:7.55pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Log Information: Information in log files,
                   including, for example: (a) details
                   about how you use
                   the App,
                   including passwords, logins, individual device
                   identifiers, geo-location statistics, check-ins,
                   page views,
                   time of
                   use of
                   the App,
                   uploads and other information
                   about how
                   you interact
                   with the
                   App; (b)
                   internet protocol addresses; (c)
                   device event information like crashes,
                   system activity, hardware settings,
                   browser type, browser language and
                   the date
                   and time
                   of various actions you have taken;
                   (d) telephony
                   log information
                   like your
                   phone number, calling-party number, forwarding numbers,
                   time and
                   date of
                   calls, duration of calls, SMS routing
                   information and types of calls;
                   and (e)
                   cookies that may uniquely identify your
                   browser or your account;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:3.4pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Location
                   information: Information about your actual
                   location. We use various technologies to determine location, including
                   IP address,
                   GPS, and
                   other sensors
                   that may, for example,
                   provide us with information about
                   nearby devices like Wi- Fi access
                   points and cell towers;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:27.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Cookies
                   and similar
                   technologies: A cookie is a tiny data
                   file that
                   resides on your device that allows
                   us to
                   recognise you when you use
                   the App
                   using the
                   same device without you
                   needing to login to your
                   account.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:22.85pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">Not</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> all of the
                   information that we collect from
                   you is
                   personally identifiable, but we may associate
                   it with
                   personally identifiable information that we
                   have collected.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:27.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> collect
                   personal information in various ways,
                   including in person; over the
                   telephone (which may include
                   by way
                   of monitoring
                   and recording
                   telephone calls); through the
                   App; through
                   our websites;
                   on hard
                   copy forms;
                   through written communications including letters, facsimiles
                   and emails;
                   from third<o:p></o:p></span></p>

                   </div>

                   <span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
                   EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
                   mso-break-type:section-break">
                   </span>

                   <div class="WordSection3">

                   <p class="MsoNormal" style="margin-top:3.05pt;margin-right:8.95pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">parties,</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> including
                   publicly available sources, information service
                   providers, credit reporting agencies or anyone
                   authorised to act on your
                   behalf, agents, employees, subcontractors,
                   suppliers and services providers, our
                   partners and related entities.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Use of Information</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;line-height:5.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:11.05pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> use your personal
                   information to provide, maintain, protect,
                   and improve
                   the App, to develop new services,
                   and to
                   protect us and our users.
                   This including
                   using the information to:<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> allow you to
                   use the
                   functions of the App;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="line-height:7.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> provide
                   you with
                   receipts and notifications relating to
                   the use
                   of the
                   App;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.1pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> administer
                   the App;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> respond
                   to specific
                   requests from you;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:10.05pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> understand
                   your use
                   of the
                   App and
                   make improvements
                   to the
                   App and
                   other existing and future products and
                   services;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:25.45pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> create a unique,
                   personalised experience for you in
                   connection with various venues, their related
                   sponsors and partners, and related
                   retail locations;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:34.9pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> provide
                   tailored information and features regarding
                   your use
                   of the
                   App, including tailored information based on
                   your location
                   in and
                   around various venues, their related sponsors
                   and partners,
                   and related
                   retail locations and transactions;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> protect
                   the security
                   or integrity
                   of the
                   App;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:2.95pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> prevent
                   unauthorised use or disclosure of
                   content that has been downloaded
                   or otherwise accessed via the App;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:13.9pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> give notice of
                   special promotions, Participating Restaurants (as
                   defined in our terms of used),
                   their related
                   sponsors and partners, or related
                   retail locations;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:6.45pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> promote
                   and market
                   DigiDine, Participating Restaurants and DigiDine’s
                   related sponsors and partners
                   and their
                   various products and services and
                   to assist
                   in the business planning, marketing, advertising
                   and sales
                   efforts of the foregoing parties; and<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:24.8pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> send you promotional
                   information, products and services offered
                   by third
                   parties that we feel
                   may be
                   of interest
                   to you,
                   including information about new developments, events, features and enhancements,
                   special offers, upgrade opportunities,
                   surveys, contests, news and general
                   offers.<o:p></o:p></span></p>

                   </div>

                   <span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
                   EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
                   mso-break-type:section-break">
                   </span>

                   <div class="WordSection4">

                   <p class="MsoNormal" style="margin-top:3.05pt;margin-right:0cm;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> may also allow
                   other companies,
                   called ad networks, to serve<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.1pt;margin-right:3.5pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:15.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-font-width:99%">advertisements</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"> to you via the App.
                   Ad networks
                   include third party ad servers,
                   ad agencies, ad technology vendors and
                   research firms. We may target
                   some ads
                   to<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-font-width:99%">Users</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"> that fit a certain general
                   profile. To deliver these ads
                   properly, the App may<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;margin-right:19.2pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">include</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> features
                   that allow
                   monitoring technologies so that the
                   ad networks
                   are able to provide anonymous, aggregated
                   auditing, research and reporting for
                   advertisers as well as
                   to target
                   ads to
                   you. Ad
                   networks may use non-personal information, such as
                   your postcode,
                   past buying
                   behaviour, in order to provide
                   advertisements about goods and
                   services that may be of
                   particular interest to you while you
                   use the
                   App.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Information we share</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;line-height:5.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:9.65pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> do not share
                   personal information with companies, organisations
                   and unless
                   one of the following circumstances applies:<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> To facilitate a
                   transaction: We share personal information
                   when appropriate
                   to facilitate your transactions with us
                   or our
                   third-party partners or to perform
                   various functions under the
                   App, such
                   as the
                   processing of credit card payments
                   by our gateway provider;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:15.55pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> To identify a
                   table: We share your geographical
                   location and account name with other
                   users of
                   the App
                   who are
                   or appear
                   to be
                   at the
                   same table
                   as you
                   in the Participating Restaurant;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="line-height:7.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:3.4pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> For an enhanced
                   experience: We may provide your
                   personal information to Participating
                   Restaurants to allow them to
                   create an enhanced, enjoyable, unique
                   or personalised experience for
                   you when
                   you dine
                   with them
                   and to
                   allow them
                   to promote and market themselves to
                   you;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:43.65pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> For legal reasons:
                   We will
                   share personal
                   information with companies, organisations
                   or individuals
                   if we
                   have a
                   good-faith belief that access, use,
                   preservation or disclosure of
                   the information
                   is reasonably
                   necessary to:<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:4.05pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> meet any applicable
                   law, regulation,
                   legal process
                   or enforceable
                   governmental request;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> enforce
                   applicable EULA, including investigation of potential violations;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> detect,
                   prevent, or otherwise address fraud,
                   security or technical issues;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:17.85pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> protect
                   against harm to the rights,
                   property or safety of DigiDine,
                   our users
                   or the public as required or
                   permitted by law;<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:27.0pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> With your consent:
                   We will
                   share personal
                   information with companies, organisations
                   or individuals
                   outside of the DigiDine group
                   when we
                   have your
                   consent to do so;<o:p></o:p></span></p>

                   </div>

                   <span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
                   EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
                   mso-break-type:section-break">
                   </span>

                   <div class="WordSection5">

                   <p class="MsoNormal" style="margin-top:3.05pt;margin-right:5.05pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">–</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> For a potential
                   sale, merger,
                   or acquisition:
                   We may
                   share personal
                   information with the other
                   parties involved in a potential
                   sale, merger,
                   acquisition, or similar event relating to
                   DigiDine or the App, only
                   where necessary,
                   and we
                   will notify
                   affected users by email
                   before personal information becomes subject
                   to a
                   different privacy policy.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:9.9pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> may share non-personally
                   identifiable information publicly and with
                   our partners and potential partners –
                   like investors,
                   publishers, advertisers or connected sites. For
                   example, we may share information
                   publicly to show trends about the general use of
                   the App.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Accessing, Correcting, Updating and Removing
                   Your</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.1pt;margin-right:0cm;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial">Personal Information</span></b><span lang="EN-US" style="font-size:16.5pt;
                   font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.9pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:3.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">If</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> you wish to
                   access the personal information that
                   we hold
                   about you,
                   if your
                   personal information changes, or
                   if you
                   no longer
                   wish to
                   receive non-account specific information
                   from us,
                   you can
                   access, correct, update and/or ask
                   us to
                   remove personal information about
                   you that
                   we have
                   collected by emailing your request to us at contact@digidine.ae
                   or by
                   using the
                   functionality contained in the App (if
                   any). When
                   updating your personal information, we may ask you
                   to verify
                   your identity before we
                   act on
                   your request.
                   We may
                   reject requests that are unreasonably repetitive, require disproportionate technical effort (for example,
                   developing a new system
                   or fundamentally
                   changing an existing practice), risk<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;margin-right:18.5pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:15.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-font-width:99%">the</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"> privacy of others, or would
                   be extremely
                   impractical (for instance, requests concerning information residing on backup
                   systems). Where we can provide
                   information access and correction,
                   we will
                   do so
                   for free,
                   except where it would<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-font-width:99%">require</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"> a disproportionate effort. We seek
                   to protect
                   information from accidental<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;margin-right:10.7pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">or</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> malicious
                   destruction. Because of this, after
                   you delete
                   information from the App, we may
                   not immediately
                   delete residual copies from our
                   active servers and may not remove
                   information from our backup systems.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Disclosure of Personal Information and Your
                   Opt-Out</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.1pt;margin-right:0cm;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial">Rights</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:
                   &quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:6.9pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">You</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> can choose whether
                   or not
                   to disclose
                   certain personal information when we ask
                   you to
                   do so,
                   but some
                   functions of the App may
                   be more
                   difficult or impossible to use if
                   you choose
                   not to
                   disclose certain personal information, and your experience in using
                   the App
                   will not
                   be as
                   personally tailored.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:10.7pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">To</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> opt out of
                   further direct marking from us,
                   please contact us at contact@digidine.ae. To opt
                   out of
                   further communications from a marketing
                   partner or sponsor with
                   whom your
                   information has already been shared,
                   please contact that partner directly. Please
                   note that
                   your personal
                   information may still<o:p></o:p></span></p>

                   </div>

                   <span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
                   EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
                   mso-break-type:section-break">
                   </span>

                   <p class="MsoNormal" style="margin-top:3.05pt;margin-right:4.6pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">be</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> disclosed
                   to third
                   parties to the extent that
                   you provide
                   it to
                   third parties
                   using the App (e.g. by posting
                   it using
                   functionality in the App).<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.1pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Protection of Personal Information</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;line-height:5.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:3.25pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">We</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> place great importance
                   on protecting
                   us and
                   our users
                   from unauthorised
                   access to, or the
                   unauthorised alteration, disclosure or destruction
                   of, the
                   information we collect. We
                   review our information collection, storage
                   and processing practices, including physical security
                   measures, to guard against unauthorised access to systems. Information
                   about Users
                   that is
                   maintained on our systems (or the
                   systems of those who provide
                   service to us or on
                   our behalf)
                   is protected using industry
                   standard security measures. However, no
                   security measures are perfect
                   or impenetrable,
                   and we
                   cannot guarantee that your personal information will be completely
                   secure. We are not responsible
                   for the
                   circumvention of any privacy
                   settings or security measures of
                   the App
                   by any
                   Users or third parties. As such,
                   we recommend
                   that you
                   use caution
                   whenever submitting personal information
                   online whether to us or
                   to anyone
                   else.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Personal Information Outside of the UAE</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.05pt;line-height:5.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
                   margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">If</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> you are using
                   the App
                   from any
                   location outside of the UAE,
                   the personal
                   information we collect will
                   be transferred
                   to —
                   and used,
                   stored and otherwise processed in — UAE, which
                   is where
                   we are
                   based. We are unlikely to
                   disclose your personal information to recipients
                   outside of UAE.<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.25pt;line-height:11.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial">Questions, complaints and contacting us</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.9pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-top:0cm;margin-right:4.15pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
                   13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-font-width:
                   99%">If</span><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> you have any
                   questions about this Privacy Policy
                   or the
                   use of
                   your personal
                   information please contact us
                   at the
                   by sending
                   an email
                   to contact@digidine.ae
                   or by writing to the following
                   address:<o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
                   exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial;mso-font-width:99%">Digidine</span></b><b><span lang="EN-US" style="font-size:13.0pt;
                   font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"> Portal L.L.C</span></b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-left:5.0pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial;mso-font-width:99%">ATTENTION:</span></b><b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;
                   mso-fareast-font-family:Arial"> Privacy
                   Policy query</span></b><span lang="EN-US" style="font-size:13.0pt;
                   font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"><o:p></o:p></span></p>

                   <p class="MsoNormal" style="margin-top:.3pt;margin-right:136.3pt;margin-bottom:
                   0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:14.0pt;mso-line-height-rule:
                   exactly"><b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial;mso-font-width:99%">Aspin</span></b><b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"> Commercial Tower, office
                   # 3001A,
                   Dubai, UAE P/O Box 62226 Dubai</span></b><span lang="EN-US" style="font-size:13.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
                   Arial"><o:p></o:p></span></p>
            """
            webView.loadHTMLString(self.text, baseURL: nil)
            activityIndicator = UIActivityIndicatorView()
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray
            
        }else if comingFrom == "Terms"{
            text = """
             <div class="WordSection1">

             <div class="WordSection1">

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">Terms of Use</span></b><span lang="EN-US" style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:25.4pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The
             DigiDine application and related software and/or services (“App”) is owned and
             operated by DIGIDINE PORTAL LLC (“DigiDine”) and is subject to these terms of
             use (“Terms of Use” or “Agreement”)).<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">KEY TERMS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.1pt;line-height:5.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Without
             limiting the other terms set out in these Terms of Use, you agree that:<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.2pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– Your use
             of the App is subject to these Terms of Use. If you do not agree with these
             Terms of Use, please do not use the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.95pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– You must
             register and maintain an account, and provide at least one acceptable payment
             method, in order to use the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– You must
             pay for all menu items ordered through your account on the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.45pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– You
             acknowledge that the prices of the menu items as shown in the App may differ
             from the prices for the same menu items: (a) sold through the App at different
             times; or (b) sold by other means.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:11.35pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– You
             consent to receiving emails, texts and app notifications from DigiDine as part
             of the App’s functionality, including receipts, codes and notices to change of
             these Terms of Use and DigiDine’s Privacy Policy.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:21.2pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">– You will
             collect location data when you are in or near a participating restaurant and
             this may be shared with other users of the App who are at, or appear to be at,
             your table.<o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-top:.75pt;line-height:10.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">AGE RESTRICTED PRODUCTS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.2pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Age
             restricted products (including, without limitation, alcohol, tobacco and
             cigarettes) can only be sold and provided to persons of legal drinking age or
             over which might differ from country to country. By placing an order for an age
             restricted product, you confirm that you are at least of legal age in the
             country.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">ACCEPTANCE OF TERMS OF USE</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:14.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Please
             read these Terms of Use carefully before using the App. DigiDine is willing to
             license, the App to you only on the condition that you accept all the terms
             contained in these Terms of Use. By creating an account with or by using the
             App, you indicate that you understand and accept these Terms of Use. By using
             the App, you agree to be bound by these Terms of Use. IF YOU DO NOT AGREE TO BE
             BOUND BY THE TERMS AND CONDITIONS OF THESE TERMS OF USE, PLEASE DO NOT USE OR
             ACCESS THE APP OR REGISTER OR BUY ANY GOODS OR SERVICES PROVIDED ON OR THROUGH
             THE APP.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The Terms
             of Use include the terms and conditions expressly set out below and those
             incorporated by reference. DigiDine reserves the right, at its discretion, to
             change, modify, add or remove portions of these Terms of Use at any time by
             posting the amended terms on the App. You agree to continually review these
             Terms of Use. If DigiDine posts amended Terms of Use on the App, your
             subsequent use of the App or any goods or services offered through the App
             constitutes your<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection2">

             <p class="MsoNormal" style="margin-top:4.0pt;margin-right:5.2pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">acceptance
             of the-then updated or modified Terms of Use. Except as stated herein, all
             amended terms shall automatically be effective after they are posted on the
             App. These Terms of Use may not be otherwise amended except in writing signed
             by you and DigiDine.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:20.4pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The
             disclaimers, terms and conditions herein may be supplemented by additional
             policies, procedures, disclaimers, guidelines, rules, terms or conditions of
             specific application disclosed by DigiDine (“Supplementary Terms”). To the
             extent that there is an inconsistency between these<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:4.2pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Terms of
             Use and any Supplementary Terms, the Supplementary Terms shall prevail. In the
             event of a conflict in the terms of the Licensed Application End User License
             Agreement (as accepted when downloading the App from the App Store) and this
             Agreement, the terms of this Agreement shall prevail.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:27.2pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">A copy of
             this document can be received by emailing your request to support@digidine.ae
             or by visiting www.digidine.ae<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:18.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             collects, stores and uses data collected from you in accordance with DigiDine’s
             privacy policy located at www.digidine.ae/privacypolicy (“Privacy Policy”). The
             terms and conditions of the Privacy Policy are hereby expressly incorporated
             into these Terms of Use.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">GENERAL TERMS OF USE AND RESTRICTIONS ON USE</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.95pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Subject to
             your acceptance of this Agreement, DigiDine hereby grants you a limited,
             non-exclusive, non-assignable, non-transferable licence to access and use the
             App solely for your own personal, non-commercial purposes. All rights not
             otherwise expressly granted by these Terms of Use are reserved by DigiDine. You
             agree not to reproduce, duplicate, copy, distribute, transmit, sell, trade,
             resell or exploit for any purpose any portion of or any information from the
             App. DigiDine may discontinue or alter any aspect of the App, remove Content
             from the App, restrict the time the App is available or restrict the amount of
             use permitted at DigiDine’s sole discretion and without prior notice or
             liability. You further agree that such measures shall be taken in DigiDine’s
             sole discretion and without liability to you or any third party.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">YOUR ACCOUNT</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:6.4pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">In order
             to use most aspects of the App, you must register for and maintain an active
             personal user services account (“Account”). You must register for an Account
             via email and mobile telephone number. Account registration requires you to
             submit to DigiDine your name, email address and mobile telephone number. You
             will also be required to provide DigiDine’s payment gateway provider with your
             credit card details. You agree to maintain accurate, complete and up-to-date
             information in your Account. Your failure to maintain accurate, complete and
             up-to-date Account information, including having an invalid or expired payment
             method on file, may result in your inability to access or use the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:7.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:10.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             acknowledge and agree that you are responsible for any activity that occurs
             through your account and you agree you will not sell, transfer, license or
             assign your Account, username, or any Account rights. You agree to maintain the
             security and secrecy of your Account username and password at all times. Unless
             otherwise permitted by DigiDine in writing, you may only possess one Account.
             You agree that you will not create an account for anyone other than yourself.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">ORDERING AND PAYMENT PROCESSING</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The
             primary function of the App is to allow you to order food and/or beverage at a
             participating<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">restaurant
             or other participating food or beverage vendor (“Participating Restaurant”)
             through the<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection3">

             <p class="MsoNormal" style="margin-top:4.0pt;margin-right:7.75pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">App. You
             acknowledge that the prices of the menu items offered by a Participating
             Restaurant on the App may differ from time to time, and may differ from the
             prices of the same menu items sold by the Participating Restaurant by any
             method other than through the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You may
             only order menu items on the App if you are dining in at a Participating
             Restaurant.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.4pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             acknowledge that your use of the App may result in charges to you. You will be
             charged for the cost of the menu items ordered through the App (being the cost
             appearing next to the menu items at the time of ordering). DigiDine will
             receive and/or enable your payment of the applicable Charges for services or
             goods obtained through your use of the App. Charges will be inclusive of
             applicable<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">taxes
             where required by law.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.9pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Charges
             you incur for menu items that you order will be owed to the Participating
             Restaurants, and DigiDine will collect payment of those charges from you, on
             the Participating Restaurant’s behalf as its limited payment collection agent,
             and payment of the Charges shall be considered the same as payment made
             directly by you to the Participating Restaurant. In all cases, Charges you
             incur will be owed and paid directly to DigiDine, where DigiDine is solely
             liable for any obligations to Participating Restaurants.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:8.4pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Once you
             order a menu item on the App, you cannot cancel that order. You must pay for
             all menu items that you order on the App, even if you leave the Participating
             Restaurant prior to receiving the ordered menu item or if the ordered menu item
             does not arrive. A refund may be payable in some circumstances, as set out
             under “Refunds” in these Terms of Use.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.4pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">It may be
             open to another user of the App who is dining with you (“Other User”) to pay
             for a menu item that you ordered through your Account through the Other User’s
             Account, provided that the Other User elects to do so on the App within a
             limited period of time. If the Other User does not elect to so pay, you will be
             liable to pay for the menu item that you ordered. DigiDine shall not be liable
             in relation to any dispute arising out of your arrangement for payment by the
             Other User.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:19.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">In
             addition to paying for the menu items, it will be open to you to pay for an
             additional tip in a sum nominated by you through the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.85pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">All
             Charges will be enabled by DigiDine by causing DigiDine’s gateway provider to
             debit those Charges directly from your credit card designated in your Account,
             after which you will receive a receipt by email. This will take place upon
             confirmation by the Participating Restaurant that you have completed your meal,
             or shortly thereafter.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.3pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">As between
             you and DigiDine, DigiDine reserves the right to establish, remove and/or
             revise Charges at any time in DigiDine’s sole discretion. You will be informed
             of the Charges for the menu items at the time of ordering the menu items.
             DigiDine may use the proceeds of any Charges for any purpose, subject to any
             payment obligations it has agreed to with any Participating Restaurants or
             other third parties.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.45pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.6pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             acknowledge that the Participating Restaurant, and not DigiDine, is the
             provider of the goods and services ordered on the App, and that any dispute
             arising in connection with the provision of those goods and services is, for
             purposes of this Agreement, between you and the Participating Restaurant, to
             the exclusion of DigiDine. DigiDine is not liable in connection with any such
             good or service provided by a Participating Restaurant. This includes, but is
             not limited to, any liability in relation to the quality of the food, beverage
             or service provided by the Participating Restaurant; any allergic reaction or
             illness you suffer as a result of consuming the food and beverage provided by
             the Participating Restaurant; any failure of the Participating Restaurant to
             meet your dietary requirements; or any injury you sustain at the Participating
             Restaurant. If you have any issue or complaint regarding the provision of goods
             and services by the Participating Restaurant, you must deal directly with the
             Participating Restaurant. A refund may be payable in some circumstances, as set
             out under “Refunds” in these Terms of Use.<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection4">

             <p class="MsoNormal" style="margin-top:3.0pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial">REFUNDS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:
             &quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:13.85pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Charges
             and other moneys paid by you are final and non-refundable, unless otherwise
             determined by DigiDine.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:11.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Prior to
             the transaction for the Charges completing, a Participating Restaurant may
             remove or reduce a Charge for a menu item showing as ordered on your App in the
             Participating Restaurant’s discretion. You must use your best endeavours to
             raise any dispute prior to the transaction completing.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:17.5pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Once a
             transaction has completed, you may still communicate with the Participating Restaurant
             in relation to a dispute, however you acknowledge that any refund agreed to by
             the Participating Restaurant may not be instantaneously processed.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">RATINGS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">After you
             have received services or goods obtained through the App, you will have the
             opportunity to rate your experience at the relevant Participating Restaurant by
             allocating a rating from 1 to 5. You agree that you will act in good faith in
             all ratings that you publish on the App. DigiDine may remove a rating which it
             in its sole discretion considers to be false or not genuine.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             acknowledge that the restaurant whose food or beverage you order through the
             App has certain obligations regarding the responsible service of alcohol. You
             acknowledge that, so far as the App is used in the United Arab Emirates
             (“UAE”), the restaurant may only serve alcohol to customers over<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:14.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">21 years
             of age. You agree that you will not reduce your rating of a Participating Restaurant
             on the basis of its compliance with its responsible service of alcohol
             obligations.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">CONTENT</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:1.0pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.7pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Unless
             otherwise specifically noted in these Terms of Use or on or in the App, images,
             trademarks, service marks, logos and icons displayed on the App are the
             property of DigiDine and/or its licensors and may not be used without
             DigiDine’s prior written consent. Any unauthorised use of any Content, whether
             owned by DigiDine or other parties, may violate copyright laws, trademark laws,
             privacy and publicity laws and communications regulations and statutes. You
             agree that you will not copy,<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:4.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">reverse engineer, disassemble, decompile,
             translate, modify, reproduce, republish, transmit, sell, offer for sale,
             disseminate or redistribute the intellectual property found in the App or any
             part thereof or grant any other person or entity the right or access to do so.<o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:11.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">LINKS AND COMMUNICATIONS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:5.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.25pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">As a
             service to DigiDine’s users, DigiDine may provide information about other
             resources that may be of interest to you. However, DigiDine does not control
             any third-party content and DigiDine is not responsible or liable for any
             content, advertising, products or other materials on or available from such
             sites or resources, and the presentation of third-party links or content by
             DigiDine is not intended to be an endorsement, sponsorship or recommendation by
             DigiDine. When you exit the App, you are subject to the policies of any new
             site or application which you enter. Any communication with third parties found
             through the App are solely between you and such third party. You further
             acknowledge and agree that DigiDine shall not be responsible or liable,
             directly or indirectly, for any damage or loss caused or alleged to be caused
             by or in connection with use of or reliance on any such third-party content,
             goods or service available on or through any third-party site or resource.<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection5">

             <p class="MsoNormal" style="margin-top:4.0pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">Email and
             Text Communications<o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:7.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             may send you emails, text messages or app notifications as part of the App’s
             functionality,<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">including
             receipts, notifications of changes to the Terms of Use and other important
             communications.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.9pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             may also send you emails or text messages informing you about potential
             promotions and offers or new and existing features that DigiDine, its partners
             or affiliates provide. You may also receive promotional emails or text messages
             from Participating Restaurants. Please see our Privacy Policy for opting out of
             promotional communications,<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:15.8pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">If you
             change your mobile phone service provider, the notification service may be
             deactivated for your phone number and you may need to re-enrol in the
             notification service. DigiDine reserves the right to cancel the notification
             service at any time.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">USER REPRESENTATIONS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:5.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:5.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="line-height:10.0pt;mso-line-height-rule:exactly"><span lang="EN-US">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:11.15pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You hereby
             represent and warrant to DigiDine that (1) all information provided by you to
             DigiDine is truthful, accurate and complete; and (2) you will comply with the
             terms and conditions of these Terms of Use and any other agreement to which you
             are subject that is related to your use of the App or any part thereof.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">PROHIBITED USES</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:6.45pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You agree
             that you will use the App in a manner consistent with any and all applicable
             laws and regulations. DigiDine reserves the right, but is not obligated to,
             investigate and terminate your use of the App if you have misused the App or
             any products or services accessed through the App, or behaved in a way which
             could be regarded as inappropriate or if your conduct is unlawful or illegal.
             With respect to your participation on the App, you agree that you will not: (1)
             impersonate any<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:4.9pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">person or
             entity; (2) use any robot, spider, site search/retrieval application or other
             manual or automatic device or process to retrieve, index, “data mine”, or in
             any way reproduce or circumvent the navigational structure or presentation of
             the App or its contents; (3) post, distribute or reproduce in any way any
             copyrighted material, trademarks, or other proprietary information without
             obtaining the prior written consent of the owner of such proprietary rights;
             (4) remove any copyright, trademark or other proprietary rights notices
             contained in the App; (5) interfere with or disrupt any services provided
             through the App or the servers or networks connected to the App; (6) post,
             email or otherwise transmit any material that contains software viruses or any
             other computer code, files or programs designed to interrupt, destroy or limit
             the functionality of any computer software or hardware or telecommunications
             equipment; (7) forge headers or otherwise manipulate identifiers in order to
             disguise the origin of any information transmitted through the App; or (8)
             “frame” or “mirror” any part of the App, without DigiDine’s prior written
             authorisation or use meta tags or code or other devices containing any
             reference to DigiDine or the App in order to direct any person to any other
             website for any purpose.<o:p></o:p></span></p>

             <p class="MsoNormal" style="line-height:7.0pt;mso-line-height-rule:exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.95pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             further agree that your information and your interactions on the App shall not:
             (1) be false, inaccurate or misleading (directly or by omission or failure to
             update information); (2) infringe any third party’s rights, including but not
             limited to: intellectual property rights, copyright, patent, trademark, trade
             secret or other proprietary rights or rights of publicity or privacy; (3)
             violate any law, statute, ordinance or regulation; (4) be defamatory; (5)
             contain any viruses, Trojan horses, worms, time bombs, cancelbots, easter eggs
             or other computer programming routines that may damage, detrimentally interfere
             with, surreptitiously intercept or expropriate any system, data or personal
             information; or (6) create liability for DigiDine. You further agree that you
             will not transfer, use, or sell your Account information to any another party.<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection6">

             <p class="MsoNormal" style="margin-top:4.0pt;margin-right:36.35pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             reserves the right, but DigiDine has no obligation, to refuse to license the
             App to any person who does not comply with these prohibitions.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">LOCATION DATA</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:6.05pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             may use location data provided through the App for purposes of confirming that
             you are at the Participating Restaurant from which you are ordering and for
             confirming your table within the Participating Restaurant. We may share your
             location with other users of the App who are or appear to be at the same table
             as you at the Participating Restaurant to allow the App to function in the
             intended way. Neither DigiDine, nor any of its content providers, guarantees
             the availability, accuracy, completeness, reliability, or timeliness of
             location data or offers or promotions provided by the use of location data.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.25pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">OTHER USERS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:8.4pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You
             acknowledge that any of your information (excluding credit card details),
             including geolocation data, that you provide or post through the App may be
             accessible to certain other users of the App and third parties. Please note
             that there are risks of dealing with underage persons or people acting under
             false pretence. By using the App, you agree to accept such risks and DigiDine
             is not responsible for the acts or omissions of users on the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">EXCLUSION OF WARRANTIES / DISCLAIMER</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.6pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">TO THE
             MAXIMUM EXTENT ALLOWED BY LAW, THE APP OR THE SERVICE, AND ANY DIGIDINE CONTENT
             ARE PROVIDED “AS IS” AND “AS AVAILABLE,” AND AT YOUR SOLE RISK. ALTHOUGH
             DIGIDINE USES REASONABLE EFFORTS TO ENSURE THAT THE INFORMATION CONTAINED ON
             THE APP AND THROUGH THE SERVICE IS AS ACCURATE AS POSSIBLE, DIGIDINE GIVES NO
             WARRANTY OF ANY KIND REGARDING THE APP OR THE SERVICE AND/OR DIGIDINE CONTENT
             POSTED OR OTHERWISE MADE AVAILABLE THEREIN. FURTHER, DIGIDINE DOES NOT WARRANT
             THE ACCURACY, COMPLETENESS, CURRENCY OR RELIABILITY OF ANY DIGIDINE CONTENT
             THAT THE RESULTS OBTAINED FROM THE<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:4.7pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">USE OF THE
             APP OR THE SERVICE OR DIGIDINE CONTENT WILL BE ACCURATE OR RELIABLE, OR THAT
             THE QUALITY OF THE APP OR THE SERVICE OR DIGIDINE CONTENT WILL MEET YOUR
             EXPECTATIONS. TO THE MAXIMUM EXTENT ALLOWED BY LAW, DIGIDINE EXPRESSLY
             DISCLAIMS ALL WARRANTIES, REPRESENTATIONS, CONDITIONS, UNDERTAKINGS OR OTHER
             OBLIGATIONS INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
             PARTICULAR PURPOSE, NON-INFRINGEMENT AND<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.1pt;margin-right:20.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">ANY WARRANTY THAT THE APP, THE SERVICE OR
             DIGIDINE CONTENT WILL BE ERROR- FREE OR THAT SUCH ERRORS WILL BE CORRECTED.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.35pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:3.95pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">ANY
             DIGIDINE CONTENT OR OTHER MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE
             USE OF THE APP OR THE SERVICE IS DONE AT YOUR SOLE RISK, AND YOU WILL BE SOLELY
             RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:48.55pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">LOSS OF
             DATA THAT RESULTS FROM THE DOWNLOAD OF THE APP OR ANY SUCH DIGIDINE CONTENT OR
             MATERIAL.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">LIMITATION OF LIABILITY</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:1.0pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:19.6pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">YOU
             EXPRESSLY UNDERSTAND AND AGREE THAT DIGIDINE SHALL NOT BE LIABLE FOR ANY
             DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES,
             INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS,<o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><div class="WordSection7">

             <p class="MsoNormal" style="margin-top:4.0pt;margin-right:6.35pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">GOODWILL,
             USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF DIGIDINE HAS BEEN ADVISED OF THE
             POSSIBILITY OF SUCH DAMAGES), RESULTING FROM: (I) THE USE OR THE INABILITY TO
             USE THE APP; (II) THE COST OF PROCUREMENT OF SUBSTITUTE PRODUCTS AND SERVICE
             RESULTING FROM THE INABILITY TO ACCESS OR UTILISE ANY PRODUCTS, DATA,
             INFORMATION OR SERVICE PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR
             TRANSACTIONS ENTERED INTO THROUGH OR FROM THE APP OR THE SERVICE; (III)
             UNAUTHORISED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; OR (IV) ANY
             OTHER MATTER RELATING TO THE APP OR THE SERVICE. IN NO EVENT SHALL DIGIDINE’S
             TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.1pt;margin-right:3.9pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">ACTION (WHETHER IN CONTRACT, TORT (INCLUDING,
             BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE EXCEED THE AMOUNT PAID BY YOU, IF
             ANY, FOR ACCESSING THE APP<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">OR THE SERVICE.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.25pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF
             CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL
             OR CONSEQUENTIAL<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt;line-height:12.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">DAMAGES. ACCORDINGLY, SOME OF THE ABOVE
             LIMITATIONS MAY NOT APPLY TO YOU.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">OTHER DISCLAIMERS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The App
             and any products or services provided through the App may be temporarily
             unavailable<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:3.75pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">from time
             to time for maintenance or other reasons. DigiDine assumes no responsibility
             for any error, omission, interruption, deletion, defect, delay in operation or
             transmission, communications line failure, theft or destruction or unauthorised
             access to, or alteration of, communications.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:5.3pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             is not responsible for any technical malfunction or other problems of any
             telephone network or service, computer systems, servers or providers, computer
             or mobile phone equipment, software, failure of email or players on account of
             technical problems or traffic congestion on the internet, on the App, on any
             website or any combination thereof, including injury or damage to you or to any<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:10.65pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">other
             person’s computer, mobile phone, or other hardware or software, related to or
             resulting from using or downloading the App or materials in connection with the
             App and/or in connection with any products or services offered through the App.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.5pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:26.25pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">DigiDine
             shall not be liable for any loss or damage you suffer as a result of the
             provision, or lack thereof, of goods and services by the Participating
             Restaurant.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.15pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">RELEASE AND INDEMNIFICATION</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.95pt;line-height:14.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:14.0pt"><o:p>&nbsp;</o:p></span></p>

             <p class="MsoNormal" style="margin-top:0cm;margin-right:10.8pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You agree
             to release DigiDine, its members, managers, officers, employees and agents,
             from any and all liability and obligations whatsoever in connection with or
             arising from your use of the App or the service provided through the App. If at
             any time you are not satisfied with the App or object to any material within
             the App, your sole remedy is cessation of use thereof. You agree to defend,
             indemnify and hold harmless DigiDine, its officers, members, directors,
             employees and agents from and against any and all claims, liabilities, damages,
             losses or expenses, including legal fees and<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:5.25pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">costs and
             expenses, arising out of or in any way connected with (1) your access to or use
             of the App or any part thereof, (2) any ratings you publish via the App, (3) a
             breach or alleged breach by you of any of your representations, warranties,
             covenants or obligations under these Terms of Use, (4) infringement or
             misappropriation of any intellectual property or other rights of DigiDine or
             third<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.05pt;margin-right:0cm;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">parties by
             you, or (5) any negligence or wilful misconduct by you.<o:p></o:p></span></p>

             <p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p>

             <p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">GOVERNING LAW AND MISCELLANEOUS ITEMS</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p>

             </div><p class="MsoNormal" style="margin-top:.1pt;line-height:6.0pt;mso-line-height-rule:
             exactly">

             <span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial;mso-ansi-language:EN-US;mso-fareast-language:
             EN-US;mso-bidi-language:AR-SA"><br clear="all" style="page-break-before:always;
             mso-break-type:section-break">
             </span>

             </p><p class="MsoNormal" style="margin-top:4.0pt;margin-right:18.1pt;margin-bottom:
             0cm;margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">The
             validity and effect of these Terms of Use shall be governed by and construed
             and enforced in accordance with the laws of the UAE.<o:p></o:p></span></p><p class="MsoNormal" style="margin-top:.3pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.0pt"><o:p>&nbsp;</o:p></span></p><p class="MsoNormal" style="margin-top:0cm;margin-right:3.6pt;margin-bottom:0cm;
             margin-left:5.0pt;margin-bottom:.0001pt"><span lang="EN-US" style="font-size:
             10.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial">You agree
             that your breach of the provisions of these Terms of Use would cause
             irreparable harm and significant injury to DigiDine which would be both
             difficult to ascertain and which would not be compensable by damages alone. As
             such, you agree that DigiDine has the right to enforce the provisions of these
             Terms of Use by injunction (without necessity of posting bond), specific performance
             or other equitable relief without prejudice to any other rights and remedies
             that DigiDine may have. DigiDine will be entitled to reasonable legal fees and
             fees of accountants and other professionals, and costs and expenses in addition
             to any other relief to which DigiDine may be entitled in any action at law or
             in equity.<o:p></o:p></span></p><p class="MsoNormal" style="margin-top:.2pt;line-height:11.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:11.0pt">&nbsp;</span></p><p class="MsoNormal" style="margin-left:5.0pt"><b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;
             mso-fareast-font-family:Arial">CONTACT</span></b><span lang="EN-US" style="font-size:16.5pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
             Arial"><o:p></o:p></span></p><p class="MsoNormal" style="margin-top:.2pt;line-height:7.0pt;mso-line-height-rule:
             exactly"><span lang="EN-US" style="font-size:7.5pt"><o:p>&nbsp;</
            """
            
            webView.loadHTMLString(self.text, baseURL: nil)
            activityIndicator = UIActivityIndicatorView()
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray
            
        }else{
                    
            let myURL = URL(string: link)
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
            activityIndicator = UIActivityIndicatorView()
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray
        }
        self.webView.addSubview(activityIndicator)
        
        showActivityIndicator(show: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.showActivityIndicator(show: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.titleLabel.text = self.titl
    }
    
    func showActivityIndicator(show: Bool) {
         if show {
             activityIndicator.startAnimating()
         } else {
             activityIndicator.stopAnimating()
         }
     }
     
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         
     }
     
     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
         showActivityIndicator(show: false)
     }
     
     @IBAction func goBack(_ sender: UIButton) {
        popVC()
     }
}
