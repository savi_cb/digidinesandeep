//
//  CardPayVC.swift
//  Sneni
//
//  Created by osvin-k01 on 4/27/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class CardPayVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARk::- IBOutlets
    @IBOutlet weak var tableView: UITableView?{
        didSet{
            tableView?.delegate = self
            tableView?.dataSource = self
            tableView?.estimatedRowHeight = 80
            
        }
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        popVC()
    }
}
//MARK::- TableView Delegate , dataSOurce
extension CardPayVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
