//
//  ChangePAssViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 4/27/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func changePass(_ sender: UIButton) {
        showAlertChangePassword()
    }
}
extension ChangePassViewController {
    
    func showAlertChangePassword(msg: String = "", old: String = "", new: String = "", confirm: String = "") {
        
        let strmsg = msg.isEmpty ? L10n.EnterYourDetails.string : msg
        let alert = TYAlertView(title: L10n.ChangePassword.string , message: strmsg)
        let alertController = TYAlertController(alert: alert, preferredStyle: .alert, transitionAnimation: .scaleFade)
        
        let action = TYAlertAction(title: L10n.Save.string, style: .default) {
            [weak self] (action) in
            //            alert.hideView()
            self?.handleAlertAction(alertController: alertController ?? TYAlertController(alert: alert))
        }
        alert?.buttonDefaultBgColor = Colors.MainColor.color()
        alert?.textFieldFont = UIFont(name: Fonts.ProximaNova.Regular, size: Size.Small.rawValue)
        
        alert?.add(TYAlertAction(title: L10n.Cancel.string , style: .destructive, handler: { (action) in
            alert?.hide()
        }))
        
        alert?.add(action)
        
        let tempPlaceholderArr = [L10n.OldPassword.string,L10n.NewPassword.string,L10n.ConfirmPassword.string]
        let arrvalue = [old, new, confirm]
        for (index,element) in tempPlaceholderArr.enumerated() {
            alert?.addTextField { (textField) in
                textField?.placeholder = element
                textField?.text = arrvalue[index]
                textField?.tag = index + 1
                textField?.returnKeyType = .done
                textField?.isSecureTextEntry = true
            }
        }
        
        presentVC(alertController ?? TYAlertController(alert: alert))
    }
    
    func handleAlertAction(alertController : TYAlertController){
        alertController.view.endEditing(true)
        var old = ""
        var new = ""
        var confirm = ""
        
        var arrTextFieldText : [String] = []
        
        let arrTextfieldNames = [L10n.OldPassword.string,L10n.NewPassword.string,L10n.ConfirmPassword.string]
        for (index,_) in arrTextfieldNames.enumerated() {
            
            let text = /(alertController.alertView.viewWithTag(index + 1) as? UITextField)?.text?.trimmed()
            if index == 0 {
                old = text
            } else if index == 1 {
                new = text
            } else if index == 2 {
                confirm = text
            }
            arrTextFieldText.append(text)
        }
        
        let message = /User.validateChangePassword(passwords: arrTextFieldText)
        //        alertController.dismissViewController(animated: false)
        
        if message.isEmpty {
            changePasswordWebservice(old: old, new: new)
        }else{
            showAlertChangePassword(msg: message, old: old, new: new, confirm: confirm)
            
            //            alertController.view.endEditing(true)
            //            alertController.view.makeToast(message)
            
            
        }
        
    }
    
    func changePasswordWebservice(old : String?,new : String?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ChangePassword(FormatAPIParameters.ChangePassword(oldPassword: old, newPassword: new).formatParameters())) { (response) in
            
            APIManager.sharedInstance.hideLoader()
            switch response {
            case .Success(_):
                
                UtilityFunctions.showSweetAlert(title: L10n.Success.string, message: L10n.PasswordChangedSuccess.string, style: AlertStyle.Success, success: {}, cancel: {})
                
            case .Failure(let validation):
                APIManager.sharedInstance.hideLoader()
                SKToast.makeToast(validation.message)
                break
            }
            
        }
    }
}
