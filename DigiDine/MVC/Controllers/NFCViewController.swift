//
//  NFCViewController.swift
//  Sneni
//
//  Created by neelam panwar on 23/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import CoreLocation
import ObjectMapper
import EZSwiftExtensions

class NFCViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var tapMeToStartLabel: UILabel!{
        didSet{
            tapMeToStartLabel.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 3));
        }
    }
    @IBOutlet weak var determineTypeView: UIView!
    @IBOutlet weak var rippleView: UIView!
    @IBOutlet weak var nfcLoaderHeight: NSLayoutConstraint!{
        didSet{
            nfcLoaderHeight.constant = 250
        }
    }
    @IBOutlet weak var nfcLoaderImageView: UIImageView!{
        didSet{
            nfcLoaderImageView.image = UIImage()
        }
    }
    @IBOutlet weak var welcomeLabel: UILabel!
    
    var ripple_View: SMRippleView?
    var imagePicker      = UIImagePickerController()
    var currentSessionData = [currentSession]()
    var comingFrom : String? = ""
    var dataDict = [String : Any]()
    var scanTableId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.goForward), name: NSNotification.Name(rawValue: "mergeQrCode"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkCurrentSession), name: NSNotification.Name(rawValue: "checkCurrentSession"), object: nil)
        self.determineTypeView.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        LocationManager.sharedInstance.startTrackingUser()
        if GDataSingleton.isAskLocationDone == true {
            //self.moveToSplashVc()
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(self.locationNotFetched), name: NSNotification.Name("LocationNotFetched"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.locationFetched), name: NSNotification.Name("LocationFetched"), object: nil)
        }
        
        let userData =  GDataSingleton.sharedInstance.loggedInUser
        welcomeLabel?.text = "Hello, \(userData?.firstName?.capitalizedFirst() ?? "") let's get started"
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    @objc func goForward() {
        guard let vc = ez.topMostVC as? DigiDineHeaderVC else { return }
        var tableId = Int()
        tableId = /Int(/GDataSingleton.sharedInstance.tableNumber)
        let VC = DigiDineHeaderVC.getVC(.digiHome)
        VC.tableNumberShoe = self.scanTableId
        VC.continueSession = true
        self.pushVC(VC)
    }
    
    @objc func willEnterForeground() {
        // do what's needed
        rippleView.transform = CGAffineTransform(scaleX: 2, y: 2)
        //You can animate the change:
        
        UIView.animate(withDuration: 1) { () -> Void in
            self.rippleView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
        // And if you want to expand the button only shortly and then have it go back to its original size you can chain two animations:
        
        UIView.animateKeyframes(withDuration: 1.0, delay: 1, options: [.repeat], animations: { () -> Void in
            self.rippleView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) { (finished) -> Void in
            UIView.animate(withDuration: 1.0, delay: 2, options: [.repeat], animations: { () -> Void in
                self.rippleView.transform = CGAffineTransform.identity
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.determineTypeView.isHidden = true
        self.checkSession()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //self.rippleView.transform = CGAffineTransform(scaleX: 2, y: 2)
            //You can animate the change:
            
            UIView.animate(withDuration: 1) { () -> Void in
                self.rippleView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }
            // And if you want to expand the button only shortly and then have it go back to its original size you can chain two animations:
            
            UIView.animateKeyframes(withDuration: 1.0, delay: 1, options: [.repeat, .allowUserInteraction], animations: { () -> Void in
                self.rippleView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }) { (finished) -> Void in
                UIView.animate(withDuration: 1.0, delay: 2, options: [.repeat, .allowUserInteraction], animations: { () -> Void in
                    self.rippleView.transform = CGAffineTransform.identity
                })
            }
            
        }
    }
    
    @IBAction func scanNFCAction(_ sender: UIButton) {
        self.checkCurrentSession()
    }
    
    @objc func locationNotFetched() {
        GDataSingleton.isAskLocationDone = false
    }
    
    @objc func locationFetched() {
        GDataSingleton.isAskLocationDone = true
    }
    
    @IBAction func enterManuallyAction(_ sender: UIButton) {
        GDataSingleton.sharedInstance.loginType = "Manual"
         GDataSingleton.sharedInstance.isHotel = false
        DBManager.sharedManager.cleanCart()
        let VC = DigiDineViewController.getVC(.digiHome)
        pushVC(VC)
    }
    
    
    @IBAction func scanQRCodeAction(_ sender: UIButton) {
        //        self.openCamera()
        //        let navigationVc = StoryboardScene.Main.instantiateCamera()
        //        UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController = navigationVc
        //        let VC = ScanVC.getVC(.digiHome)
        //        pushVC(VC)
    }
    
    @objc func checkSession(){
        let params = [String : Any]()
        print(params)
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/currentTableSession"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            GDataSingleton.sharedInstance.orderId = nil
            if let dict = response.dictionaryObject {
                if let data = dict["data"] as? [String:Any]{
                    if let tableOrNonTable = data["tableOrNonTable"] as? Int {
                        if tableOrNonTable == 1 {
                            if let insideData = data["currentSession"] as? [Dictionary<String,Any>]{
                                if let orderId = insideData.first?["id"] as? Int {
                                    print(orderId)
                                    GDataSingleton.sharedInstance.orderId = "\(orderId )"
                                }else{
                                    DBManager.sharedManager.cleanCart()
                                    //GDataSingleton.sharedInstance.selfPickup = nil
                                }
                            }
                        }
                    }
                    if let insideData = data["currentSession"] as? [Dictionary<String,Any>]{
                        if let userInfo = Mapper<currentSession>().mapArray(JSONObject: insideData){
                            self.currentSessionData = userInfo
                            if let data = self.currentSessionData.first{
                                if self.currentSessionData.count > 0 {
                                    if let self_pickup = insideData.first?["self_pickup"] as? Int {
                                        GDataSingleton.sharedInstance.selfPickup = self_pickup
                                        GDataSingleton.sharedInstance.isHotel = false
                                    }
                                    GDataSingleton.sharedInstance.supplierId = "\(data.supplier_id ?? 0)"
                                    GDataSingleton.sharedInstance.tableNumber = "\(data.id ?? 0)"
                                }
                            }
                        }
                    }
                }
                let status_code = dict["status"] as? Int
                if status_code == 401{
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window else { return }
                    GDataSingleton.sharedInstance.loggedInUser = nil
                    UtilityFunctions.showAlert(title: nil, message: L10n.SessionExpiredLoginToContinue.string, success: {
                        appDelegate.switchViewControllers()
                    }, cancel: {
                        appDelegate.switchViewControllers()
                    })
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
        }
    }
    
    @objc func checkCurrentSession(){
        let params = [String : Any]()
        APIManager.sharedInstance.showLoader()
        print(params)
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/currentTableSession"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            GDataSingleton.sharedInstance.orderId = nil
            
            if let dict = response.dictionaryObject {
                if let data = dict["data"] as? [String:Any]{
                    if let tableOrNonTable = data["tableOrNonTable"] as? Int {
                        if tableOrNonTable == 1 {
                            if let insideData = data["currentSession"] as? [Dictionary<String,Any>]{
                                if let orderId = insideData.first?["id"] as? Int {
                                    print(orderId)
                                    GDataSingleton.sharedInstance.orderId = "\(orderId )"
                                    self.determineTypeView.isHidden = false
                                }else{
                                    self.determineTypeView.isHidden = false
                                }
                            }
                        }else{
                            if let isSessionExist = data["isSessionExist"] as? Int {
                                if isSessionExist == 1 {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "invalidateSessionTimer"), object: nil, userInfo: nil)
                                    //Invalidate Timer here for seesion Api
                                    if let insideData = data["currentSession"] as? [Dictionary<String,Any>]{
                                        if let userInfo = Mapper<currentSession>().mapArray(JSONObject: insideData){
                                            self.currentSessionData = userInfo
                                            if let data = self.currentSessionData.first{
                                                if data.payment_status == 0 {
                                                    GDataSingleton.sharedInstance.supplierId = "\(data.supplier_id ?? 0)"
                                                    GDataSingleton.sharedInstance.tableNumber = "\(data.id ?? 0)"
                                                }
                                                if self.currentSessionData.count > 0 {
                                                    let title = self.currentSessionData.first?.supplier_name ?? ""
                                                    let tableNumber = self.currentSessionData.first?.table_name ?? ""
                                                    UtilityFunctions.showSweetAlert(title: "", message: "Do you want to end your current session at \(title) and leave table number \(tableNumber)?", success: { [weak self] in
                                                        guard let self = self else {return}
                                                        print(self)
                                                        if let data = self.currentSessionData.first{
                                                            if data.payment_status == 0 {
                                                                if data.order_id != 0  && (data.status == 1 || data.status == 0){
                                                                    SKToast.makeToast("Clear your payment before leaving")
                                                                    self.tabBarController?.selectedIndex = 1
                                                                }else{
                                                                    self.leaveSessionAPI(sessionId: /data.id)
                                                                }
                                                            }else{
                                                                self.leaveSessionAPI(sessionId: /data.id)
                                                            }
                                                        }
                                                    }) {
                                                        if let data = self.currentSessionData.first{
                                                            GDataSingleton.sharedInstance.loginType = "NFC"
                                                            GDataSingleton.sharedInstance.supplierId = "\(data.supplier_id ?? 0)"
                                                            GDataSingleton.sharedInstance.tableNumber = "\(data.id ?? 0)"
                                                            let VC = DigiDineHeaderVC.getVC(.digiHome)
                                                            VC.continueSession = true
                                                            self.pushVC(VC)
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    self.determineTypeView.isHidden = false
                                }
                            }
                        }
                    }
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
        }
    }
    
    func leaveSessionAPI(sessionId : Int){
        var tableId = String()
        if GDataSingleton.sharedInstance.tableNumber != nil{
            tableId = /GDataSingleton.sharedInstance.tableNumber
        }else{
            tableId = String(sessionId)
        }
        APIManager.sharedInstance.showLoader()
        let params = FormatAPIParameters.leaveSession(sessionRecordId: tableId).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.leaveSession(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                print(object)
                APIManager.sharedInstance.hideLoader()
                if object as? Bool == true{
                    SKToast.makeToast("Clear your payment before leaving")
                    self.tabBarController?.selectedIndex = 1
                }else{
                    DBManager.sharedManager.cleanCart()
                    GDataSingleton.sharedInstance.tableNumber = nil
                    GDataSingleton.sharedInstance.selfPickup = nil
                    SKToast.makeToast("Session ended successfully")
                }
            case .Failure(let error):
                //self.locationFetched(address: nil)
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func animateCircle(){
        rippleView.transform = CGAffineTransform(scaleX: 2, y: 2)
        //You can animate the change:
        
        UIView.animate(withDuration: 1) { () -> Void in
            self.rippleView.transform = CGAffineTransform(scaleX: 2, y: 2)
        }
        // And if you want to expand the button only shortly and then have it go back to its original size you can chain two animations:
        
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.rippleView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) { (finished) -> Void in
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.rippleView.transform = CGAffineTransform.identity
            })
        }
    }
}
//MARK:- UIViewControllerTransitioningDelegate
extension NFCViewController : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}


//func animateImage() {
//    addRippleEffect(to: rippleView)
//}

//func addRippleEffect(to referenceView: UIView) {
//      /*! Creates a circular path around the view*/
//      let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height))
//      /*! Position where the shape layer should be */
//      let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
//      let rippleShape = CAShapeLayer()
//      rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
//      rippleShape.path = path.cgPath
//      rippleShape.fillColor = UIColor.clear.cgColor
//      rippleShape.strokeColor = UIColor.black.cgColor
//      rippleShape.lineWidth = 2
//      rippleShape.position = shapePosition
//      rippleShape.opacity = 0
//
//      /*! Add the ripple layer as the sublayer of the reference view */
//      referenceView.layer.addSublayer(rippleShape)
//      /*! Create scale animation of the ripples */
//      let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
//      scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
//      scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 2, 1))
//      /*! Create animation for opacity of the ripples */
//      let opacityAnim = CABasicAnimation(keyPath: "opacity")
//      opacityAnim.fromValue = 1
//      opacityAnim.toValue = 0
//      /*! Group the opacity and scale animations */
//      let animation = CAAnimationGroup()
//      animation.animations = [scaleAnim, opacityAnim]
//      animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//      animation.duration = CFTimeInterval(1.0)
//      animation.repeatCount = .infinity
//      animation.isRemovedOnCompletion = true
//      rippleShape.add(animation, forKey: "rippleEffect")
//  }
