//
//  PaymentGateViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 7/7/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import WebKit

class PaymentGateViewController: UIViewController, WKNavigationDelegate {
  
  @IBOutlet weak var webView: WKWebView!
  
  var paymentLink = String()
  var order_reference = String()
  var payType = Bool()
  var success : Bool? = false
  var userPaymentLinkId = Int()
  var comingWithNewCard : String? = "false"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    webView.navigationDelegate = self
    let myURL = URL(string: paymentLink)
    let myRequest = URLRequest(url: myURL!)
    webView.load(myRequest)
  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    print(navigationAction.request.url)
    decisionHandler(.allow)
    if let host = navigationAction.request.url?.absoluteString.contains("https://api.digidine.ae/payment_success?") {
      if host == true {
        success = true
      }
    }
    //decisionHandler(.cancel)
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
    //        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    print("finish to load")
    if success == true {
      let payment = ["payNow" : self.payType]
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderNotifi"), object: nil, userInfo: payment)
      if comingWithNewCard == "true"{
        self.popViewControllerss(popViews: 3)
      }else if comingWithNewCard == "invalid"{
        self.popVC()
      }else{
        self.popViewControllerss(popViews: 2)
      }
      //popTo(toControllerType: CartViewController.self)
    }
  }
  
  @IBAction func back(_ sender: UIButton) {
    self.popVC()
  }
  
  func popViewControllerss(popViews: Int, animated: Bool = true) {
      if self.navigationController!.viewControllers.count > popViews{
          let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
           self.navigationController?.popToViewController(vc, animated: animated)
      }
  }
}
