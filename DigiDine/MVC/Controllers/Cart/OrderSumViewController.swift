////
////  OrderSumViewController.swift
////  Sneni
////
////  Created by osvin-k01 on 6/2/20.
////  Copyright © 2020 Taran. All rights reserved.
////
//
//import UIKit
//
//class OrderSumViewController: UIViewController {
//    
//    //MARK:- Variables
//    var FROMSIDEPANEL : Bool = false
//    var tableDataSource = CartDataSource(){
//        didSet{
//            tableView.delegate = tableDataSource
//            tableView.dataSource = tableDataSource
//        }
//    }
//    var deliveryData = Delivery()
//    var promoCode:PromoCode?
//    var cartProdcuts : [AnyObject]?
//    var cartArray : [Cart]?
//    var remarks : String?
//    var selectedAddress: Address?
//    var orderSummary:OrderSummary?
//    var selectedDeliverySpeed : DeliverySpeedType = .Standard
//    var parameters = Dictionary<String,Any>()
//    var selectedPaymentMethod: PaymentMode? {
//        didSet {
//            self.paymentType_label.text = selectedPaymentMethod?.displayName ?? "Choose Payment".localized()
//        }
//    }
//    var isOptionSelected = false
//    var pickupType = [Int]()
//    var deliveryType:Int = 0 // 0- delivery, 1- pickup
//    var fromCartView = false
//    var refreshPriceData = false
//    var referralApplied = false
//    var tipItems : [Int]?
//    var arrPrescription: [String]?
//    var txtPrescription: String?
//    var selectedAgentId: CblUser?
//    
//    @IBOutlet weak var tableView: UITableView! {
//            didSet {
//                tableView.registerCells(nibNames: [ProductListingCell.identifier, CartListingCell.identifier,ProductListCell.identifier, CartQuestionCell.identifier])
//    //tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
//                if #available(iOS 13.0, *) {
//                    tableView.automaticallyAdjustsScrollIndicatorInsets = false
//                }
//            }
//        }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view.
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        retrieveReferalAmount()
//        refreshPriceData = true
//        if let _ = cartProdcuts {
//            getCartFromDB()
//            //configureTableDataSource(products: cartProdcuts)
//        }else {
//            getCartFromDB()
//        }
//        AdjustEvent.Cart.sendEvent()
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.handleCartQuantity(sender:)), name: NSNotification.Name(rawValue: CartNotification), object: nil)
//        
//    }
//    
//    func retrieveReferalAmount() {
//        if /GDataSingleton.sharedInstance.loggedInUser?.token == ""{
//            return
//        }
//        
//        let objR = API.getReferalAmount
//        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
//            [weak self] (response) in
//            guard let self = self else { return }
//            switch response {
//            case APIResponse.Success(let object):
//                self.tableView.reloadData()
//            default :
//                print("Hello Nitin")
//                break
//            }
//            
//        }
//    }
//    
//    func getCartFromDB(){
//        DBManager().getCart {
//            [weak self] (array) in
//            self?.configureTableDataSource(products: array)
//        }
//    }
//    
//    func configureTableDataSource(products : [AnyObject]?){
//        
//        if products?.count == 0 {
//            selectedAgentId = nil
//            imgPlaceholderNotext?.isHidden = false
//            address_view.isHidden = true
//            bottom_view.isHidden = true
//            tableView?.isHidden = true
//            cartProdcuts = [AnyObject]()
//            DBManager.sharedManager.cleanCart()
//            //            btnProceedCheckout?.isHidden = true
//            //            btnProceedCheckout?.isEnabled = false
//            return
//        }
//        //        if let isAdded = self.checkIfAddressAdded(),!isAdded {
//        //            self.openAdressController()
//        //        }
//        cartProdcuts = products
//        configureTableView(arrayProducts: cartProdcuts)
//        tableView.reloadTableViewData(inView: view)
//        imgPlaceholderNotext?.isHidden = true
//        address_view.isHidden = false
//        bottom_view.isHidden = false
//        tableView?.isHidden = false
//    }
//    
//    func configureTableView(arrayProducts : Array<AnyObject>?){
//        guard let array = arrayProducts else { return }
//        var idArray = [String]()
//        
//        for i in 0...array.count-1 {
//            let product = Product(cart: array[/i] as? Cart)
//            idArray.append(product.product_id ?? "")
//            self.pickupType.append(product.self_pickup ?? 1)
//            GDataSingleton.sharedInstance.supplierAddress = product.supplierAddressCart ?? ""
//            //self.locationFetched(address : product.supplierAddressCart ?? "")
//        }
//        
//        self.getProductAccToAddons(array: array)
//        if idArray.count > 0 {
//            self.checkProductList(ids: idArray)
//        }
//        
//    }
//    
//    func getProductAccToAddons(array : Array<AnyObject>?) {
//        guard let arrayCart = array else {return}
//        var productArray = [Product]()
//        
//        for i in 0...arrayCart.count-1 {
//            let product = Product(cart: arrayCart[i] as? Cart)
//            guard let productId = product.id else {return}
//            
//            if let addonId = product.addOnId, addonId != "" {
//                DBManager.sharedManager.getTypeIdsOfAddonId(productId: productId, addonId: addonId) { (typeIdArray) in
//                    
//                    for j in 0...typeIdArray.count-1 {
//                        var productObj : Product?
//                        productObj = Product(cart: arrayCart[i] as? Cart)
//                        DBManager.sharedManager.getAddonsDataFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeIdArray[j]) {(addonModalArray) in
//                            print(addonModalArray)
//                            for addons in addonModalArray {
//                                let index = addons.addonData?.count == 1 ? 0 : j
//                                var tempArray = [[AddonValueModal]]()
//                                if let aa = addons.addonData?[index] {
//                                    tempArray.append(aa)
//                                }
//                                productObj?.arrayAddonValue = tempArray
//                                productObj?.typeId = addons.typeId
//                                productObj?.addOnId = addons.addonId
//                                productObj?.perAddonQuantity = addons.quantity
//                                
//                                guard let obj = productObj else {return}
//                                productArray.append(obj)
//                            }
//                        }
//                    }
//                }
//            } else {
//                productArray.append(product)
//            }
//            
//        }
//        let questions = productArray.first?.questionsSelected
//        //print(productArray)
//        tableDataSource = CartDataSource(items: productArray, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { [weak self] (cell, item) in
//            guard let self = self else { return }
//            self.configureCell(cell: cell, indexPath: item, array: productArray)
//            
//            }, aRowSelectedListener: { (indexPath) in
//                
//        }, aRowSwipeListner: { (indexPath) in
//            let product = productArray[indexPath.row]
//            guard let productId = product.id else {return}
//            if let addonId = product.addOnId, addonId != "" {
//                if let typeId = product.typeId, typeId != "" {
//                    let quant = product.perAddonQuantity ?? 0
//                    DBManager.sharedManager.removeAddonFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeId)
//                    
//                    DBManager.sharedManager.getTotalQuantityPerProduct(productId: productId) { (quantity) in
//                        guard let totalQuant = Int(quantity) else {return}
//                        let savedQuant = totalQuant-quant
//                        DBManager.sharedManager.manageCart(product: product, quantity: savedQuant)
//                        product.arrayAddOnValue?.remove(at: indexPath.row)
//                        
//                        self.getCartFromDB()
//                    }
//                }
//            } else {
//                DBManager.sharedManager.removeProductFromCart(productId: productId)
//                self.getCartFromDB()
//            }
//        })
//        
//        tableDataSource.questions = questions
//        
//    }
//    
//    func checkProductList(ids: [String]) {
//        if !refreshPriceData { return }
//        let objR = API.checkProductList(product_ids: ids)
//        APIManager.sharedInstance.opertationWithRequest(withApi: objR) { [weak self] (result) in
//            guard let `self` = self else { return }
//            switch result {
//            case .Success(let object):
//                self.refreshPriceData = false
//                let obj = object as? CheckProductList
//                self.tipItems = obj?.tips
//                guard let products = obj?.result else {
//                    self.tableView.reloadData()
//                    return
//                }
//                for product in products {
//                    DBManager.sharedManager.refreshPriceToCart(product: product)
//                }
//                self.getCartFromDB()
//                break
//            case .Failure(_):
//                APIManager.sharedInstance.hideLoader()
//                break
//            }
//        }
//        
//    }
//    
//    
//    func retrieveReferalAmount() {
//        if /GDataSingleton.sharedInstance.loggedInUser?.token == ""{
//            return
//        }
//        
//        let objR = API.getReferalAmount
//        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
//            [weak self] (response) in
//            guard let self = self else { return }
//            switch response {
//            case APIResponse.Success(let object):
//                self.tableView.reloadData()
//            default :
//                print("Hello Nitin")
//                break
//            }
//            
//        }
//    }
//    
//    func setDefaultPaymentType() {
//        if AppSettings.shared.appThemeData?.payment_method == "0" {
//            selectedPaymentMethod = PaymentMode(paymentType: .COD)
//        }
//        // else let the user choose
//    }
//    
//    func getAllowedPaymentType() -> PaymentMethod? {
//        //If referral amount is equal to netTotal, no need to select payment method
//        if referralApplied && selectedPaymentMethod?.paymentType == .DoesntMatter {
//            return nil
//        }
//        if (/AppSettings.shared.appThemeData?.payment_method).isEmpty || AppSettings.shared.appThemeData?.payment_method == "2" {
//            //Show both Cash and Card Payments
//            return .DoesntMatter
//        }
//        if AppSettings.shared.appThemeData?.payment_method == "1" {
//            //Show Card Modes
//            return .Card
//        }
//        //If only cash is allowed, return nil
//        return nil
//    }
//    
//    func configureCell(cell : Any,indexPath : Any?,array : [Product]) {
//        
//        guard let index = indexPath as? IndexPath else { return }
//        
//        if let cell = cell as? ProductListCell {
//            // cell.isForCart = true
//            cell.fromCartView = true
//            cell.backgroundColor = .white
//            cell.selectionStyle = .none
//            //            cell.category = self.passedData.categoryOrder
//            //            cell.categoryId = self.passedData.categoryId
//            
//            let data = array[index.row]
//            cell.index = index.row
//            data.supplierAddrerss = data.supplierAddressCart
//            cell.product = data
//            cell.completionBlock = { [weak self] value in
//                guard let self = self else {return}
//                if let data = value as? Double {
//                    self.getCartFromDB()
//                }
//            }
//            
//            cell.addonsCompletionBlock = { [weak self] value in
//                guard let self = self else {return}
//                if let data = value as? (Product,Bool,Double){
//                    if data.1 { // data.1 == true for open customization controller
//                        //  self.openCustomizationView(cell: cell, product: data.0, cartData: nil, quantity: data.2, index: indexPath.row)
//                    }
//                } else if let data = value as? (Product,Cart,Bool,Double,Int) {
//                    //for open checkcustomization controller
//                    let product = data.0
//                    var arrayAddonValue = product.arrayAddonValue ?? []
//                    let productId = product.id ?? ""
//                    let addonId = product.addOnId ?? ""
//                    let typeId = product.typeId ?? ""
//                    let quantity = data.3
//                    
//                    if data.4 == 1 { // for plus action
//                        self.checkAddonExistAcctoTypeid(productId: productId, addonId: addonId, typeId: typeId) { (isContain, dataArray) in
//                            
//                            var array = [[AddonValueModal]]()
//                            array = arrayAddonValue
//                            
//                            let obj = AddonsModalClass(productId: productId, addonId: addonId, addonData: array, quantity: Int(quantity + 1), typeId: typeId)
//                            DBManager.sharedManager.manageAddon(addonData: obj)
//                            
//                            DBManager.sharedManager.getTotalQuantityPerProduct(productId: productId) { (quantity) in
//                                guard let qaunt = Int(quantity) else {return}
//                                DBManager.sharedManager.manageCart(product: product, quantity: qaunt+1)
//                                self.getCartFromDB()
//                            }
//                            
//                        }
//                    } else if data.4 == 2 { // for minus action
//                        let newQuantity = quantity-1
//                        
//                        if newQuantity == 0 {
//                            DBManager.sharedManager.removeAddonFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeId)
//                            
//                            DBManager.sharedManager.getTotalQuantityPerProduct(productId: productId) { (quantity) in
//                                guard let totalQuant = Int(quantity) else {return}
//                                let savedQuant = totalQuant-1
//                                DBManager.sharedManager.manageCart(product: product, quantity: savedQuant)
//                                
//                                arrayAddonValue.removeAll()
//                                
//                                self.getCartFromDB()
//                            }
//                        } else {
//                            var array = [[AddonValueModal]]()
//                            array = arrayAddonValue
//                            
//                            let obj = AddonsModalClass(productId: productId, addonId: addonId, addonData: array, quantity: Int(newQuantity), typeId: typeId)
//                            DBManager.sharedManager.manageAddon(addonData: obj)
//                            
//                            DBManager.sharedManager.getTotalQuantityPerProduct(productId: productId) { (quanty) in
//                                guard let qaunt = Int(quanty) else {return}
//                                DBManager.sharedManager.manageCart(product: product, quantity: qaunt-1)
//                                self.getCartFromDB()
//                            }
//                            
//                        }
//                    }
//                } else if let _ = value as? (Double,Product) {
//                    self.getCartFromDB()
//                }
//            }
//            
//        }  else if let cell = cell as? CartBillCell {
//            cell.selectedAgentId = selectedAgentId
//            if SKAppType.type.isFood {
//                cell.fromCartView = GDataSingleton.sharedInstance.fromCart ?? false
//                cell.tipStackView.isHidden = (self.tipItems ?? []).isEmpty || DeliveryType.shared == .pickup ||  self.selectedPaymentMethod?.paymentType == .COD
//                cell.tipItems = self.tipItems ?? [Int]()
//                cell.tipCollectionView?.reloadData()
//                cell.newCart = array
//            } else {
//                cell.newCart = array
//                cell.tipStackView.isHidden = true
//            }
//            
//            cell.tvRemarks.delegate = self
//            cell.blockPromoCode = {
//                [weak self] code in
//                guard let self = self else { return }
//                self.promoCode = code
//            }
//            cell.referralApplied = {
//                [weak self] applied in
//                guard let self = self else { return }
//                self.referralApplied = applied
//                let referralAmount = Double(GDataSingleton.sharedInstance.referalAmount ?? 0.0)
//                //If referral amount is equal to netTotal, we need to send paymentType 2 to backend
//                if applied && referralAmount == cell.netTotal {
//                    self.selectedPaymentMethod = PaymentMode(paymentType: .DoesntMatter)
//                }
//                else {
//                    self.selectedPaymentMethod = nil
//                }
//            }
//            cell.selectTimeSlotBlock = { [weak self] in
//                
//                if GDataSingleton.sharedInstance.loggedInUser?.token  == nil {
//                    self?.presentVC(StoryboardScene.Register.instantiateLoginViewController())
//                    return
//                }
//                
//                let vcPickUp = PickupDetailsController.getVC(.laundry)
//                self?.presentVC(vcPickUp)
//                
//                vcPickUp.blockSelectOnlyDateAndTime = {
//                    [weak self] date in
//                    guard let self = self else { return }
//                    guard let arrayCart = self.tableDataSource.items as? [Cart] else { return }
//                    var tinterval: Double = 0.0
//                    arrayCart.forEach({
//                        (objC) in
//                        tinterval += /objC.totalDuration
//                    })
//                    let VC =  AgentListingVC.getVC(.order)
//                    VC.selectedDate = date
//                    VC.timeInterVal = tinterval
//                    
//                    //                    VC.orderSummary = OrderSummary(items: self.tableDataSource.items as? [Cart], promo:self.promoCode)
//                    //                    VC.orderSummary?.cartId = cartId
//                    //                    VC.orderSummary?.isAgent = true
//                    //                    VC.orderSummary?.minOrderAmount = (tempCartId)?.components(separatedBy: "$").last
//                    VC.remarks = self.remarks
//                    VC.arrayCart = arrayCart
//                    VC.completion = { [weak self] agent in
//                        GDataSingleton.sharedInstance.pickupDate = date
//                        self?.selectedAgentId = agent
//                    }
//                    self.pushVC(VC)
//                }
//                
//            }
//        }
//        else if let cell = cell as? CartQuestionCell {
//            let questions = array.first?.questionsSelected ?? []
//            let data = questions[index.row]
//            cell.configureCell(question: data, index: index.row, productPrice: Double(/array.first?.getPrice(quantity: (Double(/array.first?.quantity) ?? 1))))
//            ///(array.first?.stepValue ?? 1)
//        }
//        
//    }
//    
//    func handleCellSelction(indexPath : IndexPath){
//        if tableView.cellForRow(at: indexPath) is ProductListingCell, false {
//            
//            let productDetailVc = StoryboardScene.Main.instantiateProductDetailViewController()
//            productDetailVc.passedData.productId = (tableDataSource.items?[indexPath.row] as? Cart)?.id
//            productDetailVc.suplierBranchId = (tableDataSource.items?[indexPath.row] as? Cart)?.supplierBranchId
//            pushVC(productDetailVc)
//            
//        }
//    }
//}
////MARK:- ======== handleCartQuantity ========
//extension CartViewController {
//    
//    @objc func handleCartQuantity(sender : NSNotification){
//        
//        let visibleCells = tableView.visibleCells
//        if visibleCells.first is CartBillCell {
//            tableView?.isHidden = true
//            imgPlaceholderNotext?.isHidden = false
//            address_view.isHidden = true
//            bottom_view.isHidden = true
//            return
//        }
//        for visibleCell in visibleCells {
//            
//            guard let cell = visibleCell as? ProductListingCell else { return }
//            
//            if cell.stepper?.value == 0 {
//                guard let indexpath = tableView.indexPath(for: cell) else { return }
//                tableView.beginUpdates()
//                if tableDataSource.items?.count != 0 {
//                    tableDataSource.items?.remove(at: indexpath.row)
//                }
//                tableView.deleteRows(at: [indexpath], with: .right)
//                tableView.endUpdates()
//                
//                if tableDataSource.items?.count == 0{
//                    tableView?.isHidden = true
//                    imgPlaceholderNotext?.isHidden = false
//                    address_view.isHidden = true
//                    bottom_view.isHidden = true
//                } else {
//                    tableView?.isHidden = false
//                    imgPlaceholderNotext?.isHidden = true
//                    address_view.isHidden = false
//                    bottom_view.isHidden = false
//                    tableView.reloadData()
//                }
//            } else {
//                
//                getCartFromDB()
//            }
//        }
//        
//    }
//}
////MARK: - TextView Delegate
//extension OrderSumViewController : UITextViewDelegate {
//    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        remarks = textView.text
//    }
//}
//
////MARK: - Product Listing Gesture Recognizer
//extension CartViewController {
//    
//    func configureCellSwipe(cell : UITableViewCell?){
//        if cell?.gestureRecognizers?.isEmpty ?? true {
//            let slideGesture = DRCellSlideGestureRecognizer()
//            let (slideAction,pullAction) = (DRCellSlideAction(forFraction: 0.45),DRCellSlideAction(forFraction: 0.45))
//            pullAction?.behavior = .pushBehavior
//            pullAction?.didTriggerBlock = {
//                [weak self] (tableView,indexpath) in
//                
//                guard let arrCart = self?.tableDataSource.items as? [Cart], let indexpath = indexpath else { return }
//                tableView?.beginUpdates()
//                self?.tableDataSource.items?.remove(at: indexpath.row)
//                self?.cartProdcuts?.remove(at: indexpath.row)
//                tableView?.deleteRows(at: [indexpath], with: .right)
//                tableView?.endUpdates()
//                DBManager.sharedManager.manageCart(product: Product(cart: arrCart[indexpath.row]), quantity: 0)
//            }
//            slideGesture.addActions([slideAction,pullAction])
//            cell?.addGestureRecognizer(slideGesture)
//        }
//    }
//}
//
////MARK:- UIViewControllerTransitioningDelegate
//extension OrderSumViewController : UIViewControllerTransitioningDelegate {
//    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
//        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
//    }
//}
//
////MARK:- Delivery View Controller and Payment view controller code merged
//extension OrderSumViewController{
//    
//    func webServiceForUpdateCartInfo(orderSummary: OrderSummary?){
//        
//        //Nitin
//        var deliveryAddressId = "0"
//        if DeliveryType.shared != .pickup {
//            deliveryAddressId = self.selectedAddress?.id ?? "0"
//        }
//        self.deliveryData.deliveryMaxTime = "\(/orderSummary?.deliveryMaxTime)"
//        guard let maxDays = Int(deliveryData.deliveryMaxTime ?? "0") else { return }
//        let timeInterval = Double(60 * maxDays)
//        
//        let deliveryDate = (deliveryData.pickupDate ?? Date()).addingTimeInterval(timeInterval)
//        
//        self.deliveryData.handlingSupplier = "0.0"
//        self.deliveryData.deliveryCharges = String(orderSummary?.dDeliveryCharges ?? 0.0)
//        self.deliveryData.minOrderDeliveryCrossed = "1"
//        self.deliveryData.handlingAdmin = orderSummary?.handlingCharges
//        self.deliveryData.handlingSupplier = orderSummary?.handlingSupplier
//        //.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
//        if let minOrderPrice = self.deliveryData.minOrderDeliveryCharge?.toDouble(),let totalCharges = self.deliveryData.totalPrice?.toDouble() {
//            self.deliveryData.minOrderDeliveryCrossed = totalCharges > minOrderPrice ? "1" : "0"
//        } else {
//            self.deliveryData.minOrderDeliveryCrossed = "1"
//        }
//        
//        DeliveryViewController.updateCart(delivery: self.deliveryData, order: self.orderSummary, deliverySpeed: selectedDeliverySpeed, addressId: deliveryAddressId, deliveryDate: deliveryDate, remarks: "", addOn: orderSummary?.addOnCharge, newTotal: nil ) {
//            [weak self] in
//            
//            guard let self = self else {
//                APIManager.sharedInstance.hideLoader()
//                return }
//            
//            self.handleUpdateCartInfo()
//            
//        }
//        
//    }
//    
//    func handleUpdateCartInfo(){
//        
//        var deliveryAddress  : Address?
//        
//        deliveryAddress = self.selectedAddress
//        
//        if let maxDays = Int(deliveryData.deliveryMaxTime ?? "0") {
//            let timeInterval = Double(60 * maxDays)
//            orderSummary?.deliveryDate = (deliveryData.pickupDate ?? Date()).addingTimeInterval(timeInterval)
//        }
//        
//        orderSummary?.dDeliveryCharges = deliveryData.deliveryCharges?.toDouble()
//        
//        orderSummary?.initalizeOrderSummary(cart:  orderSummary?.items, forRental: true, rentalTotal: orderSummary?.netPayableAmount)
//        orderSummary?.deliveryAddress = deliveryAddress
//        orderSummary?.pickupAddress = deliveryData.pickupAddress
//        orderSummary?.pickupDate = deliveryData.pickupDate
//        orderSummary?.selectedDeliverySpeed = selectedDeliverySpeed
//        
//        if orderSummary?.items?.first?.category == Category.Laundry.rawValue || orderSummary?.items?.first?.category == Category.BeautySalon.rawValue {
//            orderSummary?.pickupBuffer = deliveryData.deliveryMaxTime
//        }
//        //orderSummary?.paymentMode = deliveryData.paymentMode
//        
//        self.generateOrder()
//    }
//    
//    func generateOrder(){
//        
//        APIManager.sharedInstance.showLoader()
//        var agentId = [Int]()
//        if let value = orderSummary?.agentId?.toInt() {
//            agentId.append(value)
//        }
//        
//        let timeInter = Date().timeIntervalSince(orderSummary?.currentDate ?? Date())
//        
//        var pickupAddress = ""
//        if let add = self.parameters["pickupAddress"] as? String {
//            pickupAddress = add
//        }
//        var dropoffAddress = ""
//        if let dropAddress = self.parameters["dropoffAddress"] as? String {
//            dropoffAddress = dropAddress
//        }
//        
//        var pickupApiDate = ""
//        if let pickapiDate = self.parameters["pickupApiDate"] as? String {
//            pickupApiDate = pickapiDate
//        }
//        
//        var dropoffApiDate = ""
//        if let dropoapiDate = self.parameters["dropoffApiDate"] as? String {
//            dropoffApiDate = dropoapiDate
//        }
//
//        
//        var pickupCordinates = CLLocationCoordinate2D()
//        if let pickCordinates = self.parameters["pickupCordinates"] as? CLLocationCoordinate2D {
//            pickupCordinates = pickCordinates
//        }
//        
//        var dropoffCordinates = CLLocationCoordinate2D()
//        if let dropoffCord = self.parameters["dropoffCordinates"] as? CLLocationCoordinate2D {
//            dropoffCordinates = dropoffCord
//        }
//        let pickupLatitude = pickupCordinates.latitude
//        let pickupLongitude = pickupCordinates.longitude
//        let dropoffLatitude = dropoffCordinates.latitude
//        let dropoffLongitude = dropoffCordinates.longitude
//                
//        let objR = API.GenerateOrder(FormatAPIParameters.GenerateOrder(
//            useReferral: /orderSummary?.useReferral,
//            promoCode: orderSummary?.promoCode,
//            cartId: orderSummary?.cartId,
//            isPackage: orderSummary?.isPackage,
//            paymentType:selectedPaymentMethod ?? PaymentMode(paymentType: .COD),
//            agentIds: agentId,
//            deliveryDate: orderSummary?.currentDate ?? Date(),
//            duration: /orderSummary?.duration, from_address: pickupAddress, to_address: dropoffAddress, booking_from_date: pickupApiDate, booking_to_date: dropoffApiDate, from_latitude: pickupLatitude, to_latitude: dropoffLatitude, from_longitude: pickupLongitude, to_longitude: dropoffLongitude,
//            tip_agent: GDataSingleton.sharedInstance.tipAmount,
//            arrPres: arrPrescription,
//            instructions: txtPrescription,
//            bookingDate: GDataSingleton.sharedInstance.pickupDate, questions: orderSummary?.questions, customer_payment_id: GDataSingleton.sharedInstance.customerPaymentId).formatParameters())
//        //deliveryDate?.add(seconds: Int(timeInter)) //Nitin
//        APIManager.sharedInstance.opertationWithRequest(refreshControl: nil, isLoader: false, withApi: objR) { [weak self] (response) in            APIManager.sharedInstance.hideLoader()
//            weak var weakSelf = self
//            switch response {
//            case .Success(let object):
//                weakSelf?.handleGenerateOrder(orderId: object)
//            case .Failure(_):
//                break
//            }
//        }
//    }
//    
//    func handleGenerateOrder(orderId : Any?){
//        
//        //print(orderId)
//        AdjustEvent.Order.sendEvent(revenue: orderSummary?.totalAmount)
//        GDataSingleton.sharedInstance.tipAmount = 0
//        DBManager.sharedManager.cleanCart()
//        UtilityFunctions.showSweetAlert(title: L10n.OrderPlacedSuccessfully.string, message: L10n.YourOrderHaveBeenPlacedSuccessfully.string, style: .Success, success: {
//            [weak self] in
//            guard let self = self else { return }
//            
//            if self.orderSummary?.selectedDeliverySpeed == .scheduled {
//                
//                let VC = StoryboardScene.Order.instantiateOrderSchedularViewController()
//                let orderIdArr = orderId as? [Int]
//                let ordrs = orderIdArr?.map({ (orderId) -> String in
//                    return String(orderId)
//                })
//                VC.orderId = ordrs?.joined(separator: ",")
//                VC.orderSummary = self.orderSummary
//                VC.orderSummary?.paymentMode = self.selectedPaymentMethod ?? PaymentMode(paymentType: .COD)
//                self.pushVC(VC)
//            }
//            else
//            {
//                let orderIdArr = orderId as? [Int]
//                let ordrs = orderIdArr?.map({ (orderId) -> String in
//                    return String(orderId)
//                })
//                let orders = ordrs?.joined(separator: ",")
//                let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
//                orderDetailVc.isBuyOnly = /self.orderSummary?.isBuyOnly
//                orderDetailVc.orderDetails = OrderDetails(orderSummary: self.orderSummary,orderId: orders,scheduleOrder: "")
//                orderDetailVc.type = .OrderUpcoming
//                orderDetailVc.isOrderCompletion = true
//                self.pushVC(orderDetailVc)
//            }
//            
//        })
//        
//    }
//    
//}
//
//
//extension OrderSumViewController {
//    
//    func checkAddonExistAcctoTypeid(productId: String, addonId: String,typeId:String, finished: (_ isContain: Bool, _ data: [AddonsModalClass]?)-> Void) {
//        
//        DBManager.sharedManager.getAddonsDataFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeId) { (data) in
//            print(data)
//            finished(data.count == 0 ? false : true, data.count == 0 ? nil : data)
//        }
//        
//    }
//    
//}
