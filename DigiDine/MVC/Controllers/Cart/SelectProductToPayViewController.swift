//
//  CartViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import ObjectMapper

class SelectProductToPayViewController: BaseViewController {
    
    //MARK:- IBOutlet
    
    
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var tableParticipentsLabel: UILabel!
    @IBOutlet weak var tableName: UILabel!
    @IBOutlet weak var orderHeaderLabel: UILabel!
    @IBOutlet weak var orderByLabel: UILabel!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var PriceView: UIView!
    
    @IBOutlet weak var tableViewHeightConstant: NSLayoutConstraint!{
        didSet{
            self.tableViewHeightConstant.constant = self.tableView.contentSize.height
            self.scrollView.contentInset.bottom = self.tableView.contentSize.height
        }
    }
    
    @IBOutlet weak var payNowStackHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack : UIButton! {
        didSet {
            btnBack.isHidden = false
        }
    }
    
    @IBOutlet var imgPlaceholderNotext: UIView!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            //            tableView.registerCells(nibNames: [ProductListingCell.identifier, CartListingCell.identifier,ProductListCell.identifier, CartQuestionCell.identifier])
            //tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)\
            tableView.frame.size.height = tableView.contentSize.height
            if #available(iOS 13.0, *) {
                tableView.automaticallyAdjustsScrollIndicatorInsets = false
            }
        }
    }
    
    
    @IBOutlet weak var tableNameLabel: UILabel!{
        didSet{
            
        }
    }
    @IBOutlet weak var tableJoinedLabel: UILabel!{
        didSet{
            
        }
    }
    @IBOutlet weak var totalamountLabel: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var totalAmountLabel: UILabel!{
        didSet{
            
        }
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableViewHeightConstant?.constant = self.tableView.contentSize.height + 50
    }
    
    
    //MARK:- Variables
    var FROMSIDEPANEL : Bool = false
    var tableDataSource = CartDataSource(){
        didSet{
            //            tableView.delegate = tableDataSource
            //            tableView.dataSource = tableDataSource
        }
    }
    var deliveryData = Delivery()
    var promoCode:PromoCode?
    var cartProdcuts : [AnyObject]?
    var cartArray : [Cart]?
    var productArray = [Product]()
    var remarks : String?
    var selectedAddress: Address?
    var orderSummary:OrderSummary?
    var selectedDeliverySpeed : DeliverySpeedType = .Standard
    var parameters = Dictionary<String,Any>()
    var selectedPaymentMethod: PaymentMode? {
        didSet {
            //self.paymentType_label.text = selectedPaymentMethod?.displayName ?? "Choose Payment".localized()
        }
    }
    var isOptionSelected = false
    var pickupType = [Int]()
    var deliveryType:Int = 0 // 0- delivery, 1- pickup
    var fromCartView = false
    var refreshPriceData = false
    var referralApplied = false
    var tipItems : [Int]?
    var arrPrescription: [String]?
    var txtPrescription: String?
    var selectedAgentId: CblUser?
    var orderDetails = [TableData]()
    var outstandingData = [outstandingProduct]()
    var paidData = [paidProduct]()
    var name = [String]()
    var order_reference = String()
    var total = Double()
    var tableTotal = Double()
    var payNow = String()
    var orderId = String()
    
    var CardDetail = [CardDetails]()
    var refreshControl = UIRefreshControl()
    var selectedProduct = [Dictionary<String,AnyObject>]()
    var selectedPtoductsArr = [Int]()
    var userId = Int()
    var arrayOfPrice = [String]()
    var sum = Double()
    var isOrderHistory : Bool? = false
    var sessionId = Int()
    //var product = [orderdetail]()
    
    var outstandingProductData = [OutstandingProductData]()
    var paidProductData = [OutstandingProductData]()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: UIControl.Event.valueChanged)
        self.scroll.isScrollEnabled = true
        self.scroll.alwaysBounceVertical = true
        scroll.addSubview(refreshControl)
        if isOrderHistory == false {
            self.orderHeaderLabel.text = "Pay for Selected Items"
            self.payNowStackHeight.constant = 40
        }else{
            self.orderHeaderLabel.text = "Table history summary"
            self.payNowStackHeight.constant = 0
        }
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        self.outstandingData.removeAll()
        if isOrderHistory == false {
            self.tableOrders()
        }else{
            self.orderDetail()
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        self.orderDetails.removeAll()
        self.outstandingData.removeAll()
        self.paidData.removeAll()
        self.productArray.removeAll()
        self.tableOrders()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    @IBAction func backBtnaction(_ sender: UIButton) {
        popVC()
    }
    
    @IBAction func payNowAction(_ sender: UIButton) {
        if self.selectedProduct.count == 0 {
            SKToast.makeToast("First add products")
        }else{
            self.hitPayment()
        }
    }
}

//MARK: - Add all products to cart webservice
extension SelectProductToPayViewController {
    
    func tableOrders(){
        var tableId = String()
        if let userData =  GDataSingleton.sharedInstance.loggedInUser{
            userId = Int(userData.id ?? "") ?? 0
        }
        if self.sessionId != 0 {
            tableId = "\(/self.sessionId)"
        }else{
            tableId = /GDataSingleton.sharedInstance.tableNumber
        }
        APIManager.sharedInstance.showLoader()
        let params = ["sessionTableInformationId" : tableId]
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/getTableOrderInfomation"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            self.refreshControl.endRefreshing()
            let status = response["message"].string
            self.tableNameLabel.text = "table Number"
            self.tableJoinedLabel.text = "Table Joined"
            self.totalAmountLabel.text = "Total Amount :- "
            if(status == "Success"){
                print(response)
                if let dict = response.dictionaryObject {
                    if let data = dict["data"] as? Dictionary<String,Any>{
                        if let userInfo = Mapper<TableData>().map(JSONObject: data){
                            self.orderDetails.append(userInfo)
                            if let outstandingProducts = userInfo.outstandingProduct {
                                for data in outstandingProducts{
                                    if let id = data.user_id {
                                        if self.userId == id {
                                            self.arrayOfPrice.append(/data.price)
                                            let doubles = self.arrayOfPrice.compactMap(Double.init)
                                            self.sum = doubles.reduce(0, +)
                                        }
                                    }
                                    self.totalamountLabel.text = "د.إ " + "\(self.sum)"
                                    self.outstandingData.append(data)
                                }
                                self.multipleOutstandingProducts()
                            }
                            if let paidProducts = userInfo.paidProduct {
                                for data in paidProducts{
                                    if let id = data.user_id {
                                        if self.userId == id {
                                            self.arrayOfPrice.append(/data.price)
                                            let doubles = self.arrayOfPrice.compactMap(Double.init)
                                            self.sum = doubles.reduce(0, +)
                                        }
                                    }
                                    self.totalamountLabel.text = "د.إ " + /self.orderDetails.first?.totalOrderNetAmount
                                    self.paidData.append(data)
                                }
                                self.multipulePaidProducts()
                            }
                        }
                    }
                }
                let text = NSMutableAttributedString()
                self.tableName.text = "Table Number " + /self.orderDetails.first?.table_name
                if let nameAray = self.orderDetails.first?.usersOnTable{
                    for i in 0..<nameAray.count {
                        if let name = nameAray[i].name{
                            if let color = nameAray[i].colorCode{
                                let nameColor : UIColor = UIColor(hexString: color) ?? UIColor()
                                if i == nameAray.count - 1 {
                                    text.append(NSAttributedString(string: " \(name)", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                }else{
                                    text.append(NSAttributedString(string: " \(name) ,", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                }
                            }
                        }
                    }
                }
                DispatchQueue.main.async{
                    self.getAmount(array: self.productArray)
                    if self.outstandingData.count > 0 {
                        //self.payNowButton.isHidden = false
                        //self.payNowHeight.constant = 0
                    }else{
                        //self.payNowButton.isHidden = true
                        //self.payNowHeight.constant = 50
                    }
                    self.calculateTotalPrice()
                }
                self.tableParticipentsLabel.attributedText = text
                self.tableViewHeightConstant.constant = 600
                self.tableView.reloadData()
            }else if status == "Record not found"{
                DispatchQueue.main.async{
                    self.getAmount(array: self.productArray)
                    self.calculateTotalPrice()
                    self.tableView.reloadData()
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
            }else if status_code == "The network connection was lost."{
            }
        }
    }
    
    func orderDetail(){
        var tableId = String()
        if let userData =  GDataSingleton.sharedInstance.loggedInUser{
            userId = Int(userData.id ?? "") ?? 0
        }
        if self.sessionId != 0 {
            tableId = "\(/self.sessionId)"
        }else{
            tableId = /GDataSingleton.sharedInstance.tableNumber
        }
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId) ?? 0
        APIManager.sharedInstance.showLoader()
        let params = ["sessionTableInformationId" : tableId,"history":true,"supplier_id" : supplierId] as [String : Any]
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/getTableOrderInfomation"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            self.refreshControl.endRefreshing()
            self.tableNameLabel.text = "table Number"
            self.tableJoinedLabel.text = "Table Joined"
            self.totalAmountLabel.text = "Total Amount :- "
            let status = response["message"].string
            if(status == "Success"){
                print(response)
                if let dict = response.dictionaryObject {
                    if let data = dict["data"] as? Dictionary<String,Any>{
                        if let userInfo = Mapper<TableData>().map(JSONObject: data){
                            self.orderDetails.append(userInfo)
                            if let outstandingProducts = userInfo.outstandingProduct {
                                for data in outstandingProducts{
                                    if let id = data.user_id {
                                        if self.userId == id {
                                            self.arrayOfPrice.append(/data.price)
                                            let doubles = self.arrayOfPrice.compactMap(Double.init)
                                            self.sum = doubles.reduce(0, +)
                                        }
                                    }
                                    self.totalamountLabel.text = "د.إ " + /self.orderDetails.first?.totalOrderNetAmount//"\(self.sum)"
                                    self.outstandingData.append(data)
                                }
                                self.multipleOutstandingProducts()
                            }
                            if let paidProducts = userInfo.paidProduct {
                                for data in paidProducts{
                                    if let id = data.user_id {
                                        if self.userId == id {
                                            self.arrayOfPrice.append(/data.price)
                                            let doubles = self.arrayOfPrice.compactMap(Double.init)
                                            self.sum = doubles.reduce(0, +)
                                        }
                                    }
                                    self.totalamountLabel.text = "د.إ " + /self.orderDetails.first?.totalOrderNetAmount
                                    self.paidData.append(data)
                                }
                                self.multipulePaidProducts()
                            }
                        }
                    }
                }
                let text = NSMutableAttributedString()
                self.tableName.text = "Table Number " + /self.orderDetails.first?.table_name
                if let nameAray = self.orderDetails.first?.usersOnTable{
                    for i in 0..<nameAray.count {
                        if let name = nameAray[i].name{
                            if let color = nameAray[i].colorCode{
                                let nameColor : UIColor = UIColor(hexString: color) ?? UIColor()
                                if i == nameAray.count - 1 {
                                    text.append(NSAttributedString(string: " \(name)", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                }else{
                                    text.append(NSAttributedString(string: " \(name) ,", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                }
                            }
                        }
                    }
                }
                DispatchQueue.main.async{
                    self.getAmount(array: self.productArray)
//                    if self.outstandingData.count > 0 {
//                        .//self.payNowButton.isHidden = false
//                        //self.payNowHeight.constant = 0
//                    }else{
//                        //self.payNowButton.isHidden = true
//                        //self.payNowHeight.constant = 50
//                    }
                    self.calculateTotalPrice()
                }
                self.tableParticipentsLabel.attributedText = text
                self.tableViewHeightConstant.constant = 600
                self.tableView.reloadData()
            }else if status == "Record not found"{
                DispatchQueue.main.async{
                    self.getAmount(array: self.productArray)
                    self.calculateTotalPrice()
                    self.tableView.reloadData()
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
            }else if status_code == "The network connection was lost."{
            }
        }
    }
    
    func multipleOutstandingProducts(){
        self.outstandingProductData.removeAll()
        // To show addOn Products,loops are for to calculate numbers of rows in section 0
        for i in 0 ..< self.outstandingData.count{
            var serialNUmber = 0
            
            var addOnFinalPrice = ""
            let productData = self.outstandingData[i]
            let price = Double(self.outstandingData[i].fixedPrice ?? "0")
            if /self.outstandingData[i].productDetailAddson?.count > 0{
                for j in 0 ..< /self.outstandingData[i].productDetailAddson?.count {
                    let addOnPrice = String(self.outstandingData[i].productDetailAddson?[j].price ?? 0.0)
                    // Filtering Addons on the basis of serial number and append it as a new order
                    if j == 0{
                        serialNUmber = self.outstandingData[i].productDetailAddson?[j].serial_number ?? 0
                        self.addOnCalculation(productData:productData,j:j,addOnPrice:addOnPrice)
                    }
                    if serialNUmber != self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1{
                        serialNUmber = self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1
                        self.addOnCalculation(productData:productData,j:j,addOnPrice:addOnPrice)
                        
                    }else if serialNUmber == self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1 && j != 0{
                        let arr1 = self.outstandingProductData.filter({$0.cart_id_product == self.outstandingData[i].cartProductId })
                        let arr = arr1.filter({$0.serial_number == self.outstandingData[i].productDetailAddson?[j].serial_number })
                       // let arr = self.outstandingProductData.filter({$0.serial_number == self.outstandingData[i].productDetailAddson?[j].serial_number })
                        //Calculationg add on price
                        var priceWithAddOn = 0.0
                        var addOnTypeName = productData.productDetailAddson?[j].adds_on_type_name
                        for k in 0 ..< /arr.count{
                            
                            priceWithAddOn = priceWithAddOn + /Double(arr[k].addOnPrice ?? "0")
                            addOnTypeName = "\(addOnTypeName ?? ""),  \(arr[k].adds_on_type_name ?? "")"//addOnTypeName ?? "" + "," + /arr[k].adds_on_type_name
                            
                        }
                        priceWithAddOn = priceWithAddOn + /self.outstandingData[i].productDetailAddson?[j].price
                        addOnFinalPrice = String(priceWithAddOn)
                        for (index, item) in self.outstandingProductData.enumerated() {
                            let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: addOnTypeName, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon: productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
                            if item.serial_number == self.outstandingData[i].productDetailAddson?[j].serial_number && item.cart_id_product == self.outstandingData[i].cartProductId{
                                self.outstandingProductData[index] = obj
                            }
                        }
                    }
                    
                    
                }
                
            }else{
                // Products without Addon
                let productData = self.outstandingData[i]
                let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cart_id, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id,userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id:nil, addonName: nil, cartId: nil, addOnQuantity: nil, addOnTypeId: nil, serialNumber: nil, addOnId: nil, addOnTypeName: nil, addOnPrice: nil, addOnIdIos: nil, addOnTypeQuantity: nil, isFreeAddon: false, FreeAddonQuantity: 0)
                self.outstandingProductData.append(obj)
            }
        }
    }
    
    func multipulePaidProducts(){
        self.paidProductData.removeAll()
        // To show addOn Products for Paid Orders,loops are for to calculate numbers of rows in section 0
        for i in 0 ..< self.paidData.count{
            var serialNUmber = 0
            
            var addOnFinalPrice = ""
            let productData = self.paidData[i]
            let price = Double(self.paidData[i].fixedPrice ?? "0")
            if /self.paidData[i].productDetailAddson?.count > 0{
                for j in 0 ..< /self.paidData[i].productDetailAddson?.count {
                    let addOnPrice = String(self.paidData[i].productDetailAddson?[j].price ?? 0.0)
                    if j == 0{
                        serialNUmber = self.paidData[i].productDetailAddson?[j].serial_number ?? 0
                        self.addOnCalculationForPaidData(productData:productData,j:j,addOnPrice:addOnPrice)
                    }
                    if serialNUmber != self.paidData[i].productDetailAddson?[j].serial_number ?? -1{
                        serialNUmber = self.paidData[i].productDetailAddson?[j].serial_number ?? -1
                        self.addOnCalculationForPaidData(productData:productData,j:j,addOnPrice:addOnPrice)
                        
                    }else if serialNUmber == self.paidData[i].productDetailAddson?[j].serial_number ?? -1 && j != 0{
                        let arr1 = self.paidProductData.filter({$0.cart_id_product == self.paidData[i].cartProductId })
                        let arr = arr1.filter({$0.serial_number == self.paidData[i].productDetailAddson?[j].serial_number })
                        //let arr = self.paidProductData.filter({$0.serial_number == self.paidData[i].productDetailAddson?[j].serial_number })
                        var priceWithAddOn = 0.0
                        var addOnTypeName = productData.productDetailAddson?[j].adds_on_type_name
                        for k in 0 ..< /arr.count{
                            
                            priceWithAddOn = priceWithAddOn + /Double(arr[k].addOnPrice ?? "0")
                            addOnTypeName = "\(addOnTypeName ?? ""),  \(arr[k].adds_on_type_name ?? "")"//addOnTypeName ?? "" + "," + /arr[k].adds_on_type_name
                            
                        }
                        priceWithAddOn = priceWithAddOn + /self.paidData[i].productDetailAddson?[j].price
                        addOnFinalPrice = String(priceWithAddOn)
                        for (index, item) in self.paidProductData.enumerated() {
                            let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: addOnTypeName, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon: productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
                            if item.serial_number == self.paidData[i].productDetailAddson?[j].serial_number && item.cart_id_product == self.paidData[i].cartProductId{
                                self.paidProductData[index] = obj
                            }
                        }
                    }
                    
                    
                }
                
            }else{
                let productData = self.paidData[i]
                let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id,userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id:nil, addonName: nil, cartId: nil, addOnQuantity: nil, addOnTypeId: nil, serialNumber: nil, addOnId: nil, addOnTypeName: nil, addOnPrice: nil, addOnIdIos: nil, addOnTypeQuantity: nil, isFreeAddon: false, FreeAddonQuantity: 0)
                self.paidProductData.append(obj)
            }
        }
    }
    
    func addOnCalculation(productData:outstandingProduct,j:Int,addOnPrice:String){
        var addOnFinalPrice = ""
        if addOnPrice  == ""{
            addOnFinalPrice = String(productData.productDetailAddson?[j].price ?? 0)
        }else{
            addOnFinalPrice = addOnPrice
        }
        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: productData.productDetailAddson?[j].adds_on_type_name, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon: productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
        self.outstandingProductData.append(obj)
    }
    
    func addOnCalculationForPaidData(productData:paidProduct,j:Int,addOnPrice:String){
        var addOnFinalPrice = ""
        if addOnPrice  == ""{
            addOnFinalPrice = String(productData.productDetailAddson?[j].price ?? 0)
        }else{
            addOnFinalPrice = addOnPrice
        }
        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: productData.productDetailAddson?[j].adds_on_type_name, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon: productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
        self.paidProductData.append(obj)
    }
}

//MARK::- TableView Delegate , dataSOurce
extension SelectProductToPayViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.isOrderHistory == true{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isOrderHistory == true{
            if section == 0 {
                return self.outstandingProductData.count//self.outstandingData.count
            }else {
                return self.paidProductData.count//self.paidData.count
            }
        }else{
            return self.outstandingProductData.count//self.outstandingData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
        if self.isOrderHistory == true{
            if indexPath.section == 1 {
                if self.paidProductData.count > indexPath.row{
                    let data = self.paidProductData[indexPath.row]
                    print(data)
                    if let name = data.name{
                        if let userName = data.userName{
                            cell.itemName?.text =  name +  "(\( userName))"
                            cell.itemName.textColor =  UIColor(hexString: data.colorCode ?? "")
                        }
                       /* if let price = data.price {
                            cell.itemPrice?.text = "د.إ " + "\(price)"
                        }
                        if let productDesc = data.product_desc {
                            cell.addOnLabel?.text = productDesc
                        }
                        if let quantity = data.quantity {
                            cell.quantityLAbel.text = "Quantity:- \(quantity)"
                        }*/
                        if let price = data.price {
                            cell.itemPrice?.text = "د.إ " + "\(price)"
                            
                            if let addonPrice = data.addOnPrice{
                                let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                                let addOnPrice = Double(addonPrice)
                                let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                                cell.itemPrice?.text = "د.إ " + "\(finalPrice)"
                            }
                        }
                        if let addOn = data.adds_on_type_name{
                            cell.addOnLabel?.text = "Add On: \(addOn)"
                        }
                        
                        if let quantity = data.quantity {
                            
                            if let addonQuantity = data.addOnQuantity {
                                cell.quantityLAbel.text = "Quantity:- \(addonQuantity)"
                            }else{
                                cell.quantityLAbel.text = "Quantity:- \(quantity)"
                                
                            }
                        }
                    }
                }
            }else{
                if self.outstandingProductData.count > indexPath.row{
                    let data = self.outstandingProductData[indexPath.row]
                    print(data)
                   // cell.viewHeight.constant = 1.5
                    if let name = data.name{
                        if let userName = data.userName{
                            cell.itemName?.text =  name +  "(\( userName))"
                            cell.itemName.textColor =  UIColor(hexString: data.colorCode ?? "")
                        }
                        if let price = data.price {
                            cell.itemPrice?.text = "د.إ " + "\(price)"
                        }
                       /* if let productDesc = data.product_desc {
                            cell.addOnLabel?.text = productDesc
                        }
                        if let quantity = data.quantity {
                            cell.quantityLAbel.text = "Quantity:- \(quantity)"
                        }*/
                        if let price = data.price {
                            cell.itemPrice?.text = "د.إ " + "\(price)"
                            
                            if let addonPrice = data.addOnPrice{
                                let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                                let addOnPrice = Double(addonPrice)
                                let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                                cell.itemPrice?.text = "د.إ " + "\(finalPrice)"
                            }
                        }
                        if let addOn = data.adds_on_type_name{
                            cell.addOnLabel?.text = "Add On: \(addOn)"
                        }
                        
                        if let quantity = data.quantity {
                            
                            if let addonQuantity = data.addOnQuantity {
                                cell.quantityLAbel.text = "Quantity:- \(addonQuantity)"
                            }else{
                                cell.quantityLAbel.text = "Quantity:- \(quantity)"
                                
                            }
                        }
                    }
                }else{
                  //  cell.viewHeight.constant = 0
                }
            }
        }else{
            if self.outstandingProductData.count > indexPath.row{
                let data = self.outstandingProductData[indexPath.row]
                print(data)
                if let name = data.name{
                    if let userName = data.userName{
                        cell.itemName?.text =  name +  "(\( userName))"
                        cell.itemName.textColor =  UIColor(hexString: data.colorCode ?? "")
                    }
                  /*  if let price = data.price {
                        cell.itemPrice?.text = "د.إ " + "\(price)"
                    }
                    if let productDesc = data.product_desc {
                        cell.addOnLabel?.text = productDesc
                    }
                    if let quantity = data.quantity {
                        cell.quantityLAbel.text = "Quantity:- \(quantity)"
                    }*/
                    if let price = data.price {
                        cell.itemPrice?.text = "د.إ " + "\(price)"
                        
                        if let addonPrice = data.addOnPrice{
                            let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                            let addOnPrice = Double(addonPrice)
                            let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                            cell.itemPrice?.text = "د.إ " + "\(finalPrice)"
                        }
                    }
                    if let addOn = data.adds_on_type_name{
                        cell.addOnLabel?.text = "Add On: \(addOn)"
                    }
                    
                    if let quantity = data.quantity {
                        
                        if let addonQuantity = data.addOnQuantity {
                            cell.quantityLAbel.text = "Quantity:- \(addonQuantity)"
                        }else{
                            cell.quantityLAbel.text = "Quantity:- \(quantity)"
                            
                        }
                    }
                    
                    if let freeProduct = data.isFree {
                        if freeProduct == true{
                            if let price = data.price {
                                let text = "د.إ " + "\(price)"
                                let freeQuantity = /data.freeQuantity
                                let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]

                                let partOne = NSMutableAttributedString(string: "Free(\(freeQuantity))", attributes: yourAttributes)
                                let partTwo =  NSMutableAttributedString(string: text)
                                //partTwo.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, partTwo.length))
                                let combination = NSMutableAttributedString()

                                combination.append(partOne)
                                combination.append(partTwo)
                                cell.itemPrice?.attributedText =   combination
                            }
                        }else{
                            if let price = data.price {
                                cell.itemPrice?.text = "د.إ " + "\(price)"
                            }
                        }
                    }
                }
                cell.accessoryType = .none
                if let userid = self.outstandingProductData[indexPath.row].user_id{
                    if userid != userId{
                        if self.selectedPtoductsArr.contains(self.outstandingProductData[indexPath.row].id ?? 0){
                            cell.accessoryType = .checkmark
                        }
                        else if self.selectedPtoductsArr.contains(self.outstandingProductData[indexPath.row].order_price_id ?? 0) {
                            cell.accessoryType = .checkmark
                        } else {
                            cell.accessoryType = .none
                        }
                    }
                }
            }
        }
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:0))
        view.backgroundColor = UIColor.clear
        switch section {
        case 0 :
            if self.isOrderHistory == false{
                if self.outstandingData.count > 0 {
                    let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                    let label = UILabel(frame: CGRect(x:10, y:5, width:tableView.frame.size.width, height:18))
                    label.font =  UIFont(name: Fonts.ProximaNova.Bold, size: 17)
                    view.addSubview(label)
                    label.text = "Outstanding Orders";
                    return view
                }
            }
        default:
            print("")
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isOrderHistory == false{
            if let userid = self.outstandingProductData[indexPath.row].user_id{
                if userid != userId{
                    if self.selectedPtoductsArr.contains(self.outstandingProductData[indexPath.row].id ?? 0){
                        if let selectedIndex = self.selectedPtoductsArr.firstIndex(where: {$0 == self.outstandingProductData[indexPath.row].id ?? 0}) {
                            self.selectedPtoductsArr.remove(at: selectedIndex)
                            self.selectedProduct.remove(at: selectedIndex)
                            if let addonPrice = self.outstandingProductData[indexPath.row].addOnPrice{
                                let fixedPrice = Double(self.outstandingProductData[indexPath.row].fixedPrice ?? "0.0") ?? 0.0
                                let addOnPrice = Double(addonPrice)
                                let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                                self.sum = self.sum - finalPrice
                            }
                            
                            self.totalamountLabel.text = "د.إ " + "\(String(format: "%.2f", self.sum))"
                        }
                    }else if self.selectedPtoductsArr.contains(self.outstandingProductData[indexPath.row].order_price_id ?? 0){
                        if let selectedIndex = self.selectedPtoductsArr.firstIndex(where: {$0 ==  self.outstandingProductData[indexPath.row].order_price_id ?? 0}) {
                            self.selectedPtoductsArr.remove(at: selectedIndex)
                            self.selectedProduct.remove(at: selectedIndex)
                            let amt = /Double(/self.outstandingProductData[indexPath.row].price)
                            self.sum = self.sum - amt
                            self.totalamountLabel.text = "د.إ " + "\(String(format: "%.2f", self.sum))"
                        }
                    }
                   /* if self.selectedPtoductsArr.contains(self.outstandingProductData[indexPath.row].order_price_id ?? 0){
                        if let selectedIndex = self.selectedPtoductsArr.firstIndex(where: {$0 ==  self.outstandingProductData[indexPath.row].order_price_id ?? 0}) {
                            self.selectedPtoductsArr.remove(at: selectedIndex)
                            self.selectedProduct.remove(at: selectedIndex)
                            let amt = /Double(/self.outstandingProductData[indexPath.row].price)
                            self.sum = self.sum - amt
                            self.totalamountLabel.text = "د.إ " + "\(self.sum)"
                        }
                    }*/else {
                        // TO Select
                        if let addOnId = self.outstandingProductData[indexPath.row].id{
                            selectedPtoductsArr.append(addOnId)
                        }else if let selectedOrderId = self.outstandingProductData[indexPath.row].order_price_id {
                            selectedPtoductsArr.append(selectedOrderId )
                        }
                        let data = self.outstandingProductData[indexPath.row]
                        let dict = ["cart_product_productId" : /data.cart_product_productId, "cart_id" : /data.cart_id, "product_id" : /data.product_id, "order_id" : /data.order_id, "user_id" : /data.user_id , "amount" : /data.price , "isFree" : /data.isFree] as [String : Any]
                        self.selectedProduct.append(dict as [String : AnyObject])
                        //
                        if let price = data.price {
                            
                            if let addonPrice = data.addOnPrice{
                                let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                                let addOnPrice = Double(addonPrice)
                                let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                                self.sum = self.sum + finalPrice
                            }else{
                                self.sum = self.sum + /Double(/price)
                            }
                        }
                        //
                       // let amt = /Double(/self.outstandingProductData[indexPath.row].price)
                       // self.sum = self.sum + amt
                        
                        self.totalamountLabel.text = "د.إ " + "\(String(format: "%.2f", self.sum))"
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func calculateTotalPrice(){
        if let sum = self.orderDetails.first?.totalOrderNetAmount{
            tableTotal = total + Double(sum)!
            self.totalPrice.text = "د.إ " + "\(tableTotal)"
        }
    }
}
//MARK: - Product Listing Gesture Recognizer
extension SelectProductToPayViewController {
    
    func getAmount(array : [Product]){
        var addOnCharge: Double = 0
        CartBillCell.getNewTotalPrice(promo: self.promoCode, addOnCharges: addOnCharge, cart: array) {
            [weak self] (totalPrice, deliveryCharges, discountOnTotal, handlingCharges, qtyTotal) in
            guard let self = self else { return }
            
            var discount = 0.0
            
            Cart.totalTax = String(handlingCharges)
            self.total = (totalPrice.rounded())//+ handlingCharges // deliveryCharges
            // Nitin, price replaced for ui
            //self.labelSubTotal?.text = totalPrice.addCurrencyLocale //  subTotal.addCurrencyLocale
            
            //self.labelNetTotal?.text = self.netTotal.rounded().addCurrencyLocale
        }
    }
    
    func hitPayment(){
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId) ?? 0
        let tableId = Int(/GDataSingleton.sharedInstance.tableNumber) ?? 0
        APIManager.sharedInstance.showLoader()
        let params = ["supplier_id" : supplierId , "order_id" : "" , "sessionTableInformationId" : tableId , "amount" : "" , "payMode" : "my_order_bill-addition_production" , "cartProductAndOrder" : self.selectedProduct , "totalUnpaidDiscount" : /self.orderDetails.first?.totalUnpaidDiscount, "userUnpaidDiscount" : /self.orderDetails.first?.userUnpaidDiscount ] as [String : Any]
        print(params)
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/payLaterWithPayMode"
        print(url)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            if let dict = response.dictionaryObject {
                print(dict)
                if let data = dict["data"] as? Dictionary<String,AnyObject>{
                    print(data)
                    //SKToast.makeToast("Payment made successfully")
                    // create the alert
                    let alert = UIAlertController(title: "", message: "Payment made successfully", preferredStyle: UIAlertController.Style.alert)
                    // add the actions (buttons)
                    let Ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        GDataSingleton.sharedInstance.PayOption = "paid"
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                        self.popViewControllerss(popViews: 2)
                    })
                    alert.addAction(Ok)
                    self.present(alert, animated: true, completion: nil)
                    GDataSingleton.sharedInstance.PayOption = "paid"
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                    self.popViewControllerss(popViews: 2)
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            //SKToast.makeToast("Can not join this table")
        }
    }
    
    func popViewControllerss(popViews: Int, animated: Bool = true) {
        if self.navigationController!.viewControllers.count > popViews
        {
            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
            self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
    
}

