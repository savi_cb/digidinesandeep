//
//  TableOrder.swift
//  Sneni
//
//  Created by osvin-k01 on 7/3/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation
import ObjectMapper

class TableData : Mappable {
    var outstandingProduct          : [outstandingProduct]?
    var paidProduct                 : [paidProduct]?
    var usersOnTable                : [usersOnTable]?
    var totalOrderNetAmount         : String?
    var table_name                  : String?
    var isOutstandingPayment        : Int?
    var myOrderAmount               : String?
    var onlyMyOustandingOrders      : Int?
    var finalPayAfterPromo          : String?
    var promoCodeAppliedCount       : Int?
    var promocodeDiscount           : String?
    var userUnpaidAmount            : String?
    var totalUnpaidAmount           : String?
    var totalUnpaidDiscount         : String?
    var userUnpaidDiscount          : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        outstandingProduct          <- map["outstandingProduct"]
        usersOnTable                <- map["usersOnTable"]
        paidProduct                 <- map["paidProduct"]
        totalOrderNetAmount         <- map["totalOrderNetAmount"]
        isOutstandingPayment        <- map["isOutstandingPayment"]
        table_name                  <- map["table_name"]
        myOrderAmount               <- map["myOrderAmount"]
        finalPayAfterPromo          <- map["finalPayAfterPromo"]
        promoCodeAppliedCount       <- map["promoCodeAppliedCount"]
        promocodeDiscount           <- map["promocodeDiscount"]
        onlyMyOustandingOrders      <- map["onlyMyOustandingOrders"]
        userUnpaidAmount            <- map["userUnpaidAmount"]
        totalUnpaidAmount           <- map["totalUnpaidAmount"]
        userUnpaidDiscount          <- map["userUnpaidDiscount"]
        totalUnpaidDiscount         <- map["totalUnpaidDiscount"]
    }
}

class outstandingProduct : Mappable {
    var firstname            : String?
    var product_desc         : String?
    var product_name         : String?
    var quantity             : Int?
    var order_id             : Int?
    var pres_description     : String?
    var price                : String?
    var cart_product_productId : Int?
    var colorCode  : String?
    var cart_id : Int?
    var product_id : Int?
    var user_id    : Int?
    var isFree : Bool?
    var freeQuantity : Int?
    var order_price_id : Int?
    var productDetailAddson : [ProductDetailAddon]?
    var fixedPrice : String?
    var cartProductId : Int?
    
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        firstname             <- map["firstname"]
        product_desc             <- map["product_desc"]
        product_name          <- map["product_name"]
        order_id              <- map["order_id"]
        quantity              <- map["quantity"]
        price                 <- map["price"]
        colorCode             <- map["colorCode"]
        cart_product_productId <- map["cart_product_productId"]
        cart_id                <- map["cart_id"]
        product_id             <- map["product_id"]
        user_id                <- map["user_id"]
        pres_description       <- map["pres_description"]
        isFree                 <- map["isFree"]
        freeQuantity           <- map["freeQuantity"]
        order_price_id         <- map["order_price_id"]
        productDetailAddson    <- map["adds_on"]
        fixedPrice             <- map["fixed_price"]
        cartProductId          <- map["cartProductId"]
    }
}

class ProductDetailAddon: Mappable  {
    
    var id : Int?
    var adds_on_name : String?
    var cart_id : String?
    var quantity : Int?
    var adds_on_type_jd : String?
    var serial_number : Int?
    var adds_on_id : Int?
    var adds_on_type_name : String?
    var price : Double?
    var add_on_id_ios : String?
    var adds_on_type_quantity : String?
    var isFreeAddon : Bool?
    var FreeAddonQuantity : Int?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map){
        id                 <- map["id"]
        adds_on_name             <- map["adds_on_name"]
        cart_id                <- map["cart_id"]
        quantity                <- map["quantity"]
        adds_on_type_jd             <- map["adds_on_type_jd"]
        serial_number             <- map["serial_number"]
        adds_on_id                <- map["adds_on_id"]
        adds_on_type_name             <- map["adds_on_type_name"]
        price                    <- map["price"]
        add_on_id_ios             <- map["add_on_id_ios"]
        adds_on_type_quantity             <- map["adds_on_type_quantity"]
        isFreeAddon               <- map["isFree"]
        FreeAddonQuantity        <- map["freeQuantity"]
    }
}

class paidProduct : Mappable {
    var firstname            : String?
    var product_desc         : String?
    var product_name         : String?
    var quantity             : Int?
    var order_id             : Int?
    var price                : String?
    var pres_description     : String?
    var colorCode  : String?
    var user_id    : Int?
    var isFree : Bool?
    var freeQuantity : Int?
    var productDetailAddson : [ProductDetailAddon]?
    var fixedPrice : String?
    var cart_id : Int?
    var product_id : Int?
    var order_price_id : Int?
    var cartProductId : Int?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        firstname             <- map["firstname"]
        product_desc             <- map["product_desc"]
        product_name          <- map["product_name"]
        order_id              <- map["order_id"]
        quantity              <- map["quantity"]
        price                 <- map["price"]
        colorCode    <- map["colorCode"]
        pres_description       <- map["pres_description"]
        user_id                <- map["user_id"]
        isFree                 <- map["isFree"]
        freeQuantity           <- map["freeQuantity"]
        productDetailAddson    <- map["adds_on"]
        fixedPrice             <- map["fixed_price"]
        cart_id                <- map["cart_id"]
        product_id             <- map["product_id"]
        order_price_id         <- map["order_price_id"]
        cartProductId          <- map["cartProductId"]
    }
}


class orderdetail : Mappable {
    var product          : [product]?
    var net_amount       : Int?
    var order_price      : Int?
    var firstname        : String?
    var payment_status     : Int?
    var isPayNow           : Int?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        product           <- map["product"]
        net_amount        <- map["net_amount"]
        order_price       <- map["order_price"]
        firstname         <- map["firstname"]
        payment_status    <- map["payment_status"]
        isPayNow          <- map["isPayNow"]
    }
}

class product : Mappable {
    var product_desc         : String?
    var product_name            : String?
    var fixed_price            : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        product_desc           <- map["product_desc"]
        product_name            <- map["product_name"]
        fixed_price            <- map["fixed_price"]
    }
}

class usersOnTable : Mappable {
    var id         : String?
    var name       : String?
    var colorCode  : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        id           <- map["id"]
        name         <- map["name"]
        colorCode    <- map["colorCode"]
    }
}


class CardDetails : Mappable {
    var expiry         : String?
    var maskedPan       : String?
    var cardholderName  : String?
    var scheme          : String?
    var id              : Int?
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        expiry           <- map["expiry"]
        maskedPan         <- map["maskedPan"]
        scheme           <- map["scheme"]
        id         <- map["id"]
        cardholderName           <- map["cardholderName"]
    }
}


class currentSession : Mappable {
    var id                  : Int?
    var order_id            : Int?
    var payment_status      : Int?
    var status              : Int?
    var supplier_id         : Int?
    var user_id             : Int?
    var supplier_name       : String?
    var table_name          : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        id                  <- map["id"]
        order_id            <- map["order_id"]
        payment_status      <- map["payment_status"]
        status              <- map["status"]
        supplier_id         <- map["supplier_id"]
        user_id             <- map["user_id"]
        supplier_name       <- map["supplier_name"]
        table_name          <- map["table_name"]
    }
}
