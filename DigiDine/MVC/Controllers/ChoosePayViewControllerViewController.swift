//
//  ChoosePayViewControllerViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 7/10/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class ChoosePayViewControllerViewController: UIViewController {
  
  @IBOutlet weak var pyNowBtn: UIButton!
  @IBOutlet weak var payLatterBtn: UIButton!
  
  var amount = Double()
  var cardId = Int()
  var paymentBool = Bool()
  var comingWithNewCard : String? = "false"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  @IBAction func actionBack(sender: AnyObject) {
    popVC()
  }
  
  @IBAction func payNowAction(_ sender: Any) {
   //self.pyNowBtn.backgroundColor = UIColor.appRed
    //self.payLatterBtn.backgroundColor = UIColor.white
    GDataSingleton.sharedInstance.PayOption = "paid"
    if paymentBool == true{
      self.getPaymantLink(paynow: true)
    }else{
       self.doPayment(paynow: true)
    }
  }
  
  @IBAction func payLatterAction(_ sender: UIButton) {
    //self.payLatterBtn.backgroundColor = UIColor.appRed
    //self.pyNowBtn.backgroundColor = UIColor.white
    GDataSingleton.sharedInstance.PayOption = "paylatter"
    if paymentBool == true{
      self.getPaymantLink(paynow: false)
    }else{
      self.doPayment(paynow: false)
    }
  }
  
  func getPaymantLink(paynow : Bool) {
    APIManager.sharedInstance.showLoader(message: "Processing")
    let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
    let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
    
    let params = FormatAPIParameters.confirmPayment(table_id: tableId, net_amount: amount, supplier_id: supplierId, paynow : paynow, userSavedCardId : self.cardId).formatParameters()
    
    APIManager.sharedInstance.opertationWithRequest(withApi: API.getPaymentLink(params)) {
      [weak self] (response) in
      guard let self = self else { return }
      
      switch response {
      case .Success(let object):
        print(object)
        APIManager.sharedInstance.hideLoader()
        if let obj = object as? [String:Any]{
          if let paymentLink = obj["payment_href"]{
            print(paymentLink)
            if let orderRef = obj["order_reference"]{
              print(orderRef)
              GDataSingleton.sharedInstance.OrderRef = "\(orderRef)"
              let payment = ["payNow" : paynow]
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderNotifi"), object: nil, userInfo: nil)
              self.popViewControllerss(popViews: 2)
            }
          }
        }
      case .Failure(let error):
        APIManager.sharedInstance.hideLoader()
        print(error.message ?? "")
        break
      }
    }
  }
  
  func doPayment(paynow : Bool){
    APIManager.sharedInstance.showLoader()
    let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
    let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
    
    let params = FormatAPIParameters.getPaymentLink(table_id: tableId, net_amount: amount, supplier_id: supplierId, paynow: paynow).formatParameters()
    
    APIManager.sharedInstance.opertationWithRequest(withApi: API.getPaymentLink(params)) {
      [weak self] (response) in
      guard let self = self else { return }
      
      switch response {
      case .Success(let object):
        print(object)
        APIManager.sharedInstance.hideLoader()
        if let obj = object as? [String:Any]{
          if let paymentLink = obj["payment_href"]{
            print(paymentLink)
            if let orderRef = obj["order_reference"]{
              print(orderRef)
              let VC = PaymentGateViewController.getVC(.digiHome)
              VC.paymentLink = "\(paymentLink)"
              VC.payType = paynow
              VC.comingWithNewCard = self.comingWithNewCard
              GDataSingleton.sharedInstance.OrderRef = "\(orderRef)"
              self.pushVC(VC)
            }
          }
        }
      case .Failure(let error):
        APIManager.sharedInstance.hideLoader()
        print(error.message ?? "")
        break
      }
    }
  }
    
  func popViewControllerss(popViews: Int, animated: Bool = true) {
      if self.navigationController!.viewControllers.count > popViews
      {
          let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
           self.navigationController?.popToViewController(vc, animated: animated)
      }
  }
}
