//
//  ProductTableViewCell.swift
//  Sneni
//
//  Created by osvin-k01 on 5/21/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit


class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDiscription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var customised: UILabel! {
        didSet{
            customised.textColor = SKAppType.type.color
        }
    }
    
    @IBOutlet weak var labelOutOfStock: UILabel! {
        didSet{
            labelOutOfStock.backgroundColor = SKAppType.type.alphaColor
            if let headerColor = AppSettings.shared.appThemeData?.header_text_color {
                let header = UIColor(hexString: headerColor)
                labelOutOfStock.textColor = header ?? .darkGray
            }
        }
    }
    
    @IBOutlet weak var productImageWidth: NSLayoutConstraint!{
        didSet{
            productImageWidth.constant = 0
        }
    }
    
    @IBOutlet var stepper: GMStepper? {
        didSet {
            stepper?.willHideRemoveCart = true
            stepper?.isCartView = false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK:- Variables
    var selectedIndex: Int?
    var completionBlock : AnyCompletionBlock?
    var addonsCompletionBlock: NewListner?
    var supplierData : Supplier?
    var fromCartView = false
    var index : Int?
    var comingFromcustomization : Bool = false
    var product : Product? {
        didSet {
            //                if SKAppType.type == .food {
            //    //                viewSingleStarRating.isHidden = false
            //                    viewStarRating.isHidden = true
            //                    if let rating = product?.averageRating {
            //                       let doubleRating = rating.toDouble
            //                       self.rating_label.text = String(doubleRating)
            //                   } else if let rating = product?.averageRating {
            //                       let doubleRating = rating.toDouble
            //                       self.rating_label.text = String(doubleRating)
            //                   }
            //                }
            //                else {
            //                    viewSingleStarRating.isHidden = true
            //                    viewStarRating.isHidden = false
            //                    if let rating = product?.averageRating {
            //                        let doubleRating = rating.toDouble
            //                        viewStarRating.rating = doubleRating
            //                    } else if let rating = product?.averageRating {
            //                        let doubleRating = rating.toDouble
            //                        viewStarRating.rating = doubleRating
            //                    }
            //                }
            
            if fromCartView {
                var addons = ""
                for array in product?.arrayAddonValue ?? [] {
                    for addon in array {
                        if let addonName = addon.type_name {
                            addons = addons + "," + addonName
                        }
                    }
                }
                if addons.first == "," {
                    addons.removeFirst()
                }
                if addons.last == "," {
                    addons.removeLast()
                }
                
                //////////////
                if SKAppType.type.isFood {
                    let arr = addons.components(separatedBy: ",")
                    var aryDict = [[String:String]]()
                    if arr.count > 0 {
                        for addon in arr {
                            let tok =  addons.components(separatedBy:addon)
                            let dict = ["name":addon, "count":"\(tok.count-1)"] as [String : String]
                            aryDict.append(dict)
                        }
                    }
                    aryDict.removeDuplicates()
                    
                    var addonStr = ""
                    for addon in aryDict {
                        if let addonName = addon["name"] {
                            addonStr = addonStr + "," + addonName + "(\(addon["count"] ?? "1"))"
                        }
                    }
                    if addonStr.first == "," {
                        addonStr.removeFirst()
                    }
                    if addonStr.last == "," {
                        addonStr.removeLast()
                    }
                    self.customised.text = "Extras: " + addonStr
                } else {
                    self.customised.text = "Extras: " + addons
                }
                //////////////
                self.customised.isHidden = addons.count == 0 ? true : false
                //                self.customised.text = "Extras: " + addons
                
            } else {
               // self.customised.text = "Customizable".localized()
                self.customised.isHidden = product?.adds_on?.count ?? 0 == 0 ? true : false
            }
            
            productTitle?.text = product?.name
            //lblSupplierName?.text = product?.supplierName
            //lblSupplierName?.isHidden = AppSettings.shared.isSingleVendor
            productPrice?.text = "\(/Double(/product?.price)?.addCurrencyLocale)"
            //productDiscription.text =  product?.desc
            if product?.desc != nil {
                productDiscription.attributedText = (product?.desc ?? "").htmlToAttributedString(label: self.productDiscription)
            }
            imgProduct.sd_setImage(with: URL(string: /product?.image), placeholderImage: #imageLiteral(resourceName: "placeholder_image"),completed: nil)
            //imgProduct?.loadImage(thumbnail: product?.image, original: nil)
            if product?.image != "" {
                productImageWidth.constant = 70
            }
            self.updatePrice()
            stepper?.associatedProduct = product
            stepper?.fromCartView = self.fromCartView
            
            stepper?.addonStepperListner = { [weak self] (value) in
                guard let self = self else { return }
                guard let block = self.addonsCompletionBlock else {return}
                
                if let data = value as? (Product,Bool,Double) { // for opening customization view
                    block(data)
                } else if let data = value as? (Product,Cart,Bool,Double) { // for opening check customization view
                    block(data)
                } else if let data = value as? Double { // for simply showing bottom bar without customization
                    self.stepper?.value = data
                    self.updatePrice()
                    if self.fromCartView {
                        block((data,self.product))
                    }
                } else if let data = value as? (Product,Cart,Bool,Double,Int) { // only for cartview controller
                    block(data)
                }
            }
            
            stepper?.stepperValueListener = {[weak self] (value) in
                guard let self = self else { return }
                guard let block = self.completionBlock else {return}
                
                if let data = value {
                    self.stepper?.value = data
                    self.updatePrice()
                    block(data as AnyObject)
                }
            }
            stepper?.value = 0.0
            
            if fromCartView {
                if let quant = product?.perAddonQuantity {
                    print(quant)
                    self.stepper?.value = quant.toDouble
                    
                } else {
                    DBManager.sharedManager.getProductToCart(productId: product?.id) {
                        [weak self] (products) in
                        guard let self = self else { return }
                        
                        let currentProduct = products.first
                        self.stepper?.value = currentProduct?.quantity?.toDouble() ?? 0.0
                    }
                }
            } else {
                DBManager.sharedManager.getProductToCart(productId: product?.id) {
                    [weak self] (products) in
                    guard let self = self else { return }
                    
                    let currentProduct = products.first
                    self.stepper?.value = currentProduct?.quantity?.toDouble() ?? 0.0
                    
                }
            }
            
            //labelOfferPrice?.isHidden = true
            //                if let isOffer = product?.isOffer, isOffer {
            //
            //                    let offerPrice = (/product?.actualPrice).addCurrencyLocale
            //                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: offerPrice)
            //                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            //
            //                    labelOfferPrice?.attributedText = attributeString
            //                    labelOfferPrice?.isHidden = false
            //                }
            
            //                for view in stackDetails.arrangedSubviews {
            //                    if view is VariantView {
            //                        view.removeFromSuperview()
            //                    }
            //                }
            
            //                for variant in product?.selectedVariants ?? [] {
            //                    let variantView = VariantView(frame: CGRect(x: 0, y: 0, w: stackDetails.size.width, h: 16))
            //                    variantView.configureVariant(variant)
            //                    stackDetails.addArrangedSubview(variantView)
            //                }
        }
    }
    
    private func updatePrice() {
        if comingFromcustomization == false{
            product?.getCartPriceLabel(block: {
                [weak self] (value) in
                guard let self = self else { return }
                if value != "د.إ 0.00"{
                    self.productPrice.text = value
                }
            })
        }
    }
    
}
