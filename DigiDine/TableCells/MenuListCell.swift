//
//  MenuListCell.swift
//  Sneni
//
//  Created by neelam panwar on 24/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {


    @IBOutlet weak var quantityLAbel: UILabel!
    @IBOutlet weak var addOnLabel: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var product : Product?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func updatePrice() {
         product?.getCartPriceLabel(block: {
             [weak self] (value) in
             guard let self = self else { return }
             self.itemPrice.text = value
         })
     }

}
