//
//  ProfileUserCell.swift
//  Sneni
//
//  Created by neelam panwar on 24/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class ProfileUserCell: UITableViewCell {

    //MARK::- IBOUTLETS
    @IBOutlet weak var profileUserImgView: UIImageView?
    @IBOutlet weak var labelUserName: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let userData =  GDataSingleton.sharedInstance.loggedInUser
        labelUserName?.text = userData?.firstName
        profileUserImgView?.loadImage(thumbnail: userData?.userImage, original: nil, placeHolder: UIImage(asset: Asset.User_placeholder))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
