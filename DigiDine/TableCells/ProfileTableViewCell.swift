//
//  ProfileTableViewCell.swift
//  
//
//  Created by neelam panwar on 24/04/20.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    //MARK::- IBOUTLETS
    @IBOutlet weak var labelTitle: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
