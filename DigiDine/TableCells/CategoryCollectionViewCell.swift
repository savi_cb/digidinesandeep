//
//  CategoryCollectionViewCell.swift
//  Sneni
//
//  Created by osvin-k01 on 5/20/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
}
