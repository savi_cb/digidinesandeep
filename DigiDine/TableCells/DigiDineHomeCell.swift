//
//  DigiDineHomeCell.swift
//  Sneni
//
//  Created by neelam panwar on 23/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit



class DigiDineHomeCell: UITableViewCell {

    //MARK::- IBOUTLETS
    @IBOutlet weak var viewStatus: UIView?
    @IBOutlet weak var labelStatus: UILabel?
    @IBOutlet weak var labelResturantName: UILabel?
    @IBOutlet weak var resturantImgView: UIImageView?
    @IBOutlet weak var resturantAdress: UILabel!
    @IBOutlet weak var resturantDistance: UILabel!
    @IBOutlet weak var resturantImageWidth: NSLayoutConstraint!{
        didSet{
            resturantImageWidth.constant = 0
        }
    }
    
    
}
