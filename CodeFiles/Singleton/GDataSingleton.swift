//
//  GDataSingleton.swift
//  Real Estate
//
//  Created by Gagan on 08/04/15.
//  Copyright (c) 2015 Gagan. All rights reserved.
//

import Foundation
import ObjectMapper

enum SingletonKeys : String {
    case Home = "ClikatHome"
    case User = "ClikatUser"
    case SupplierId = "ClikatSupplierId"
    case supplier = "ClikatSupplier"
    case PickupDate = "ClikatPickupDate"
    case PickupAddress = "ClikatPickupAddress"
    case PickupAddressId = "ClikatPickupAddressID"
    case DeviceToken = "ClikatDeviceToken"
    case ShowPopUp = "ClikatShowPopUp"
    case categoryId = "ClikatCategoryId"
    case settingApp = "SettingApp"
    case autoSearched = "autoSearched"
    case agentDbKey = "agentDbKey"
    case onBoardingDone = "onBoardingDone"
    case deliveryType = "DeliveryType"
    //Nitin
    case fcmToken = "fcmToken"
    case askLocationDone = "askLocationDone"
    case isFacebookLogin = "isFacebookLogin"
//    case element_color
//    case theme_color
//    case font_family
//    case header_color
//    case header_text_color
//    case logo_background
//    case terminology
//    case terminologyEnglish
//    case terminologyOther
    case profilePicDone
    case fromCart
    case agentUniqueId
    case termsAndConditions
    case tipAmount
    
    case customerPaymentId
    case LoginType = "loginType"
}

class GDataSingleton {
    
    static let sharedInstance = GDataSingleton()
    
    static var isOnBoardingDone: Bool {
        get {
            return /(UserDefaults.standard.value(forKey: SingletonKeys.onBoardingDone.rawValue) as? Bool)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: SingletonKeys.onBoardingDone.rawValue)
            defaults.synchronize()
        }
    }
    
    static var isAskLocationDone: Bool {
        get {
            return /(UserDefaults.standard.value(forKey: SingletonKeys.askLocationDone.rawValue) as? Bool)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: SingletonKeys.askLocationDone.rawValue)
            defaults.synchronize()
        }
    }
    
    static var isProfilePicDone: Bool {
        get {
            return /(UserDefaults.standard.value(forKey: SingletonKeys.profilePicDone.rawValue) as? Bool)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: SingletonKeys.profilePicDone.rawValue)
            defaults.synchronize()
        }
    }

    var languageId : String {
        return "14" //homeData?.arrayLanguages?[Localize.currentLanguage() == Languages.Arabic ? 1 : 0].id
    }

    var languageEnId : String {
        return homeData?.arrayLanguages?[0].id ?? ""
    }
    
    var languageArId : String {
        return homeData?.arrayLanguages?[1].id ?? ""
    }
    
    static var agentUniqueId: String {
        get {
            return /(UserDefaults.standard.value(forKey: SingletonKeys.agentUniqueId.rawValue) as? String)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: SingletonKeys.agentUniqueId.rawValue)
            defaults.synchronize()
        }
    }

    var appSettingsData : AppSettings? {
        get{
            if AppSettings.shared.appThemeData != nil {
                return AppSettings.shared
            }
            var appSettings : AppSettings?
            if let userString = UserDefaults.standard.object(forKey: SingletonKeys.settingApp.rawValue) as? String {
                let userJson = Mapper<AppSettings>.parseJSONStringIntoDictionary(JSONString: userString)
                appSettings = Mapper<AppSettings>().map(JSON: userJson ?? [:])
            }
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.settingApp.rawValue) as? AppSettings{
//                appSettings = data
//            }
            return appSettings
        }
        set{
            AppSettings.shared = newValue ?? AppSettings()
            let defaults = UserDefaults.standard
            if let value = newValue{
                let userString = /(value.toJSONString())
                UserDefaults.standard.set(userString, forKey: SingletonKeys.settingApp.rawValue)
                //defaults.rm_setCustomObject(value, forKey: SingletonKeys.settingApp.rawValue)
            }
            else{
                defaults.removeObject(forKey: SingletonKeys.settingApp.rawValue)
            }
            defaults.synchronize()
        }
    }
//
//    var terminology : TerminologyModalClass? {
//        get {
//            var terms : TerminologyModalClass?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.terminology.rawValue) as? Data{
//                if let decodedItem = NSKeyedUnarchiver.unarchiveObject(with: data) as? TerminologyModalClass {
//                    terms = decodedItem
//                }
//            }
//            return terms
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                let encodedData = NSKeyedArchiver.archivedData(withRootObject: value)
//                defaults.rm_setCustomObject(encodedData, forKey: SingletonKeys.terminology.rawValue)
//
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.terminology.rawValue)
//            }
//            defaults.synchronize()
//        }
//
//    }
    
//    var terminologyEnglishStatus : Dictionary<String,Any>? {
//        get {
//            var terms : Dictionary<String,Any>?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.terminologyEnglish.rawValue) as? Dictionary<String,Any>{
//                terms = data
//            }
//            return terms
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.terminologyEnglish.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.terminologyEnglish.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
    
    var supplierAddress : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "supplierAddress") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "supplierAddress")
            } else{
                defaults.removeObject(forKey: "supplierAddress")
            }
            defaults.synchronize()
        }
    }
    
    var tableNumber : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "tableNumber") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "tableNumber")
            } else{
                defaults.removeObject(forKey: "tableNumber")
            }
            defaults.synchronize()
        }
    }
    
    var orderId : String? {
           get {
               var terms : String?
               if let data = UserDefaults.standard.rm_customObject(forKey: "orderId") as? String{
                   terms = data
               }
               return terms
           }
           set {
               let defaults = UserDefaults.standard
               if let value = newValue{
                   defaults.rm_setCustomObject(value, forKey: "orderId")
               } else{
                   defaults.removeObject(forKey: "orderId")
               }
               defaults.synchronize()
           }
       }
    
    var resturantName : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "resturantName") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "resturantName")
            } else{
                defaults.removeObject(forKey: "resturantName")
            }
            defaults.synchronize()
        }
    }
    
    var supplierId : String? {
           get {
               var terms : String?
               if let data = UserDefaults.standard.rm_customObject(forKey: "supplierId") as? String{
                   terms = data
               }
               return terms
           }
           set {
               let defaults = UserDefaults.standard
               if let value = newValue{
                   defaults.rm_setCustomObject(value, forKey: "supplierId")
               } else{
                   defaults.removeObject(forKey: "supplierId")
               }
               defaults.synchronize()
           }
       }
    
    var isVoucher : Bool? {
        get {
            var isVoucher : Bool?
            if let data = UserDefaults.standard.rm_customObject(forKey: "isVoucher") as? Bool{
                isVoucher = data
            }
            return isVoucher
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "isVoucher")
            } else{
                defaults.removeObject(forKey: "isVoucher")
            }
            defaults.synchronize()
        }
    }
    
    var isHotel : Bool? {
        get {
            var isVoucher : Bool?
            if let data = UserDefaults.standard.rm_customObject(forKey: "isHotel") as? Bool{
                isVoucher = data
            }
            return isVoucher
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "isHotel")
            } else{
                defaults.removeObject(forKey: "isHotel")
            }
            defaults.synchronize()
        }
    }
    
    var loginType : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "loginType") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "loginType")
            } else{
                defaults.removeObject(forKey: "loginType")
            }
            defaults.synchronize()
        }
    }
    
    var PaymentType : String? {
           get {
               var terms : String?
               if let data = UserDefaults.standard.rm_customObject(forKey: "PaymentType") as? String{
                   terms = data
               }
               return terms
           }
           set {
               let defaults = UserDefaults.standard
               if let value = newValue{
                   defaults.rm_setCustomObject(value, forKey: "PaymentType")
               } else{
                   defaults.removeObject(forKey: "PaymentType")
               }
               defaults.synchronize()
           }
       }
       

  var PayOption : String? {
         get {
             var terms : String?
             if let data = UserDefaults.standard.rm_customObject(forKey: "payOption") as? String{
                 terms = data
             }
             return terms
         }
         set {
             let defaults = UserDefaults.standard
             if let value = newValue{
                 defaults.rm_setCustomObject(value, forKey: "payOption")
             } else{
                 defaults.removeObject(forKey: "payOption")
             }
             defaults.synchronize()
         }
     }
    
    var OrderRef : String? {
           get {
               var terms : String?
               if let data = UserDefaults.standard.rm_customObject(forKey: "OrderRef") as? String{
                   terms = data
               }
               return terms
           }
           set {
               let defaults = UserDefaults.standard
               if let value = newValue{
                   defaults.rm_setCustomObject(value, forKey: "OrderRef")
               } else{
                   defaults.removeObject(forKey: "OrderRef")
               }
               defaults.synchronize()
           }
       }
    
    var hotelRoomNo : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "hotelRoomNo") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "hotelRoomNo")
            } else{
                defaults.removeObject(forKey: "hotelRoomNo")
            }
            defaults.synchronize()
        }
    }
    
    var hotelId : String? {
           get {
               var terms : String?
               if let data = UserDefaults.standard.rm_customObject(forKey: "hotelId") as? String{
                   terms = data
               }
               return terms
           }
           set {
               let defaults = UserDefaults.standard
               if let value = newValue{
                   defaults.rm_setCustomObject(value, forKey: "hotelId")
               } else{
                   defaults.removeObject(forKey: "hotelId")
               }
               defaults.synchronize()
           }
       }
  
  var userPaymentLinkId : Int? {
            get {
                var terms : Int?
                if let data = UserDefaults.standard.rm_customObject(forKey: "userPaymentLinkId") as? Int{
                    terms = data
                }
                return terms
            }
            set {
                let defaults = UserDefaults.standard
                if let value = newValue{
                    defaults.rm_setCustomObject(value, forKey: "userPaymentLinkId")
                } else{
                    defaults.removeObject(forKey: "userPaymentLinkId")
                }
                defaults.synchronize()
            }
        }
       
    
    var orderStatus : Int? {
               get {
                   var terms : Int?
                   if let data = UserDefaults.standard.rm_customObject(forKey: "status") as? Int{
                       terms = data
                   }
                   return terms
               }
               set {
                   let defaults = UserDefaults.standard
                   if let value = newValue{
                       defaults.rm_setCustomObject(value, forKey: "status")
                   } else{
                       defaults.removeObject(forKey: "status")
                   }
                   defaults.synchronize()
               }
           }

  var OrderId : String? {
            get {
                var terms : String?
                if let data = UserDefaults.standard.rm_customObject(forKey: "OrderId") as? String{
                    terms = data
                }
                return terms
            }
            set {
                let defaults = UserDefaults.standard
                if let value = newValue{
                    defaults.rm_setCustomObject(value, forKey: "OrderId")
                } else{
                    defaults.removeObject(forKey: "OrderId")
                }
                defaults.synchronize()
            }
        }
    
    var userServiceCharge : String? {
        get {
            var terms : String?
            if let data = UserDefaults.standard.rm_customObject(forKey: "userServiceCharge") as? String{
                terms = data
            }
            return terms
        }
        set {
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: "userServiceCharge")
            } else{
                defaults.removeObject(forKey: "userServiceCharge")
            }
            defaults.synchronize()
        }
    }
    
//    var terminologyOtherStatus : Dictionary<String,Any>? {
//        get {
//            var terms : Dictionary<String,Any>?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.terminologyOther.rawValue) as? Dictionary<String,Any>{
//                terms = data
//            }
//            return terms
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.terminologyOther.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.terminologyOther.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
    
//    var elementColor : String? {
//        get {
//            var tempColor : String?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.element_color.rawValue) as? String{
//                tempColor = data
//            }
//            return tempColor
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.element_color.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.element_color.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
//
//    var themeColor : String? {
//        get {
//            var tempColor : String?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.theme_color.rawValue) as? String{
//                tempColor = data
//            }
//            return tempColor
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.theme_color.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.theme_color.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
//
//    var headerColor : String? {
//        get {
//            var tempColor : String?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.header_color.rawValue) as? String{
//                tempColor = data
//            }
//            return tempColor
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.header_color.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.header_color.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
//
//    var headerTextColor : String? {
//        get {
//            var tempName : String?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.header_text_color.rawValue) as? String{
//                tempName = data
//            }
//            return tempName
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.header_text_color.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.header_text_color.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
//
//    var logoBackground : String? {
//        get {
//            var tempName : String?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.logo_background.rawValue) as? String{
//                tempName = data
//            }
//            return tempName
//        }
//        set {
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.logo_background.rawValue)
//            } else{
//                defaults.removeObject(forKey: SingletonKeys.logo_background.rawValue)
//            }
//            defaults.synchronize()
//        }
//    }
//
//    var fontName : String? {
//       get {
//           var tempName : String?
//           if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.font_family.rawValue) as? String{
//               tempName = data
//           }
//           return tempName
//       }
//       set {
//           let defaults = UserDefaults.standard
//           if let value = newValue{
//               defaults.rm_setCustomObject(value, forKey: SingletonKeys.font_family.rawValue)
//           } else{
//               defaults.removeObject(forKey: SingletonKeys.font_family.rawValue)
//           }
//           defaults.synchronize()
//       }
//
//    }
//
    var customerPaymentId : String{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.customerPaymentId.rawValue) as? String ?? ""
        }
         set{
                       UserDefaults.standard.set(newValue, forKey: SingletonKeys.customerPaymentId.rawValue)
               }
    }
    

     var tipAmount : Int{
            get{
                return UserDefaults.standard.object(forKey: SingletonKeys.tipAmount.rawValue) as? Int ?? 0
            }
             set{
    //                   if let value = newValue {
                           UserDefaults.standard.set(newValue, forKey: SingletonKeys.tipAmount.rawValue)
                   }
        }
    
    var arrayServiceType:[ServiceType]?{
        return (Localize.currentLanguage() == Languages.Arabic ? homeData?.arrayServiceTypesAR : homeData?.arrayServiceTypesEN)
    }
    
    var referalAmount: Float?
    var botResponce : BotResponce?
    var homeData : Home?
//    {
//        get{
//            var home : Home?
//            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.Home.rawValue) as? Home {
//                home = data
//            }
//            return home
//        }
//        set{
//            let defaults = UserDefaults.standard
//            if let value = newValue{
//                defaults.rm_setCustomObject(value, forKey: SingletonKeys.Home.rawValue)
//            }
//            else{
//                defaults.removeObject(forKey: SingletonKeys.Home.rawValue)
//            }
//        }
//    }
    
    //supplier
    var supplier : Supplier?
    
    //set when user search
    static var filteredProducts: [ProductList]?
    static var isSearching = false
    var homeProductList: [ProductList]? {
        get {
            !GDataSingleton.isSearching ?  GDataSingleton.sharedInstance.homeData?.arrProductsList : GDataSingleton.filteredProducts
        }
    }
    
  var selfPickup : Int? {
      get {
          var terms : Int?
          if let data = UserDefaults.standard.rm_customObject(forKey: "selfPickup") as? Int{
              terms = data
          }
          return terms
      }
      set {
          let defaults = UserDefaults.standard
          if let value = newValue{
              defaults.rm_setCustomObject(value, forKey: "selfPickup")
          } else{
              defaults.removeObject(forKey: "selfPickup")
          }
          defaults.synchronize()
      }
  }
  
    var loggedInUser : User?{
        get{
            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.User.rawValue) as? User
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.User.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.User.rawValue)
            }
        }
    }
    
    var VoucherListing : [VouchersByUser]?{
           get{
               return UserDefaults.standard.rm_customObject(forKey: "vouchers") as? [VouchersByUser]
           }
           set{
               if let value = newValue {
                   UserDefaults.standard.rm_setCustomObject(value, forKey: "vouchers")
               }else{
                   UserDefaults.standard.removeObject(forKey: "vouchers")
               }
           }
       }
    
    var isLoggedIn: Bool {
        guard let user = GDataSingleton.sharedInstance.loggedInUser, let otpVerified = user.otpVerified, otpVerified == "1" else {
                      return false
                  }
        return true
    }
    
//    var isFacebookLogin : Bool?{
//        get{
//            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.isFacebookLogin.rawValue) as? Bool
//        }
//        set{
//            if let value = newValue {
//                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.isFacebookLogin.rawValue)
//            }else{
//                UserDefaults.standard.removeObject(forKey: SingletonKeys.isFacebookLogin.rawValue)
//            }
//        }
//    }
    
    var currentSupplierId : String?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.SupplierId.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.SupplierId.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.SupplierId.rawValue)
            }
        }
    }
    
    var currentCategoryId : String?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.categoryId.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.categoryId.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.categoryId.rawValue)
            }
        }
    }
    
    
    var currentSupplier : Supplier?{
        get{
            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.supplier.rawValue) as? Supplier
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.supplier.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.supplier.rawValue)
            }
        }
    }
    
    var termsAndConditions : [TermsModel]?{
           get{
               return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.termsAndConditions.rawValue) as? [TermsModel]
           }
           set{
               if let value = newValue {
                   UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.termsAndConditions.rawValue)
               }else{
                   UserDefaults.standard.removeObject(forKey: SingletonKeys.termsAndConditions.rawValue)
               }
           }
       }
    
    //MARK: - Pickup Details
    var pickupDate : Date?{
        get{
            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.PickupDate.rawValue) as? Date
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.PickupDate.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.PickupDate.rawValue)
            }
        }
    }
    
    var pickupAddressId : String?{
        get{
            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.PickupAddressId.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.PickupAddressId.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.PickupAddressId.rawValue)
            }
        }
    }
    
    var pickupAddress : Address?{
        get{
            return UserDefaults.standard.rm_customObject(forKey: SingletonKeys.PickupAddress.rawValue) as? Address
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: SingletonKeys.PickupAddress.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.PickupAddress.rawValue)
            }
        }
    }
    
    //MARK: - Device Token
    
    var deviceToken : String?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.DeviceToken.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.DeviceToken.rawValue)
            }else {
                UserDefaults.standard.removeObject(forKey: SingletonKeys.DeviceToken.rawValue)
            }
        }
    }
    
    var fcmToken : String?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.fcmToken.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.fcmToken.rawValue)
            }else {
                UserDefaults.standard.removeObject(forKey: SingletonKeys.fcmToken.rawValue)
            }
        }
    }
    
    var pushDict : Any? {
        get{
            return UserDefaults.standard.rm_customObject(forKey: RemoteNotification) as Any
        }
        set{
            if let value = newValue {
                UserDefaults.standard.rm_setCustomObject(value, forKey: RemoteNotification)
            }else{
                UserDefaults.standard.removeObject(forKey: RemoteNotification)
            }
        }
    }
    
    //MARK: - Rate Order
    
    var showRatingPopUp : String?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.ShowPopUp.rawValue) as? String
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.ShowPopUp.rawValue)
            }else {
                UserDefaults.standard.removeObject(forKey: SingletonKeys.ShowPopUp.rawValue)
            }
        }
        
    }
    
    var autoSearchedArray : [String]?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.autoSearched.rawValue) as? [String]
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.autoSearched.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.autoSearched.rawValue)
            }
        }
    }
    
    var fromCart : Bool?{
        get{
            return UserDefaults.standard.object(forKey: SingletonKeys.fromCart.rawValue) as? Bool
        }
        set{
            if let value = newValue {
                UserDefaults.standard.set(value, forKey: SingletonKeys.fromCart.rawValue)
            }else{
                UserDefaults.standard.removeObject(forKey: SingletonKeys.fromCart.rawValue)
            }
        }
    }
    
    //AgentDbSecretKey
   var agentDBSecretKey : AgentDBSecretKey?{
        get{
            var agentDBSecretKey : AgentDBSecretKey?
            if let data = UserDefaults.standard.rm_customObject(forKey: SingletonKeys.agentDbKey.rawValue) as? AgentDBSecretKey{
                agentDBSecretKey = data
            }
            return agentDBSecretKey
        }
        set{
            let defaults = UserDefaults.standard
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: SingletonKeys.agentDbKey.rawValue)
            }
            else{
                defaults.removeObject(forKey: SingletonKeys.agentDbKey.rawValue)
            }
        }
    }

}

extension UserDefaults {

    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }

    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
