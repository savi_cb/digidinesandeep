//
//  OrderStatusCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderStatusCell: ThemeTableCell {

    @IBOutlet weak var labelItemsText: ThemeLabel!
    @IBOutlet weak var scrollViewState: UIScrollView!
    @IBOutlet weak var lblNotConfirmedTitle: ThemeLabel!
    @IBOutlet weak var lblPlacedTitle: UILabel!
    @IBOutlet weak var lblShippedTitle: UILabel!
    @IBOutlet weak var lblNearYouTitle: UILabel!
    @IBOutlet weak var lblStartTitle: UILabel!
    @IBOutlet weak var lblDeliveryTitle: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelItemsCount: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var OrderRateBtn: UIButton! {
        didSet {
            OrderRateBtn.setTitle(L10n.RateOrder.string, for: .normal)
        }
    }
    @IBOutlet var imgPlaced: UIImageView! {
        didSet {
            imgPlaced.tintColor = UIColor.red
        }
    }
    @IBOutlet weak var imgNotConfirmed: ThemeImgView!
    @IBOutlet var imgShipped: UIImageView!
    @IBOutlet var imgNearYou: UIImageView!
    @IBOutlet var imgStartOn: UIImageView!
    @IBOutlet var imgDelivered: UIImageView!
    @IBOutlet weak var labelNotConfirmed: ThemeLabel!
    @IBOutlet weak var labelDatePlaced: UILabel!
    @IBOutlet weak var labelDateShipped: UILabel!
    @IBOutlet weak var labelDateNearYou: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var labelDateDelivered: UILabel!
    @IBOutlet weak var viewStatus1: UIView!
    @IBOutlet weak var viewLine1: UIView!
    @IBOutlet var viewPS: UIView!
    @IBOutlet var viewSN: UIView!
    @IBOutlet var viewNS: UIView!
    @IBOutlet var viewSD: UIView!
    
    @IBOutlet weak var confirmedView: UIView!
    @IBOutlet weak var preparingView: UIView!
    @IBOutlet weak var readyToPickedView: UIView!
    @IBOutlet weak var confirmedDateLabel: UILabel!
    @IBOutlet weak var preparingDateLabel: UILabel!
    @IBOutlet weak var pickedUpDateLAbel: UILabel!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!{
        didSet{
            stackHeight.constant = 0
        }
    }
    @IBOutlet weak var confirmedStatus: UILabel!
    @IBOutlet weak var prepStatus: UILabel!
    @IBOutlet weak var readyStatus: UILabel!
    

    var orderDetails : OrderDetails?{
        didSet{
            //updateUI()
            //updaterOrder()
            updateStatusView()
        }
    }
    var cellType : OrderCellType = .OrderHistory
    var totalCount : Int?
    var tax: String = ""
    
    
    func updateStatusView(){
        guard let detail = orderDetails else{
                   return
               }
        
        if detail.status == .Confirmed {
            self.stackHeight.constant = 100
            self.confirmedStatus.text = "Confirmed Order"
            self.prepStatus.text = "Preparing Order"
            self.readyStatus.text = "Order Ready for Collection"
            self.confirmedView.backgroundColor = SKAppType.type.color
            self.preparingView.backgroundColor = SKAppType.type.color
            self.readyToPickedView.backgroundColor = UIColor.white
            self.confirmedDateLabel.text = detail.createdOn
            self.preparingDateLabel.text = detail.createdOn
        }else if detail.status == .Reached ||  detail.status == .Delivered{
            self.stackHeight.constant = 100
            self.confirmedStatus.text = "Confirmed Order"
            self.prepStatus.text = "Preparing Order"
            self.readyStatus.text = "Order Ready for Collection"
            self.confirmedView.backgroundColor = SKAppType.type.color
            self.preparingView.backgroundColor = SKAppType.type.color
            self.readyToPickedView.backgroundColor = SKAppType.type.color
            self.pickedUpDateLAbel.text = detail.deliveredOn
            self.confirmedDateLabel.text = detail.createdOn
            self.preparingDateLabel.text = detail.createdOn
        }else {
            self.pickedUpDateLAbel.text = ""
            self.confirmedDateLabel.text = ""
            self.preparingDateLabel.text = ""
            self.confirmedStatus.text = ""
            self.prepStatus.text = ""
            self.readyStatus.text = ""
            self.stackHeight.constant = 0
        }
    }

    private func updaterOrder(){
        
        guard let detail = orderDetails else{
            return
        }
        
        switch detail.status   {
         
        case .Delivered: OrderRateBtn.isHidden = false
            default: OrderRateBtn.isHidden = true
        }
        
    }

    private func updateUI(){

        let tipAmount = /orderDetails?.tipAgent?.toDouble()
        let discount = orderDetails?.discountAmount?.toDouble() ?? 0.0
        let taxDouble = self.tax.toDouble() ?? 0.0
        let newAmount = orderDetails?.netAmount?.toDouble() ?? 0.0
        let totalAmount =  newAmount-discount+tipAmount
        
        labelItemsText.text = orderDetails?.productCount == "1" ? "Item" : "Items"
        labelItemsCount?.text = String(totalCount ?? 0)
        if let detail = orderDetails {
            if SKAppType.type == .eCom || SKAppType.type == .home {
                if detail.status.isDone5st {
                    labelStatus?.text = SKAppType.type == .home ? L11n.Ended.string : L11n.delivered.string
                }
                else if detail.status.isDone4st {
                    labelStatus?.text = SKAppType.type == .home ? L11n.Started.string : L11n.outForDelivery.string
                }
                else if detail.status.isDone3st {
                    labelStatus?.text = SKAppType.type == .home ? L11n.Reached.string : L11n.shipped.string
                }
                else if detail.status.isDone2st {
                    labelStatus?.text = SKAppType.type == .home ? L11n.onTheWay.string : L11n.packed.string
                }
                else if detail.status.isDone1st {
                    labelStatus?.text = L11n.confirmed.string
                }
                else if detail.status.isDone0st {
                    labelStatus?.text = L11n.pending.string
                }
                else {
                    labelStatus?.text = orderDetails?.status.stringValue(deliveryType: detail.deliveryStatus ?? .delivery)
                }
            }
            else {
                labelStatus?.text = detail.status.stringValue(deliveryType: detail.deliveryStatus ?? .delivery)

            }
            
        }
        labelPrice?.text = totalAmount.addCurrencyLocale

        
        
        labelDatePlaced?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateShipped?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateNearYou?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateDelivered?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        lblStartDate?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4

        if orderDetails?.status == .CustomerCancel || orderDetails?.status == .Rejected {
            scrollViewState.isHidden = true
        }
        else {
            updateStatusBar()
        }
        
    }

    
    func updateStatusBar(){
        
        guard let detail = orderDetails else { return }
        let deliveryType = detail.deliveryStatus ?? .delivery
        
        scrollViewState.isHidden = false
        
        var arrayTitle = [lblPlacedTitle, lblShippedTitle, lblStartTitle]
//        if SKAppType.type == .food || SKAppType.type.isJNJ {
//            arrayTitle.remove(at: 2)
//        }
       
        
        var arrayDates = [labelDatePlaced, labelDateShipped, labelDateNearYou, lblStartDate]
//        if SKAppType.type == .food || SKAppType.type.isJNJ {
//            arrayDates.remove(at: 2)
//        }
        
        var arrayImgs = [imgPlaced, imgShipped, imgStartOn]
//        if SKAppType.type == .food || SKAppType.type.isJNJ {
//            arrayImgs.remove(at: 2)
//        }
        
        var arrayViewColor = [viewPS,viewSN,viewNS] //Nitin
                
        var arrayStrTitle: [String] = []
        var arrayStrDates: [String] = []
        var arrayStrImgs: [UIColor] = []
       // if SKAppType.type == .food || SKAppType.type.isJNJ {

        if DeliveryType.shared == .pickup {  // Nitin
                        arrayStrTitle = [
                            OrderDeliveryStatus.Pending.stringValue(deliveryType: deliveryType),
                             OrderDeliveryStatus.Confirmed.stringValue(deliveryType: deliveryType)
                            , OrderDeliveryStatus.inTheKitchan.stringValue(deliveryType: deliveryType)
                            , OrderDeliveryStatus.Reached.stringValue(deliveryType: deliveryType)
            //                , OrderDeliveryStatus.Shipped.stringValue()
                           // , L11n.nearby.string //Nitin Food new changes
                            , OrderDeliveryStatus.Delivered.stringValue(deliveryType: deliveryType)
                        ]
            
            arrayStrDates = [
               detail.status.getPendingDate(order: detail) ,
                   orderDetails?.confirmed_onLine ?? "\n"
               , orderDetails?.inProgressDate ?? "\n"
               , orderDetails?.shippedOnLine ?? "\n"
               //, orderDetails?.nearOnLine ?? "\n" //Nitin Food new changes
               , orderDetails?.deliveredOnLine ?? "\n"
           ]
        } else if SKAppType.type == .food || SKAppType.type.isJNJ {

            arrayStrTitle = [
                OrderDeliveryStatus.Pending.stringValue(deliveryType: deliveryType),
                 OrderDeliveryStatus.Confirmed.stringValue(deliveryType: deliveryType)
                , OrderDeliveryStatus.Reached.stringValue(deliveryType: deliveryType)
            ]
            
            
            
            arrayStrDates = [
                detail.status.getPendingDate(order: detail) ,
                    orderDetails?.confirmed_onLine ?? "\n"
                , orderDetails?.inProgressDate ?? "\n"
                , orderDetails?.shippedOnLine ?? "\n"
                //, orderDetails?.nearOnLine ?? "\n" //Nitin Food new changes
                , orderDetails?.deliveredOnLine ?? "\n"
            ]
        }
        arrayStrImgs = [
            detail.status.isDone1st ? SKAppType.type.color : SKAppType.type.alphaColor
            , detail.status.isDone2st ? SKAppType.type.color : SKAppType.type.alphaColor
            , detail.status.isDone4st ? SKAppType.type.color : SKAppType.type.alphaColor
        ]
        
        
//        if SKAppType.type == .food || SKAppType.type.isJNJ {
//            arrayStrImgs.remove(at: 2)
//        }
        arrayTitle.enumerated().forEach({ $1?.text = arrayStrTitle[$0] })
        if !detail.status.isDelivered {
            arrayStrDates.removeLast()
            arrayStrDates.append("")
        }
        arrayDates.enumerated().forEach({ $1?.text = arrayStrDates[$0] })
        //arrayImgs.enumerated().forEach({ $1?.image = UIImage(asset: arrayStrImgs[$0]) })
        arrayImgs.enumerated().forEach({$1?.backgroundColor = arrayStrImgs[$0]})
        arrayViewColor.enumerated().forEach({$1?.backgroundColor = arrayStrImgs[$0]})
    }
}


extension OrderStatusCell{
    
    @IBAction func orderRateClick(sender:UIButton){
        
        let vc =  self.superview?.superview?.next as? OrderDetailController
        let rateReviewVc = StoryboardScene.Order.instantiateRateReviews()
        rateReviewVc.orderDetails = orderDetails
        rateReviewVc.product = vc?.orderDetails?.product?.first
        vc?.pushVC(rateReviewVc)
        
    }
}
