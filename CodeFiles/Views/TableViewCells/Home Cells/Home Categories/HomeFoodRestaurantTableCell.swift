//
//  HomeFoodRestaurantTableCell.swift
//  Sneni
//
//  Created by Sandeep Kumar on 02/07/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class HomeRateTextView: UIView {
    //MARK:- ======== Outlets ========
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- ======== Variables ========
    var rating: Double? {
        didSet {
            let int = /rating?.toInt
            lblTitle.text = int == 0 ? "New".localized() : "\(/rating)"
            backgroundColor = getColor(rating: int)
        }
    }
}

class HomeFoodRestaurantTableCell: UITableViewCell {
    
    //MARK:- ======== Outlets ========
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnLiveTrack: UIButton!
    @IBOutlet weak var viewRating: HomeRateTextView!
    @IBOutlet weak var background_view: UIView!{
        didSet{
            //background_view.backgroundColor = SKAppType.type.alphaColor
        }
    }
    
    //MARK:- ======== Variables ========
    var blockSelect: ((_ supplier : Supplier?) -> ())?

    var objModel: Supplier? {
        didSet {
            
            lblTitle?.text = /objModel?.name
            lblSubTitle?.text = /objModel?.address
            imgPic.loadImage(thumbnail: objModel?.supplierImages?.first, original: nil)
            btnLiveTrack.isHidden = DeliveryType.shared == .pickup
            var array:[String] = []
            if (/objModel?.deliveryMaxTime?.toInt()) > 0 {
                array.append(/objModel?.deliveryMaxTime?.toInt()?.toStringHours)
            }
            if (/objModel?.minOrder?.toInt()) > 0 {
                array.append(/objModel?.minOrder?.toDouble()?.addCurrencyLocale)
            }
            lblPrice.text = array.joined(separator: " - ")
            lblPrice.isHidden = array.isEmpty
            viewRating.rating = /objModel?.rating?.toDouble()
        }
    }
    
    //MARK:- ======== LifeCycle ========
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgPic.isSkeletonable = true
        self.lblTitle.isSkeletonable = true
        self.lblSubTitle.isSkeletonable = true
        self.lblPrice.isSkeletonable = true
        self.btnLiveTrack.isSkeletonable = true
        self.btnLiveTrack.setTitle("Live Tracking".localized(), for: .normal)
        initalSetup()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(btnClick(_:)))
        addGestureRecognizer(tap)
    }
    
    //MARK:- ======== Actions ========
    @IBAction func btnClick(_ sender: Any) {
        guard let block = blockSelect else { return }
        block(objModel)
    }
    
    
    //MARK:- ======== Functions ========
    func initalSetup() {
        selectedBackgroundView = UIView()
    }
    
}
