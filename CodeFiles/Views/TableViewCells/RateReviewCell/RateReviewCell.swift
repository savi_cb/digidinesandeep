//
//  RateReviewCell.swift
//  Sneni
//
//  Created by Mac_Mini17 on 20/03/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit
import Cosmos
class RateReviewCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var txtView: UITextView!{
        didSet{
            txtView.text = "Write review here..."
            txtView.textColor = UIColor.lightGray
        }
    }
    
    var orderDetails : OrderDetails?
    var product:Product?{
        
        didSet{
            
            nameLabel?.text = product?.name
            supplierLabel.isHidden =  AppSettings.shared.isSingleVendor
            supplierLabel?.text = product?.supplierName

            itemImageView.loadImage(thumbnail: product?.images?.first, original: nil)
//            itemImageView?.yy_setImage(with: URL(string: image), options: [.setImageWithFadeAnimation])
            
           // rateView.rating = /(product?.avgRating?.toDouble())
        }
        

        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}


extension RateReviewCell{
    
    @IBAction func submitBtnClick(sender:UIButton){
        
        guard rateView.rating > 0 else {
            
            UtilityFunctions.showAlert(message: "Please select rating.")
            return
        }
        
        self.webServiceRateProduct()
       
       
        
    }
    
   
}

// MARK: - UITextViewDelegate
extension RateReviewCell:UITextViewDelegate{
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write review here..."
            textView.textColor = UIColor.lightGray
        }
    }
  
}

// MARK: - WebServiceProductVariantList
extension RateReviewCell{
    
    
    func webServiceRateProduct()  {
        APIManager.sharedInstance.opertationWithRequest(withApi: API.RateProduct(isOrder: orderDetails != nil, value:rateView.rating.toString, product_id: orderDetails != nil ? orderDetails?.orderId : product?.id, reviews: txtView.text, title:/(txtField.text))) { (response) in

//        APIManager.sharedInstance.opertationWithRequest(withApi: API.RateProduct(value:rateView.rating.toString, product_id: product?.id, reviews: txtView.text, title:/(txtField.text))) { (response) in
            
            switch response{
                
            case .Success(_):
                if self.orderDetails != nil {
                    let vc = self.superview?.superview?.next as? RateReviewsVC
                    vc?.delegate?.updateProductDetail()
                    vc?.popVC()

//                    vc?.popToRootVC()
//                vc?.sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateOrderHistoryViewController())
//                    vc?.sideMenuController()?.sideMenu?.toggleMenu()

                } else {
                    let vc = self.superview?.superview?.next as? RateReviewsVC
                    vc?.delegate?.updateProductDetail()
                    vc?.popVC()

                }
                break
                
               
                
            default :
                break
            }
            
           
        }
        

        
    }
    
    
    
}
