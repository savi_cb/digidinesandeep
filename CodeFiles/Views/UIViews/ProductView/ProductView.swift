//
//  ProductView.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ProductView: UIView {

    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelProductmodel: UILabel!
    @IBOutlet weak var labelProductCode: UILabel!
    @IBOutlet weak var labelPrice: UILabel!{
        didSet{
            labelPrice.textColor = SKAppType.type.color
        }
    }
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var stackDetails: UIStackView!
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        self.removeFromSuperview()
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: CellIdentifiers.ProductView , bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func configureProductView(product : ProductDetailData?){
        
        defer{
            labelTitle.text = product?.name
            labelProductmodel?.text = product?.measuringUnit
            labelQuantity?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.Quantity.string,product?.quantity])
            
            var name = ""
            var totalPrice:Double = 0.0
            var totalPriceDouble:Double = (/product?.price?.toDouble())
            
            for addonData in product?.productDetailAddson ?? [] {
                if let addonName = addonData.adds_on_type_name {
                    name = name + "," + addonName
                }
                if let price = addonData.price {
                    if let priceDouble = Double(price) {
                        totalPrice += priceDouble
                    }
                }
            }
            if name.first == "," {
                name.removeFirst()
            }
            if name.last == "," {
                name.removeLast()
            }
            labelPrice.text = (totalPrice + totalPriceDouble).addCurrencyLocale
            labelProductCode?.text = name.count == 0 ? "" : "Addons: " + name
            
            if SKAppType.type == .eCom {
                labelProductmodel.isHidden = AppSettings.shared.isSingleVendor
            }
            
            for view in stackDetails.arrangedSubviews {
               if view is VariantView {
                   view.removeFromSuperview()
               }
           }
           
           for variant in product?.variants ?? [] {
               let variantView = VariantView(frame: CGRect(x: 0, y: 0, w: stackDetails.size.width, h: 16))
               variantView.configureVariant(variant)
               stackDetails.addArrangedSubview(variantView)
           }
        
        }
        
        imageViewProduct.loadImage(thumbnail: product?.image, original: nil)
    }
}
