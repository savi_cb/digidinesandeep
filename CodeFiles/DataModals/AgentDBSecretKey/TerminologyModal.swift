//
//  TerminologyModal.swift
//  Sneni
//
//  Created by cbl41 on 1/23/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

enum TerminologyKeys: String {
    case product
    case products
    case supplier
    case suppliers
    case order
    case orders
    case agent
    case agents
    case brand
    case brands
    case category
    case categories
    case catalogue
    case status
    case popularRestaurantes
    case noOrderFound
}

class TerminologyModalClass: NSObject,Mappable,NSCoding {
    
    var other : TerminologyDataModal?
    var english : TerminologyDataModal?
    
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map) {
        other <- map ["other"]
        english <- map["english"]
    }

    @objc required init(coder aDecoder: NSCoder){
        other = aDecoder.decodeObject(forKey: "other") as? TerminologyDataModal
        english = aDecoder.decodeObject(forKey: "english") as? TerminologyDataModal

    }
    
    @objc func encode(with aCoder: NSCoder){
        if other != nil{
            aCoder.encode(other, forKey: "other")
        }
        if english != nil{
            aCoder.encode(english, forKey: "english")
        }
        
    }
    
    func localizedStatus(status: OrderDeliveryStatus, deliveryType: DeliveryType) -> String {
        let statusDict = Localize.currentLanguage() == Languages.English ? self.english?.status : self.other?.status
        if let str = statusDict?[status.rawValue] as? String, !str.isEmpty {
            return str
        }
        
        switch status {
        case .Pending:
            return SKAppType.type == .food ? L11n.placed.string : L11n.pending.string //L10n.PENDING.string
        case .Confirmed: // 1
            return L11n.confirmed.string //SKAppType.type == .food ? L11n.approved.string : .//L10n.CONFIRMED.string
        case .inTheKitchan: // 11 //InProcess || in Kitchan
            return SKAppType.type == .food ? L11n.inTheKitchan.string : (SKAppType.type == .home ? L11n.onTheWay.string : L11n.packed.string)
        case .Reached: // 10 //ready to be picked
            if SKAppType.type == .food {
                return deliveryType == .pickup ? "Order Ready for Collection" : L11n.onTheWay.string
            }
            return SKAppType.type == .home ? L11n.Reached.string : L11n.shipped.string
        case .Shipped: // 3 //Ship || on the way
            if SKAppType.type == .food {
                return deliveryType == .pickup ? "Order Ready for Collection" : L11n.onTheWay.string
            }
            return SKAppType.type == .home ? L11n.Started.string : L11n.outForDelivery.string
        case .Delivered: // 5
            if SKAppType.type == .food {
                return deliveryType == .pickup ? "Picked Up" : L11n.delivered.string
            }
            return SKAppType.type == .home ? L11n.Ended.string : L11n.delivered.string
        case .CustomerCancel:
            return L11n.canceled.string //L10n.CUSTOMERCANCELLED.string
        case .Rejected:
            return L11n.rejected.string //L10n.REJECTED.string
        case .FeedbackGiven:
            return L10n.FEEDBACKGIVEN.string
            
        case .Nearby:
            return L10n.NEARBY.string
            
        case .Tracked:
            return L10n.TRACKED.string
            
        case .Schedule:
            return L10n.SCHEDULED.string
        }

//         , "Ready to be Picked"//L11n.onTheWay.string
//        // , L11n.nearby.string //Nitin Food new changes
//         , "Picked Up"//L11n.delivered.string
        
        
//               case .inTheKitchan:
//                   return (SKAppType.type == .food || SKAppType.type.isJNJ) ? L11n.inTheKitchan.string : ((SKAppType.type == .home) ? L11n.start.string : L11n.inProgress.string
//               case .Shipped:
//                   return (SKAppType.type.isJNJ ? "Ready to be Picked" : (SKAppType.type == .eCom ? L10n.SHIPPED.string : L11n.onTheWay.string))
//
//               case .Reached:
//                   //Nitin
//                   return (SKAppType.type.isJNJ ? "Ready to be Picked" : ((SKAppType.type == .home) ? L11n.Reached.string : L11n.nearby.string))

        
    }
    
    func returnValueForKey(key : String) -> Any? {

        if key == TerminologyKeys.product.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.product ?? "Food Item" == "" ? "Food Item" : self.english?.product ?? "Food Item" : self.other?.product ?? "Food Item" == "" ? "Food Item" : self.other?.product ?? "Food Item"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.product ?? "Service" == "" ? "Service" : self.english?.product ?? "Service" : self.other?.product ?? "Service" == "" ? "Service" : self.other?.product ?? "Service"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.product ?? "Product" == "" ? "Product" : self.english?.product ?? "Product" : self.other?.product ?? "Product" == "" ? "Product" : self.other?.product ?? "Product"
            }
        } else if key == TerminologyKeys.products.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.products ?? "Food Items" == "" ? "Food Items" : self.english?.products ?? "Food Items" : self.other?.products ?? "Food Items" == "" ? "Food Items" : self.other?.products ?? "Food Items"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.products ?? "Services" == "" ? "Services" : self.english?.products ?? "Services" : self.other?.products ?? "Services" == "" ? "Services" : self.other?.products ?? "Services"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.products ?? "Products" == "" ? "Products" : self.english?.products ?? "Products" : self.other?.products ?? "Products" == "" ? "Products" : self.other?.products ?? "Products"
            }
        } else if key == TerminologyKeys.supplier.rawValue {
            var str = Localize.currentLanguage() == Languages.English ? self.english?.supplier : self.other?.supplier
            if str == nil || str == "" {
                if SKAppType.type.isFood {
                    str = "Restaurant".localized()
                } else if SKAppType.type.isHome {
                    str = "Agency".localized()
                } else {
                    str = "Supplier".localized()
                }
            }
            return str

        } else if key == TerminologyKeys.suppliers.rawValue {
            var str = Localize.currentLanguage() == Languages.English ? self.english?.suppliers : self.other?.suppliers
            if str == nil || str == "" {
                if SKAppType.type.isFood {
                    str = "Restaurants".localized()
                } else if SKAppType.type.isHome {
                    str = "Agencies".localized()
                } else {
                    str = "Suppliers".localized()
                }
            }
            return str
        } else if key == TerminologyKeys.order.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.order ?? "Order" == "" ? "Order" : self.english?.order ?? "Order" : self.other?.order ?? "Order" == "" ? "Order" : self.other?.order ?? "Order"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.order ?? "Booking" == "" ? "Booking" : self.english?.order ?? "Booking" : self.other?.order ?? "Booking" == "" ? "Booking" : self.other?.order ?? "Booking"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.order ?? "Order" == "" ? "Order" : self.english?.order ?? "Order" : self.other?.order ?? "Order" == "" ? "Order" : self.other?.order ?? "Order"
            }
        } else if key == TerminologyKeys.orders.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.orders ?? "Orders" == "" ? "Orders" : self.english?.orders ?? "Orders" : self.other?.orders ?? "Orders" == "" ? "Orders" : self.other?.orders ?? "Orders"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.orders ?? "Bookings" == "" ? "Bookings" : self.english?.orders ?? "Bookings" : self.other?.orders ?? "Bookings" == "" ? "Bookings" : self.other?.orders ?? "Bookings"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.orders ?? "Orders" == "" ? "Orders" : self.english?.orders ?? "Orders" : self.other?.orders ?? "Orders" == "" ? "Orders" : self.other?.orders ?? "Orders"
            }
        } else if key == TerminologyKeys.agent.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.agent ?? "Agent" == "" ? "Agent" : self.english?.agent ?? "Agent" : self.other?.agent ?? "Agent" == "" ? "Agent" : self.other?.agent ?? "Agent"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.agent ?? "Service Provider" == "" ? "Service Provider" : self.english?.agent ?? "Service Provider" : self.other?.agent ?? "Service Provider" == "" ? "Service Provider" : self.other?.agent ?? "Service Provider"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.agent ?? "Agent" == "" ? "Agent" : self.english?.agent ?? "Agent" : self.other?.agent ?? "Agent" == "" ? "Agent" : self.other?.agent ?? "Agent"
            }
        } else if key == TerminologyKeys.agents.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.agents ?? "Agents" == "" ? "Agents" : self.english?.agents ?? "Agents" : self.other?.agents ?? "Agents" == "" ? "Agents" : self.other?.agents ?? "Agents"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.agents ?? "Service Providers" == "" ? "Service Providers" : self.english?.agents ?? "Service Providers" : self.other?.agents ?? "Service Providers" == "" ? "Service Providers" : self.other?.agents ?? "Service Providers"
            } else {
                 return Localize.currentLanguage() == Languages.English ? self.english?.agents ?? "Agents" == "" ? "Agents" : self.english?.agents ?? "Agents" : self.other?.agents ?? "Agents" == "" ? "Agents" : self.other?.agents ?? "Agents"
            }
        } else if key == TerminologyKeys.brand.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.brand ?? "Brand" == "" ? "Brand" : self.english?.brand ?? "Brand" : self.other?.brand ?? "Brand" == "" ? "Brand" : self.other?.brand ?? "Brand"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.brand ?? "Brand" == "" ? "Brand" : self.english?.brand ?? "Brand" : self.other?.brand ?? "Brand" == "" ? "Brand" : self.other?.brand ?? "Brand"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.brand ?? "Brand" == "" ? "Brand" : self.english?.brand ?? "Brand" : self.other?.brand ?? "Brand" == "" ? "Brand" : self.other?.brand ?? "Brand"
            }
        } else if key == TerminologyKeys.brands.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.brands ?? "Brands" == "" ? "Brands" : self.english?.brands ?? "Brands" : self.other?.brands ?? "Brands" == "" ? "Brands" : self.other?.brands ?? "Brands"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.brands ?? "Brands" == "" ? "Brands" : self.english?.brands ?? "Brands" : self.other?.brands ?? "Brands" == "" ? "Brands" : self.other?.brands ?? "Brands"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.brands ?? "Brands" == "" ? "Brands" : self.english?.brands ?? "Brands" : self.other?.brands ?? "Brands" == "" ? "Brands" : self.other?.brands ?? "Brands"
            }
        } else if key == TerminologyKeys.category.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.category ?? "Category" == "" ? "Category" : self.english?.category ?? "Category" : self.other?.category ?? "Category" == "" ? "Category" : self.other?.category ?? "Category"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.category ?? "Category" == "" ? "Category" : self.english?.category ?? "Category" : self.other?.category ?? "Category" == "" ? "Category" : self.other?.category ?? "Category"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.category ?? "Category" == "" ? "Category" : self.english?.category ?? "Category" : self.other?.category ?? "Category" == "" ? "Category" : self.other?.category ?? "Category"
            }
        } else if key == TerminologyKeys.categories.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.categories ?? "Categories" == "" ? "Categories" : self.english?.categories ?? "Categories" : self.other?.categories ?? "Categories" == "" ? "Categories" : self.other?.categories ?? "Categories"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.categories ?? "Categories" == "" ? "Categories" : self.english?.categories ?? "Categories" : self.other?.categories ?? "Categories" == "" ? "Categories" : self.other?.categories ?? "Categories"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.categories ?? "Categories" == "" ? "Categories" : self.english?.categories ?? "Categories" : self.other?.categories ?? "Categories" == "" ? "Categories" : self.other?.categories ?? "Categories"
            }
        } else if key == TerminologyKeys.catalogue.rawValue {
            if SKAppType.type.isFood {
                return Localize.currentLanguage() == Languages.English ? self.english?.catalogue ?? "Menu" == "" ? "Menu" : self.english?.catalogue ?? "Menu" : self.other?.catalogue ?? "Menu" == "" ? "Menu" : self.other?.catalogue ?? "Menu"
            } else if SKAppType.type.isHome {
                return Localize.currentLanguage() == Languages.English ? self.english?.catalogue ?? "Services" == "" ? "Services" : self.english?.catalogue ?? "Services" : self.other?.catalogue ?? "Services" == "" ? "Services" : self.other?.catalogue ?? "Services"
            } else {
                return Localize.currentLanguage() == Languages.English ? self.english?.catalogue ?? "Catalogue" == "" ? "Catalogue" : self.english?.catalogue ?? "Catalogue" : self.other?.catalogue ?? "Catalogue" == "" ? "Catalogue" : self.other?.catalogue ?? "Catalogue"
            }
        } else if key == TerminologyKeys.status.rawValue {
            if SKAppType.type.isFood {
               return Localize.currentLanguage() == Languages.English ? self.english?.status ?? [] : self.other?.status ?? []
            } else if SKAppType.type.isHome {
               return Localize.currentLanguage() == Languages.English ? self.english?.status ?? [] : self.other?.status ?? []
            } else {
               return Localize.currentLanguage() == Languages.English ? self.english?.status ?? [] : self.other?.status ?? []
            }
        }else if key == TerminologyKeys.popularRestaurantes.rawValue {
            return self.english?.popularRestaurantes
        }else if key == TerminologyKeys.noOrderFound.rawValue {
            return self.english?.noOrderFound
        }else {
            return nil
        }
        
    }
    
    
//    func returnEnglishLanguageValue(key : String) -> String{
//
//        if key == TerminologyKeys.product.rawValue {
//            self.english?.product ?? "Food Item" == "" ? "Food Item" : self.english?.product ?? "Food Item"
//        }
//
//    }
//
}

class TerminologyDataModal : NSObject,Mappable,NSCoding {
    
    var product : String?
    var products : String?
    var supplier : String?
    var suppliers : String?
    var order : String?
    var orders : String?
    var agent : String?
    var agents : String?
    var brand : String?
    var brands : String?
    var category : String?
    var categories : String?
    var catalogue : String?
    var status : [String:Any]?
    var popularRestaurantes:String?
    var noOrderFound:String?
    
    required init?(map: Map){}
    private override init(){}
       
    
    func mapping(map: Map) {
        product <- map ["product"]
        products <- map["products"]
        supplier <- map ["supplier"]
        suppliers <- map["suppliers"]
        order <- map ["order"]
        orders <- map["orders"]
        agent <- map ["agent"]
        agents <- map["agents"]
        brand <- map ["brand"]
        brands <- map["brands"]
        category <- map ["category"]
        categories <- map["categories"]
        catalogue <- map ["catalogue"]
        status <- map["status"]
        popularRestaurantes <- map["Popular Restaurantes"]
        noOrderFound <- map["No Orden found"]
    }

    @objc required init(coder aDecoder: NSCoder){
        product = aDecoder.decodeObject(forKey: "product") as? String
        products = aDecoder.decodeObject(forKey: "products") as? String
        supplier = aDecoder.decodeObject(forKey: "supplier") as? String
        suppliers = aDecoder.decodeObject(forKey: "suppliers") as? String
        order = aDecoder.decodeObject(forKey: "order") as? String
        orders = aDecoder.decodeObject(forKey: "orders") as? String
        agent = aDecoder.decodeObject(forKey: "agent") as? String
        agents = aDecoder.decodeObject(forKey: "agents") as? String
        brand = aDecoder.decodeObject(forKey: "brand") as? String
        brands = aDecoder.decodeObject(forKey: "brands") as? String
        category = aDecoder.decodeObject(forKey: "category") as? String
        categories = aDecoder.decodeObject(forKey: "categories") as? String
        catalogue = aDecoder.decodeObject(forKey: "catalogue") as? String
        status = aDecoder.decodeObject(forKey: "status") as? [String:Any]
        popularRestaurantes = aDecoder.decodeObject(forKey: "popularRestaurantes") as? String
        noOrderFound = aDecoder.decodeObject(forKey: "noOrderFound") as? String

    }
       
   @objc func encode(with aCoder: NSCoder){
        if product != nil{
           aCoder.encode(product, forKey: "product")
        }
        if products != nil{
           aCoder.encode(products, forKey: "products")
        }
        if supplier != nil{
           aCoder.encode(supplier, forKey: "supplier")
        }
        if suppliers != nil{
            aCoder.encode(suppliers, forKey: "suppliers")
        }
        if order != nil{
            aCoder.encode(order, forKey: "order")
        }
        if orders != nil{
            aCoder.encode(orders, forKey: "orders")
        }
        if agent != nil{
            aCoder.encode(agent, forKey: "agent")
        }
        if agents != nil{
            aCoder.encode(agents, forKey: "agents")
        }
        if brand != nil{
            aCoder.encode(brand, forKey: "brand")
        }
        if brands != nil{
            aCoder.encode(brands, forKey: "brands")
        }
        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if categories != nil{
            aCoder.encode(categories, forKey: "categories")
        }
        if catalogue != nil{
            aCoder.encode(catalogue, forKey: "catalogue")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if popularRestaurantes != nil{
            aCoder.encode(status, forKey: "popularRestaurantes")
        }
    if noOrderFound != nil{
        aCoder.encode(status, forKey: "noOrderFound")
    }
   }
    
}
