//
//  NewRequest.swift
//  Sneni
//
//  Created by osvin-k01 on 6/30/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import Foundation


enum NewRequestKeys : String {
    case user_join_id = "id"
    case firstName = "firstname"
  
}
import Foundation


import SwiftyJSON
import RMMapper

class NewRequest : NSObject , RMMapping, NSCoding {
    
    var firstname : String?
    var user_join_id : String?

    init (attributes : Dictionary<String, JSON>?){
        self.user_join_id = attributes?["id"]?.stringValue
        self.firstname = attributes?[NewRequestKeys.firstName.rawValue]?.stringValue
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.user_join_id, forKey: NewRequestKeys.user_join_id.rawValue)
        aCoder.encode(self.firstname, forKey: NewRequestKeys.firstName.rawValue)
    }
    
    required init(coder aDecoder: NSCoder) {
        self.user_join_id  = aDecoder.decodeObject(forKey: NewRequestKeys.user_join_id.rawValue) as? String
        self.firstname  = aDecoder.decodeObject(forKey: NewRequestKeys.firstName.rawValue) as? String
    }
    
    override init(){
        super.init()
    }
}



