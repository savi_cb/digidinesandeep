//
//  ServiceType.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper


enum ServiceTypeKeys : String {
    case id = "id"
    case supplier_placement_level = "supplier_placement_level"
    case image = "image"
    case icon = "icon"
    case name = "name"
    case description = "description"
    case category_flow = "category_flow"
    case order = "order"
    case agentlist = "agent_list"
    case isProduct = "is_product"
    case parent_id
    case sub_category
    case is_assign
}

class ServiceType : NSObject , RMMapping, NSCoding{
    
    var id : String?
    var supplier_placement_level : String?
    var image : String?
    var icon : String?
    var name : String?
    var desc : String?
    var category_flow : String?
    var order : String?
    var agent_list : String?
    var supplierId : String?
    var parent_id : Int?
    var sub_category: [SubSub_Category]?
    var is_assign: Int?
    var is_subcategory: Int?
    
    init (attributes : SwiftyJSONParameter){
        
        guard let data = attributes else {return}
        //Nitin
        self.parent_id = data[ServiceTypeKeys.parent_id.rawValue]?.intValue
        
        if let subcategory = data[ServiceTypeKeys.sub_category.rawValue]?.arrayValue {
            self.sub_category = []
            for json in subcategory {
                let obj = SubSub_Category(attributes: json.dictionary)
                self.sub_category?.append(obj)
            }
        }
        
        self.is_assign = data[ServiceTypeKeys.is_assign.rawValue]?.intValue
        
        self.id = data[ServiceTypeKeys.id.rawValue]?.stringValue
        self.supplier_placement_level = data[ServiceTypeKeys.supplier_placement_level.rawValue]?.stringValue
        self.image = data[ServiceTypeKeys.image.rawValue]?.stringValue
        self.icon = data[ServiceTypeKeys.icon.rawValue]?.stringValue
        self.name = data[ServiceTypeKeys.name.rawValue]?.stringValue
        self.desc = data[ServiceTypeKeys.description.rawValue]?.stringValue
        self.category_flow = data[ServiceTypeKeys.category_flow.rawValue]?.stringValue
        self.order = data[ServiceTypeKeys.order.rawValue]?.stringValue
        self.agent_list = data[ServiceTypeKeys.agentlist.rawValue]?.stringValue

    }
    
    override init() {
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(parent_id, forKey: "parent_id")
        aCoder.encode(sub_category, forKey: "sub_category")
        aCoder.encode(is_assign, forKey: "is_assign")
        
        aCoder.encode(id, forKey: "id")
        aCoder.encode(supplier_placement_level, forKey: "supplier_placement_level")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(icon, forKey: "icon")
        aCoder.encode(name, forKey: "name")
        
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(category_flow, forKey: "category_flow")
        aCoder.encode(order, forKey: "order")
        aCoder.encode(agent_list, forKey: ServiceTypeKeys.agentlist.rawValue)
      // self.agent_list = attributes?[ServiceTypeKeys.agentlist.rawValue]?.stringValue

    }
    
    required init(coder aDecoder: NSCoder) {
        
        parent_id = aDecoder.decodeObject(forKey: "parent_id") as? Int
        sub_category = aDecoder.decodeObject(forKey: "sub_category") as? [SubSub_Category]
        is_assign = aDecoder.decodeObject(forKey: "is_assign") as? Int
        
        id = aDecoder.decodeObject(forKey: "id") as? String
        supplier_placement_level = aDecoder.decodeObject(forKey: "supplier_placement_level") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        icon = aDecoder.decodeObject(forKey: "icon") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        
        desc = aDecoder.decodeObject(forKey: "desc") as? String
        category_flow = aDecoder.decodeObject(forKey: "category_flow") as? String
      
        order = aDecoder.decodeObject(forKey: "order") as? String
        agent_list = aDecoder.decodeObject(forKey: ServiceTypeKeys.agentlist.rawValue) as? String
       
    }
    
}
//Nitin

class SubSub_Category : NSObject , RMMapping, NSCoding{
    
    var parent_id : Int?
    var is_assign : Int?
    var image : String?
    var icon : String?
    var name : String?
    var id : String?
    var sub_category: [SubSub_Category]?

    init (attributes : SwiftyJSONParameter){
        
        guard let data = attributes else {return}
        if let subcategory = data[ServiceTypeKeys.sub_category.rawValue]?.arrayValue {
            self.sub_category = []
            for json in subcategory {
                let obj = SubSub_Category(attributes: json.dictionary)
                self.sub_category?.append(obj)
            }
        }
        
        self.parent_id = data[ServiceTypeKeys.parent_id.rawValue]?.intValue
        self.is_assign = data[ServiceTypeKeys.is_assign.rawValue]?.intValue
        self.id = data[ServiceTypeKeys.id.rawValue]?.stringValue
        self.image = data[ServiceTypeKeys.image.rawValue]?.stringValue
        self.icon = data[ServiceTypeKeys.icon.rawValue]?.stringValue
        self.name = data[ServiceTypeKeys.name.rawValue]?.stringValue
 
    }
    
    override init() {
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(sub_category, forKey: "sub_category")
        aCoder.encode(parent_id, forKey: "parent_id")
        aCoder.encode(is_assign, forKey: "is_assign")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(icon, forKey: "icon")
        aCoder.encode(name, forKey: "name")
        
    }
    
    required init(coder aDecoder: NSCoder) {
        
        sub_category = aDecoder.decodeObject(forKey: "sub_category") as? [SubSub_Category]
        parent_id = aDecoder.decodeObject(forKey: "parent_id") as? Int
        is_assign = aDecoder.decodeObject(forKey: "is_assign") as? Int
        id = aDecoder.decodeObject(forKey: "id") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        icon = aDecoder.decodeObject(forKey: "icon") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        
    }
}
