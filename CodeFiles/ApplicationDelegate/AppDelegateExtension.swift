//
//  AppDelegateExtension.swift
//  Clikat
//
//  Created by cblmacmini on 8/28/16.
//  Copyright ©️ 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON
import EZSwiftExtensions
//import LNNotificationsUI
import UserNotifications

extension AppDelegate{
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        guard let dict = userInfo as? [String: Any] else{return}
        if dict["status"] as? String == "5" || dict["status"] as? String == "2"{
            print("Here we go")
            GDataSingleton.sharedInstance.orderId = nil
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hitorder"), object: nil, userInfo: nil)
        }else if dict["status"] as? String == "1"  || dict["status"] as? String == "10" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hitorder"), object: nil, userInfo: nil)
        }
        
        if /(dict["type"] as? String) == "chat"{
            let storyboard = UIStoryboard(name: "Chats", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "ChatHeadVC") as? ChatHeadVC else { return }
            let agnt = CblUser()
            agnt.name = /(dict["sender_name"] as? String)
            agnt.image = /(dict["sender_image"] as? String)
            agnt.agent_created_id = /(dict["sender_image"] as? String)
            vc.agent = agnt
            vc.agentId = /(dict["agent_created_id"] as? String)
            vc.orderId = /(dict["order_id"] as? String)
            UIApplication.topViewController()?.pushVC(vc)
        }else if /(dict["type"] as? String) == "request_for_join_table"{
            let storyboard = UIStoryboard(name: "DigiDineHome", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "AcceptRejectViewController") as? AcceptRejectViewController else { return }
            
            guard  let str = dict["userRequestForJoinTableDetail"] as? String else{return}
            
            if let data = str.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                    
                    let id = json?["id"] as? Int
                    let firstname = json?["firstname"] as? String
                    vc.userId =  id?.toString
                    vc.firstName = firstname
                    vc.sessionRecordId = /(dict["sessionRecordId"] as? String)
                    UIApplication.topViewController()?.pushVC(vc)
                    
                } catch {
                    print("Something went wrong")
                }
            }
        }else if /(dict["type"] as? String) == "split_confirmation"{
            
            let storyboard = UIStoryboard(name: "DigiDineHome", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "SplitPaymentViewController") as? SplitPaymentViewController else { return }
            
            guard  let str = dict["notificationSendToUserDetail"] as? String else{return}
            if let data = str.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                    
                    let firstname = json?["firstname"] as? String
                    vc.name = firstname
                    let splitAmount = dict["splitAmount"] as? String
                    vc.splitAmount = splitAmount
                    let id = dict["sessionRecordId"] as? String
                    vc.sessionId = id
                    UIApplication.topViewController()?.pushVC(vc)
                    
                } catch {
                    print("Something went wrong")
                }
            }
        }else if  /(dict["type"] as? String) == "split_payment_confirmed_by_all"{
            let id = dict["sessionRecordId"] as? String
            if let id = id{
                self.hitSplitEvenly(sessionRecodrId: id)
            }
        }else if  /(dict["type"] as? String) == "split_payment_reject"{
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "", message: "Split request already rejected by someone, try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
        }else if /(dict["type"] as? String) == "request_confirmed_for_join_table"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "invalidateSessionTimer"), object: nil, userInfo: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "merge"), object: nil, userInfo: nil)
        }else if /(dict["type"] as? String) == "request_reject_for_join_table"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "merge"), object: nil, userInfo: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "invalidateSessionTimer"), object: nil, userInfo: nil)
        }else if /(dict["type"] as? String) == "split_payment_done"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
        }else if /(dict["type"] as? String) == "status_reject"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
        }else if /(dict["type"] as? String) == "status_confirm"{
            GDataSingleton.sharedInstance.orderId = nil
        }else if /(dict["type"] as? String) == "leave_table_session_after_place_order"{
            DBManager.sharedManager.cleanCart()
            GDataSingleton.sharedInstance.tableNumber = nil
            GDataSingleton.sharedInstance.selfPickup = nil
            (UIApplication.shared.delegate as? AppDelegate)?.onload()
        }else {
           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
        }
        //handlePush(userInfo: dict)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        guard let dict = userInfo as? [String: Any] else{return}
        if dict["status"] as? String == "5"{
            print("Here we go")
            GDataSingleton.sharedInstance.orderId = nil
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hitorder"), object: nil, userInfo: nil)
            completionHandler([.alert,.badge,.sound])
        }else if dict["status"] as? String == "1"  || dict["status"] as? String == "10" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hitorder"), object: nil, userInfo: nil)
            completionHandler([.alert,.badge,.sound])
        }
        
        // Post a notification
        if let type = dict["type"] as? String{
            if type == "chat"{
                let orderId = dict["orderId"] as? String
                let agent_created_id = dict["agent_created_id"] as? String
                if UserData.share.currentConversationId == orderId && UserData.share.currentAgentId == agent_created_id{
                    completionHandler([])
                }else {
                    completionHandler([.alert,.badge,.sound])
                }
            }else if /(dict["type"] as? String) == "request_for_join_table"{
                let storyboard = UIStoryboard(name: "DigiDineHome", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "AcceptRejectViewController") as? AcceptRejectViewController else { return }
                
                guard  let str = dict["userRequestForJoinTableDetail"] as? String else{return}
                
                if let data = str.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                        
                        let id = json?["id"] as? Int
                        let firstname = json?["firstname"] as? String
                        vc.userId =  id?.toString
                        vc.firstName = firstname
                        vc.sessionRecordId = /(dict["sessionRecordId"] as? String)
                        UIApplication.topViewController()?.pushVC(vc)
                        
                    } catch {
                        print("Something went wrong")
                    }
                }
                completionHandler([.alert,.badge,.sound])
            }else if type == "split_confirmation"{
                let storyboard = UIStoryboard(name: "DigiDineHome", bundle: nil)
                
                guard let vc = storyboard.instantiateViewController(withIdentifier: "SplitPaymentViewController") as? SplitPaymentViewController else { return }
                
                guard  let str = dict["notificationSendToUserDetail"] as? String else{return}
                
                if let data = str.data(using: String.Encoding.utf8) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                        
                        let firstname = json?["firstname"] as? String
                        vc.name = firstname
                        let id = dict["sessionRecordId"] as? String
                        vc.sessionId = id
                        let splitAmount = dict["splitAmount"] as? String
                        vc.splitAmount = splitAmount
                        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "split"), object: nil, userInfo: dict)
                        UIApplication.topViewController()?.pushVC(vc)
                        
                        print("test fhejkwf")
                        
                    } catch {
                        print("Something went wrong")
                    }
                }
                completionHandler([.alert,.badge,.sound])
            } else if  /(dict["type"] as? String) == "split_payment_confirmed_by_all"{
                let id = dict["sessionRecordId"] as? String
                if let id = id{
                    self.hitSplitEvenly(sessionRecodrId: id)
                }
                completionHandler([.alert,.badge,.sound])
            }else if  /(dict["type"] as? String) == "split_payment_reject"{
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "", message: "Split request already rejected by someone, try again.", preferredStyle: .alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
                completionHandler([.alert,.badge,.sound])
            }else if type == "request_confirmed_for_join_table"{
                if self.window?.rootViewController?.topMostVC() is DigiDineHeaderVC {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "mergeQrCode"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "merge"), object: nil, userInfo: nil)
                }else{
                    guard  let str = dict["sessionDetail"] as? String else{return}
                    
                    if let data = str.data(using: String.Encoding.utf8) {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                            
                            let supplierId = json?["supplier_id"] as? Int ?? 0
                            let sessionId = userInfo["session_id"] as? String  ?? ""
                            //vc.splitAmount = splitAmount
                            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "split"), object: nil, userInfo: dict)
                            GDataSingleton.sharedInstance.orderId = nil
                            GDataSingleton.sharedInstance.loginType = "NFC"
                            GDataSingleton.sharedInstance.supplierId = "\(supplierId)"
                            GDataSingleton.sharedInstance.tableNumber = "\(sessionId)"
                            let tableName = json?["table_name"] as? String ?? ""
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
                                let VC = DigiDineHeaderVC.getVC(.digiHome)
                                VC.tableNumberShoe =  Int(tableName)
                                ez.topMostVC?.pushVC(VC)
                            }
                            print("test fhejkwf")
                            
                        } catch {
                            print("Something went wrong")
                        }
                    }
                }
                completionHandler([.alert,.badge,.sound])
            }else if type == "request_reject_for_join_table"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "merge"), object: nil, userInfo: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "mergeQrCode"), object: nil, userInfo: nil)
                completionHandler([.alert,.badge,.sound])
            }else if /(dict["type"] as? String) == "split_payment_done"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
                completionHandler([.alert,.badge,.sound])
            }else if /(dict["type"] as? String) == "status_reject"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
                completionHandler([.alert,.badge,.sound])
            }else if /(dict["type"] as? String) == "leave_table_session_after_place_order"{
                DBManager.sharedManager.cleanCart()
                GDataSingleton.sharedInstance.tableNumber = nil
                GDataSingleton.sharedInstance.selfPickup = nil
                (UIApplication.shared.delegate as? AppDelegate)?.onload()
                completionHandler([.alert,.badge,.sound])
            }
        }else{
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshData"), object: nil, userInfo: nil)
//            if let orderId = dict["orderId"] as? String {
//                self.refreshViewController(orderId: orderId)
//            }
            completionHandler([.alert,.badge,.sound])
        }
    }
    
    //MARK :- Function to check login
    func refreshViewController(orderId: String?) {
        
        if let baseVc = window?.rootViewController as? LeftNavigationViewController {
            if let vc = ez.topMostVC as? OrderDetailController {
                vc.getOrderDetails(orderId: orderId ?? "")
            } else {
                self.setBaseVc(navigationVc: baseVc, orderId: orderId)
            }
        } else {
            let navigationVc = StoryboardScene.Main.instantiateLeftNavigationViewController()
            window?.rootViewController = navigationVc
            self.setBaseVc(navigationVc: navigationVc, orderId: orderId)
        }
        
        
        //        else if let vc = ez.topMostVC as? OrderHistoryViewController {
        //            if vc.bottomView_leadingConstraint.constant == vc.pending_button.frame.x {
        //                vc.upcomingOrders()
        //            } else if vc.bottomView_leadingConstraint.constant == vc.completed_button.frame.x {
        //                vc.webService()
        //            }
        //        }
    }
    
    func setBaseVc(navigationVc: UINavigationController,orderId:String?) {
        
        if let homeVc = navigationVc.viewControllers.first as? MainTabBarViewController {
            let index = homeVc.viewControllers?.firstIndex(where: {$0 is OrderHistoryViewController}) ?? 2
            homeVc.selectedIndex = index
            guard let topVc = homeVc.viewControllers?[index] as? OrderHistoryViewController else {return}
            let VC = StoryboardScene.Order.instantiateOrderDetailController()
            let order = OrderDetails(orderId: orderId ?? "")
            VC.type = .OrderUpcoming
            VC.orderDetails = order
            topVc.pushVC(VC)
        }
    }
    
    func handlePush(userInfo : [String : Any]?){
        guard let dict = userInfo else { return }
        let json = JSON(dict)
        if UIApplication.shared.applicationState == .active {
            HDNotificationView.show(with: UIImage(asset: Asset.Ic_notification), title: "Royo Customer", message: userInfo?["message"] as? String, isAutoHide: true) { [weak self] in
                self?.pushSound?.stop()
                HDNotificationView.hide()
                self?.handlePushNavigation(pushDict: json)
            }
            
            if let filePath = Bundle.main.path(forResource: "push", ofType: "mp3"){
                soundURL = URL(fileURLWithPath: filePath)
                do {
                    let sound = try AVAudioPlayer(contentsOf: soundURL!)
                    pushSound = sound
                    sound.play()
                } catch {
                    // couldn't load file :(
                }
            }
        }else {
            handlePushNavigation(pushDict: json)
        }
    }
    
    func handlePushNavigation(pushDict : JSON?) {
        print(pushDict)
        switchViewControllers(language:  Localize.currentLanguage())
        //GDataSingleton.sharedInstance.pushDict = nil
        if pushDict?["venueId"].intValue != 0{
            let venue = pushDict?["venueId"].stringValue ?? ""
            self.hitQRCodeScanAPI(suppId: 0, tableId: 0, venueId:  Int(venue) ?? 0, hotelId: 0, roomNo: 0)
//            let VC = DigiDineViewController.getVC(.digiHome)
//            //VC.supplier = supplierList
//            ez.topMostVC?.pushVC(VC)
        }else if pushDict?["hotelId"].intValue != 0{
            if pushDict?["roomNo"].intValue != 0{
                let hotel =  pushDict?["hotelId"].stringValue ?? ""
                let roomNo =  pushDict?["roomNo"].stringValue ?? ""
                self.hitQRCodeScanAPI(suppId: 0, tableId: 0, venueId: 0, hotelId: Int(hotel) ?? 0, roomNo: Int(roomNo) ?? 0)
//                let VC = DigiDineViewController.getVC(.digiHome)
//                //VC.supplier = supplierList
//                ez.topMostVC?.pushVC(VC)
            }
        }else if pushDict?["suppId"].intValue != 0 {
            if pushDict?["tableId"].intValue != 0 {
                let supplier =  pushDict?["suppId"].stringValue ?? ""
                let table =  pushDict?["tableId"].stringValue ?? ""
                self.hitQRCodeScanAPI(suppId: Int(supplier) ?? 0, tableId: Int(table) ?? 0, venueId: 0, hotelId:  0, roomNo: 0)
//                let VC = DigiDineViewController.getVC(.digiHome)
//                //VC.supplier = supplierList
//                ez.topMostVC?.pushVC(VC)
            }
        }
        
        GDataSingleton.sharedInstance.pushDict = nil
        
//        let vc = StoryboardScene.DigiDine.instantiateMainTabBarController()
//        let index = vc.viewControllers?.firstIndex(where: {$0 is OrderHistoryViewController}) ?? 2
//        vc.selectedIndex = index
//        guard let topVc = vc.viewControllers?[index] as? OrderHistoryViewController else {return}
//
//        let VC = StoryboardScene.Order.instantiateOrderDetailController()
//        let order = OrderDetails(orderId: pushDict?["orderId"].stringValue)
//        VC.type = .OrderUpcoming
//        VC.orderDetails = order
//        topVc.pushVC(VC)
    }
    
    
    //MARK:- hit split
    func hitSplitEvenly(sessionRecodrId : String){
        //       //        let amo = String(amount)
        //       APIManager.sharedInstance.showLoader()
        //       let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
        //       let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
        //       //          let orderId = GDataSingleton.sharedInstance.OrderId
        //        let params = FormatAPIParameters.splitEvenly(table_id: tableId, amount: 100.0, supplier_id: supplierId, order_id : "" , payMode : "split").formatParameters()
        
        APIManager.sharedInstance.showLoader(message: "Processing Payment")
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
        let tableId = Int(/sessionRecodrId)
        //          let orderId = GDataSingleton.sharedInstance.OrderId
        let params = FormatAPIParameters.splitEvenly(table_id: tableId, amount: 100.0, supplier_id: supplierId, order_id : "" , payMode : "split", totalUnpaidDiscount: "", userUnpaidDiscount: "").formatParameters()
        
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.splitEvenly(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                print(object)
                //APIManager.sharedInstance.hideLoader()
                if let obj = object as? [String:Any]{
                    SKToast.makeToast("Payment made successfully")
                    GDataSingleton.sharedInstance.PayOption = "paid"
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                }
            case .Failure(let error):
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
}
