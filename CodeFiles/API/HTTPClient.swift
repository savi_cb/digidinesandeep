//
//  APIClient.swift
//  Clikat
//
//  Created by cbl73 on 4/22/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import Alamofire
import AFNetworking
import SwiftyJSON

typealias HttpClientSuccess = (Any?) -> ()
typealias HttpClientFailure = (String) -> ()

class HTTPClient {
    
    func JSONObjectWithData(data: Data) -> Any? {
        do { return try JSONSerialization.jsonObject(with: data, options: []) }
        catch { return .none }
    }
    
    func postRequest(refreshControl : UIRefreshControl? = nil, withApi api : API  , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure )  {
        
        let params = api.parameters
        let fullPath = /apiApiPath(api: api) + api.route
        let method = api.method
        let headers: HTTPHeaders? = apiHeader(api: api)
        
        //Api Header
        
//        if isLoaderNeeded(api: api){
//            headers = apiHeader(api: api)
//
//
//            //["secretdbkey": APIConstants.secretdbkey,
//            //   "Authorization": /(GDataSingleton.sharedInstance.loggedInUser?.token)]
//        }
        
        print(">>>>>>=========== Pre Request ===========")
        print(fullPath)
        
        if let params = params, let dataBody = try? JSONSerialization.data(withJSONObject: params) {
            guard let url = URL(string: fullPath) else {
                failure("Please enter valid agent code!")
                return}
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.allHTTPHeaderFields = headers
            
            if !params.isEmpty {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = dataBody
            }
            
            Alamofire.request(request)
                .responseJSON {
                    (response) in
                    
                    refreshControl?.endRefreshing()
                    
                    print(">>>>>>=========== Post Request ===========")
                    print(fullPath)
                    print(method)
                    print(headers ?? "NA")
                    print(params )
                    
                    //            DispatchQueue.main.async {
                    
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        print(error )
                        failure(error.localizedDescription)
                    }
                    
            }
            return
        }
        
        Alamofire.request(fullPath, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON {
            (response) in
            
            print(">>>>>>=========== Request ===========")
            print(fullPath)
            print(method)
            print(headers ?? "NA")
            print(params ?? "NA")
            
//            DispatchQueue.main.async {
            
                switch response.result {
                case .success(let data):
                    success(data)
                case .failure(let error):
                    print(error )
                    failure(error.localizedDescription)
                }
                
//            }
        }
        
        /*
         .responseString { (response) in
         print(response)
         }
         */
    }
    
    func postRequest(withApi api : API,image : UIImage?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        let params = api.parameters
        let fullPath = APIConstants.BasePath + api.route
        // let method = api.method
        let headers: HTTPHeaders? = apiHeader(api: api)

        Alamofire.upload(multipartFormData: { (formData) in
            if let ppic = image, let data = ppic.jpegData(compressionQuality: 0.0) {
                switch api {
                case .UploadReceipt(_):
                    formData.append(data, withName: "file", fileName: "temp" + "\(Int(arc4random_uniform(100000)))" + ".jpeg", mimeType: "image/jpeg")
                default:
                    formData.append(data, withName: "profilePic", fileName: "temp" + "\(Int(arc4random_uniform(100000)))" + ".jpeg", mimeType: "image/jpeg")
                }
            }
            for (key, value) in params ?? [:] {
                formData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
            }
        }, to: fullPath,headers:headers) { (result) in
            switch result {
            case .success(request:let req, streamingFromDisk: _, streamFileURL: _):
                req.responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                })
                req.responseString(completionHandler: { (response) in
                    print(response)
                })
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
        
    }
    
    func postPayFortRequest(completion : @escaping APICompletion){
        
//        let manager : AFHTTPSessionManager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
//        manager.requestSerializer = AFJSONRequestSerializer()
//
//        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
//
//        let url = "https://paymentservices.payfort.com/FortAPI/paymentApi"
//
//        var signatureStr = ""
//
//        var params = [
//            "service_command" : "SDK_TOKEN",
//            "device_id" : /PayFortController().getUDID(),
//            "language" : "en",
//            "access_code" : "ELvn2sPwsH5CXUFp0BjI",
//            "merchant_identifier" : "UWQlGgoP",
//            ]
//        let req = params.sorted(by: {$0.0 < $1.0})
//        for (key,value) in req {
//            signatureStr += key + "=" + /value
//        }
//        signatureStr = "CLIKATIN" + "access_code=ELvn2sPwsH5CXUFp0BjIdevice_id=" + PayFortController().getUDID() + "language=enmerchant_identifier=UWQlGgoPservice_command=SDK_TOKEN" + "CLIKATIN"
//
//        signatureStr = signatureStr.sha256()
//        params["signature"] = signatureStr
//
//        Alamofire.request(url, method: .post, parameters: params , encoding: JSONEncoding.default, headers: ["Content-Type":"application/json","Accept" : "application/json"]).responseJSON { (response) in
//            switch response.result {
//            case .success(let data):
//                if JSON(data).dictionaryValue["status"] != "00" {
//                    completion(APIResponse.Success(PayFort(attributes: JSON(data).dictionaryValue)))
//                    return
//                }
//                print(JSON(data).dictionaryValue["response_message"] ?? "")
//                completion(APIResponse.Failure(APIValidation.ApiError))
//            case .failure(let error):
//                print(error)
//                completion(APIResponse.Failure(APIValidation.ApiError))
//            }
//        }
        
        
    }
    
    
    func isLoaderNeeded(api : API) -> Bool{
        switch api {
            
        case .RateProduct(_) , .OrderDetail(_): return true
        case .GetAgentDBKeys(_): return true
        case .ServiceAgentlist(_): return true
        case .GenerateOrder(_): return true
        case .getAgentAvailabilties(_), .getAgentAvailabilty: return true

        default:
            return false
        }
    }
    
    func apiApiPath(api : API) -> String?{
        
        switch api {
            
        case .ServiceAgentlist(_), .getAgentAvailabilties, .getSlotAvailabilties, .getAgentAvailabilty : return  APIConstants.agentBasePath
            
        case .getBotResponce :
            return  SKAppType.type.apiBot
            
        case .getSecretKey(_): return APIConstants.agentTokenBasePath
            
        default: return  APIConstants.BasePath
            
        }
    }
    
    func apiHeader(api : API) -> HTTPHeaders?{
        
        switch api {
        case .getBotResponce :
            return  [
                "Content-Type":"application/json",
                "Authorization":"Bearer \(SKAppType.type.apiBotKey)"
            ]
            
        case .ServiceAgentlist(_), .getAgentAvailabilties(_), .getAgentAvailabilty, .getSlotAvailabilties:
            //Nitin Check
            var headers: HTTPHeaders? = nil
            
            var apikey:String?
            var db_SecretKey:String?
            
            let agentDBSecretKey  =  GDataSingleton.sharedInstance.agentDBSecretKey //AgentCodeClass.shared.agentSecretKey
            agentDBSecretKey?.agentDBData?.forEach({ (obj) in
                
                if obj.key == "api_key"{
                    apikey = obj.value
                }
                else if obj.key == "secret_key"{
                    db_SecretKey = obj.value
                }        
            })
            
//            headers = ["api_key": "964UTvzJKCblds1J&&^Saas&x8gIbTXcEEJSUilGqpxCcmnx",
//                       "secret_key": "a0e767bdeb37fd7a02b9cbc6d6b142c6"]
            headers = ["api_key": /apikey,
                       "secret_key": AgentCodeClass.shared.agentSecretKey]
            return headers
//        case .GetAgentDBKeys(_), .GenerateOrder(_), .OrderDetail(_), .RateProduct(_):
//            let header = [
//                "secretdbkey":APIConstants.secretdbkey,
//                "Authorization": /(GDataSingleton.sharedInstance.loggedInUser?.token)
//            ]
//            return header
            
        case .getSecretKey(_) : return [:]
           //Nitin
        default:
            let token = /(GDataSingleton.sharedInstance.loggedInUser?.token)
            if token.isEmpty {
                return ["secretdbkey":AgentCodeClass.shared.clientSecretKey]

            }
//             return ["secretdbkey":"55c55b26868b539a6444fce7ab5c91b366ed76e6729ef5926db69aeabc73e00f",
//                     "Authorization": /(GDataSingleton.sharedInstance.loggedInUser?.token)
//            ]
            return [
                "secretdbkey":AgentCodeClass.shared.clientSecretKey,
                "Authorization": /(GDataSingleton.sharedInstance.loggedInUser?.token),
            ]
        }
    }
    
}
