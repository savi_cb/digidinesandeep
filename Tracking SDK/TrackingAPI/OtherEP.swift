//
//  OtherEP.swift
//  Nequore
//
//  Created by MAC_MINI_6 on 17/07/18.
//  Copyright © 2018 Code-Brew Labs. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import SwiftyJSON

enum OtherEP {
    case polyLineRoutes(sourceLat: String? , sourceLng: String? , destLat: String? , destLng: String?)
}

extension OtherEP: TargetType, AccessTokenAuthorizable {
    
    var authorizationType: AuthorizationType {
        return .bearer
    }
    
    var headers: [String : String]? {
        return [:]
    }
    
    var multipartBody: [MultipartFormData]? {
        switch self {
            
        default:
            return nil
        }
    }
    
    var baseURL: URL {
        switch self {
        case .polyLineRoutes(_):
            return URL(string: "https://maps.googleapis.com/maps/api/directions/")!
       
        }
    }
    
    var path: String {
        switch self {
        case .polyLineRoutes(_):
            return TrackingConstants.polyLine
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .polyLineRoutes(_): return .get
        
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .polyLineRoutes(let sourceLat , let sourceLng , let destLat , let destLng):
            var dict = ["sensor" : "true" , "key" : GoogleApiKey]
            dict["origin"] = "\(/sourceLat),\(/sourceLng)"
            dict["destination"] = "\(/destLat),\(/destLng)"
            return dict
        }
    }
    
    var task: Task {
        switch self {
        default:
            return .requestParameters(parameters: parameters ?? [:], encoding: URLEncoding.default)
        }
    }
}


extension TargetType {
    func parse(response : [String : Any]?) -> AnyObject? {
        switch self {
        case is OtherEP:
            let target = self as! OtherEP
            switch target {
            case .polyLineRoutes(_):
                 return PolyRootClass(map: Map(mappingType: .fromJSON, JSON: response!))
                
            }
        default:
            return nil
        }
    }
}

