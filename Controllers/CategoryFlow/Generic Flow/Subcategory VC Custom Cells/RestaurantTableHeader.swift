//
//  myView.swift
//  testing
//
//  Created by Apple on 04/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class RestaurantTableHeader: UIView {
    
    //MARK:- IBOutlet
    @IBOutlet weak var buttonBack: ThemeButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var imgRating: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!{
        didSet{
            pageControl.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            pageControl.isHidden = !AppSettings.shared.isFoodApp
        }
    }
    @IBOutlet weak var btnManu: UIButton! {
        didSet{
            btnManu.setBackgroundColor(SKAppType.type.color, forState: .normal)
           // btnManu.isUserInteractionEnabled = false
            if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.catalogue.rawValue) as? String{
                btnManu.setTitle(term, for: .normal)
            }
        }
    }
    @IBOutlet weak var lblBlank: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.tintColor = SKAppType.type.color
        }
    }
    @IBOutlet weak var favourite_button: UIButton!
    
    //MARK:- Variables
    var imageDelegate : ImageClickListenerDelegate?
    var collectionDataSource = SupplierInfoHeaderDataSource()
    var supplier : Supplier? {
        didSet{
            
            configureCollectionView()
            lblLocation.text = /supplier?.address
            lblRating.text = "\(/supplier?.rating?.toDouble())"
            lblTitle.text = /supplier?.name
            
//            let imge = UIImage(asset : Asset.Ic_star_small_yellow)?.maskWithColor(color: getColor(rating: /supplier?.rating?.toInt()))
//            imgRating.image = imge
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        commonInit()
        collectionView.register(UINib(nibName: CellIdentifiers.SupplierInfoHeaderCollectionCell,bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell)

    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit(){
        Bundle.main.loadNibNamed("RestaurantTableHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight , .flexibleWidth]
        
    }
    
    @IBAction func favourite_buttonAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {

            APIManager.sharedInstance.opertationWithRequest(withApi: API.UnFavoriteSupplier(FormatAPIParameters.UnFavoriteSupplier(supplierId: supplier?.id).formatParameters()), completion: { [weak self] (response) in
                
                guard let self = self else { return }
                
                print(response)

                switch response{
                case APIResponse.Success( _):
                    print("Done")
                    
                default : sender.isSelected = !sender.isSelected

                }
            })
            
        } else {

            APIManager.sharedInstance.opertationWithRequest(withApi: API.MarkSupplierFav(FormatAPIParameters.MarkSupplierFavorite(supplierId: supplier?.id).formatParameters()), completion: { [weak self] (response) in

                guard let self = self else { return }

                print(response)
                
                switch response{
                case APIResponse.Success( _):
                    print("Done")
                    
                default : sender.isSelected = !sender.isSelected
                    
                }
            })
        }
        
    }
}

extension RestaurantTableHeader {
    
    func configurePageControl(scrollView : UIScrollView){
        
        let page = Int(collectionView.contentOffset.x/collectionView.frame.width)
        pageControl.currentPage = page
    }
}

extension RestaurantTableHeader {
    
    func configureCollectionView(){
        
        imgTitle.loadImage(thumbnail: supplier?.supplierImages?.first, original: nil)
        //        let pages = supplier?.supplierImages?.count ?? 0
        
        let images = supplier?.supplierImages ?? []
        pageControl?.numberOfPages = images.count
        
        collectionDataSource = SupplierInfoHeaderDataSource(
            items: images,
            tableView: collectionView,
            cellIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell,
            headerIdentifier: nil,
            cellHeight: collectionView.frame.size.height,
            cellWidth: ScreenSize.SCREEN_WIDTH,
            configureCellBlock: {
                (cell, item) in
                
                if let cell = cell as? SupplierInfoHeaderCollectionCell,
                    let imageUrl = item as? String
                {
                    cell.imageViewCover.loadImage(thumbnail: imageUrl, original: nil)
                }
                
        }, aRowSelectedListener: {
            [weak self] (indexPath) in
            guard let self = self else { return }
            
            self.imageDelegate?.imageCliked(atIndexPath: indexPath, cell: self.collectionView?.cellForItem(at: indexPath), images: self.imagesToSKPhotoArray(withImages: images, caption: nil) ?? [])
            
        }) { [weak self] (scrollView) in
            guard let self = self else { return }
            
            self.configurePageControl(scrollView: scrollView)
        }
        
        collectionView.delegate = collectionDataSource
        collectionView.dataSource = collectionDataSource
    }
}
