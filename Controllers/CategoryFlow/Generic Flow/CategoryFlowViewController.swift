//
//  CategoryFlowViewController.swift
//  Clikat
//
//  Created by cbl73 on 5/6/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class PassedData {
    
    var productId : String?
    var supplierId : String?
    var supplier : Supplier?
    var supplierBranchId : String?
    var categoryId : String? {
        didSet {
            if mainCategoryId == nil {
                mainCategoryId = categoryId
            }
        }
    }
    var mainCategoryId: String?
    var subCategoryId : String?
    var subCategoryName : String?
    var categoryFlow : String?
    var categoryOrder : String?
    var categoryName : String?
    var hasSubCats = true
    var subCats : [Categorie]?
    var isQuestion = false

    convenience init(withCatergoryId categoryId : String? , categoryFlow : String? , supplierId : String? , subCategoryId : String? , productId : String? , branchId : String?,subCategoryName : String? ,categoryOrder : String?,categoryName : String?, hasSubCats: Bool = true, isQuestion: Bool = false){
        self.init()
       if mainCategoryId == nil {
           mainCategoryId = categoryId
       }
        self.categoryId = categoryId
        self.categoryFlow = categoryFlow
        self.supplierId = supplierId
        self.categoryId = categoryId
        self.subCategoryId = subCategoryId
        self.supplierBranchId = branchId
        self.productId = productId
        self.subCategoryName = subCategoryName
        self.categoryOrder = categoryOrder
        self.categoryName = categoryName
        self.hasSubCats = hasSubCats
        self.isQuestion = isQuestion
    }
    init(){
    }
}

class CategoryFlowBaseViewController : BaseViewController {
    
    var passedData : PassedData = PassedData()
    var laundryData : OrderSummary?
    func pushNextVc(){
        do {
            
            let nextVC = try CategoryMapping.nextViewController(withFlow: passedData.categoryFlow, hasSubCat: passedData.hasSubCats, currentViewController: self) as? CategoryFlowBaseViewController
            guard let tempNextVc = nextVC else{
                return
            }
            FilterCategory.supplierId = passedData.supplierId
//passedData.supplierBranchId
            let passedD = PassedData(withCatergoryId: passedData.categoryId , categoryFlow:  passedData.categoryFlow ,supplierId: passedData.supplierId ,subCategoryId: passedData.subCategoryId , productId: passedData.productId , branchId: nil ,subCategoryName : passedData.subCategoryName ,categoryOrder:  passedData.categoryOrder, categoryName:passedData.categoryName, hasSubCats: passedData.hasSubCats)
            passedD.mainCategoryId = passedData.mainCategoryId
            nextVC?.passedData = passedD
            if !(nextVC is ItemListingViewController) {
                nextVC?.passedData.supplierBranchId = passedData.supplierBranchId//TODO: Maybe only for .home service
            }
            nextVC?.passedData.isQuestion = passedData.isQuestion
            nextVC?.laundryData = laundryData
            
//            if SKAppType.type == .home && passedData.isQuestion {
//                    getQuestions(supplier: supplier, supplierId: supplierId, supplierBranchId: supplierBranchId)
//                }
//                else {
//                    SupplierListingViewController.showProducts(supplier: supplier, supplierId: supplierId, supplierBranchId: supplierBranchId)
//                }
//            func getQuestions(supplier: Supplier, supplierId: String, supplierBranchId: String)  {
//
//                let vc = QuestionsViewController.getVC(.options)
//                vc.categoryId =  passedData.subCategoryId ?? passedData.categoryId
//                vc.completionBlock = { questions in
//                    supplier.associatedQuestions = questions
//                }
//                vc.poppedBlock = {
//                    if (supplier.associatedQuestions?.count ?? 0) > 0 {
//                        SupplierListingViewController.showProducts(supplier: supplier, supplierId: supplierId, supplierBranchId: supplierBranchId)
//                    }
//                }
//                ez.topMostVC?.pushVC(vc)
//            }
            
            if let tempNextVc = tempNextVc as? SupplierListingViewController {
                
                func proceed(questions: [Question]? = nil) {
                    GDataSingleton.sharedInstance.currentSupplier = nil
                    self.isLoadingFirstTime = true
                    tempNextVc.startSkeletonAnimation(tempNextVc.tableView)
                    SupplierListingViewController.getSuppliers(categoryId: passedD.mainCategoryId, subCategoryId: SKAppType.type == .home ? nil : passedD.subCategoryId, order: laundryData) {
                        [weak self] (array) in
                        guard let self = self else { return }
                        array.forEach({ $0.associatedQuestions = questions })
                        if let supplierId = self.passedData.supplierId, !supplierId.isEmpty {
                            tempNextVc.suppliers = array.filter({$0.id == supplierId})
                        }
                        else {
                            tempNextVc.suppliers = array
                        }
                        if (tempNextVc.suppliers?.count ?? 0) == 1 {
                            tempNextVc.view.alpha = 0.0
                            self.navigationController?.pushViewController(tempNextVc, animated: false)
                            tempNextVc.itemClicked(atIndexPath: IndexPath(row: 0, section: 0))

                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                if let ind = self.navigationController?.viewControllers.firstIndex(where: { $0 is SupplierListingViewController }) {
                                    self.navigationController?.viewControllers.remove(at: ind)
                                }
                            })
                        } else {
                            self.pushVC(tempNextVc)
                        }
                    }
                }
                
                if SKAppType.type == .home && passedData.isQuestion {
                    var questionsSelected: [Question]?
                    let vc = QuestionsViewController.getVC(.options)
                    vc.categoryId =  passedData.subCategoryId ?? passedData.categoryId
                    vc.completionBlock = { questions in
                        questionsSelected = questions
                    }
                    vc.poppedBlock = { completed in
                        if completed && (questionsSelected?.count ?? 0) > 0 {
                            proceed(questions: questionsSelected)
                        }
                        else {
                            FilterCategory.shared.arraySubCatIds.removeAll()
                            FilterCategory.shared.arraySubCatNames.removeAll()
                            FilterCategory.shared.arrayBrandId.removeAll()
                            if SKAppType.type == .home {
                                // to select questions again
                                GDataSingleton.sharedInstance.currentSupplier = nil
                            }
                        }
                    }
                    ez.topMostVC?.pushVC(vc)
                }
                else {
                    proceed()
                }
                
                return
            }
            pushVC(tempNextVc)
        }
        catch let exception{
            print(exception)
        }
    }
    
    func openCategory(category: ServiceType?) {
        
        if SKAppType.type.isJNJ {
            let VC = RestaurantDetailVC.getVC(.splash)
            // VC.passedData.supplierBranchId = notification.userInfo?["branchId"]?.stringValue
            VC.passedData.supplierId = "5"
            VC.passedData.supplierBranchId = "1"
            VC.passedData.categoryId = category?.id
            VC.passedData.categoryName = category?.name

            self.pushVC(VC)
            return
        }
        if SKAppType.type == .home {
            category?.category_flow = "Category>SubCategory>Suppliers>Pl"
        }
        if let flowComponents = category?.category_flow?.components(separatedBy: ">") {
            print(flowComponents)
            AdjustEvent.CategoryDetail.sendEvent()
            
            if flowComponents.contains(CategoryFlowMap.PickUpTime.rawValue)
                || flowComponents.contains(CategoryFlowMap.PackageProducts.rawValue)
                || flowComponents.contains(CategoryFlowMap.LaundryOrder.rawValue)
                || flowComponents.count == 0 {
                
                guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else {
                    self.presentVC(StoryboardScene.Register.instantiateLoginViewController())
                    return
                }
                
                self.passedData = PassedData(withCatergoryId: category?.id, categoryFlow: category?.category_flow,supplierId: nil ,subCategoryId: nil ,productId: nil,branchId: nil,subCategoryName: nil , categoryOrder: category?.order,categoryName : category?.name)
                self.pushNextVc()
                
            } else{
                
                ez.runThisInMainThread {
                    [weak self] in
                    guard let self = self else { return }
                    
                    //                    let category = Localize.currentLanguage() == Languages.Arabic ? self?.home?.arrayServiceTypesAR?[indexPath.row] : self?.home?.arrayServiceTypesEN?[indexPath.row]
                    
                    FilterCategory.shared.reset()
                    
                    FilterCategory.shared.arrayCatIds = [/category?.id]
                    FilterCategory.shared.arrayCatNames = [/category?.name]
                    
                    let hasSubCats = (/category?.sub_category?.count > 0) || category?.is_subcategory == 1
                    
                    //To apply filter/enable filter done button, when no sub categories available
                    if !hasSubCats {
                        FilterCategory.shared.arraySubCatIds.append(/category?.id)
                        FilterCategory.shared.arraySubCatNames.append(/category?.name)
                    }
                    
                    FilterCategory.shared.agentListFlow = category?.agent_list
                    var catFlow = category?.category_flow
                    
                    if SKAppType.type != .food {
                        let str = "SupplierInfo>"
                        let newFlow = catFlow?.replacingOccurrences(of: str, with: "")
                        print(newFlow)
                        catFlow = newFlow
                    }
                    //Nitin
                    
                    self.passedData = PassedData(withCatergoryId: category?.id, categoryFlow: catFlow,supplierId: category?.supplierId ,subCategoryId: category?.id ,productId: nil,branchId: nil,subCategoryName: nil , categoryOrder: category?.order, categoryName : category?.name, hasSubCats: hasSubCats)
                    self.pushNextVc()
                }
                
            }
        }
    }
}



