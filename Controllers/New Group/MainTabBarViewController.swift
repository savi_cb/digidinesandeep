//
//  MainTabBarViewController.swift
//  Sneni
//
//  Created by Apple on 18/08/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    var deliveryType: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.tabBar.tintColor = SKAppType.type.color
        
        var homeVc = UIViewController()
        
        homeVc = UINavigationController(rootViewController: NFCViewController.getVC(.digiHome))
        // SKAppType.type == .carRental ? StoryboardScene.Main.instantiateRoyorentalHomeController() : DigiDineViewController.getVC(.digiHome)
        
        let homeItem =  UITabBarItem(title: "Home".localized(), image: #imageLiteral(resourceName: "digiDineHomeInactive"), tag: 0)
        homeItem.selectedImage = #imageLiteral(resourceName: "digiDineHomeActive")
        homeVc.tabBarItem = homeItem
        
        var vc = UIViewController()
        var item : UITabBarItem?
        if SKAppType.type == .carRental {
            vc = StoryboardScene.Main.instantiateRentalFavouritesController()
            item = UITabBarItem(title: "Favourites".localized(), image: #imageLiteral(resourceName: "digiCartInactive"), tag: 1)
            item?.selectedImage = UIImage(named: "favouriteSelected")
        } else {
            vc = StoryboardScene.Order.instantiateCartViewController()
            item = UITabBarItem(title: "Current Order".localized(), image: #imageLiteral(resourceName: "digiCartInactive"), tag: 1)
            item?.selectedImage = #imageLiteral(resourceName: "digiCartActive")
        }
        vc.tabBarItem = item
        
        var titl = ""
        if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.orders.rawValue) as? String{
            titl = term
        }
        let vc1 = StoryboardScene.Order.instantiateOrderHistoryViewController()
        let item1 = UITabBarItem(title: titl, image: #imageLiteral(resourceName: "ordersUnselected"), tag: 2)
        item1.selectedImage = #imageLiteral(resourceName: "ordersSelected")
        vc1.tabBarItem = item1
        var vc2 = UIViewController()
        
        vc2 = DigiDineProfileController.getVC(.digiHome)
        let item2 = UITabBarItem(title: "Profile".localized(), image: #imageLiteral(resourceName: "digiProfileInactive"), tag: 3)
        item2.selectedImage = #imageLiteral(resourceName: "digiProfileActive")
        vc2.tabBarItem = item2
        
        if SKAppType.type == .eCom {
            var category = ""
            if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.categories.rawValue) as? String{
                category = term
            }
            let vc3 = CategoryTabVC.getVC(.main)
            let item3 = UITabBarItem(title: category, image: #imageLiteral(resourceName: "ic_categories_unactive"), tag: 2)
            item3.selectedImage = #imageLiteral(resourceName: "ic_categories")
            vc3.tabBarItem = item3
            
            self.viewControllers = [homeVc, vc3, vc, vc1, vc2]
        }
        else {
            self.viewControllers = [homeVc, vc, vc2] //DIGIDINE FLOW
        }
        
        getTermsAndConditions()
    }
    
    func getTermsAndConditions()  {
        let objR = API.getTermsAndConditions
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) { (response) in
            switch response{
            case APIResponse.Success(let object):
                if let data = object as? TermsResponse {
                    GDataSingleton.sharedInstance.termsAndConditions = data.array
                }
            default :
                break
            }
        }
    }
    
}

//MARK:- UITabBarControllerDelegate
extension MainTabBarViewController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CartNotification), object: self, userInfo: nil)
        
        if tabBarController.selectedIndex != 0 {
            if let vc = tabBarController.viewControllers?[selectedIndex] as? CartViewController {
                vc.hideBackButton = true
                vc.cartProdcuts = nil
                vc.deliveryType = self.deliveryType
            } else if let vc = tabBarController.viewControllers?[selectedIndex] as? OrderSummaryController {
                vc.hideBackButton = true
            }
        }
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch self.selectedIndex {
        case 0:
            if let vc = self.viewControllers?[0] as? HomeViewController {
                self.deliveryType = vc.deliveryType
            }
        default:
            break
        }
    }
    
}
