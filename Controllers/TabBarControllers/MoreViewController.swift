//
//  MoreViewController.swift
//  Sneni
//
//  Created by Apple on 20/08/19.
//  Copyright © 2019 Taran. All rights reserved.
//


import UIKit
import EZSwiftExtensions

class MoreViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var title_label: ThemeLabel!
    @IBOutlet weak var navigation_view: NavigationView!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            self.tableView.tableFooterView = UIView(frame: CGRect.zero)
            self.tableView.register(UINib(nibName: CellIdentifiers.MoreTableCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.MoreTableCell)
        }
    }
    @IBOutlet weak var login_and_sign_up_view: UIView!
    @IBOutlet weak var login_signup_btn: UIButton!
    @IBOutlet weak var profile_view: UIView!
    @IBOutlet weak var profile_btn: UIButton!
    @IBOutlet weak var profile_image: UIImageView! {
           didSet {
                profile_image.layer.cornerRadius = profile_image.frame.width/2.0
                profile_image.layer.borderWidth = 1.0
                profile_image.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            }
       }
    @IBOutlet weak var name_label: UILabel!
    @IBOutlet weak var email_label: UILabel!
   
    
    //MARK:- Variables
    var iconArray = [#imageLiteral(resourceName: "shareAppIcon"),#imageLiteral(resourceName: "myFavourites"),#imageLiteral(resourceName: "ic_referal_menu"),#imageLiteral(resourceName: "aboutUs"),#imageLiteral(resourceName: "termsAndConditions"),#imageLiteral(resourceName: "privacy_policy"),#imageLiteral(resourceName: "privacy_policy"),#imageLiteral(resourceName: "logoutIcon")]
    var titleArray = ["Share App", "My Wishlist", "Referral", "About Us", "Terms and Conditions", "Privacy Policy", "Change Language", "Logout"]
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tableView.reloadData()
        self.title_label.textColor = .white
        setUPUI()
    }
 
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    
    func setUPUI() {
        configureUIElements()
        self.name_label.text = GDataSingleton.sharedInstance.loggedInUser?.firstName
        self.email_label.text = GDataSingleton.sharedInstance.loggedInUser?.email
         profile_image.loadImage(thumbnail: GDataSingleton.sharedInstance.loggedInUser?.userImage, original: nil, placeHolder: UIImage(asset: Asset.User_placeholder))
    }
    
    func configureUIElements() {
       if GDataSingleton.sharedInstance.isLoggedIn == false {
        self.login_and_sign_up_view.isHidden = false
        self.profile_view.isHidden = true
                }
       else{
         self.login_and_sign_up_view.isHidden = true
         self.profile_view.isHidden = false
        }
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    
    @IBAction func loginSignUpBtnAction(_ sender: UIButton) {
        if /AppSettings.shared.appThemeData?.app_selected_template == "1" {
            let vc = StoryboardScene.Register.instantiateLoginNewVC()
            vc.delegate = self
            UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.presentVC(vc)
        }else {
            let vc = StoryboardScene.Register.instantiateLoginViewController()
            vc.delegate = self
            UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.presentVC(vc)
        }
        
    }

     
    @IBAction func profileBtnAction(_ sender: UIButton) {
        let settingsVC = StoryboardScene.Options.instantiateSettingsViewController()
        pushVC(settingsVC)
    }

    func logOut(){
        UtilityFunctions.showSweetAlert(title: L10n.LogOut.string, message: L10n.AreYouSureYouWantToLogout.string, success: { [weak self] in
            guard let self = self else {return}
            print(self)
            // LocationSingleton.sharedInstance.selectedAddress = nil
            // LocationSingleton.sharedInstance.searchedAddress = nil //Nitin check
            LocationSingleton.sharedInstance.tempAddAddress = nil
            LocationSingleton.sharedInstance.scheduledOrders = "0"
            GDataSingleton.sharedInstance.loggedInUser = nil
            //  GDataSingleton.sharedInstance.agentDBSecretKey = nil
            GDataSingleton.sharedInstance.autoSearchedArray = nil
            GDataSingleton.sharedInstance.showRatingPopUp = nil
            GDataSingleton.sharedInstance.pushDict = nil
            GDataSingleton.sharedInstance.deviceToken = nil
            GDataSingleton.sharedInstance.pickupAddress = nil
//            GDataSingleton.isOnBoardingDone = false
//            GDataSingleton.isAskLocationDone = false
            GDataSingleton.isProfilePicDone = false
            GDataSingleton.sharedInstance.fromCart = false
            // self.resetDefaults()
            DBManager.sharedManager.cleanCart()
            let cache = YYWebImageManager.shared().cache
            cache?.diskCache.removeAllObjects()
            cache?.memoryCache.removeAllObjects()
            let loginManager = LoginManager()
            loginManager.logOut()
            let address = LocationSingleton.sharedInstance.searchedAddress
            address?.id = ""
            LocationSingleton.sharedInstance.searchedAddress = address

           // (UIApplication.shared.delegate as? AppDelegate)?.moveHere()
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            appDelegate.switchViewControllers()

//            let navigationVc = StoryboardScene.Main.instantiateLeftNavigationViewController()
//            window.rootViewController = navigationVc
            
            //            if let vc = ez.topMostVC as? MoreViewController {
            //                guard let parentVc = vc.parent as? MainTabBarViewController else { return }
            //                parentVc.selectedIndex = 0
            //            }
            
        }) {
        }
    }
    
}

//MARK:-  UITableViewDelegate,UITableViewDataSource
extension MoreViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
        // return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell", for: indexPath) as! MoreTableViewCell
        
        cell.title_label.text = titleArray[indexPath.row].localized()
        cell.titleIcon_imageView.image = iconArray[indexPath.row]
        cell.titleIcon_imageView.tintColor = SKAppType.type.color
        cell.selectionStyle = .none
        return cell
    }
    
    //mark row height 0 where you dont wan to show
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let title = titleArray[indexPath.row]

        if !AppSettings.shared.showReferral && title == "Referral" {
            return 0
        }
        if !AppSettings.shared.showChangeLanguage && title == "Change Language" {
            return 0
        }
        if !GDataSingleton.sharedInstance.isLoggedIn {
            if title == "My Wishlist" || title == "Logout" || title == "Referral" {
                return 0
            }
        }
        if SKAppType.type != .eCom && title == "My Wishlist" {
            return 0
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let title = titleArray[indexPath.row]
        let languageId = GDataSingleton.sharedInstance.languageId
        
        let terms = GDataSingleton.sharedInstance.termsAndConditions?.first(where: {$0.languageId == languageId})
        switch indexPath.row {
            
//        case 0:
//
//            guard GDataSingleton.sharedInstance.isLoggedIn else{
//                let vc = StoryboardScene.Register.instantiateLoginViewController()
//                vc.delegate = self
//                UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.presentVC(vc)
//                return
//            }
//            let settingsVC = StoryboardScene.Options.instantiateSettingsViewController()
//            pushVC(settingsVC)
        case 0:
            UtilityFunctions.shareContentOnSocialMedia(withViewController: UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController, message: L10n.TheLeadingOnlineHomeServicesInUAE.string)
        case 1:
                  if title == "My Wishlist" {
                      let cartVC = WishListViewController.getVC(.main)
                      self.pushVC(cartVC)
                  }
                  else {
                      self.logOut()
                  }

        case 2:
            let storyboard = UIStoryboard(name: "Referal", bundle: nil)
            guard let vc =
                storyboard.instantiateViewController(withIdentifier: "ReferalDetailViewController") as? ReferalDetailViewController else { return }
            self.pushVC(vc)
        case 3:
            let vc = StoryboardScene.Options.instantiateTermsAndConditionsController()
            vc.titleStr = L10n.AboutUs.string
            vc.htmlStr = terms?.aboutUs ?? "No data found".localized()
            self.pushVC(vc)
        case 4:
            let vc = StoryboardScene.Options.instantiateTermsAndConditionsController()
            vc.titleStr = L10n.TermsAndConditions.string
            vc.htmlStr = terms?.terms ?? "No data found".localized()
            self.pushVC(vc)
        case 5:
            let vc = StoryboardScene.Options.instantiateTermsAndConditionsController()
            vc.titleStr = L10n.PrivacyPolicy.string
            vc.htmlStr = terms?.privacyPolicy ?? "No data found".localized()
            self.pushVC(vc)
        case 6:
            actionLanguage()
        case 7:
            self.logOut()
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    
    func actionLanguage() {
           
           if DBManager.sharedManager.isCartEmpty() {
               chooseLanguageAlert()
           } else {
               UtilityFunctions.showSweetAlert(title: L10n.AreYouSure.string, message: L10n.ChangingTheLanguageWillClearYourCart.string, style: AlertStyle.Success, success: { [weak self] in
                self?.chooseLanguageAlert()
               }) {
               }
           }
       }
    
    func chooseLanguageAlert() {
        let secLang = AppSettings.shared.appThemeData?.secondary_language == "es" ? "Spanish" : "Arabic"
        UtilityFunctions.show(nativeActionSheet: "Change Language", subTitle: nil, vc: self, senders: ["English", secLang]) { [weak self] (lang, index) in
            if index == 0 {
                self?.changeLanguage(language: "en")
            }
            else {
                self?.changeLanguage(language: AppSettings.shared.appThemeData?.secondary_language ?? "en")
            }
        }
    }
       
    func changeLanguage(language: String){
        let delegate = UtilityFunctions.sharedAppDelegateInstance()
        delegate.switchViewControllers(language: language)
        DBManager.sharedManager.cleanCart()
    }
}

//MARK:- LoginViewControllerDelegate
extension MoreViewController: LoginViewControllerDelegate {
    
    func userSuccessfullyLoggedIn(withUser user : User?) {
        DispatchQueue.main.async {
            self.setUPUI()
            self.tableView.reloadData()
        }
    }
    
    func userFailedLoggedIn() {
        
    }
}
