//
//  AgentTimeSlotVC.swift
//  Sneni
//
//  Created by Mac_Mini17 on 10/04/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit
import AMRatingControl

enum SlotTypeOfTime {
    case morning
    case evening
    case afternoon
    case night

    var title: String {
        switch self {
        case .morning: return "Morning"
        case .evening: return "Evening"
        case .afternoon: return "Afternoon"
        case .night: return "Night"
        }
    }
}

class AgentTimeSlotVC: BaseVC {
    
    //MARK:- ======== Outlets ========
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblCountReviews: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var viewRating : UIView! {
        didSet {
            productRate = 0

//            rateControl?.rating = 3
//            rateControl?.starWidthAndHeight = 12
//            viewRating.addSubview(rateControl ?? AMRatingControl())
//            rateControl?.isUserInteractionEnabled = false
        }
    }
    
    //MARK:- Variables
    var productRate : Int = 0 {
        didSet {
            
            defer {
                
                if !viewRating.subviews.isEmpty {
                    
                    viewRating.subviews.forEach({ $0.removeFromSuperview() })
                    rateControl = nil
                }
                
                let imge = UIImage(asset : Asset.Ic_star_small_yellow)?.maskWithColor(color: getColor(rating: productRate))
                rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), empty: UIImage(asset : Asset.Ic_star_small_grey), solidImage: imge, andMaxRating: 5)
                
                //                rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), empty: UIColor.gray, solidColor: getColor(rating: productRate), andMaxRating: 5)
                rateControl.frame = viewRating.bounds
                rateControl.isUserInteractionEnabled = false
                rateControl?.rating = productRate
                rateControl?.starWidthAndHeight = 12
                viewRating.addSubview(rateControl)
                
                //                ratingBgView.backgroundColor = .red
            }
            //            rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), empty: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)
            
            
        }
    }

    //MARK:- ======== Variables ========
    var objModel: AgentListingData?
    var blockDone: ((Date?) -> ())?
    var rateControl: AMRatingControl!// = AMRatingControl(location: CGPoint(x: 0,y: 0), empty: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)
    private let reuseIdentifier = "TimeSlotCell"
    
    
    //    var timeSlotter = TimeSlotter()
    //    var appointmentDate: Date!
    //    var formatter = DateFormatter()
    //    var timeSlots = [Date]()
    //
    let currentDate = Date()
    var selectedAgentId: String?
    var selectedIndex: Int = 0 {
        didSet {
            getSlots(agentId: selectedAgentId, date: arrayDates[selectedIndex])
        }
    }
    lazy var vcTab: UIViewController = {
        return UIViewController()
    }()
    var arrayDates = [Date]()
    var tabs: [ViewPagerTab] = []
    func setTabs() {
        var array: [ViewPagerTab] = []
        for date in arrayDates {
//            array.append(ViewPagerTab(title: currentDate.add(days: i).toString(format: Formatters.EEEddMMMM), image: UIImage(named: "")))
            array.append(ViewPagerTab(title: date.toString(format: Formatters.EEEddMMMM), image: UIImage(named: "")))
        }
        tabs = array
    }
    var arrayApiSlots = [String]() {
        didSet {
            let seletedDate = currentDate.add(days: selectedIndex).toString(format: Formatters.date)
            
            var arrayDates: [Date] = arrayApiSlots.map {
                (time) -> Date in
                return "\(seletedDate) \(time) \(Date().timeZone)".toDate(format: .YYYYMMDDHHMMSSZZZZZ) ?? currentDate
            }
            arrayDates.sort()
            
            morningSlots = []
            afternoonSlots = []
            eveningSlots = []
            nightSlots = []
            sectionTitle = []
            
            arrayDates.forEach {
                (date) in
                if date > Date().add(seconds: 60*60) {
                    let hour = Calendar.current.component(.hour, from: date)
                    switch hour {
                    case 0..<11 :
                        morningSlots.append(date)
                    case 12..<15 :
                        afternoonSlots.append(date)
                    case 16..<20 :
                        eveningSlots.append(date)
                    case 21..<23 :
                        nightSlots.append(date)
                    default: break
                    }
                }
            }
            
            if !morningSlots.isEmpty {
                sectionTitle.append(.morning)
            }
            
            if !afternoonSlots.isEmpty {
                sectionTitle.append(.afternoon)
            }
            
            if !eveningSlots.isEmpty {
                sectionTitle.append(.evening)
            }
            
            if !nightSlots.isEmpty {
                sectionTitle.append(.night)
            }
            
           // placeNoModeStots.isHidden = !sectionTitle.isEmpty
            collectionView.reloadData()
            
//            let seletedDate = currentDate.add(days: selectedIndex).toString(format: Formatters.date)
//
//            var arrayDates: [Date] = arrayApiSlots.map {
//                (time) -> Date in
//                return "\(seletedDate) \(time) \(Date().timeZone)".toDate(format: .YYYYMMDDHHMMSSZZZZZ) ?? currentDate
//            }
//            arrayDates.sort()
//            morningSlots = []
//            afternoonSlots = []
//            eveningSlots = []
//            sectionTitle = []
//            arrayDates.forEach {
//                (date) in
//
//                let hour = Calendar.current.component(.hour, from: date)
//                switch hour {
//                case 4..<12 :
//                    morningSlots.append(date)
//                case 12..<17 :
//                    afternoonSlots.append(date)
//                case 17..<22 :
//                    eveningSlots.append(date)
//                default://Night
//                    print(NSLocalizedString("Night", comment: "Night"))
//                }
//            }
//
//            if !morningSlots.isEmpty {
//                sectionTitle.append(.morning)
//            }
//
//            if !eveningSlots.isEmpty {
//                sectionTitle.append(.evening)
//            }
//
//            if !afternoonSlots.isEmpty {
//                sectionTitle.append(.afternoon)
//            }
//            collectionView.reloadData()
        }
    }
    
    var sectionTitle = [SlotTypeOfTime]()
    var morningSlots = [Date]()
    var eveningSlots = [Date]()
    var afternoonSlots = [Date]()
    var nightSlots = [Date]()
    var seletedTime: Date?
    var viewPager:ViewPagerController!
    var options:ViewPagerOptions!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTabs()
        lblTitle.text =   "\(L11n.select.string) \(SKAppType.type.agent)"
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        options = ViewPagerOptions(viewPagerWithFrame: topView.bounds)
        options.tabType = ViewPagerTabType.basic
        options.tabViewBackgroundDefaultColor = UIColor.white
        options.tabViewTextHighlightColor = UIColor.white
        options.tabViewTextDefaultColor = UIColor.black
        options.tabIndicatorViewBackgroundColor = UIColor.black
        options.tabViewImageSize = CGSize(width: 20, height: 20)
        options.tabViewTextFont = UIFont.systemFont(ofSize: 16)
        options.tabViewPaddingLeft = 20
        options.tabViewPaddingRight = 20
        options.isTabHighlightAvailable = false
        viewPager = ViewPagerController()
        viewPager.options = options
        viewPager.dataSource = self
        viewPager.delegate = self
        
        topView.addSubview(viewPager.view)
        
//        self.setupTimeSlotter()
        selectedIndex = 0
        
        lblName.text = objModel?.cblUser?.name
        
        let occ = objModel?.cblUser?.occupation ?? ""
        let exp = "\(/objModel?.cblUser?.experience)" + " " + L11n.yearsExperience.string
        lblOccupation.text = [occ, exp].joined(separator: " | ")
        lblCountReviews.text = "0"

        imgUser.loadImage(thumbnail: objModel?.cblUser?.image, original: nil, placeHolder: Asset.ic_dummy_user.image)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        options.viewPagerFrame = topView.bounds
    }

}

extension AgentTimeSlotVC{
    
    
//    func setupTimeSlotter() {
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
//
//        let formatter2 = DateFormatter()
//        formatter2.dateFormat = "hh:mm a"
//
//        let startDate = "20-08-2018 8:45 AM"
//        let endDate = "20-08-2018 07:00 PM"
//
//        let date1 = formatter.date(from: startDate)
//        let date2 = formatter.date(from: endDate)
//
//        sectionTitle = [
//            SlotTypeOfTime.morning,
//            .evening,
//            .afternoon
//        ]
//
//        var i = 1
//        while true {
//            let date = date1?.addingTimeInterval(TimeInterval(i*15*60))
//            let string = formatter2.string(from: date!)
//
//            if date! >= date2! {
//                break;
//            }
//
//            i += 1
//
//            let hour = Calendar.current.component(.hour, from: date!)
//            print("hour---------\(hour)")
//            switch hour {
//            case 6..<12 :
//                morningSlots.append(string)
//                print(NSLocalizedString("Morning", comment: "Morning"))
//            case 12 :
//                afternoonSlots.append(string)
//                print(NSLocalizedString("Noon", comment: "Noon"))
//            case 13..<17 :
//                afternoonSlots.append(string)
//                print(NSLocalizedString("Afternoon", comment: "Afternoon"))
//            case 17..<22 :
//                eveningSlots.append(string)
//                print(NSLocalizedString("Evening", comment: "Evening"))
//            default: print(NSLocalizedString("Night", comment: "Night"))
//            }
//        }
//
//
//        //
//        //        timeSlotter.configureTimeSlotter(openTimeHour: 9, openTimeMinutes: 0, closeTimeHour: 24, closeTimeMinutes: 0, appointmentLength: 30, appointmentInterval: 15)
//        ////        if let appointmentsArray = currentAppointments {
//        ////            timeSlotter.currentAppointments = appointmentsArray.map { $0.date }
//        //
//        //
//        //
//        ////        }
//        //
//        //
//        //        let dateComponents = NSDateComponents();
//        //        dateComponents.year = 2019
//        //        dateComponents.day = 11
//        //        let calendar = NSCalendar.current
//        //        let date = calendar.date(from: dateComponents as DateComponents)
//        //
//        //        print(date)
//        //
//        //        guard let timeSlots = timeSlotter.getTimeSlotsforDate(date: date!) else {
//        //            print("There is no appointments")
//        //            return }
//        ////
//        ////        sectionTitle.append("Morning")
//        ////        sectionTitle.append("Afternoon")
//        ////         sectionTitle.append("Evening")
//        ////
//        //
//        ////        for date in timeSlots {
//        ////
//        ////            let hour = Calendar.current.component(.hour, from: date)
//        ////            print(hour)
//        ////            switch hour {
//        ////            case 6..<12 :
//        ////                morningSlots.append(date)
//        ////
//        ////                print(NSLocalizedString("Morning", comment: "Morning"))
//        ////            case 12 : afternoonSlots.append(date)
//        ////
//        ////                print(NSLocalizedString("Noon", comment: "Noon"))
//        ////            case 13..<17 :
//        ////                afternoonSlots.append(date)
//        ////                print(NSLocalizedString("Afternoon", comment: "Afternoon"))
//        ////            case 17..<22 :
//        ////                eveningSlots.append(date)
//        ////                print(NSLocalizedString("Evening", comment: "Evening"))
//        ////            default: print(NSLocalizedString("Night", comment: "Night"))
//        ////            }
//        ////
//        ////
//        ////
//        ////        }
//        //
//        //        self.timeSlots = timeSlots
//    }
    
    
}

extension AgentTimeSlotVC: ViewPagerControllerDataSource, ViewPagerControllerDelegate {
    func viewControllerAtPosition(position: Int) -> UIViewController {
        return vcTab
    }
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    //    func viewControllerAtPosition(position:Int) -> UIViewController {
    //
    //         let vc = StoryboardScene.Order.instantiateAgentTimeSlotVC()
    //        return vc
    //    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
        selectedIndex = index
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to pagess \(index)")
        selectedIndex = index
    }
}

//MARK:- ======== UICollectionViewDelegate, UICollectionViewDataSource ========
extension AgentTimeSlotVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let title = sectionTitle[section]
        if title == .morning {
            return morningSlots.count
        }
        else if title == .afternoon {
            return afternoonSlots.count
        }
        else if title == .evening {
            return eveningSlots.count
        }
        else {
            return nightSlots.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? SectionHeader
        {
            
            let obj = sectionTitle[indexPath.section]
            sectionHeader.sectionHeaderlabel.text = obj.title
            return sectionHeader
        }
        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TimeSlotCell
        let section = sectionTitle[indexPath.section]

        var slot: Date?
        if section == .morning {
            slot =  morningSlots[indexPath.row]
        }
        else if section == .afternoon {
            slot =  afternoonSlots[indexPath.row]
        }
        else if section == .evening {
            slot =  eveningSlots[indexPath.row]
        }
        else {
            slot =  nightSlots[indexPath.row]
        }
        cell.date = slot
        cell.isSelect = seletedTime == slot
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if let cell = collectionView.cellForItem(at: indexPath) as? TimeSlotCell {
//            seletedTime = cell.date
//            collectionView.reloadData()
//        }
        
        //        if let destinationVC = self.navigationController?.viewControllers[0] as?  NewApptTableViewController {
        //            destinationVC.selectedTimeSlot = timeSlots[indexPath.row]
        //            self.navigationController?.popViewController(animated: true)
        //        } else if let destinationVC = self.navigationController?.viewControllers[0] as? UpdateApptTVC {
        //            destinationVC.selectedTimeSlot = timeSlots[indexPath.row]
        //            self.navigationController?.popViewController(animated: true)
        //        }
    }
}

//MARK:- ======== Actions ========
extension AgentTimeSlotVC{
    
    @IBAction func backBtnClick(sender:UIButton) {
//        self.popVC()
        self.dismissVC(completion: nil)
    }
    
    @IBAction func didTapBookNow(_ sender:UIButton) {
        //        self.popVC()
        
//        guard let seletedTime = seletedTime else {
//            print("Select Time Slot")
//            return
//        }
        
        self.dismissVC {
            [weak self] in
            guard let self = self else { return }
            self.blockDone?(nil)
        }
    }
}

//MARK:- ======== Api's ========
extension AgentTimeSlotVC{
    func getSlots(agentId: String?, date: Date){

        self.seletedTime = nil
        if false {
            self.arrayApiSlots = [
                "08:00:00",
                "08:15:00",
                "08:30:00",
                "08:45:00",
                "09:00:00",
                "09:15:00",
                "09:30:00",
                "09:45:00",
                "10:00:00",
                "10:15:00",
                "10:30:00",
                "10:45:00",
                "11:00:00",
                "11:15:00",
                "11:30:00",
                "11:45:00",
                "12:00:00",
                "12:15:00",
                "12:30:00",
                "12:45:00",
                "13:00:00",
                "13:15:00",
                "13:30:00",
                "13:45:00",
                "14:00:00",
                "14:15:00",
                "14:30:00",
                "14:45:00",
                "15:00:00",
                "15:15:00",
                "15:30:00",
                "15:45:00",
            ]
            return
        }
        
        let objR = API.getAgentAvailabilties(id: agentId, date: date)
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                if let obj = object as? AgentSlotListing {
                    self.arrayApiSlots = obj.array
                }
                
            default:
                break
            }
        }
    }
    
    class func getAvailabilty(id: String, block: (([Date]) -> ())?) {
        let objR = API.getAgentAvailabilty(id: id)//"61")
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            (response) in
            
            switch response {
            case .Success(let object):
                if let obj = object as? AvailabilityModel {
                    block?(obj.getAllDates())
                } else {
                    block?([])
                }
                
            default:
                block?([])
            }
        }
    }
}
