//
//  RentalProductSpecificationCollectionViewCell.swift
//  Sneni
//
//  Created by Apple on 02/11/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class RentalProductSpecificationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var specificationImage_imageView: UIImageView!
    @IBOutlet weak var specificationName_label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
