//
//  RentalHomeCollectionViewCell.swift
//  Sneni
//
//  Created by Apple on 01/11/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class RentalHomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var info_label: UILabel!
    @IBOutlet weak var month_label: UILabel!
    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var background_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
