//
//  PhoneNoViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import SKCountryPicker

enum CountryKeys : String {
    case name = "name"
    case dial_code = "dial_code"
    case code = "code"
}

class PhoneNoViewController: LoginRegisterBaseViewController {
    
    weak var delegate : LoginViewControllerDelegate?
    @IBOutlet weak var storyboardPickerView: CountryPickerView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var flagImage: UIImageView!
    
    @IBOutlet weak var stackReferral: UIStackView!
    @IBOutlet weak var textFieldReferal: TextField!
    @IBOutlet weak var btnOTP : UIButton!{
        didSet{
            self.btnOTP.setBackgroundColor(SKAppType.type.color, forState: .normal)
            btnOTP.kern(kerningValue: ButtonKernValue)
        }
    }
        
    var user : User?
    var lastStatusOfInvalidPhone = false
    var countryCode = String()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //tfPhoneNumber.dividerColor = TextFieldTheme.shared.txtFld_DividerColor
        
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        //        NotificationCenter.default.addObserver(self, selector: #selector(PhoneNoViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        //   NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        // self.countryPickerSetUp()
        stackReferral.isHidden = !AppSettings.shared.showReferral
        guard let country = CountryManager.shared.currentCountry else {
            //self.codeLabel.setTitle("Pick Country", for: .normal)
            return
        }
        codeLabel.text = country.dialingCode! + " " + country.countryCode
        self.countryCode = country.dialingCode ?? ""
        flagImage.image = country.flag
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func openPicker(_ sender: UIButton) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
            guard let self = self else { return }
            self.flagImage.image = country.flag
            self.codeLabel.text = country.dialingCode! + " " + country.countryCode
            self.countryCode =  country.dialingCode ?? ""
            
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
    }
    
}


//MARK: - Button Actions
extension PhoneNoViewController{
    
    @IBAction func actionSendOTP(sender: AnyObject) {
        view.endEditing(true)
//        if !lastStatusOfInvalidPhone{
//            SKToast.makeToast("Please enter valid phone number".localized())
//            return
//        }
        guard let phoneNumber = self.textField.text else { return }
        if Register.isValidPhoneNumber(testStr: phoneNumber) {
            let mobileNumber = phoneNumber
            APIManager.sharedInstance.opertationWithRequest(withApi: API.SendOTP(FormatAPIParameters.SendOTP(mobileNumber: mobileNumber,countryCode: self.countryCode, referalCode: /self.textFieldReferal.text ).formatParameters()), completion: {
                [weak self] (response) in
                guard let self = self else { return }

                switch response {
                case APIResponse.Success(let object):
                    if SKAppType.type.isJNJ {
                        self.actionSubmitOTP(user: object as? User)
                        return
                    }
                    var user : User?
                    user = object as? User
                    user?.mobileNo = phoneNumber
                    user?.fbId = self.user?.fbId
                    user?.countryCode = self.countryCode
                    self.handleOTPSentResponse(tempUser: user)
                    break
                default:
                    break
                }
            })
        }else{
            SKToast.makeToast("Please enter phone number".localized())
        }
    }
    
    
    func handleOTPSentResponse(tempUser : Any?){
        
        let VC = StoryboardScene.Register.instantiateOTPViewController()
        VC.user = tempUser as? User
        VC.delegate = delegate
        presentVC(VC)
    }
    
    func actionSubmitOTP(user: User?) {
        
        let objR = API.CheckOTP(FormatAPIParameters.CheckOTP(accessToken :user?.token ,OTP: "12345").formatParameters())
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case APIResponse.Success(_):
                self.handleCheckOTPResponse()
                
            case APIResponse.Failure(_):
                break
            }
            
        }
    }
    
    func handleCheckOTPResponse(){
        let VC = StoryboardScene.Register.instantiateRegisterViewController()
        user?.countryCode = self.countryCode
        VC.user = user
        VC.delegate = delegate
        presentVC(VC)
    }
}
    



//extension PhoneNoViewController{
//
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//
//         tfPhoneNumber.dividerNormalColor = UIColor.black
//        print("Hiiiiiii")
//
//        }
//
//
//}



////MARK: - CountryPicker Delegate
//extension PhoneNoViewController:MRCountryPickerDelegate{
//
//    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
//         tfCountryCode.text = phoneCode
//
//    }
////        self.countryName.text = name
////        self.countryCode.text = countryCode
////        self.phoneCode.text = phoneCode
////        self.countryFlag.image = flag
////    }
//
//}
