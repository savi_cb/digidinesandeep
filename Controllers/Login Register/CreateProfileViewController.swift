//
//  CreateProfileViewController.swift
//  Sneni
//
//  Created by osvin-k01 on 4/29/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class CreateProfileViewController: LoginRegisterBaseViewController {

    var user : User?
    weak var delegate : LoginViewControllerDelegate?
    
    @IBOutlet weak var nameTextfield: UITextField!
    
    @IBOutlet weak var btnSubmit : UIButton!{
        didSet{
            self.btnSubmit.setBackgroundColor(SKAppType.type.color, forState: .normal)
            btnSubmit.kern(kerningValue: ButtonKernValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func actionFinishSignUp(sender: AnyObject) {
        let message = validateCredentials()
        if message.isEmpty {
            
            APIManager.sharedInstance.showLoader()
            let objR = API.RegisterLastStep(FormatAPIParameters.RegisterLastStep(accessToken : user?.token, name: nameTextfield.text ?? "").formatParameters())
            APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
                [weak self] (response) in
                guard let self = self else { return }
//                let userNew = object as? User
//                userNew?.fbId = self.user?.fbId
//                GDataSingleton.sharedInstance.loggedInUser = userNew
                
                AdjustEvent.SignUp.sendEvent()
                var VC = self.presentingViewController
                while ((VC?.presentingViewController) != nil) {
                    VC = VC?.presentingViewController
                }
                if VC is LoginViewController && VC?.presentingViewController == nil {
                    (UIApplication.shared.delegate as? AppDelegate)?.userSuccessfullyLoggedIn(withUser: nil)
                    GDataSingleton.isProfilePicDone = true
                    VC?.dismissVC{}
                }
                else {
                    self.delegate?.userSuccessfullyLoggedIn(withUser: nil)
                    GDataSingleton.isProfilePicDone = true
                    VC?.dismissVC{}
                }
                
            }
//                {
//                [weak self] (response) in
//                APIManager.sharedInstance.hideLoader()
//                guard let `self` = self else { return }
//                switch response {
//                case .Success(let object):
//
//                    let userNew = object as? User
//                    userNew?.fbId = self.user?.fbId
//                    GDataSingleton.sharedInstance.loggedInUser = userNew
//
//                    AdjustEvent.SignUp.sendEvent()
//                    var VC = self.presentingViewController
//                    while ((VC?.presentingViewController) != nil) {
//                        VC = VC?.presentingViewController
//                    }
//                    if VC is LoginViewController && VC?.presentingViewController == nil {
//                        (UIApplication.shared.delegate as? AppDelegate)?.userSuccessfullyLoggedIn(withUser: nil)
//                        GDataSingleton.isProfilePicDone = true
//                        VC?.dismissVC{}
//                    }
//                    else {
//                        self.delegate?.userSuccessfullyLoggedIn(withUser: nil)
//                        GDataSingleton.isProfilePicDone = true
//                        VC?.dismissVC{}
//                    }
//                default:
//                    break
//                }
//            }
        }else {
            SKToast.makeToast(message)
        }
    }
}
extension CreateProfileViewController {
    
    func validateCredentials() -> String{
        guard let name = nameTextfield.text, name.trim().count != 0 else {
            
            return L10n.PleaseEnterYourName.string
        }
//        guard let _ = imageViewProfilePic.image else {
//            return L10n.PleaseSelectYourProfilePicture.string
//        }
        return ""
    }
}
