//
//  AgentListingVC.swift
//  Sneni
//
//  Created by Mac_Mini17 on 10/04/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit
typealias SelectedAgentBlock = (CblUser) -> ()

class AgentListingVC: BaseViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var viewNoData: UIView!
    
    var arrayServiceId = [Int]()
    var arrayCart:[Cart]?
    
//    var orderSummary : OrderSummary?
    var selectedDate : Date = Date()
    var timeInterVal : Double = 0
    
    
    var remarks : String?
    var completion: SelectedAgentBlock?
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView()
        }
    }
    
    var tableDataSource : TableViewDataSource?{
        didSet{
            tableView.reloadData()
        }
    }
    
    var agentListing:AgentListing?{
        didSet{
            self.configureTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text =   "\(L11n.select.string) \(SKAppType.type.agent)"
        
        for cart in arrayCart ?? []{
            
            arrayServiceId.append(cart.id?.toInt() ?? 0)
        }
        
        self.webServiceGetAgentDBKeys()
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AgentListingVC{
    
    
    func webServiceGetAgentDBKeys (){
        let objR = API.GetAgentDBKeys(date: selectedDate, interval: timeInterVal)
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let _):
                self.webServiceAgentListing()
                
            default:
                break
            }
            
            print(response)
        }
    }
    
    
    func webServiceAgentListing()  {
        
        let objR = API.ServiceAgentlist(FormatAPIParameters.ServiceAgentlist(serviceIds: arrayServiceId, date: selectedDate, interval: Int(timeInterVal)).formatParameters())
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                self.agentListing = object as? AgentListing
//                print(self.agentListing?.agentListingData)
                
            default:
                break
            }
            
            print(response)
        }
        
    }
    
}


extension AgentListingVC{
    
    // MARK: - configureTableView
    func pushToDlivery(agent: CblUser?) {
//        self.orderSummary?.agentId = agentId///agentListingData?.cblUser?.id
        if let agent = agent {
            popVC()
            completion?(agent)
        }

//        
//        let branchId = orderSummary?.items?.first?.supplierBranchId
//        DeliveryViewController.getUserAdresses(isAgent: (/self.orderSummary?.isAgent), branchId: branchId) {
//            [weak self] (delivery) in
////            let deliveryAddress = GDataSingleton.sharedInstance.pickupAddress
//
//            guard let self = self,
//                let deliveryAddressId = GDataSingleton.sharedInstance.pickupAddressId,//deliveryAddress?.id,
//                let pickUp = GDataSingleton.sharedInstance.pickupDate
//                else { return }
//            
//            delivery.initalizeDelivery(cart: self.orderSummary?.items)
//            delivery.handlingAdmin = self.orderSummary?.handlingCharges
//            delivery.handlingSupplier = self.orderSummary?.handlingSupplier
//
//            let deliveryDate = pickUp//.add(seconds: Int(60.0*self.timeInterVal))
//            
//            DeliveryViewController.updateCart(
//                delivery: delivery,
//                order: self.orderSummary,
//                deliverySpeed: DeliverySpeedType.Standard,
//                addressId: deliveryAddressId,
//                deliveryDate: deliveryDate,
//                remarks: self.remarks) {
//                    [weak self] in
//                    guard let self = self else { return }
//                    APIManager.sharedInstance.hideLoader()
//                    let VC = StoryboardScene.Order.instantiateOrderSummaryController()
//                    VC.orderSummary = self.orderSummary
//                    VC.remarks = self.remarks
//                    self.pushVC(VC)
//            }
//            
//        }
    }
    func configureTableView(){
        if (agentListing?.agentListingData?.count ?? 0) == 0 {
            viewNoData.isHidden = false
            return
        }
        tableDataSource = TableViewDataSource(items: agentListing?.agentListingData, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.AgentListingTblCell, configureCellBlock: {
            [weak self] (cell, item) in
            guard let self = self else { return }
            self.configureCell(cell: cell, item: item)
            }, aRowSelectedListener: {
                (indexPath) in
                
                let agentListingData =  self.agentListing?.agentListingData?[indexPath.row]
//                self.viewSlots(agentListingData: agentListingData)

                
                
                
               
                
                
                
                
                
                //                let agentListingData =  weak?.agentListing?.agentListingData?[indexPath.row] as? AgentListingData
                //                let VC = StoryboardScene.Order.instantiateDeliveryViewController()
                //                VC.orderSummary = OrderSummary(items: self?.arrayCart)
                //                VC.orderSummary?.cartId = agentListingData?.cblUser.
                //                //VC.orderSummary?.minOrderAmount = (tempCartId as? String)?.components(separatedBy: "$").last
                //                //VC.remarks = remarks
                //                pushVC(VC)
                
                
        })
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
    }
    
    func viewSlots(agentListingData: AgentListingData?) {
        
        AgentTimeSlotVC.getAvailabilty(id: /agentListingData?.cblUser?.id, block: {
            [weak self] (dates) in
            guard let self = self else { return }
            
            if dates.isEmpty {
                return
            }
            
            let vc = AgentTimeSlotVC.getVC(.order)
            vc.arrayDates = dates
            vc.objModel = agentListingData
            vc.selectedAgentId = /agentListingData?.cblUser?.id
            self.presentVC(vc)
            
            vc.blockDone = {
                [weak self] (dateSlot) in
                guard let self = self else { return }
                
//                self.orderSummary?.agentId = /agentListingData?.cblUser?.id
//                
//                if false {
//                    let VC = StoryboardScene.Order.instantiateDeliveryViewController()
//                    VC.orderSummary = self.orderSummary
//                    VC.orderSummary?.cartId =  self.orderSummary?.cartId
//                    VC.orderSummary?.minOrderAmount =  self.orderSummary?.minOrderAmount
//                    VC.orderSummary?.agentId =   agentListingData?.cblUser?.id
//                    VC.remarks = self.remarks
//                    self.pushVC(VC)
//                    
//                } else {
                    self.pushToDlivery(agent: agentListingData?.cblUser)
                    
//                }
                
            }
        })
    }
    
    func configureCell(cell : Any?,item : Any?){
        if let cell = cell as? AgentListingTblCell {
            cell.agentListingData = item as? AgentListingData
            
            cell.blockBookSlots = {
                [weak self] agent in
                guard let self = self else { return }
                self.pushToDlivery(agent: agent?.cblUser)
            }
            
            cell.blockViewSlots = {
                [weak self] agent in
                guard let self = self else { return }
                self.viewSlots(agentListingData: agent)
            }
        }
    }
    
}


extension AgentListingVC{
    
    @IBAction func backBtnClick(sender:UIButton){
        self.popVC()
    }
    
}
