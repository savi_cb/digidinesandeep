//
//  CardListingVC.swift
//  Sneni
//
//  Created by admin on 06/04/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

protocol CardListProtocol  {
    func updateList()
}

class CardListingVC: BaseViewController , CardListProtocol{
    
    //MARK::- OUTLETS
    @IBOutlet var btnAddCard: UIButton?{
        didSet{
            btnAddCard?.setBackgroundColor(SKAppType.type.color, forState: .normal)
        }
    }
    @IBOutlet weak var tblView: UITableView!
    
    //MARK::- PROPERTIES
    var tableDataSource : TableViewDataSource?{
        didSet{
            tblView.reloadData()
        }
    }
    var arrayCard : [CardDetails]?
    var amount = Double()
    var paymentDone: ((_ token: String?, _ card_id:String?) -> ())?
    
    //MARK::- VIEWCYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setCardDta()
        self.tblView.isHidden = false
        //hitGetCardApi()
    }
    
    //MARK::- ACTION
    //0 table, 1 non
    @IBAction func btnAddCard(_ sender: Any) {
        if GDataSingleton.sharedInstance.selfPickup == 1 || GDataSingleton.sharedInstance.isHotel == true{
            self.doPayment(cardId: -1)
        }else{
            let VC = ChoosePayViewControllerViewController.getVC(.digiHome)
            VC.amount = self.amount
            VC.paymentBool = false
            VC.comingWithNewCard = "true"
            self.pushVC(VC)
        }
    }
    
    
    //MARK: - Delegate function
    func updateList() {
        //hitGetCardApi()
    }
    
    //MARK::- FUNCTIONS
    func setCardDta(){
        tableDataSource = TableViewDataSource(items: arrayCard, height: 130 , tableView: tblView, cellIdentifier: CellIdentifiers.CardUICell, configureCellBlock: {  (cell, item) in
            if let item = item as? CardDetails, let cell = cell as? CardUICell{
                if (/item.cardholderName).isEmpty {
                    cell.lblCardHolderName?.text = /item.scheme
                    cell.lblCardHolderText?.text = "Card Type"
                }else {
                    cell.lblCardHolderText?.text = "Card Holder Name"
                    cell.lblCardHolderName?.text = /item.cardholderName
                }
                cell.lblCardNumber?.text = "Card Number " +  /item.maskedPan
                cell.lblExpDate?.text = "\(/item.expiry)"//\(/item.expYear)"
                cell.cardId = /"\(item.id)"
                cell.deleteCard = {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }, aRowSelectedListener: { [ weak self] (indexPath) in
            //self?.paymentDone?("" , /self?.arrayCard?[indexPath.row].id)
            if GDataSingleton.sharedInstance.isHotel == true || GDataSingleton.sharedInstance.selfPickup == 1{
                self?.doPayment(cardId: /self?.arrayCard?[indexPath.row].id)
            }else if GDataSingleton.sharedInstance.selfPickup == 0{
                let VC = ChoosePayViewControllerViewController.getVC(.digiHome)
                VC.amount = self?.amount ?? 0.0
                VC.paymentBool = true
                VC.cardId = /self?.arrayCard?[indexPath.row].id
                VC.comingWithNewCard = "false"
                self?.pushVC(VC)
            }
        })
        tblView.delegate = tableDataSource
        tblView.dataSource = tableDataSource
    }
    

  @IBAction func backBtn(_ sender: UIButton) {
    self.popVC()
  }
  
    func doPayment(cardId : Int){
        
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId) ?? 0
        APIManager.sharedInstance.showLoader()
        var params = [String : Any]()
        if cardId == -1{
            params = ["net_amount" : self.amount, "supplier_id" : supplierId] as [String : Any]
        }else{
            params = ["net_amount" : self.amount, "supplier_id" : supplierId, "userSavedCardId" : cardId ] as [String : Any]
        }
        
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/getPaymentLinkNonTable"
        print(url)
        print(params)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            let status = response["message"].string
            if(status == "Success"){
                print(response)
                if let dict = response.dictionaryObject {
                    if let obj = dict["data"] as? Dictionary<String,Any>{
                        if let paymentLink = obj["payment_href"]{
                            print(paymentLink)
                            if let userPayment = obj["userPaymentLinkId"]{
                                if cardId == -1{
                                    let VC = PaymentGateViewController.getVC(.digiHome)
                                    VC.paymentLink = "\(paymentLink)"
                                    VC.userPaymentLinkId = userPayment as? Int ?? 1
                                    VC.comingWithNewCard = "false"
                                    GDataSingleton.sharedInstance.userPaymentLinkId = userPayment as? Int ?? 1
                                    self.pushVC(VC)
                                }else{
                                    GDataSingleton.sharedInstance.userPaymentLinkId = userPayment as? Int ?? 1
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderNotifi"), object: nil, userInfo: nil)
                                    self.popVC()
                                }
                            }
                        }
                    }
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
            }else if status_code == "The network connection was lost."{
            }
        }
    }
}
