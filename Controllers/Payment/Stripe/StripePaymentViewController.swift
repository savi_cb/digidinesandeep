//
//  StripePaymentViewController.swift
//  Sneni
//
//  Created by Gagandeep Singh on 18/03/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import Stripe

class StripePaymentViewController: UIViewController {
    
    //MARK::- OUTLETS
    @IBOutlet var tfCardDetails: STPPaymentCardTextField!
    @IBOutlet var btnAddCard: UIButton?{
        didSet{
            btnAddCard?.setBackgroundColor(SKAppType.type.color, forState: .normal)
        }
    }
    
    //MARK::- PROPERTIES
    var paymentDone: ((_ token: String?) -> ())?
    
    //MARK::- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        self.modalPresentationStyle = .currentContext
        // Do any additional setup after loading the view.
    }
    
    //MARK::- ACTIONS
    func hitAPiTestStripe() {
        
        let cardParams = STPCardParams()
        cardParams.number = tfCardDetails.cardNumber
        cardParams.expMonth = UInt(/String(tfCardDetails.expirationMonth))!
        cardParams.expYear = UInt(/String(tfCardDetails.expirationYear))!
        cardParams.cvc = tfCardDetails.cvc
        
        STPAPIClient.shared().createToken(withCard: cardParams) {(token: STPToken?, error: Error?) in
            
            guard let token = token, error == nil else {
                print(/error?.localizedDescription)
                return
            }
            self.paymentDone?(token.tokenId)
            self.dismissVC(completion: nil)
            
            //      self.proceedCheckout()
            //            self.apiAddCard(token: "\(token)")
            
        }
    }
    
    
    @IBAction func actionAddCard(_ sender: UIButton) {
        
        
        if /self.tfCardDetails.cardNumber?.isBlank || /self.tfCardDetails.cvc?.isBlank {
            SKToast.makeToast("Please fill out all card details.")
            return
        }
        
        self.hitAPiTestStripe()
    }
    
    @IBAction func btnActionBack(_ sender: UIButton) {
        self.dismissVC(completion: nil)
    }
    
}
