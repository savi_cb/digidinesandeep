//
//  ReferalViewController.swift
//  Sneni
//
//  Created by Gagandeep Singh on 16/03/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit

class ReferalViewController: UIViewController {
    
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var lblTapToCopy: UILabel!
    @IBOutlet weak var viewReferalBorder: UIView!
    @IBOutlet weak var btnRefer: UIButton!
    @IBOutlet weak var lblReferal: ElementLabel!
    @IBOutlet weak var viewTapBg: UIView!
    
    //MARK::- PROPERIES
    
    
    
    //MARK::- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    //MARK::- FUNCTIONS
    
    func updateUI(){
        lblTapToCopy.addTapGesture { (gesture) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = /GDataSingleton.sharedInstance.loggedInUser?.referral_id
        }
        lblReferal?.text = /GDataSingleton.sharedInstance.loggedInUser?.referral_id
        viewTapBg.backgroundColor = SKAppType.type.color
        btnRefer.backgroundColor = SKAppType.type.color
        viewReferalBorder.layer.borderColor = SKAppType.type.color.cgColor
    }
    
    
    //MARK::- ACTIONS
    
    @IBAction func btnActionreferNow(_ sender: UIButton) {
        UtilityFunctions.shareContentOnSocialMedia(withViewController: UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController, message: L10n.TheLeadingOnlineHomeServicesInUAE.string)
    }
}
