//
//  OrderHistoryViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

enum OrderType: String {
    case pending = "pending"
    case upcoming = "upcoming"
}

class OrderHistoryViewController: BaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var placeholder_label: ThemeLabel! {
        didSet {
            placeholder_label.textColor = SKAppType.type.color
            if APIConstants.defaultAgentCode == "yummy_0122"{
                if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.noOrderFound.rawValue) as? String{
                    self.placeholder_label.text = term
                }
            }else{
            if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.order.rawValue) as? String{
                self.placeholder_label.text = String(format: "No %@ found!".localized(), "\(term)")//"You have no past " + term
            }
            }
        }
    }
    @IBOutlet weak var bottom_view: UIView! {
        didSet{
            bottom_view.backgroundColor = SKAppType.type.color
        }
    }
    @IBOutlet weak var placeholder_imageView: UIImageView!{
        didSet{
            self.placeholder_imageView.isHidden = true
        }
    }

    @IBOutlet weak var menu_button: ThemeButton!{
        didSet {//Nitin
            menu_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? true : false
        }
    }
    @IBOutlet weak var top_view: UIView!
    @IBOutlet weak var bottomView_leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var title_label: ThemeLabel! {
        didSet {
            title_label.textColor = .white
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPlaceholder : UIView!
    
    @IBOutlet weak var back_button: ThemeButton!{
        didSet {//Nitin
            back_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? false : true
        }
    }
    
    //MARK:- Variable
    
    var isHiddenBack = true
    
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    var orderListing : OrderListing? {
        didSet{
            dataSource.items = orderListing?.orders
            tableView?.reloadTableViewData(inView: view)
            
            guard let orders = orderListing?.orders, orders.count > 0 else{
                viewPlaceholder?.isHidden = false
                return
            }
            viewPlaceholder?.isHidden = true
        }
    }
    var upcomingOrderListing : OrderListing?
    var orderType: OrderType = .pending
    private var refreshControl : UIRefreshControl?

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        //back_button.isHidden = isHiddenBack
        if let term = AppSettings.shared.appThemeData?.terminology?.returnValueForKey(key: TerminologyKeys.order.rawValue) as? String{
            self.labelTitle.text = term + " \("History".localized())"
        }
        tableView.register(UINib(nibName: CellIdentifiers.OrderParentCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.OrderParentCell)
        configureTableViewInitialization()

        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)

        tableView.addSubview(refreshControl!)
        
        self.webService()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.firstName else{
            viewPlaceholder.isHidden = false
            tableView.isHidden = true
            return
        }
        viewPlaceholder.isHidden = true
        tableView.isHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .default
       }
    
    @objc func refreshTableData() {
        
    }
    
    @IBAction func back_buttonAction(_ sender: Any) {
        popVC()
    }
}

//MARK: - Webservice Methods
extension OrderHistoryViewController {
    func webService(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderHistory(FormatAPIParameters.OrderHistory.formatParameters())) { (response) in
            
            weak var weak : OrderHistoryViewController? = self
            if self.refreshControl?.isRefreshing ?? false {
                self.refreshControl?.endRefreshing()
            }
            switch response{
                
            case .Success(let listing):
                self.orderType = .upcoming
                weak?.orderListing = listing as? OrderListing
                break
            default :
                break
            }
            
        }
        
    }
}


//MARK: - TableView Configuration
extension OrderHistoryViewController{
    
    func configureTableViewInitialization(){
        dataSource = TableViewDataSource(items: orderListing?.orders, height: 180, tableView: tableView, cellIdentifier: CellIdentifiers.OrderParentCell , configureCellBlock: {
            [weak self] (cell, item) in
            guard let self = self else { return }

            self.configureCell(withCell : cell , item : item)
            
            }, aRowSelectedListener: {
                [weak self] (indexPath) in
                guard let self = self else { return }
                if let sessionId =  self.orderListing?.orders?[indexPath.row].session_id{
                    if sessionId > 0 {
                        print("order summary")
                        let orderDetailVc = SelectProductToPayViewController.getVC(.digiHome)
                        orderDetailVc.sessionId = sessionId
                        orderDetailVc.isOrderHistory = true
                        self.pushVC(orderDetailVc)
                        
                    }else{
                        let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
                        orderDetailVc.type = self.orderType == .pending ? .OrderUpcoming : .OrderHistory
                        orderDetailVc.orderDetails = self.orderListing?.orders?[indexPath.row]
                        self.pushVC(orderDetailVc)
                    }
                }
        })
        tableView.reloadData()
    }
    
    func configureCell(withCell cell : Any , item : Any? ){
        
        (cell as? OrderParentCell)?.orderType = self.orderType
        (cell as? OrderParentCell)?.cellType = self.orderType == .pending ? .OrderUpcoming : .OrderHistory
        (cell as? OrderParentCell)?.order = item as? OrderDetails
        (cell as? OrderParentCell)?.orderDelegate = self
        
    }
    
    func itemClicked(withIndexPath : IndexPath){
        
    }
    
}

//MARK: - Button Actions
extension OrderHistoryViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
}

//MARK: - Order Parent cell delegate
extension OrderHistoryViewController : OrderParentCellDelegate {
    func actionOrderTypeButton(cell: OrderParentCell, order: OrderDetails?) {
        
        if cell.btnOrderType.titleLabel?.text == L10n.CANCELORDER.string {
            cancelOrder(orderId: /order?.orderId)
        }
        else {
            //Haspinder Singh
            //Check cart -> If cart == nil then direct reorder
            DBManager.sharedManager.getCart { (products) in
                if products.count > 0{
                    
                    UtilityFunctions.showAlert(title: nil, message: L10n.ReOrderingWillClearYouCart.string, success: {
                        weak var weakSelf = self
                        weakSelf?.reorderAllitems(orderDetails: order)
                    }) {
                        
                    }}
                else{
                    
                    self.reorderAllitems(orderDetails: order)
                }
            }
        }
        
    }
    
    func actionOrderTypeButton(cell: OrderParentCell, orderId: String?) {
        
    }
    
    func reorderAllitems(orderDetails : OrderDetails?){
        
        AdjustEvent.Reorder.sendEvent()
        //        DBManager.sharedManager.cleanCart()
        guard let products = orderDetails?.product else { return }
        //        GDataSingleton.sharedInstance.currentSupplierId = orderDetails?.supplierBranchId
        for product in products {
            guard let quantity = Int(product.quantity ?? "") else { return }
            product.supplierBranchId = orderDetails?.supplierBranchId
            DBManager.sharedManager.manageCart(product: product, quantity: quantity)
        }
        GDataSingleton.sharedInstance.currentCategoryId = products.first?.categoryId
        let VC = StoryboardScene.Options.instantiateCartViewController()
        pushVC(VC)
    }
    
    
//    func actionScheduleOrder() {
//
//        let schedularVC = StoryboardScene.Order.instantiateOrderSchedularViewController()
//        schedularVC.orderId = orderDetails?.orderId
//        pushVC(schedularVC)
//    }
    
    func cancelOrder(orderId: String) {
        
        UtilityFunctions.showSweetAlert(title: L10n.AreYouSure.string, message: L10n.DoYouReallyWantToCancelThisOrder.string, success: { [weak self] in
            self?.cancelOrderWebservice(orderId: orderId)
            }, cancel: {
                
        })
    }

}

extension OrderHistoryViewController{
    
    func cancelOrderWebservice(orderId: String){
       // guard let indexpath = tableView.indexPath(for: cell) else { return }
//        tableView.beginUpdates()
//        dataSource.items?.remove(at: indexpath.row)
//        orderListing?.orders?.remove(at: indexpath.row)
//        tableView.deleteRows(at: [indexpath], with: .top)
//        tableView.endUpdates()
      //  viewPlaceholder?.isHidden = (dataSource.items?.count ?? 0) > 0 ? true : false
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CancelOrder(FormatAPIParameters.CancelOrder(orderId: orderId, isScheduled: "0").formatParameters())) { (response) in
            switch response {
            case .Success(_):
                
                self.refreshTableData()
                break
            case .Failure(_):
                break
            }
        }
    }
    
}
