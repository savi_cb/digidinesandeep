//
//  RateReviewsVC.swift
//  Sneni
//
//  Created by Mac_Mini17 on 19/03/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

protocol RateReviewsVCDelegate: class {
    func updateProductDetail()
}

class RateReviewsVC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!{
        didSet{
              tableView.tableFooterView = UIView()
        }
    }
    var product : Product?
    var orderDetails : OrderDetails?
    weak var delegate: RateReviewsVCDelegate?
    var tableDataSource : TableViewDataSource?{
        didSet{
            tableView.reloadData()
        }
    }

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
    }

}

// MARK: - configureTableView
extension RateReviewsVC {
    
    func configureTableView(){
        
        var items = [Any]()
       
        items.append(1)
      
        tableDataSource = TableViewDataSource(items: items, height: 372, tableView: tableView, cellIdentifier: CellIdentifiers.RateReviewCell, configureCellBlock: { [weak self] (cell, item) in
            
            self?.configureCell(cell: cell, item: item)
            
            }, aRowSelectedListener: { [weak self] (indexPath) in
                
               // weak var weak : RateReviewsVC? = self
//                let cell = weak?.tableView.cellForRow(at: indexPath) as? FilterSearchCell
//
//                let productVariant = StoryboardScene.Main.instantiateProductVariantVC()
//                productVariant.suplierBranchId =  cell?.product?.supplierBranchId
//                productVariant.passedData.productId = cell?.product?.id
//                self?.pushVC(productVariant)
                
        })
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
    }
    
    func configureCell(cell : Any?,item : Any?){
        (cell as? RateReviewCell)?.orderDetails = orderDetails
        (cell as? RateReviewCell)?.product = product
        (cell as? RateReviewCell)?.submitBtn.tag = (cell as? RateReviewCell)?.tag ?? 0
        
    }
}



extension RateReviewsVC{
    
    @IBAction func crossBtnClick(sender:UIButton){
        
        self.popVC()
        
    }
}
