 //
 //  PaymentVC.swift
 //  Buraq24
 //
 //  Created by MANINDER on 01/09/18.
 //  Copyright © 2018 CodeBrewLabs. All rights reserved.
 //
 
 import UIKit
 import Stripe
 import SquareInAppPaymentsSDK
 
 class PaymentVC: BaseViewController{
    
    //MARK:- OUTLETS
    @IBOutlet weak var tblView: UITableView!
    
    //MARK::- VARIABLES
    var type: PaymentMethod = .DoesntMatter
    var tableDataSource : TableViewDataSource?{
        didSet{
            tblView.reloadData()
        }
    }
    
    var arrayItems = [FeatureDataModal]()
    var returnPaymentDetail:((_ token:String , _ paymentFatewayName:String?,  _ card_id:String? ) -> ())?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //showing the list of gateways that are enabled from backend only
        //arrayItems = AgentCodeClass.shared.agentFeatureData ?? []
        tblView.tableFooterView = UIView()
        filterEnabledGateways()
        setCardDta()
        tblView.reloadData()
        
    }
    
    //MARK:- FUNCTIONS
    func filterEnabledGateways() {
        let allItems = AgentCodeClass.shared.agentFeatureData ?? []
        for obj in allItems {
            if obj.type_name == "payment_gateway" && obj.is_active == 1 && !(obj.key_value_front ?? []).isEmpty {
                if "spicemaster_0134" == APIConstants.defaultAgentCode {
                    if obj.name == "Stripe" {
                        arrayItems.append(obj)
                    }
                }
                else {
                    arrayItems.append(obj)
                }
            }
        }
        if type == .DoesntMatter {
            let cod = FeatureDataModal(attributes: [:])
            cod.name = "Cash On Delivery"
            arrayItems.append(cod)
        }
        
    }
    
    //MARK:- ACTIONS
    
    @IBAction func btnBack(_ sender: Any) {
        self.popVC()
    }
    
    
    
 }
 
 //MARK::- TableView
 extension PaymentVC{
    
    func setCardDta(){
//        tableDataSource = TableViewDataSource(items: arrayItems, height: 64 , tableView: tblView, cellIdentifier: CellIdentifiers.CardTVC, configureCellBlock: {  (cell, item) in
//            var name = (item as? FeatureDataModal)?.name
//            if name == "Stripe" {
//                name = "Credit Card"
//            }
//            (cell as? CardTVC)?.textLabel?.text = name
//        }, aRowSelectedListener: { [ weak self] (indexPath) in
//            self?.selectedPaymentMethod(name: /self?.arrayItems[indexPath.row].name , index: indexPath.row)
//        })
//        tblView.delegate = tableDataSource
//        tblView.dataSource = tableDataSource
        tableDataSource = TableViewDataSource(items: arrayItems, height: 64 , tableView: tblView, cellIdentifier: CellIdentifiers.CardTVC, configureCellBlock: {  (cell, item) in
            var name = (item as? FeatureDataModal)?.name
            if name == "Stripe" {
                name = "Credit Card"
            }
            else if name == "Conekta" {
                name = "Credit Card / Debit Card"
            }
            (cell as? CardTVC)?.textLabel?.text = name
        }, aRowSelectedListener: { [ weak self] (indexPath) in
            self?.selectedPaymentMethod(name: /self?.arrayItems[indexPath.row].name , index: indexPath.row)
        })
        tblView.delegate = tableDataSource
        tblView.dataSource = tableDataSource
    }
    
    func selectedPaymentMethod(name: String, index: Int){
        switch name {
        case "Square":
            for keys in self.arrayItems[index].key_value_front ?? []{
                if keys.key == "square_publish_key"{
                    SQIPInAppPaymentsSDK.squareApplicationID = keys.value
                    payViaSquare()
                }
            }
            
        case "Conekta":
            for keys in self.arrayItems[index].key_value_front ?? []{
                if keys.key == "conekta_publish_key"{
                    payViaConekta(key: /keys.value)
                }
            }
            
        case "Stripe":
            for keys in self.arrayItems[index].key_value_front ?? []{
                if keys.key == "stripe_publish_key"{
                    Stripe.setDefaultPublishableKey(/keys.value)
                    payViaStripe()
                }
            }
            
        case "Paypal":
            for keys in self.arrayItems[index].key_value_front ?? []{
                if keys.key == "paypal_client_key"{
                    payViaPayPal()
                }
            }
            
        case "Venmo":
            payViaVenmo()
            
        case "Zelle":
            var email = ""
            var phone = ""
            for keys in self.arrayItems[index].key_value_front ?? []{
                if keys.key == "email"{
                    email = /keys.value
                }
                if keys.key == "phone_number"{
                    phone = /keys.value
                }
            }
            payViaZelle(email: email, phon: phone)
            
        case "Cash On Delivery":
            
            returnPaymentDetail?("","Cash On Delivery", "")
            popVC()
        //
        default:
            break
        }
    }
    
    
    
    
    
    //MARK: - Venmo Payment
    func payViaVenmo() {
        BrainTreeManager.sharedInstance.payViaVenmo( success: { [unowned self] (nonce) in
            self.returnPaymentDetail?(nonce,"Venmo","")
            self.popVC()
        }) { [weak self] (error) in
            self?.handleError(error)
        }
    }
    
    //MARK: - Paypal Payment
    func payViaPayPal() {
        BrainTreeManager.sharedInstance.payViaPayPal( success: { [unowned self] (nonce) in
            debugPrint(nonce)
            print(nonce.description)
            self.returnPaymentDetail?("","Paypal","")
            self.popVC()
        }) { [weak self] (error) in
            print(error?.localizedDescription)
            SKToast.makeToast(error?.localizedDescription)
            self?.handleError(error)
        }
    }
    
    //Mark::- Conekta
    func payViaConekta(key: String){
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        guard let vc =
            storyboard.instantiateViewController(withIdentifier: "ConektaPaymentVC") as? ConektaPaymentVC else { return }
        vc.key = key
        vc.paymentDone = { [weak self] (token) in
            self?.returnPaymentDetail?(/token,"Conekta","")
            self?.popVC()
        }
        self.presentVC(vc)
        //
    }
    
    //MARK: - STRIPE
    func payViaStripe() {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        guard let vc =
            storyboard.instantiateViewController(withIdentifier: "StripePaymentViewController") as? StripePaymentViewController else { return }
        
        vc.paymentDone = { [weak self] token in
            self?.returnPaymentDetail?(/token,"Stripe","")
            self?.popVC()
        }
        self.presentVC(vc)
    }
    
    
    //MARK: - ZELLE
    func payViaZelle(email: String , phon: String) {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        guard let vc =
            storyboard.instantiateViewController(withIdentifier: "ZelleViewController") as? ZelleViewController else { return }
        vc.email = email
        vc.phone = phon
        vc.selectedImage = { [weak self] (imageUrl , image) in
            
            //use the image if you want to show at cart
            self?.returnPaymentDetail?(imageUrl, "Zelle","")
            self?.popVC()
        }
        self.presentVC(vc)
    }
    
    func payViaSquare() {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        guard let vc =
            storyboard.instantiateViewController(withIdentifier: "CardListingVC") as? CardListingVC else { return }
        
        vc.paymentDone = { [weak self] token,card_id  in
            self?.returnPaymentDetail?(/token,"Square",card_id)
            self?.popVC()
        }
        self.presentVC(vc)
    }

    
    //MARK: - Handle Error
    func handleError(_ error: Error?) {
        SKToast.makeToast(/error?.localizedDescription)
    }
    
 }
 
 
