//
//  CustomizationViewController.swift
//  Sneni
//
//  Created by Apple on 26/09/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class CustomizationViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var foodName_label: UILabel!
    
    
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var customiseyourfood_label: ThemeLabel!
        {
        didSet
        {
            customiseyourfood_label.text = "Customise your food".localized()
        }
    }
    @IBOutlet weak var foodDescription_label: UILabel!
    @IBOutlet weak var proceed_button: ThemeButton! {
        didSet{
            proceed_button.setBackgroundColor(SKAppType.type.color, forState: .normal)
            proceed_button.setTitle("Add".localized(), for: .normal)
        }
    }
    @IBOutlet weak var back_button: ThemeButton!
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.register(UINib(nibName: "CustomizationTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomizationTableViewCell")
            tableView.tableFooterView = UIView(frame: CGRect.zero)
        }
    }
    
    //MARK:- Variables
    var completionBlock : AnyCompletionBlock?
    var headerView : CutomizationTableHeaderView? = nil
    var tempModel : [[AddonValueModal]]?
    var hideAddCustom :Bool?
    var isUpdated = false
    var index: Int?
    var addonsModalArray = [AddonsModalClass]()
    var cartData : Cart?
    var prices = [String]()
    var type_id = [String]()
    var product: Product? {
        didSet {
            if let addon = product?.adds_on {
                for i in 0...addon.count-1 {
                    if let value = addon[i].value?[0].is_multiple {
                        self.product?.adds_on?[i].is_multiple = value
                    }
                }
            }
        }
    }
    var boolArray = [[Bool]]()
    
    var aryQuantity = [Int]()
    var quantity = 0
    var btnIndex = IndexPath()
    var isQuantity = false
    var sum = Double()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let product = self.product {
            self.productPrice.text = "\(/Double(/product.price)?.addCurrencyLocale)"
            prices.append(/product.price)
            sum = /Double(/product.price)
            self.totalPriceLabel.text = "\(/Double(/product.price)?.addCurrencyLocale)"
            self.foodName_label.text = product.name ?? ""
            //            self.foodDescription_label.text = /product.desc
            // need to change in this method for default font also
            self.foodDescription_label.attributedText = (product.desc ?? "").htmlToAttributedString(label: self.foodDescription_label)
        }
        if let ishide = self.hideAddCustom,ishide{
            self.proceed_button.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        guard let product = self.product else {return}
        
        DBManager.sharedManager.getAddonsDataFromDb(productId: product.id ?? "", addonId: String(index ?? 0)) { [weak self] (data) in
            guard let self = self else { return }
            print(data)
            self.addonsModalArray = data
        }
        
    }
    
    //MARK:- ButtonActions
    @IBAction func back_buttonAction(_ sender: Any) {
        self.dismiss(animated: true) {
            guard let block = self.completionBlock else {return}
            guard let data = self.product else {return}
            data.addOnValue?.removeAll()
            block((data,self.hideAddCustom ?? false) as AnyObject)
        }
    }
    
    @IBAction func proceed_buttonAction(_ sender: Any) {
        
        var ids = [String]()
        var minAddOn = 0
        var selectedMinAddOns = 0
        
        for object in self.product?.addOnValue ?? [AddonValueModal]() {
            ids.append(object.id ?? "0")
        }
        ids.removeDuplicates()
        ids.removeObject("0")
        
        
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    minAddOn += Int(object.min_adds_on ?? "0") ?? 0
                    break
                }
            }
        }
        
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    selectedMinAddOns += 1
                }
            }
        }
        
        if minAddOn >= selectedMinAddOns {
            SKToast.makeToast("Please select minimum addOns.")
            return
        }
        self.dismiss(animated: true) {
            self.addDataToSpecificObject()
        }
    }
    
    func addonQuantityMethod() -> Bool{
        var ids = [String]()
        var max = 0
        var selectedMaxAddOns = 0
        
        for object in self.product?.addOnValue ?? [AddonValueModal]() {
            ids.append(object.id ?? "0")
        }
        ids.removeDuplicates()
        ids.removeObject("0")
        
        
        var isIn = false
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    //                                  max += Int(object.max_adds_on ?? "0") ?? 0
                    max = Int(object.max_adds_on ?? "0") ?? 0
                    isIn = true
                    break
                }
            }
            if isIn {
                print("Exit loop")
                break
            }
        }
        
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    selectedMaxAddOns += 1 // (Int(object.selectedQuantity) ?? 1)
                }
            }
        }
        
        if max > selectedMaxAddOns {
            guard let value =  self.product?.adds_on?[self.btnIndex.section].value?[self.btnIndex.row] else {return false}
            guard let index = self.index else {return false}
            value.add_on_id_ios = String(index)
            var dict = Dictionary<Int,Any>()
            value.selectedQuantity = "\(self.quantity)"
            dict[self.btnIndex.section] = value
            self.saveSelected(data: dict)
            return true
        }
        
        return false
    }
    func removeAddOn() -> Bool{
        
        var ids = [String]()
        var min = 0
        var selectedMinAddOns = 0
        
        for object in self.product?.addOnValue ?? [AddonValueModal]() {
            ids.append(object.id ?? "0")
        }
        ids.removeDuplicates()
        ids.removeObject("0")
        
        
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    min = Int(object.min_adds_on ?? "0") ?? 0
                    break
                }
            }
        }
        
        for id in ids {
            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                if object.id == id {
                    selectedMinAddOns += (Int(object.selectedQuantity) ?? 1)
                }
            }
        }
        
        if min < selectedMinAddOns {
            guard let value =  self.product?.adds_on?[self.btnIndex.section].value?[self.btnIndex.row] else {return false}
            guard let index = self.index else {return false}
            value.add_on_id_ios = String(index)
            var dict = Dictionary<Int,Any>()
            value.selectedQuantity = "\(self.quantity)"
            dict[self.btnIndex.section] = value
            self.saveSelected(data: dict)
            return true
        }
        
        return false
    }
    
    func addDataToSpecificObject() {
        
        guard let productObj = self.product else {return}
        guard var addonValue = productObj.addOnValue else {return}
        guard let block = self.completionBlock else {return}
        
        addonValue = addonValue.sorted { (obj1, obj2) -> Bool in
            obj1.type_id ?? "" < obj2.type_id ?? ""
        }
        var typeIdStr = ""
        for value in addonValue {
            print(value.type_id ?? "")
            typeIdStr = typeIdStr + (value.type_id ?? "")
        }
        if typeIdStr == "" { // for no addons added
            block(false as AnyObject)
        } else {
            productObj.addOnId = String(index ?? 0)
            if let _ = self.cartData {
                self.getAddonsAcctoTypeid(typeId: typeIdStr, addonValue: addonValue)
            } else {
                self.typeCastData(product: productObj, addonValue: addonValue, typeIdStr: typeIdStr,quantity : 1, shouldClear: true)
            }
            
            block((self.isUpdated,productObj) as AnyObject)
        }
        
    }
    
    func typeCastData(product : Product?, addonValue : [AddonValueModal]?, typeIdStr : String?,quantity :Int, shouldClear : Bool?) {
        
        guard let productObj = product , let addonValueObj = addonValue , let typeId = typeIdStr  else {return}
        
        var array = [[AddonValueModal]]()
        array.append(addonValueObj)
        
        self.saveAddons(productId:  productObj.id ?? "", addonId: String(index ?? 0), arrayAddonValue: array, typeIdStr: typeId, quantity: quantity, product: productObj, addonObj: addonValueObj, shouldClear: shouldClear)
    }
    
    func saveAddons(productId : String?, addonId: String?, arrayAddonValue : [[AddonValueModal]]?, typeIdStr : String?, quantity: Int, product: Product?, addonObj: [AddonValueModal]? , shouldClear : Bool?) {
        
        DBManager.sharedManager.getTotalQuantityPerProduct(productId: productId ?? "") { (quanti) in
            guard let qaunt = Int(quanti) else {return}
            
            let obj = AddonsModalClass(productId: productId ?? "", addonId: String(index ?? 0), addonData: arrayAddonValue, quantity: quantity, typeId: typeIdStr ?? "")
            DBManager.sharedManager.manageAddon(addonData: obj)
            
            guard let productData = product else {return}
            productData.typeId = typeIdStr
            
            guard let adds = productData.arrayAddonValue else {return}
            var array = [[AddonValueModal]]()
            
            for data in adds {
                array.append(data)
            }
            if shouldClear ?? false {
                array.append(addonObj ?? [])
            }
            productData.arrayAddonValue = array
            
            DBManager.sharedManager.manageCart(product: productData, quantity: qaunt+1)
        }
        
    }
    
    func getAddonsAcctoTypeid(typeId:String?,addonValue : [AddonValueModal]?) {
        
        guard let productObj = self.product else {return}
        guard let addonValueObj = addonValue else {return}
        let productId = productObj.id ?? ""
        
        DBManager.sharedManager.getAddonsDataFromDbAcctoTypeId(productId: productId, addonId: String(index ?? 0), typeId: typeId) { (data) in
            print(data)
            
            self.typeCastData(product: productObj, addonValue: addonValueObj, typeIdStr: typeId,quantity: (data.first?.quantity ?? 0) + 1, shouldClear: data.count == 0 ? true : false)
        }
        
    }
    
    func saveSelected(data : Dictionary<Int,Any>){
        
        if SKAppType.type.isFood {
            for (key,value) in data {
                if let obj = value as? AddonValueModal {
                    if let isMultiple = self.product?.adds_on?[key].is_multiple,isMultiple == "0" {
                        guard let addons = self.product?.adds_on?[key].value else { return }
                        let output = self.product?.addOnValue?.filter({ (addonValue) -> Bool in
                            return addons.contains(where: {$0.type_id == addonValue.type_id})
                        })
                        if output?.count ?? 0 > 0 {
                            if let index = self.product?.addOnValue?.firstIndex(where: {$0.type_id == output?[0].type_id}) {
                                let price = Double(/obj.price)
                                if price == 0.0{
                                    sum = /Double(/product?.price)
                                    self.totalPriceLabel.text =  "\(/Double(/product?.price)?.addCurrencyLocale)"
                                }else if sum > /Double(/product?.price) {
                                    sum = sum - /price
                                    self.totalPriceLabel.text = "\(sum.addCurrencyLocale)"
                                }
                                self.product?.addOnValue?.remove(at: index)
                            }
                        }
                        type_id.append(/obj.type_id)
                        let addOnprice = Double(/obj.price)
                        sum = sum + /addOnprice
                        self.totalPriceLabel.text = "\(sum.addCurrencyLocale)"
                        self.product?.addOnValue?.append(obj)
                    } else {
                        if let contain = self.product?.addOnValue?.contains(where: {$0.type_id == obj.type_id}),contain {
                            if let index = self.product?.addOnValue?.firstIndex(where: {$0.type_id == obj.type_id}) {
                                if self.isQuantity {
                                    var prodCount = 0
                                    for object in self.product?.addOnValue ?? [AddonValueModal]() {
                                        if object.type_id == obj.type_id {
                                            prodCount += 1
                                        }
                                    }
                                    if prodCount > Int(obj.selectedQuantity) ?? 0 {
                                        let cc = prodCount - (Int(obj.selectedQuantity) ?? 0)
                                        for _ in stride(from: 0, to: cc, by: 1) {
                                            self.product?.addOnValue?.remove(at: index)
                                            //                                        self.product?.addOnValue?.append(obj)
                                        }
                                        
                                    }else if prodCount < Int(obj.selectedQuantity) ?? 0 {
                                        let cc = (Int(obj.selectedQuantity) ?? 0) - prodCount
                                        for _ in stride(from: 0, to: cc, by: 1) {
                                            self.product?.addOnValue?.append(obj)
                                        }
                                    }else {
                                        //
                                    }
                                    
                                }else{
                                    for _ in self.product?.addOnValue ?? [AddonValueModal]() {
                                        if let contain = self.product?.addOnValue?.contains(where: {$0.type_id == obj.type_id}),contain {
                                            if let index = self.product?.addOnValue?.firstIndex(where: {$0.type_id == obj.type_id}) {
                                                let addOnprice = Double(/obj.price)
                                                sum -= /addOnprice
                                                self.totalPriceLabel.text = "\(sum.addCurrencyLocale)"
                                                self.product?.addOnValue?.remove(at: index)
                                                
                                            }
                                        }
                                    }
                                }
                                
                            }
                        } else {
                            // Not exists
                            var id = "0"
                            var addOn = 0
                            
                            for object in self.product?.addOnValue ?? [AddonValueModal]() {
                                if object.id == obj.id {
                                    id = object.id ?? "0"
                                    addOn += 1
                                }
                            }
                            if id == obj.id ?? "0" && "\(addOn)" >= obj.max_adds_on ?? "0" {
                                return
                            }
                            let addOnprice = Double(/obj.price)
                            sum += /addOnprice
                            self.totalPriceLabel.text = "\(sum.addCurrencyLocale)"
                            self.product?.addOnValue?.append(obj)
                        }
                    }
                    self.tableView.reloadSections(IndexSet(integer: key), with: .none)
                }
            }
        }else {
            for (key,value) in data {
                if let obj = value as? AddonValueModal {
                    if let isMultiple = self.product?.adds_on?[key].is_multiple,isMultiple == "0" {
                        guard let addons = self.product?.adds_on?[key].value else { return }
                        let output = self.product?.addOnValue?.filter({ (addonValue) -> Bool in
                            return addons.contains(where: {$0.type_id == addonValue.type_id})
                        })
                        if output?.count ?? 0 > 0 {
                            if let index = self.product?.addOnValue?.firstIndex(where: {$0.type_id == output?[0].type_id}) {
                                self.product?.addOnValue?.remove(at: index)
                            }
                        }
                        self.product?.addOnValue?.append(obj)
                    } else {
                        if let contain = self.product?.addOnValue?.contains(where: {$0.type_id == obj.type_id}),contain {
                            if let index = self.product?.addOnValue?.firstIndex(where: {$0.type_id == obj.type_id}) {
                                self.product?.addOnValue?.remove(at: index)
                            }
                        } else {
                            self.product?.addOnValue?.append(obj)
                        }
                    }
                    self.tableView.reloadSections(IndexSet(integer: key), with: .none)
                }
            }
        }
    }
    
    func saveDefalut(indexPath: IndexPath) {
        
        guard let value =  self.product?.adds_on?[indexPath.section].value?[indexPath.row] else {return }
        guard let index = self.index else {return}
        value.add_on_id_ios = String(index)
        var dict = Dictionary<Int,Any>()
        dict[indexPath.section] = value
        self.saveSelected(data: dict)
    }
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension CustomizationViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.product?.adds_on?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.product?.adds_on?[section].value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomizationTableViewCell", for: indexPath) as! CustomizationTableViewCell
        
        guard let data =  self.product?.adds_on?[indexPath.section].value?[indexPath.row] else {return UITableViewCell()}
        
        cell.name_label.text = data.type_name ?? ""
        cell.price_label.text = /data.price?.toDouble()?.addCurrencyLocale + " (Additional)"
        cell.lblPrice.text = /data.price?.toDouble()?.addCurrencyLocale
        
        if SKAppType.type.isFood {
            cell.selection_view.isHidden = true
            cell.lblPrice.isHidden = true
            cell.price_label.isHidden = false
            
            cell.quantity_label.text = data.selectedQuantity
            
            cell.actionPlusBlock = {
                let maxQuantity = data.max_adds_on ?? "1"
                self.quantity = Int(cell.quantity_label.text ?? "1") ?? 1
                if cell.quantity_label.text != maxQuantity {
                    print("Button tapped")
                    self.btnIndex = indexPath
                    self.isQuantity = true
                    self.quantity += 1
                    let res = self.addonQuantityMethod()
                    if res {
                        let qt = Int(data.selectedQuantity) ?? 1
                        //                                qt += 1
                        data.selectedQuantity = qt.toString
                        self.tableView.reloadData()
                    }
                }
            }
            
            cell.actionMinusBlock = {
                self.quantity = Int(cell.quantity_label.text ?? "1") ?? 1
                if cell.quantity_label.text != "1" {
                    print("Button tapped")
                    self.btnIndex = indexPath
                    let maxQuantity = Int(data.max_adds_on ?? "1") ?? 1
                    self.isQuantity = true
                    self.quantity -= 1
                    let res = self.removeAddOn()
                    
                    if res {
                        let qt = Int(data.selectedQuantity) ?? 2
                        data.selectedQuantity = qt.toString
                        self.tableView.reloadData()
                    }
                }
            }
            
            if data.is_default ?? "" == "1" {
                cell.sideImage.image = UIImage(named: "ic_radio_checked")
                cell.sideImage.tintColor = .gray //SKAppType.type.color
                cell.selection_view.isHidden = true
                self.saveDefalut(indexPath: indexPath)
            } else {
                if let contain = self.product?.addOnValue?.contains(where: {$0.type_id == data.type_id}),contain {
                    cell.sideImage.image = UIImage(named: "ic_radio_checked")
                    cell.sideImage.tintColor = SKAppType.type.color
                    cell.selection_view.isHidden = false
                } else {
                    cell.sideImage.image = UIImage(named: "ic_radio_icon")
                    cell.sideImage.tintColor = SKAppType.type.alphaColor
                    cell.selection_view.isHidden = true
                }
            }
        }
        else {
            cell.selection_view.isHidden = true
            cell.lblPrice.isHidden = false
            cell.price_label.isHidden = true
            
            if data.is_default ?? "" == "1" {
                cell.sideImage.image = UIImage(named: "ic_radio_checked")
                cell.sideImage.tintColor = .gray //SKAppType.type.color
                self.saveDefalut(indexPath: indexPath)
            } else {
                if let contain = self.product?.addOnValue?.contains(where: {$0.type_id == data.type_id}),contain {
                    cell.sideImage.image = UIImage(named: "ic_radio_checked")
                    cell.sideImage.tintColor = SKAppType.type.color
                    cell.selection_view.isHidden = false
                } else {
                    cell.sideImage.image = UIImage(named: "ic_radio_icon")
                    cell.sideImage.tintColor = SKAppType.type.alphaColor
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isQuantity = false
        self.saveDefalut(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if SKAppType.type.isFood{
            return 60
        }
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerName =  self.product?.adds_on?[section].name else {return UIView()}
        
        self.headerView = CutomizationTableHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 70))
        
        if SKAppType.type.isFood {
            guard let min =  self.product?.adds_on?[section].value?[0].min_adds_on else {return UIView()}
            guard let max =  self.product?.adds_on?[section].value?[0].max_adds_on else {return UIView()}
           // self.headerView?.option_label.text = "Please select multiple options (Min \(min) Max \(max))"
        }
        self.headerView?.choise_label.text = "Choice of".localized() + " " + headerName
        
        return self.headerView
    }
    
}
