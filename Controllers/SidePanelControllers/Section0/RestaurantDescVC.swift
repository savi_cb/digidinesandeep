//
//  RestaurantDetailVC.swift
//  Sneni
//
//  Created by Daman on 27/03/20.
//  Copyright © 2020 Taran. All rights reserved.
//

import UIKit
import SkeletonView
import EZSwiftExtensions

class RestaurantDescVC: CategoryFlowBaseViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var requirementNote: UITextView!{
        didSet{
            requirementNote.borderColor = UIColor.lightGray
            requirementNote.borderWidthW = 0.5
            requirementNote.cornerRadiusR = 8
        }
    }
    
    @IBOutlet weak var stepper: GMStepper!
    
    var product : Product? {
        didSet {
            productTitle?.text = product?.name
            //            lblSupplierName?.text = product?.supplierName
            //            lblSupplierName?.isHidden = AppSettings.shared.isSingleVendor
            //                productPrice?.text = "$ \(product?.price ?? "0.0")"
            productDescription?.text =  product?.desc
            productImage?.loadImage(thumbnail: product?.image, original: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productTitle?.text = product?.name
        productDescription.attributedText = (product?.desc ?? "").htmlToAttributedString(label: self.productDescription)
        productImage?.loadImage(thumbnail: product?.image, original: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view != contentView {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func addToOrderAction(_ sender: UIButton) {
    }
    
    
    func openCustomizationView(cell:ProductListCell?,product: Product?,cartData: Cart?,quantity: Double?,shouldHide:Bool = false,index:Int?) {
           
           let vc = StoryboardScene.Options.instantiateCustomizationViewController()
           vc.transitioningDelegate = self
           vc.modalPresentationStyle = .custom
           vc.product = product
           vc.hideAddCustom = shouldHide
           vc.index = index
           vc.cartData = cartData
           
           vc.completionBlock = { [weak self] data in
               guard let self = self else {return}
               if let obj = data as? (Bool,Product) {
                   if let _ = quantity { // called when add to card button is added
                       cell?.product = obj.1
                   }
               }
               self.removeViewAndSaveData()
           }
           
           self.present(vc, animated: true) {
               self.createTempView()
           }
           
       }
    
    func removeViewAndSaveData() {
         self.view.subviews.forEach { (view) in
             if view.tag == 10001 {
                 view.removeFromSuperview()
             }
         }
     }
     
     func createTempView(){
         let view = UIView()
         view.frame = self.view.frame
         view.backgroundColor = UIColor(white: 0.10, alpha: 0.8)
         view.tag = 10001
         self.view.addSubview(view)
     }
    
    func openCheckCustomizationController(cell:ProductListCell?,productData: Product?,cartData: Cart?, shouldShow: Bool, index:Int?) {
          
          let vc = StoryboardScene.Options.instantiateCheckCustomizationViewController()
          vc.transitioningDelegate = self
          vc.modalPresentationStyle = .custom
          vc.cartProdcuts = cartData
          vc.product = productData
          vc.completionBlock = {[weak self] data in
              guard let self = self else {return}
              guard let productCell = cell else {return}
              if let dataValue = data as? (Bool,Product) {
                  productCell.product = dataValue.1
                  if !dataValue.0 {
                      dataValue.1.addOnValue?.removeAll()
                      self.removeViewAndSaveData()
                  }
              } else if let dataValue = data as? (Product,Cart,Bool),let productCell = cell{
                  let obj = Product(cart: dataValue.1)
                  dataValue.0.addOnValue?.removeAll()
                  let addonId = Int(obj.addOnId ?? "0")
                  self.openCustomizationView(cell: cell, product: dataValue.0,cartData: dataValue.1, quantity: productCell.stepper?.value ?? 0.0, index: addonId)
              } else if let _ = data as? Bool{
                  //productCell.stepper?.stepperState = !obj ? .ShouldDecrease : .ShouldIncrease
                  self.removeViewAndSaveData()
              } else if let obj = data as? Product {
                  productCell.product = obj
              } else if let _ = data as? Int {
                  self.removeViewAndSaveData()
              } else if let value = data as? (Bool,Double) {
                  if value.1 == 0 {
                      self.removeViewAndSaveData()
                  }
                  productCell.stepper?.stepperState = .ShouldDecrease
              }
          }
          
          self.present(vc, animated: true) {
              self.createTempView()
          }
          
      }
}

//MARK:- UIViewControllerTransitioningDelegate
extension RestaurantDescVC : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}
