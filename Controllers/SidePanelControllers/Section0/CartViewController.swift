//
//  CartViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import ObjectMapper

class CartViewController: BaseViewController, voucher {
    
    //MARK:- IBOutlet
    //Non-table
    @IBOutlet var nonTableOrderView: ThemeView!
    @IBOutlet weak var resturantNameLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var nontableView: UITableView!
    @IBOutlet weak var applyVoucherButton: UIButton!
    
    
    //table
    @IBOutlet weak var nonTanleOrderLabel: UILabel!
    @IBOutlet weak var priceDetailHeight: NSLayoutConstraint!
    @IBOutlet weak var tableInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var tableTotalLabel: UILabel!
    @IBOutlet weak var itemSelectedPriceLAbel: UILabel!
    @IBOutlet weak var priceDetailLabel: UILabel!
    @IBOutlet weak var placeOrderHeight: NSLayoutConstraint!
    @IBOutlet weak var payNowHeight: NSLayoutConstraint!
    @IBOutlet weak var backToMenyBtn: UIButton!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var tableParticipentsLabel: UILabel!
    @IBOutlet weak var tableName: UILabel!
    @IBOutlet weak var orderHeaderLabel: UILabel!
    @IBOutlet weak var orderByLabel: UILabel!
    @IBOutlet weak var leaveSessionBtn: UIButton!
    @IBOutlet weak var amountAfterVoucherTitle: UILabel!
    
    @IBOutlet var dummyText_labels: [ThemeLabel]! {
        didSet {
            dummyText_labels.forEach { (label) in
                label.textColor = SKAppType.type.color
            }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageTick: UIImageView! {
        didSet{
            imageTick.tintColor = SKAppType.type.color
        }
    }
    
    @IBOutlet weak var PriceView: UIView!
    @IBOutlet weak var heading_label: UILabel!
    @IBOutlet weak var address_view: UIView!{
        didSet {
            address_view.backgroundColor = SKAppType.type.alphaColor
        }
    }
    @IBOutlet weak var change_button: UIButton!
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet weak var paymentType_button: UIButton!
    @IBOutlet weak var paymentType_label: UILabel! {
        didSet{
            paymentType_label.text = "Choose Payment".localized()
        }
    }
    
    @IBOutlet weak var tableViewHeightConstant: NSLayoutConstraint!{
        didSet{
            self.tableViewHeightConstant.constant = self.tableView.contentSize.height
            self.scrollView.contentInset.bottom = self.tableView.contentSize.height
        }
    }
    @IBOutlet weak var tableMembersTitle: UILabel!
    @IBOutlet weak var address_label: UILabel!
    @IBOutlet weak var title_label: ThemeLabel!
    @IBOutlet weak var menu_button: ThemeButton!{
        didSet {//Nitin
            menu_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? true : false
        }
    }
    
    @IBOutlet weak var btnBack : UIButton! {
        didSet {
            btnBack.isHidden = true
        }
    }
    
    @IBOutlet var imgPlaceholderNotext: UIView!
    @IBOutlet var navigation_view: NavigationView! {
        didSet{
        }
    }
    
    @IBOutlet var lblStaticText: [UILabel]! {
        didSet{
            for lbl in lblStaticText {
                lbl.kern(kerningValue: ButtonKernValue)
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            //            tableView.registerCells(nibNames: [ProductListingCell.identifier, CartListingCell.identifier,ProductListCell.identifier, CartQuestionCell.identifier])
            //tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)\
            tableView.frame.size.height = tableView.contentSize.height
            if #available(iOS 13.0, *) {
                tableView.automaticallyAdjustsScrollIndicatorInsets = false
            }
        }
    }
    @IBOutlet var btnProceedCheckout: UIButton! {
        didSet{
            if SKAppType.type == .home {
                btnProceedCheckout.setTitle("Place Booking".localized(), for: .normal)
            }
            btnProceedCheckout.setBackgroundColor(SKAppType.type.color, forState: .normal)
        }
    }
    
    
    @IBOutlet weak var AmountAfterVoucherApplied: UIView!
    @IBOutlet weak var voucherAppliedNumberStack: UIView!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var noteViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noteTextfield: UITextField!
    @IBOutlet weak var voucherAppliedValue: UILabel!
    @IBOutlet weak var voucherCountLabel: UILabel!
    @IBOutlet weak var detailsOfVoucher: UILabel!
    
    @IBOutlet weak var cartButton: ThemeButton!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var selectedItemPrice: UILabel!
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableViewHeightConstant?.constant = self.tableView.contentSize.height
    }
    
    
    //MARK:- Variables
    var FROMSIDEPANEL : Bool = false
    var tableDataSource = CartDataSource(){
        didSet{
            //            tableView.delegate = tableDataSource
            //            tableView.dataSource = tableDataSource
        }
    }
    var deliveryData = Delivery()
    var promoCode:PromoCode?
    var cartProdcuts : [AnyObject]?
    var cartArray : [Cart]?
    var productArray = [Product]()
    var remarks : String?
    var selectedAddress: Address?
    var orderSummary:OrderSummary?
    var userColor = String()
    var selectedDeliverySpeed : DeliverySpeedType = .Standard
    var parameters = Dictionary<String,Any>()
    var selectedPaymentMethod: PaymentMode? {
        didSet {
            //self.paymentType_label.text = selectedPaymentMethod?.displayName ?? "Choose Payment".localized()
        }
    }
    var isOptionSelected = false
    var pickupType = [Int]()
    var deliveryType:Int = 0 // 0- delivery, 1- pickup
    var fromCartView = false
    var refreshPriceData = false
    var referralApplied = false
    var tipItems : [Int]?
    var arrPrescription: [String]?
    var txtPrescription: String?
    var selectedAgentId: CblUser?
    var orderDetails = [TableData]()
    var outstandingData = [outstandingProduct]()
    var paidData = [paidProduct]()
    var name = [String]()
    lazy var order_reference = String()
    lazy var total = Double()
    lazy var tableTotal = Double()
    lazy var payNow = String()
    lazy var orderId = String()
    lazy var userPaymentLinkId = Int()
    
    var CardDetail = [CardDetails]()
    var refreshControl = UIRefreshControl()
    var voucherListing : [VouchersByUser]?
    var product : Product?
    
    //Non-table
    //MARK:- Variables
    var nonTableDataSource = OrderDetailDataSource()
    var nonTableOrderDetails : OrderDetails?
    var type : OrderCellType?
    var isOrderCompletion = false
    var isPush = false
    var isConfirmOrder = false
    var isBuyOnly = false
    var currentIndex = 0
    // var cancelOrder : ActionCancelOrder?
    var orderHistory : OrderHistory?
    var timer: Timer?
    var rows = 0
    //var product = [orderdetail]()
    var outstandingProductData = [OutstandingProductData]()
    var paidProductData = [OutstandingProductData]()
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: UIControl.Event.valueChanged)
        self.scroll.isScrollEnabled = true
        self.scroll.alwaysBounceVertical = true
        scroll.addSubview(refreshControl)
        
        self.payNowButton.isHidden = true
        //self.payNowHeight.constant = 0
        self.btnProceedCheckout.isHidden = true
        //self.placeOrderHeight.constant = 0
        payNow = "nil"
        self.orderHeaderLabel.text = "Order Summary"
        NotificationCenter.default.addObserver(self, selector: #selector(self.placeOrder), name: NSNotification.Name(rawValue: "orderNotifi"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.orderPaid), name: NSNotification.Name(rawValue: "paymentDone"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh(_:)), name: NSNotification.Name(rawValue: "refreshData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.cleanOrder(_:)), name: NSNotification.Name(rawValue: "hitorder"), object: nil)
        
        if let userData =  GDataSingleton.sharedInstance.loggedInUser{
            let name  = userData.firstName ?? ""
            //self.orderByLabel.text = "Order By : \(name)"
        }
    }
    
    //MARK:- view will appear
    override func viewWillAppear(_ animated: Bool) {
        self.update()
        timer =  Timer.scheduledTimer(withTimeInterval: 15.0, repeats: true) { (timer) in
            // Do what you need to do repeatedly
            if GDataSingleton.sharedInstance.selfPickup == 0{
                self.clearData()
                self.tableOrders(showLoader: false, promoCode1: 0)
                self.getCartFromDB()
            }
        }
    }
    
    func update(){
        if GDataSingleton.sharedInstance.orderId != nil{
            if GDataSingleton.sharedInstance.selfPickup == 1 {
                self.nonTanleOrderLabel.text = "Above your order receipt number you need to show this reciept number to the resturant to collect the order."
            }else{
                self.nonTanleOrderLabel.text = "This is your order reciept for your room, order will be delivered soon"
            }
            let orderId = GDataSingleton.sharedInstance.orderId
            getOrderDetails(orderId: orderId)
            DispatchQueue.main.async{
                if let nonTable = self.nonTableOrderView{
                    nonTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.view.addSubview(nonTable)
                }
            }
        }else{
            self.nonTableOrderView.removeFromSuperview()
            if GDataSingleton.sharedInstance.loginType == "NonTable" ||  GDataSingleton.sharedInstance.selfPickup == 1{
                leaveSessionBtn.isHidden = true
            }else{
                leaveSessionBtn.isHidden = false
            }
        }
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        self.clearData()
        refreshPriceData = true
        AdjustEvent.Cart.sendEvent()
        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.handleCartQuantity(sender:)), name: NSNotification.Name(rawValue: CartNotification), object: nil)
        btnProceedCheckout.setBackgroundColor(SKAppType.type.color, forState: .normal)
        btnBack.isHidden = self.navigationController?.viewControllers.last is MainTabBarViewController
        if let _ = cartProdcuts {
            getCartFromDB()
        }else {
            getCartFromDB()
        }
        self.AmountAfterVoucherApplied.isHidden = true
        self.voucherAppliedNumberStack.isHidden = true
        self.applyVoucherButton.isHidden = true
        if GDataSingleton.sharedInstance.selfPickup != nil {
            self.PriceView.isHidden = true
            self.tableInfoHeight.constant = 0
            self.leaveSessionBtn.isHidden = true
            self.tableTotalLabel.text = ""
            self.totalPrice.text = ""
            self.PriceView.isHidden = true
            
            if GDataSingleton.sharedInstance.isHotel == true{
                self.total = 0.0
                self.tableTotal = 0.0
                self.orderDetails.removeAll()
                self.outstandingData.removeAll()
                self.paidData.removeAll()
                tableMembersTitle.text = "In Hotel"
                self.tableInfoHeight.constant = 70
                self.tableParticipentsLabel.text = ""
                self.tableName.text = "Room Number:- \(/GDataSingleton.sharedInstance.hotelRoomNo)"
            }else if GDataSingleton.sharedInstance.selfPickup != 1 {
                tableMembersTitle.text = "Table Members"
                self.tableOrders(showLoader: true, promoCode1: 0)
            } else{
                GDataSingleton.sharedInstance.tableNumber = "0"
                self.tableTotalLabel.text = ""
                self.totalPrice.text = ""
                self.tableInfoHeight.constant = 0
            }
        }else{
            self.PriceView.isHidden = true
            self.tableInfoHeight.constant = 0
            self.orderDetails.removeAll()
            self.outstandingData.removeAll()
            self.paidData.removeAll()
            self.productArray.removeAll()
            self.tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        self.clearData()
        self.tableOrders(showLoader: true, promoCode1: 0)
        self.getCartFromDB()
        //        if GDataSingleton.sharedInstance.selfPickup != 1{
        //
        //        }
    }
    
    @objc func cleanOrder(_ sender: AnyObject) {
        getOrderDetails(orderId: orderId)
    }
    
    func clearData(){
        self.tableInfoHeight.constant = 0
        self.productArray.removeAll()
        self.tableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func applyVoucherAction(_ sender: UIButton) {
        //voucherListing = GDataSingleton.sharedInstance.VoucherListing
        let VC = VouchersViewController.getVC(.digiHome)
        VC.delegate = self
        VC.comingFrom = "cart"
        VC.voucherListing = voucherListing
        VC.supplier_id = /GDataSingleton.sharedInstance.supplierId
        self.presentVC(VC)
    }
    
    func getVoucherData(promoCode1: Int?) {
        if let code = promoCode1 {
            print(code)
            self.tableOrders(showLoader: false, promoCode1: code)
        }
    }
    
    @IBAction func switchOrderSummary(_ sender: UIButton) {
        self.nonTableOrderView.removeFromSuperview()
    }
    
    @IBAction func backBtnaction(_ sender: UIButton) {
        popVC()
    }
    
    
    @IBAction func placeNewOrderNonTable(_ sender: UIButton) {
        self.nonTableOrderView.removeFromSuperview()
    }
    
    @IBAction func payNowAction(_ sender: UIButton) {
        var MyAmount = String()
        MyAmount = /self.orderDetails.first?.myOrderAmount
        if /self.orderDetails.first?.onlyMyOustandingOrders == 1 {
            DispatchQueue.main.async {
                if let count =  self.orderDetails.first?.promoCodeAppliedCount{
                    if count != 0{
                        MyAmount = /self.orderDetails.first?.finalPayAfterPromo
                        self.tableTotal = /Double(/self.orderDetails.first?.finalPayAfterPromo)
                    }
                }
                let alert = UIAlertController(title: "", message: "You will pay د.إ  \(/self.orderDetails.first?.userUnpaidAmount)", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                    self.hitSplitEvenly(payMode: "my_order_bill")
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let VC = PayOptionsViewController.getVC(.digiHome)
            let userCount =  self.orderDetails.first?.usersOnTable?.count ?? 0
            VC.orderId = self.orderId
            VC.outstandingProductData = self.outstandingProductData
            VC.amount = self.tableTotal
            VC.myAmount = MyAmount
            VC.userCount = userCount
            VC.userUnpaidAmount = /self.orderDetails.first?.userUnpaidAmount
            VC.totalUnpaidAmount = /self.orderDetails.first?.totalUnpaidAmount
            VC.userUnpaidDiscount = /self.orderDetails.first?.userUnpaidDiscount
            VC.totalUnpaidDiscouunt = /self.orderDetails.first?.totalUnpaidDiscount
            //VC.referalAmount = MyAmount
            VC.onlyMyOustandingOrders = /self.orderDetails.first?.onlyMyOustandingOrders
            self.pushVC(VC)
        }
    }
    
    func hitSplitEvenly(payMode : String){
        //        let amo = String(amount)
        APIManager.sharedInstance.showLoader(message: "Processing Payment")
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId)
        let tableId = Int(/GDataSingleton.sharedInstance.tableNumber)
        //          let orderId = GDataSingleton.sharedInstance.OrderId
        let params = FormatAPIParameters.splitEvenly(table_id: tableId, amount: self.tableTotal, supplier_id: supplierId, order_id : orderId , payMode : payMode, totalUnpaidDiscount: /self.orderDetails.first?.totalUnpaidDiscount, userUnpaidDiscount: /self.orderDetails.first?.userUnpaidDiscount).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.splitEvenly(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                print(object)
                APIManager.sharedInstance.hideLoader()
                if let obj = object as? [String:Any]{
                    //SKToast.makeToast("Payment made successfully")
                    // create the alert
                    let alert = UIAlertController(title: "", message: "Payment made successfully", preferredStyle: UIAlertController.Style.alert)
                    // add the actions (buttons)
                    let Ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        GDataSingleton.sharedInstance.PayOption = "paid"
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                        self.popVC()
                    })
                    alert.addAction(Ok)
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    GDataSingleton.sharedInstance.PayOption = "paid"
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "paymentDone"), object: nil, userInfo: nil)
                    self.popVC()
                }
            case .Failure(let error):
                SKToast.makeToast("Payment failed")
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
    
    @IBAction func backToMenuAction(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func leaveSessionAction(_ sender: UIButton) {
        UtilityFunctions.showSweetAlert(title: "", message:  "Are you sure you want to end session?", success: { [weak self] in
            guard let self = self else {return}
            print(self)
            self.leaveSessionAPI()
        }) {
        }
    }
    
    @IBAction func actionSupport(_ sender: UIButton){
        UtilityFunctions.shareContentOnSocialMedia(withViewController: UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController, message: AgentCodeClass.shared.settingData?.email)
    }
    
    func leaveSessionAPI(){
        let tableId = /GDataSingleton.sharedInstance.tableNumber
        APIManager.sharedInstance.showLoader()
        let params = FormatAPIParameters.leaveSession(sessionRecordId: tableId).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.leaveSession(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                print(object)
                APIManager.sharedInstance.hideLoader()
                if object as? Bool == true{
                    SKToast.makeToast("Clear your payment before leaving")
                }else{
                    self.imgPlaceholderNotext.isHidden = false
                    self.PriceView.isHidden = true
                    self.tableInfoHeight.constant = 0
                    GDataSingleton.sharedInstance.tableNumber = nil
                    GDataSingleton.sharedInstance.selfPickup = nil
                    (UIApplication.shared.delegate as? AppDelegate)?.onload()
                }
            case .Failure(let error):
                //self.locationFetched(address: nil)
                APIManager.sharedInstance.hideLoader()
                print(error.message ?? "")
                break
            }
        }
    }
    
}

//MARK: - Button Actions
extension CartViewController{
    
    @IBAction func actionBack(sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CartNotification), object: self, userInfo: nil)
        //        DeliveryType.shared = DeliveryType(rawValue: 0) ?? .delivery
        popVC()
    }
    
    @IBAction func btnCheckout(_ sender: UIButton) {
        if GDataSingleton.sharedInstance.selfPickup != 1{
            if self.productArray.count == 0 {
                //self.placeOrderHeight.constant = 0
                self.btnProceedCheckout.isHidden = true
            }else{
                //self.placeOrderHeight.constant = 50
                self.btnProceedCheckout.isHidden = false
                self.getCardListing()
            }
        }else{
            self.getCardListing()
            //self.doPayment()
        }
    }
}

//MARK: - Add all products to cart webservice 
extension CartViewController {
    
    func webServiceAddToCart() {
        guard let products = productArray as? [Cart] else { return }
        let tableId = /GDataSingleton.sharedInstance.tableNumber
        APIManager.sharedInstance.showLoader()
        let params = FormatAPIParameters.AddToCart(
            cart: products,
            supplierBranchId: products[0].supplierBranchId,
            promotionType: nil,
            remarks: remarks, table_id : tableId).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.AddToCart(params)) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let object):
                
                self.handleWebService(tempCartId: object)
            case .Failure(let error):
                //self.locationFetched(address: nil)
                APIManager.sharedInstance.hideLoader()
                
                // self.getAllAdresses(orderSummary: nil)
                print(error.message ?? "")
                break
            }
        }
    }
    
    func tableOrders(showLoader : Bool, promoCode1 : Int?){
        let tableId = /GDataSingleton.sharedInstance.tableNumber
        if showLoader == true{
            APIManager.sharedInstance.showLoader()
        }
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId) ?? 0
        var params = [String : Any]()
        if promoCode1 != 0 {
            params = ["sessionTableInformationId" : tableId, "isVoucher" : true, "supplier_id" : supplierId, "promoId" : promoCode1]
        }else{
            params = ["sessionTableInformationId" : tableId,  "supplier_id" : supplierId]
        }
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/getTableOrderInfomation"
        print(url)
        print(headers)
        print(params)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            self.refreshControl.endRefreshing()
            self.total = 0.0
            self.tableTotal = 0.0
            self.orderDetails.removeAll()
            self.outstandingData.removeAll()
            self.paidData.removeAll()
            print(response)
            if let resultStatus = response["status"].int {
                if resultStatus == 4 || resultStatus == 200 {
                    if let dict = response.dictionaryObject {
                        let message = dict["message"] as? String
                        if message != "Success" &&   message !=  "Record not found" {
                            SKToast.makeToast(message)
                        }
                        if let data = dict["data"] as? Dictionary<String,Any>{
                            if let userInfo = Mapper<TableData>().map(JSONObject: data){
                                self.orderDetails.append(userInfo)
                                if /userInfo.outstandingProduct?.count > 0 &&  GDataSingleton.sharedInstance.isVoucher == true{
                                    self.applyVoucherButton.isHidden = false
                                }
                                if let outstandingProducts = userInfo.outstandingProduct {
                                    self.outstandingData = outstandingProducts
                                    self.rows = 0
                                    self.outstandingProductData.removeAll()
                                    // To show addOn Products,loops are for to calculate numbers of rows in section 0
                                    for i in 0 ..< self.outstandingData.count{
                                        var serialNUmber = 0
                                        
                                        var addOnFinalPrice = ""
                                        let productData = self.outstandingData[i]
                                        let price = Double(self.outstandingData[i].fixedPrice ?? "0")
                                        if /self.outstandingData[i].productDetailAddson?.count > 0{
                                            for j in 0 ..< /self.outstandingData[i].productDetailAddson?.count {
                                                let addOnPrice = String(self.outstandingData[i].productDetailAddson?[j].price ?? 0.0)
                                                // Filtering Addons on the basis of serial number and append it as a new order
                                                if j == 0{
                                                    serialNUmber = self.outstandingData[i].productDetailAddson?[j].serial_number ?? 0
                                                    self.rows = self.rows + 1
                                                    self.addOnCalculation(productData:productData,j:j,addOnPrice:addOnPrice)
                                                }
                                                if serialNUmber != self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1{
                                                    serialNUmber = self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1
                                                    self.rows = self.rows + 1
                                                    self.addOnCalculation(productData:productData,j:j,addOnPrice:addOnPrice)
                                                    
                                                }else if serialNUmber == self.outstandingData[i].productDetailAddson?[j].serial_number ?? -1 && j != 0{
                                                    let arr1 = self.outstandingProductData.filter({$0.cart_id_product == self.outstandingData[i].cartProductId })
                                                    let arr = arr1.filter({$0.serial_number == self.outstandingData[i].productDetailAddson?[j].serial_number })
                                                    //Calculationg add on price
                                                    var priceWithAddOn = 0.0
                                                    var addOnTypeName = productData.productDetailAddson?[j].adds_on_type_name
                                                    for k in 0 ..< /arr.count{
                                                        
                                                        priceWithAddOn = priceWithAddOn + /Double(arr[k].addOnPrice ?? "0")
                                                        addOnTypeName = "\(addOnTypeName ?? ""),  \(arr[k].adds_on_type_name ?? "")"//addOnTypeName ?? "" + "," + /arr[k].adds_on_type_name
                                                        
                                                    }
                                                    priceWithAddOn = priceWithAddOn + /self.outstandingData[i].productDetailAddson?[j].price
                                                    addOnFinalPrice = String(priceWithAddOn)
                                                    for (index, item) in self.outstandingProductData.enumerated() {
                                                        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cart_id, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: addOnTypeName, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon:productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
                                                        if item.serial_number == self.outstandingData[i].productDetailAddson?[j].serial_number && item.cart_id_product == self.outstandingData[i].cartProductId{
                                                            self.outstandingProductData[index] = obj
                                                        }
                                                    }
                                                }
                                                
                                                
                                            }
                                            
                                        }else{
                                            // Products without Addon
                                            let productData = self.outstandingData[i]
                                            let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cart_id, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id,userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id:nil, addonName: nil, cartId: nil, addOnQuantity: nil, addOnTypeId: nil, serialNumber: nil, addOnId: nil, addOnTypeName: nil, addOnPrice: nil, addOnIdIos: nil, addOnTypeQuantity: nil, isFreeAddon: false, FreeAddonQuantity: 0)
                                            self.outstandingProductData.append(obj)
                                        }
                                    }
                                    
                                }
                                if let paidProducts = userInfo.paidProduct {
                                    self.paidData = paidProducts
                                    self.paidProductData.removeAll()
                                    // To show addOn Products for Paid Orders,loops are for to calculate numbers of rows in section 2
                                    for i in 0 ..< self.paidData.count{
                                        var serialNUmber = 0
                                        
                                        var addOnFinalPrice = ""
                                        let productData = self.paidData[i]
                                        let price = Double(self.paidData[i].fixedPrice ?? "0")
                                        if /self.paidData[i].productDetailAddson?.count > 0{
                                            for j in 0 ..< /self.paidData[i].productDetailAddson?.count {
                                                let addOnPrice = String(self.paidData[i].productDetailAddson?[j].price ?? 0.0)
                                                if j == 0{
                                                    serialNUmber = self.paidData[i].productDetailAddson?[j].serial_number ?? 0
                                                    self.addOnCalculationForPaidData(productData:productData,j:j,addOnPrice:addOnPrice)
                                                }
                                                if serialNUmber != self.paidData[i].productDetailAddson?[j].serial_number ?? -1{
                                                    serialNUmber = self.paidData[i].productDetailAddson?[j].serial_number ?? -1
                                                    self.addOnCalculationForPaidData(productData:productData,j:j,addOnPrice:addOnPrice)
                                                    
                                                }else if serialNUmber == self.paidData[i].productDetailAddson?[j].serial_number ?? -1 && j != 0{
                                                    let arr1 = self.paidProductData.filter({$0.cart_id_product == self.paidData[i].cartProductId })
                                                    let arr = arr1.filter({$0.serial_number == self.paidData[i].productDetailAddson?[j].serial_number })
                                                    var priceWithAddOn = 0.0
                                                    var addOnTypeName = productData.productDetailAddson?[j].adds_on_type_name
                                                    for k in 0 ..< /arr.count{
                                                        
                                                        priceWithAddOn = priceWithAddOn + /Double(arr[k].addOnPrice ?? "0")
                                                        addOnTypeName = "\(addOnTypeName ?? ""),  \(arr[k].adds_on_type_name ?? "")"//addOnTypeName ?? "" + "," + /arr[k].adds_on_type_name
                                                        
                                                    }
                                                    priceWithAddOn = priceWithAddOn + /self.paidData[i].productDetailAddson?[j].price
                                                    addOnFinalPrice = String(priceWithAddOn)
                                                    for (index, item) in self.paidProductData.enumerated() {
                                                        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: addOnTypeName, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity, isFreeAddon:productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
                                                        if item.serial_number == self.paidData[i].productDetailAddson?[j].serial_number && item.cart_id_product == self.paidData[i].cartProductId{
                                                            self.paidProductData[index] = obj
                                                        }
                                                    }
                                                }
                                                
                                                
                                            }
                                            
                                        }else{
                                            let productData = self.paidData[i]
                                            let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id,userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id:nil, addonName: nil, cartId: nil, addOnQuantity: nil, addOnTypeId: nil, serialNumber: nil, addOnId: nil, addOnTypeName: nil, addOnPrice: nil, addOnIdIos: nil, addOnTypeQuantity: nil, isFreeAddon: false, FreeAddonQuantity: 0)
                                            self.paidProductData.append(obj)
                                        }
                                    }
                                }
                                
                                if self.orderDetails.first?.promoCodeAppliedCount  != 0 {
                                    self.voucherAppliedNumberStack.isHidden = false
                                    self.AmountAfterVoucherApplied.isHidden = false
                                    if let voucherAppliedAmount = self.orderDetails.first?.finalPayAfterPromo {
                                        self.voucherAppliedValue.text = "د.إ " +  "\(voucherAppliedAmount)"
                                    }
                                    //self.detailsOfVoucher.text = "test1"
                                    if let count = self.orderDetails.first?.promoCodeAppliedCount {
                                        self.voucherCountLabel.text =  "\(count)"
                                    }
                                }else{
                                    self.voucherAppliedNumberStack.isHidden = true
                                    self.AmountAfterVoucherApplied.isHidden = true
                                }
                            }
                        }
                    }
                    let text = NSMutableAttributedString()
                    self.tableInfoHeight.constant = 70
                    self.leaveSessionBtn.isHidden = false
                    self.tableTotalLabel.text = "Table Total"
                    self.PriceView.isHidden = false
                    self.tableName.text = "Table Number " + /self.orderDetails.first?.table_name
                    if let userData =  GDataSingleton.sharedInstance.loggedInUser{
                        let userId = /userData.id
                        if let nameAray = self.orderDetails.first?.usersOnTable{
                            for i in 0..<nameAray.count {
                                if let name = nameAray[i].name{
                                    if let color = nameAray[i].colorCode{
                                        if nameAray[i].id == userId {
                                            self.userColor = color
                                        }
                                        let nameColor : UIColor = UIColor(hexString: color) ?? UIColor()
                                        if i == nameAray.count - 1 {
                                            text.append(NSAttributedString(string: " \(name)", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                        }else{
                                            text.append(NSAttributedString(string: " \(name) ,", attributes: [NSAttributedString.Key.foregroundColor: nameColor]))
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    DispatchQueue.main.async{
                        self.getAmount(array: self.productArray)
                        if self.orderDetails.first?.isOutstandingPayment == 1 {
                            self.payNowButton.isHidden = false
                            self.applyVoucherButton.isHidden = false
                        }else{
                            self.payNowButton.isHidden = true
                            self.applyVoucherButton.isHidden = true
                        }
                        self.calculateTotalPrice()
                    }
                    self.imgPlaceholderNotext.isHidden = true
                    self.tableParticipentsLabel.attributedText = text
                    self.tableViewHeightConstant.constant = 600
                    self.tableView.reloadData()
                }else if resultStatus == 400 {
                    let message = response["message"].string
                    if message !=  "Record not found"{
                        self.imgPlaceholderNotext.isHidden = true
                        SKToast.makeToast(message)
                    }
                    self.imgPlaceholderNotext.isHidden = false
                    //                    self.tableView.reloadData()
                }else if resultStatus == 401{
                    self.imgPlaceholderNotext.isHidden = true
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window else { return }
                    GDataSingleton.sharedInstance.loggedInUser = nil
                    UtilityFunctions.showAlert(title: nil, message: L10n.SessionExpiredLoginToContinue.string, success: {
                        appDelegate.switchViewControllers()
                    }, cancel: {
                        appDelegate.switchViewControllers()
                    })
                }
            }
        }){ (error) in
            print(error)
            self.imgPlaceholderNotext.isHidden = true
            self.tableView.reloadData()
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window else { return }
                GDataSingleton.sharedInstance.loggedInUser = nil
                UtilityFunctions.showAlert(title: nil, message: L10n.SessionExpiredLoginToContinue.string, success: {
                    appDelegate.switchViewControllers()
                }, cancel: {
                    appDelegate.switchViewControllers()
                })
            }else if status_code == "The network connection was lost."{
            }
        }
    }
    
    func addOnCalculation(productData:outstandingProduct,j:Int,addOnPrice:String){
        var addOnFinalPrice = ""
        if addOnPrice  == ""{
            addOnFinalPrice = String(productData.productDetailAddson?[j].price ?? 0)
        }else{
            addOnFinalPrice = addOnPrice
        }
        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: productData.productDetailAddson?[j].adds_on_type_name, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity,isFreeAddon:productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
        self.outstandingProductData.append(obj)
    }
    
    func addOnCalculationForPaidData(productData:paidProduct,j:Int,addOnPrice:String){
        var addOnFinalPrice = ""
        if addOnPrice  == ""{
            addOnFinalPrice = String(productData.productDetailAddson?[j].price ?? 0)
        }else{
            addOnFinalPrice = addOnPrice
        }
        let obj = OutstandingProductData(order_id: productData.order_id, fixedPrice:productData.fixedPrice,cartIdProduct: productData.cartProductId, colorCode: productData.colorCode, productId: productData.product_id, userId: productData.user_id, isFree: productData.isFree, freeQuantity: productData.freeQuantity, orderPriceId: productData.order_price_id, userName:productData.firstname,name: productData.product_name, quantity: productData.quantity, measuringUnit: "", price: productData.price, id: productData.productDetailAddson?[j].id, addonName: productData.productDetailAddson?[j].adds_on_name, cartId: productData.productDetailAddson?[j].cart_id, addOnQuantity: productData.productDetailAddson?[j].quantity, addOnTypeId: productData.productDetailAddson?[j].adds_on_type_jd, serialNumber: productData.productDetailAddson?[j].serial_number, addOnId: productData.productDetailAddson?[j].adds_on_id, addOnTypeName: productData.productDetailAddson?[j].adds_on_type_name, addOnPrice: addOnFinalPrice, addOnIdIos: productData.productDetailAddson?[j].add_on_id_ios, addOnTypeQuantity: productData.productDetailAddson?[j].adds_on_type_quantity,isFreeAddon:productData.productDetailAddson?[j].isFreeAddon ?? false, FreeAddonQuantity: productData.productDetailAddson?[j].FreeAddonQuantity ?? 0)
        self.paidProductData.append(obj)
    }
    func handleWebService(tempCartId : Any?) {
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.token else {
            let loginVc = StoryboardScene.Register.instantiateLoginViewController()
            presentVC(loginVc)
            APIManager.sharedInstance.hideLoader()
            
            return
        }
        
        if SKAppType.type.isFood {
            
            guard let cartId = (tempCartId as? String)?.components(separatedBy: "$").first else { return }
            
            let ordersummary = OrderSummary(items: self.productArray as? [Cart], promo:self.promoCode)
            ordersummary.cartId = cartId
            ordersummary.minOrderAmount = (tempCartId as? String)?.components(separatedBy: "$").last
            ordersummary.isAgent = false
            ordersummary.useReferral = referralApplied
            self.orderSummary = ordersummary
            
            self.getAllAdresses(orderSummary: ordersummary)
            return
            
        }
        
        DBManager.sharedManager.getCart {
            [weak self] (array) in
            guard let self = self, let arrayCart = array as? [Cart], !arrayCart.isEmpty  else { return }
            
            let isAgent = arrayCart.first(where: { $0.agentList == "1" }) != nil
            let isNotAgent = arrayCart.first(where: { $0.agentList == "0" }) != nil
            
            let isProducts = arrayCart.first(where: { $0.isProduct == .product }) != nil
            let isServices = arrayCart.first(where: { $0.isProduct == .service }) != nil
            
            let isHourly = false
            
            // let isHourly = arrayCart.first(where: { $0.priceType == PriceType.Hourly }) != nil
            let isFixed = arrayCart.first(where: { $0.priceType == PriceType.Fixed }) != nil
            
            if SKAppType.type == .home {
                if isAgent && isNotAgent {
                    
                    UtilityFunctions.showAlert(title: nil, message: L10n.AddingProductsFromDiffrentSuppliersWillClearYourCart.string, success: {
                        // weak var weakSelf = self
                        DBManager.sharedManager.cleanCart()
                        
                    }, cancel: {})
                    return
                }
            }
            
            if isHourly && isFixed {
                
                UtilityFunctions.showAlert(title: nil, message: L11n.tryToAddDiffrentProductsToCart.string, success: {
                    // weak var weakSelf = self
                    DBManager.sharedManager.cleanCart()
                    
                }, cancel: {})
                return
            }
            if isAgent && isProducts && isServices {
                UtilityFunctions.showAlert(title: nil, message: L10n.Agentsnotavailableforsomeitems.string, success: {
                    
                }, cancel: {})
                return
            }
            
            if (isServices || isHourly) {
                
                var tinterval: Double = 0.0
                arrayCart.forEach({
                    (objC) in
                    tinterval += /objC.totalDuration
                })
                guard let cartId = (tempCartId as? String)?.components(separatedBy: "$").first else { return }
                
                self.openNextStep(isAgent: isAgent, tinterval: tinterval, cartId: cartId, tempCartId: (tempCartId as? String), arrayCart: arrayCart)
                
            } else {
                guard let cartId = (tempCartId as? String)?.components(separatedBy: "$").first else { return }
                
                let ordersummary = OrderSummary(items: self.tableDataSource.items as? [Cart], promo:self.promoCode)
                ordersummary.cartId = cartId
                ordersummary.minOrderAmount = (tempCartId as? String)?.components(separatedBy: "$").last
                ordersummary.isAgent = false
                ordersummary.useReferral = self.referralApplied
                self.orderSummary = ordersummary
                
                self.getAllAdresses(orderSummary: ordersummary)
                
                if DeliveryType.shared == DeliveryType.pickup {
                    self.webServiceForUpdateCartInfo(orderSummary: ordersummary)
                    return
                }
            }
        }
    }
    
    func openNextStep(isAgent: Bool, tinterval: Double, cartId: String?, tempCartId: String?, arrayCart: [Cart]) {
        
        if isAgent {
            let ordersummary = OrderSummary(items: self.tableDataSource.items as? [Cart], promo:self.promoCode)
            ordersummary.cartId = cartId
            ordersummary.minOrderAmount = (tempCartId)?.components(separatedBy: "$").last
            ordersummary.isAgent = true
            ordersummary.agentId = selectedAgentId?.id
            ordersummary.useReferral = self.referralApplied
            
            self.orderSummary = ordersummary
            self.getAllAdresses(orderSummary: ordersummary)
            
        } else {
            let orderSummary: OrderSummary? = OrderSummary(items: self.tableDataSource.items as? [Cart], promo:self.promoCode)
            orderSummary?.cartId = cartId
            orderSummary?.isAgent = false
            orderSummary?.minOrderAmount = (tempCartId)?.components(separatedBy: "$").last
            
            if GDataSingleton.sharedInstance.pickupDate == nil {
                let VC = StoryboardScene.Order.instantiateDeliveryViewController()
                VC.orderSummary = orderSummary
                VC.remarks = self.remarks
                self.pushVC(VC)
                return
            }
            
            let branchId = orderSummary?.items?.first?.supplierBranchId
            DeliveryViewController.getUserAdresses(isAgent: (/orderSummary?.isAgent), branchId: branchId) {
                [weak self] (delivery) in
                
                guard let self = self,
                      let deliveryAddressId = GDataSingleton.sharedInstance.pickupAddressId,//deliveryAddress?.id,
                      let pickUp = GDataSingleton.sharedInstance.pickupDate
                else { return }
                
                delivery.initalizeDelivery(cart: orderSummary?.items)
                
                let deliveryDate = pickUp//.add(seconds: Int(60.0*self.timeInterVal))
                
                DeliveryViewController.updateCart(
                    delivery: delivery,
                    order: orderSummary,
                    deliverySpeed: DeliverySpeedType.Standard,
                    addressId: deliveryAddressId,
                    deliveryDate: deliveryDate,
                    remarks: self.remarks,
                    addOn: nil) {
                    [weak self] in
                    APIManager.sharedInstance.hideLoader()
                    guard let self = self else { return }
                    
                    let VC = StoryboardScene.Order.instantiateOrderSummaryController()
                    VC.orderSummary = orderSummary
                    VC.remarks = self.remarks
                    self.pushVC(VC)
                }
            }
        }
    }
    
    func getAllAdresses(orderSummary: OrderSummary?) {
        
        let branchId = (tableDataSource.items?.first as? Cart)?.supplierBranchId
        
        self.getUserAdresses(isAgent: (orderSummary?.isAgent ?? false), branchId: branchId) {
            [weak self] (delivery) in
            guard let self = self else { return }
            self.deliveryData = delivery
            self.setAddressData(ordersummary : orderSummary)
        }
    }
    
    func setAddressData(ordersummary : OrderSummary?) {
        
        if LocationSingleton.sharedInstance.searchedAddress != nil {
            let add = LocationSingleton.sharedInstance.searchedAddress?.formattedAddress ?? ""
            let city = LocationSingleton.sharedInstance.searchedAddress?.locality ?? ""
            let country = LocationSingleton.sharedInstance.searchedAddress?.country ?? ""
            let id = LocationSingleton.sharedInstance.searchedAddress?.id ?? ""
            var lati = String()
            var longi = String()
            
            if let lat = LocationSingleton.sharedInstance.searchedAddress?.lat {
                lati = String(lat)
            }
            if let long = LocationSingleton.sharedInstance.searchedAddress?.long  {
                longi = String(long)
            }
            
            let obj : Address = Address(name: nil, address: add, landmark: nil, houseNo: nil, buildingName: nil, city: city, country: country, placeLink: nil, area: nil, lat: lati, long: longi, id: id)
            
            self.selectedAddress = obj
        }
        
        self.webServiceForUpdateCartInfo(orderSummary: ordersummary)
    }
}

//MARK: - LoginScreen Delegates
extension CartViewController : LoginViewControllerDelegate {
    
    func userFailedLoggedIn() {
        // Show Alert here
        
    }
    
    func userSuccessfullyLoggedIn(withUser user: User?) {
        
    }
    
}

//MARK: - Table View Methods
extension CartViewController {
    
    func getCartFromDB(){
        DBManager().getCart {
            [weak self] (array) in
            self?.configureTableDataSource(products: array)
        }
    }
    
    func configureTableDataSource(products : [AnyObject]?){
        if products?.count == 0 {
            selectedAgentId = nil
            cartProdcuts = [AnyObject]()
            DBManager.sharedManager.cleanCart()
            //self.placeOrderHeight.constant = 0
            self.noteViewHeight.constant = 0
            self.btnProceedCheckout.isHidden = true
            self.imgPlaceholderNotext.isHidden = false
            return
        }
        self.imgPlaceholderNotext.isHidden = true
        cartProdcuts = products
        configureTableView(arrayProducts: cartProdcuts)
        //        tableView.reloadTableViewData(inView: view)
        self.PriceView.isHidden = false
        //self.placeOrderHeight.constant = 50
        self.btnProceedCheckout.isHidden = false
        self.noteViewHeight.constant = 70
    }
    
    func configureTableView(arrayProducts : Array<AnyObject>?){
        guard let array = arrayProducts else { return }
        var idArray = [String]()
        
        for i in 0...array.count-1 {
            let product = Product(cart: array[/i] as? Cart)
            idArray.append(product.product_id ?? "")
            self.pickupType.append(product.self_pickup ?? 1)
            GDataSingleton.sharedInstance.supplierAddress = product.supplierAddressCart ?? ""
            //self.locationFetched(address : product.supplierAddressCart ?? "")
        }
        
        self.getProductAccToAddons(array: array)
        if idArray.count > 0 {
            //self.checkProductList(ids: idArray)
        }
        
    }
    
    func getProductAccToAddons(array : Array<AnyObject>?) {
        guard let arrayCart = array else {return}
        
        
        for i in 0...arrayCart.count-1 {
            let product = Product(cart: arrayCart[i] as? Cart)
            guard let productId = product.id else {return}
            
            if let addonId = product.addOnId, addonId != "" {
                DBManager.sharedManager.getTypeIdsOfAddonId(productId: productId, addonId: addonId) { (typeIdArray) in
                    
                    for j in 0...typeIdArray.count-1 {
                        var productObj : Product?
                        productObj = Product(cart: arrayCart[i] as? Cart)
                        DBManager.sharedManager.getAddonsDataFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeIdArray[j]) {(addonModalArray) in
                            print(addonModalArray)
                            for addons in addonModalArray {
                                let index = addons.addonData?.count == 1 ? 0 : j
                                var tempArray = [[AddonValueModal]]()
                                if let aa = addons.addonData?[index] {
                                    tempArray.append(aa)
                                }
                                productObj?.arrayAddonValue = tempArray
                                productObj?.arrayAddOnValue = tempArray
                                productObj?.typeId = addons.typeId
                                productObj?.addOnId = addons.addonId
                                productObj?.perAddonQuantity = addons.quantity
                                guard let obj = productObj else {return}
                                productArray.append(obj)
                                self.getAmount(array: self.productArray)
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            } else {
                productArray.append(product)
                self.getAmount(array: self.productArray)
                self.tableView.reloadData()
            }
        }
        
    }
    
    func checkProductList(ids: [String]) {
        if !refreshPriceData { return }
        let objR = API.checkProductList(product_ids: ids)
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) { [weak self] (result) in
            guard let `self` = self else { return }
            switch result {
            case .Success(let object):
                self.refreshPriceData = false
                let obj = object as? CheckProductList
                self.tipItems = obj?.tips
                guard let products = obj?.result else {
                    self.tableView.reloadData()
                    return
                }
                for product in products {
                    DBManager.sharedManager.refreshPriceToCart(product: product)
                }
                self.getCartFromDB()
                break
            case .Failure(_):
                APIManager.sharedInstance.hideLoader()
                break
            }
        }
    }
    
    
    
    func handleCellSelction(indexPath : IndexPath){
        if tableView.cellForRow(at: indexPath) is ProductListingCell, false {
            
            let productDetailVc = StoryboardScene.Main.instantiateProductDetailViewController()
            productDetailVc.passedData.productId = (tableDataSource.items?[indexPath.row] as? Cart)?.id
            productDetailVc.suplierBranchId = (tableDataSource.items?[indexPath.row] as? Cart)?.supplierBranchId
            pushVC(productDetailVc)
            
        }
    }
    
    
    func getUserAdresses(isAgent: Bool, branchId: String?, block: ((Delivery) -> ())?) {
        
        //APIManager.sharedInstance.showLoader()
        let objR = API.Addresses(FormatAPIParameters.Addresses(supplierBranchId: branchId, areaId: LocationSingleton.sharedInstance.location?.areaEN?.id).formatParameters())
        
        APIManager.sharedInstance.opertationWithRequest(withApi: objR) {
            (result) in
            
            switch result {
            case .Success(let object):
                guard let delivery = object as? Delivery else { return }
                
                if !isAgent && /GDataSingleton.sharedInstance.appSettingsData?.isScheduled {
                    
                    let objD = DeliverySpeed(selected: false, name: L10n.SCHEDULED.string)
                    objD.type = .scheduled
                    delivery.deliverySpeeds?.append(objD)
                }
                block?(delivery)
            case .Failure(_):
                APIManager.sharedInstance.hideLoader()
                break
            }
        }
    }
    
}

//MARK:- ======== handleCartQuantity ========
extension CartViewController {
    
    @objc func placeOrder(){
        self.orderHeaderLabel.text = "Order Confirmation"
        if let payType = GDataSingleton.sharedInstance.PayOption {
            if payType == "paid"{
                //self.payNowHeight.constant = 0
                self.payNowButton.isHidden = true
                self.payNow = "true"
            }else if payType == "paylatter"{
                //self.payNowHeight.constant = 50
                self.payNowButton.isHidden = false
                self.payNow = "false"
            }
        }
        if GDataSingleton.sharedInstance.selfPickup == 1 || GDataSingleton.sharedInstance.isHotel == true{
            self.payNow = "true"
        }
        if self.productArray.count != 0 {
            self.webServiceAddToCart()
        }
    }
    
    @objc func orderPaid(){
        self.orderHeaderLabel.text = "Order Confirmation"
        if let payType = GDataSingleton.sharedInstance.PayOption {
            if payType == "paid"{
                //self.payNowHeight.constant = 0
                self.payNowButton.isHidden = true
                self.payNow = "true"
            }else if payType == "paylatter"{
                //self.payNowHeight.constant = 50
                self.payNowButton.isHidden = false
                self.payNow = "false"
            }
        }
    }
    
    @objc func handleCartQuantity(sender : NSNotification){
        
        let visibleCells = tableView.visibleCells
        if visibleCells.first is CartBillCell {
            tableView?.isHidden = true
            address_view.isHidden = true
            bottom_view.isHidden = true
            return
        }
        for visibleCell in visibleCells {
            
            guard let cell = visibleCell as? ProductListingCell else { return }
            
            if cell.stepper?.value == 0 {
                guard let indexpath = tableView.indexPath(for: cell) else { return }
                tableView.beginUpdates()
                if tableDataSource.items?.count != 0 {
                    tableDataSource.items?.remove(at: indexpath.row)
                }
                tableView.deleteRows(at: [indexpath], with: .right)
                tableView.endUpdates()
                
                if tableDataSource.items?.count == 0{
                    tableView?.isHidden = true
                    address_view.isHidden = true
                    bottom_view.isHidden = true
                } else {
                    tableView?.isHidden = false
                    address_view.isHidden = false
                    bottom_view.isHidden = false
                    tableView.reloadData()
                }
            } else {
                
                getCartFromDB()
            }
        }
        
    }
}
//MARK: - TextView Delegate
extension CartViewController : UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        remarks = textView.text
    }
}


//MARK::- TableView Delegate , dataSOurce
extension CartViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.outstandingProductData.count//rows//self.outstandingData.count
        }else if section == 1 {
            return productArray.count
        }else{
            return self.paidProductData.count //self.order.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
            if self.outstandingProductData.count > indexPath.row{
                let data = self.outstandingProductData[indexPath.row]
                print(data)
                if let name = data.name{
                    cell.itemName?.text = name
                    if let userName = data.userName{
                        cell.itemName?.text =  name +  " (\( userName))"  
                        cell.itemName.textColor =  UIColor(hexString: data.colorCode ?? "")
                    }
                    
                    /* if let productDesc = data.pres_description {
                     cell.addOnLabel?.text = productDesc
                     }*/
                    if let quantity = data.quantity {
                        
                        if let addonQuantity = data.addOnQuantity {
                            cell.quantityLAbel.text = "Quantity:- \(addonQuantity)"
                        }else{
                            cell.quantityLAbel.text = "Quantity:- \(quantity)"
                            
                        }
                    }
                    
                    if let price = data.price {
                        cell.itemPrice?.text = "د.إ " + "\(price)"
                        
                        if let addonPrice = data.addOnPrice{
                            let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                            let addOnPrice = Double(addonPrice)
                            let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                            cell.itemPrice?.text = "د.إ " + "\(finalPrice)"
                        }
                    }
                    
                    
                    if let addOn = data.adds_on_type_name{
                        cell.addOnLabel?.text = "Add On: \(addOn)"
                    }
                    
                    if let freeProduct = data.isFree {
                        if freeProduct == true{
                            if let price = data.price {
                                let text = "د.إ " + "\(price)"
                                let freeQuantity = /data.freeQuantity
                                let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
                                
                                let partOne = NSMutableAttributedString(string: "Free(\(freeQuantity)) ", attributes: yourAttributes)
                                let partTwo =  NSMutableAttributedString(string: text)
                                //partTwo.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, partTwo.length))
                                let combination = NSMutableAttributedString()
                                
                                combination.append(partOne)
                                combination.append(partTwo)
                                cell.itemPrice?.attributedText =   combination
                            }
                        }else{
                            if let price = data.price {
                                cell.itemPrice?.text = "د.إ " + "\(price)"
                            }
                        }
                    }
                    
                    if let freeAddon = data.isFreeAddon{
                        if freeAddon == true{
                            if let addonPrice = data.addOnPrice{
                                let text = "د.إ " + "\(addonPrice)"
                                let freeQuantity = /data.FreeAddonQuantity
                                let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
                                
                                let partOne = NSMutableAttributedString(string: "Free(\(freeQuantity)) ", attributes: yourAttributes)
                                let partTwo =  NSMutableAttributedString(string: text)
                                //partTwo.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, partTwo.length))
                                let combination = NSMutableAttributedString()
                                
                                combination.append(partOne)
                                combination.append(partTwo)
                                cell.itemPrice?.attributedText =   combination
                            }
                        }
                    }
                }
            }
            return cell
        }else  if indexPath.section == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
            if self.paidProductData.count > indexPath.row{
                let data = self.paidProductData[indexPath.row]
                print(data)
                if let name = data.name{
                    if let userName = data.userName{
                        cell.itemName?.text =  name +  " (\( userName))"
                        cell.itemName.textColor = UIColor(hexString: data.colorCode ?? "")
                    }
                    /*  if let price = data.price {
                     cell.itemPrice?.text = "د.إ " + "\(price)"
                     }*/
                    if let price = data.price {
                        cell.itemPrice?.text = "د.إ " + "\(price)"
                        
                        if let addonPrice = data.addOnPrice{
                            let fixedPrice = Double(data.fixedPrice ?? "0.0") ?? 0.0
                            let addOnPrice = Double(addonPrice)
                            let finalPrice = /fixedPrice  + /addOnPrice//Double(data.fixedPrice ?? "0.0") ?? 0.0 + /Double(addonPrice)
                            cell.itemPrice?.text = "د.إ " + "\(finalPrice)"
                        }
                    }
                    if let freeProduct = data.isFree {
                        if freeProduct == true{
                            if let price = data.price {
                                let text = "د.إ " + "\(price)"
                                let freeQuantity = /data.freeQuantity
                                let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
                                
                                let partOne = NSMutableAttributedString(string: "Free(\(freeQuantity)) ", attributes: yourAttributes)
                                let partTwo =  NSMutableAttributedString(string: text)
                                //partTwo.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, partTwo.length))
                                let combination = NSMutableAttributedString()
                                
                                combination.append(partOne)
                                combination.append(partTwo)
                                cell.itemPrice?.attributedText =   combination
                            }
                        }else{
                            if let price = data.price {
                                cell.itemPrice?.text = "د.إ " + "\(price)"
                            }
                        }
                    }
                    
                    if let addOn = data.adds_on_type_name{
                        cell.addOnLabel?.text = "Add On: \(addOn)"
                    }
                    
                    /*  if let productDesc = data.pres_description {
                     cell.addOnLabel?.text = productDesc
                     }
                     
                     if let quantity = data.quantity {
                     cell.quantityLAbel.text = "Quantity:- \(quantity)"
                     }*/
                    if let quantity = data.quantity {
                        
                        if let addonQuantity = data.addOnQuantity {
                            cell.quantityLAbel.text = "Quantity:- \(addonQuantity)"
                        }else{
                            cell.quantityLAbel.text = "Quantity:- \(quantity)"
                            
                        }
                    }
                    if let freeAddon = data.isFreeAddon{
                        if freeAddon == true{
                            if let addonPrice = data.addOnPrice{
                                let text = "د.إ " + "\(addonPrice)"
                                let freeQuantity = /data.FreeAddonQuantity
                                let yourAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
                                
                                let partOne = NSMutableAttributedString(string: "Free(\(freeQuantity)) ", attributes: yourAttributes)
                                let partTwo =  NSMutableAttributedString(string: text)
                                //partTwo.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, partTwo.length))
                                let combination = NSMutableAttributedString()
                                
                                combination.append(partOne)
                                combination.append(partTwo)
                                cell.itemPrice?.attributedText =   combination
                            }
                        }
                    }
                }
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as? MenuListCell else { return UITableViewCell() }
            if self.productArray.count > indexPath.row{
                let product = self.productArray[indexPath.row]
                cell.product = product
                //let userData =  GDataSingleton.sharedInstance.loggedInUser
                //let name  = userData?.firstName ?? ""
                cell.itemName?.text = /product.name //+ " (\(name))"
                var addOn = ""
                for i in 0 ..< /product.arrayAddOnValue?[0].count{
                    let data = product.arrayAddOnValue?[0]
                    if i != 0{
                        addOn = addOn + "," + /data?[i].type_name
                    }else{
                        addOn = /data?[i].type_name
                    }
                    
                }
                if addOn != ""{
                    cell.addOnLabel?.text = "Add On: \(addOn)"
                }
                print(product.arrayAddonValue?.count)
                //cell.addOnLabel?.text = "Add On: \(addOn)"//product.addOnValue?[0].type_name //"" change by sandeep
                cell.itemName.textColor = UIColor(hexString: self.userColor )
                cell.itemPrice?.text =  "د.إ " + "\(product.price ?? "")"
                cell.quantityLAbel.text = "Quantity:- \(product.perAddonQuantity?.toString  ?? "")"
                cell.updatePrice()
            }
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:0))
        view.backgroundColor = UIColor.clear
        switch section {
        case 0 :
            if self.outstandingData.count > 0 {
                let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                let label = UILabel(frame: CGRect(x:10, y:5, width:tableView.frame.size.width, height:18))
                label.font =  UIFont(name: Fonts.ProximaNova.Bold, size: 17)
                view.addSubview(label)
                label.text = "Outstanding Orders";
                return view
            }
        case 1:
            if self.productArray.count > 0 {
                let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                let label = UILabel(frame: CGRect(x:10, y:5, width:tableView.frame.size.width, height:18))
                label.font =  UIFont(name: Fonts.ProximaNova.Bold, size: 17)
                view.addSubview(label)
                label.text = "Your Orders"
                return view
            }
        case 2:
            if self.paidData.count > 0 {
                let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
                let label = UILabel(frame: CGRect(x:10, y:5, width:tableView.frame.size.width, height:18))
                label.font =  UIFont(name: Fonts.ProximaNova.Bold, size: 17)
                view.addSubview(label)
                label.text = "Paid Orders"
                return view
            }
        default:
            print("")
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func calculateTotalPrice(){
        if let sum = self.orderDetails.first?.totalOrderNetAmount{
            tableTotal = total + Double(sum)!
            self.totalPrice.text = "د.إ " + "\(tableTotal)"
        }
    }
}
//MARK: - Product Listing Gesture Recognizer
extension CartViewController {
    func getAmount(array : [Product]){
        let addOnCharge: Double = 0
        CartBillCell.getNewTotalPrice(promo: self.promoCode, addOnCharges: addOnCharge, cart: array) {
            [weak self] (totalPrice, deliveryCharges, discountOnTotal, handlingCharges, qtyTotal) in
            guard let self = self else { return }
            
            Cart.totalTax = String(handlingCharges)
            self.itemSelectedPriceLAbel.text = "Your order total :- "
            if totalPrice == 0.0 {
                //self.priceDetailHeight.constant = 50
                self.selectedItemPrice.text = "" // totalPrice.addCurrencyLocale
            }else{
                //self.priceDetailHeight.constant = 100
                self.total = (totalPrice.rounded())//+ handlingCharges // deliveryCharges
                self.tableTotal = self.total
                self.selectedItemPrice.text = "د.إ " + "\(self.total)" // totalPrice.addCurrencyLocale
            }
            if let orderAmount = self.orderDetails.first?.myOrderAmount{
                let myTotal = self.total + Double(orderAmount)!
                self.selectedItemPrice.text  = "د.إ " + "\(myTotal)"
            }
        }
    }
    
    func getCardListing(){
        APIManager.sharedInstance.showLoader()
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/cardListing"
        print(url)
        print(headers)
        executeGETTHEADERS(view: self.view, path: url , headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            self.refreshControl.endRefreshing()
            let status = response["message"].string
            if(status == "Success"){
                print(response)
                if let dict = response.dictionaryObject {
                    if let data = dict["data"] as? Dictionary<String,Any>{
                        if let cards = data["cards"] as? [Dictionary<String,AnyObject>]{
                            if cards.count > 0 {
                                if let userInfo = Mapper<CardDetails>().mapArray(JSONObject: cards){
                                    self.CardDetail = userInfo
                                }
                                DispatchQueue.main.async {
                                    let VC = CardListingVC.getVC(.digiHome)
                                    VC.arrayCard = self.CardDetail
                                    VC.amount = self.total
                                    self.pushVC(VC)
                                }
                            }
                        }
                    }
                }
            }else if status == "No Data found" {
                if GDataSingleton.sharedInstance.selfPickup == 1{
                    self.doPayment(cardId: -1)
                }else{
                    let VC = ChoosePayViewControllerViewController.getVC(.digiHome)
                    VC.amount = self.total
                    VC.paymentBool = false
                    VC.comingWithNewCard = "false"
                    self.pushVC(VC)
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
            }else if status_code == "The network connection was lost."{
            }
        }
    }
    
    func doPayment(cardId : Int){
        
        let supplierId = Int(/GDataSingleton.sharedInstance.supplierId) ?? 0
        APIManager.sharedInstance.showLoader()
        var params = [String : Any]()
        if cardId == -1{
            params = ["net_amount" : self.tableTotal, "supplier_id" : supplierId] as [String : Any]
        }else{
            params = ["net_amount" : self.tableTotal, "supplier_id" : supplierId, "userSavedCardId" : cardId ] as [String : Any]
        }
        
        let headers = ["secretdbkey": AgentCodeClass.shared.clientSecretKey, "Authorization": /GDataSingleton.sharedInstance.loggedInUser?.token]
        let url = "https://api.digidine.ae/getPaymentLinkNonTable"
        print(url)
        print(params)
        print(headers)
        executePOSTHEADERS(view: self.view, path: url , parameter: params, headers: headers, success: { response in
            APIManager.sharedInstance.hideLoader()
            let status = response["message"].string
            if(status == "Success"){
                print(response)
                if let dict = response.dictionaryObject {
                    if let obj = dict["data"] as? Dictionary<String,Any>{
                        if let paymentLink = obj["payment_href"]{
                            print(paymentLink)
                            if let userPayment = obj["userPaymentLinkId"]{
                                if cardId == -1{
                                    let VC = PaymentGateViewController.getVC(.digiHome)
                                    VC.paymentLink = "\(paymentLink)"
                                    VC.userPaymentLinkId = userPayment as? Int ?? 1
                                    VC.comingWithNewCard = "false"
                                    GDataSingleton.sharedInstance.userPaymentLinkId = userPayment as? Int ?? 1
                                    self.pushVC(VC)
                                }else{
                                    GDataSingleton.sharedInstance.userPaymentLinkId = userPayment as? Int ?? 1
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderNotifi"), object: nil, userInfo: nil)
                                    self.popVC()
                                }
                            }
                        }
                    }
                }
            }
        }){ (error) in
            print(error)
            APIManager.sharedInstance.hideLoader()
            let status_code = error.localizedDescription
            //            self.showToast(message: status_code)
            if status_code.contains("401"){
            }else if status_code == "The network connection was lost."{
            }
        }
    }
}



//MARK:- UIViewControllerTransitioningDelegate
extension CartViewController : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}

//MARK:- Delivery View Controller and Payment view controller code merged
extension CartViewController{
    
    func webServiceForUpdateCartInfo(orderSummary: OrderSummary?){
        
        //Nitin
        var deliveryAddressId = "0"
        if DeliveryType.shared != .pickup {
            deliveryAddressId = self.selectedAddress?.id ?? "0"
        }
        self.deliveryData.deliveryMaxTime = "\(/orderSummary?.deliveryMaxTime)"
        guard let maxDays = Int(deliveryData.deliveryMaxTime ?? "0") else { return }
        let timeInterval = Double(60 * maxDays)
        
        let deliveryDate = (deliveryData.pickupDate ?? Date()).addingTimeInterval(timeInterval)
        
        self.deliveryData.handlingSupplier = "0.0"
        self.deliveryData.deliveryCharges = String(orderSummary?.dDeliveryCharges ?? 0.0)
        self.deliveryData.minOrderDeliveryCrossed = "1"
        self.deliveryData.handlingAdmin = orderSummary?.handlingCharges
        self.deliveryData.handlingSupplier = orderSummary?.handlingSupplier
        //.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
        if let minOrderPrice = self.deliveryData.minOrderDeliveryCharge?.toDouble(),let totalCharges = self.deliveryData.totalPrice?.toDouble() {
            self.deliveryData.minOrderDeliveryCrossed = totalCharges > minOrderPrice ? "1" : "0"
        } else {
            self.deliveryData.minOrderDeliveryCrossed = "1"
        }
        
        DeliveryViewController.updateCart(delivery: self.deliveryData, order: self.orderSummary, deliverySpeed: selectedDeliverySpeed, addressId: deliveryAddressId, deliveryDate: deliveryDate, remarks: "", addOn: orderSummary?.addOnCharge, newTotal: nil ) {
            [weak self] in
            
            guard let self = self else {
                APIManager.sharedInstance.hideLoader()
                return }
            
            self.handleUpdateCartInfo()
        }
    }
    
    func handleUpdateCartInfo(){
        
        var deliveryAddress  : Address?
        
        deliveryAddress = self.selectedAddress
        
        if let maxDays = Int(deliveryData.deliveryMaxTime ?? "0") {
            let timeInterval = Double(60 * maxDays)
            orderSummary?.deliveryDate = (deliveryData.pickupDate ?? Date()).addingTimeInterval(timeInterval)
        }
        
        orderSummary?.dDeliveryCharges = deliveryData.deliveryCharges?.toDouble()
        
        orderSummary?.initalizeOrderSummary(cart:  orderSummary?.items, forRental: true, rentalTotal: orderSummary?.netPayableAmount)
        orderSummary?.deliveryAddress = deliveryAddress
        orderSummary?.pickupAddress = deliveryData.pickupAddress
        orderSummary?.pickupDate = deliveryData.pickupDate
        orderSummary?.selectedDeliverySpeed = selectedDeliverySpeed
        
        if orderSummary?.items?.first?.category == Category.Laundry.rawValue || orderSummary?.items?.first?.category == Category.BeautySalon.rawValue {
            orderSummary?.pickupBuffer = deliveryData.deliveryMaxTime
        }
        //orderSummary?.paymentMode = deliveryData.paymentMode
        
        self.generateOrder()
    }
    
    func generateOrder(){
        
        APIManager.sharedInstance.showLoader()
        var agentId = [Int]()
        if let value = orderSummary?.agentId?.toInt() {
            agentId.append(value)
        }
        
        let timeInter = Date().timeIntervalSince(orderSummary?.currentDate ?? Date())
        
        var pickupAddress = ""
        if let add = self.parameters["pickupAddress"] as? String {
            pickupAddress = add
        }
        var dropoffAddress = ""
        if let dropAddress = self.parameters["dropoffAddress"] as? String {
            dropoffAddress = dropAddress
        }
        
        var pickupApiDate = ""
        if let pickapiDate = self.parameters["pickupApiDate"] as? String {
            pickupApiDate = pickapiDate
        }
        
        var dropoffApiDate = ""
        if let dropoapiDate = self.parameters["dropoffApiDate"] as? String {
            dropoffApiDate = dropoapiDate
        }
        
        
        var pickupCordinates = CLLocationCoordinate2D()
        if let pickCordinates = self.parameters["pickupCordinates"] as? CLLocationCoordinate2D {
            pickupCordinates = pickCordinates
        }
        
        var dropoffCordinates = CLLocationCoordinate2D()
        if let dropoffCord = self.parameters["dropoffCordinates"] as? CLLocationCoordinate2D {
            dropoffCordinates = dropoffCord
        }
        let pickupLatitude = pickupCordinates.latitude
        var tableId = Int()
        var pay_mode = String()
        var paynow = Bool()
        var OrdeRef = String()
        var userPaymentLinkId = Int()
        var pres_description = String()
        let pickupLongitude = pickupCordinates.longitude
        let dropoffLatitude = dropoffCordinates.latitude
        let dropoffLongitude = dropoffCordinates.longitude
        if self.payNow == "true"{
            paynow = true
        }else if self.payNow == "false"{
            paynow = false
        }
        pres_description = self.noteTextfield.text ?? ""
        tableId = Int(/GDataSingleton.sharedInstance.tableNumber) ?? 0
        pay_mode = "4"
        OrdeRef = /GDataSingleton.sharedInstance.OrderRef
        userPaymentLinkId = /GDataSingleton.sharedInstance.userPaymentLinkId
        //        }else{
        //            tableId = "0"
        //            pay_mode = "0"
        //            pay_type = "false"
        //            OrdeRef = ""
        //        }
        let objR = API.GenerateOrder(FormatAPIParameters.GenerateOrder(
                                        useReferral: /orderSummary?.useReferral,
                                        promoCode: orderSummary?.promoCode,
                                        cartId: orderSummary?.cartId,
                                        isPackage: orderSummary?.isPackage,
                                        paymentType:selectedPaymentMethod ?? PaymentMode(paymentType: .Card),
                                        agentIds: agentId,
                                        deliveryDate: orderSummary?.currentDate ?? Date(),
                                        duration: /orderSummary?.duration, from_address: pickupAddress, to_address: dropoffAddress, booking_from_date: pickupApiDate, booking_to_date: dropoffApiDate, from_latitude: pickupLatitude, to_latitude: dropoffLatitude, from_longitude: pickupLongitude, to_longitude: dropoffLongitude,
                                        tip_agent: GDataSingleton.sharedInstance.tipAmount,
                                        arrPres: arrPrescription,
                                        instructions: txtPrescription,
                                        bookingDate: GDataSingleton.sharedInstance.pickupDate, questions: orderSummary?.questions, customer_payment_id: GDataSingleton.sharedInstance.customerPaymentId, table_id : tableId, gateway_unique_id : "ngenius", order_reference : OrdeRef, paynow : paynow, userPaymentLinkId : userPaymentLinkId, pres_description : pres_description).formatParameters())
        //deliveryDate?.add(seconds: Int(timeInter)) //Nitin
        
        APIManager.sharedInstance.opertationWithRequest(refreshControl: nil, isLoader: false, withApi: objR) { [weak self] (response) in            APIManager.sharedInstance.hideLoader()
            weak var weakSelf = self
            switch response {
            case .Success(let object):
                self?.noteTextfield.text = ""
                weakSelf?.handleGenerateOrder(orderId: object)
            case .Failure(_):
                break
            }
        }
    }
    
    func handleGenerateOrder(orderId : Any?){
        
        //print(orderId)
        AdjustEvent.Order.sendEvent(revenue: orderSummary?.totalAmount)
        GDataSingleton.sharedInstance.tipAmount = 0
        DBManager.sharedManager.cleanCart()
        DispatchQueue.main.async {
            self.productArray.removeAll()
            //self.selectedItemPrice.text =  "د.إ " + "0.0"
            //self.placeOrderHeight.constant = 0
            self.noteViewHeight.constant = 0
            self.btnProceedCheckout.isHidden = true
            if GDataSingleton.sharedInstance.selfPickup == 0{
                self.PriceView.isHidden = false
                self.tableOrders(showLoader: false, promoCode1: 0)
                self.tableView.reloadData()
            }else{
                self.PriceView.isHidden = true
                self.tableInfoHeight.constant = 0
            }
        }
        UtilityFunctions.showSweetAlert(title: L10n.OrderPlacedSuccessfully.string, message: L10n.YourOrderHaveBeenPlacedSuccessfully.string, style: .Success, success: {
            [weak self] in
            guard let self = self else { return }
            
            if self.orderSummary?.selectedDeliverySpeed == .scheduled {
                
                let VC = StoryboardScene.Order.instantiateOrderSchedularViewController()
                let orderIdArr = orderId as? [Int]
                let ordrs = orderIdArr?.map({ (orderId) -> String in
                    return String(orderId)
                })
                VC.orderId = ordrs?.joined(separator: ",")
                VC.orderSummary = self.orderSummary
                VC.orderSummary?.paymentMode = self.selectedPaymentMethod ?? PaymentMode(paymentType: .COD)
                self.pushVC(VC)
            }
            else{
                let orderIdArr = orderId as? [Int]
                let ordrs = orderIdArr?.map({ (orderId) -> String in
                    //order = String(orderId)
                    //orderId = GDataSingleton.sharedInstance.OrderId
                    self.orderId = String(orderId)
                    return String(orderId)
                })
                if GDataSingleton.sharedInstance.selfPickup == 1 || GDataSingleton.sharedInstance.isHotel == true{
                    self.outstandingData.removeAll()
                    self.paidData.removeAll()
                    self.tableView.reloadData()
                    let orders = ordrs?.joined(separator: ",")
                    GDataSingleton.sharedInstance.orderId = self.orderId
                    self.getOrderDetails(orderId: orders)
                    DispatchQueue.main.async{
                        if let nonTable = self.nonTableOrderView{
                            if GDataSingleton.sharedInstance.selfPickup == 1 {
                                self.nonTanleOrderLabel.text = "Above your order receipt number you need to show this reciept number to the resturant to collect the order."
                            }else{
                                self.nonTanleOrderLabel.text = "This is your order reciept for your room, order will be delivered soon"
                            }
                            nonTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                            self.view.addSubview(nonTable)
                        }
                    }
                    
                    //                    let orders = ordrs?.joined(separator: ",")
                    //                    let orderDetailVc = OrderDetailController.getVC(.digiHome)
                    //                    orderDetailVc.isBuyOnly = /self.orderSummary?.isBuyOnly
                    //                    orderDetailVc.orderDetails = OrderDetails(orderSummary: self.orderSummary,orderId: orders,scheduleOrder: "")
                    //                    orderDetailVc.type = .OrderUpcoming
                    //                    orderDetailVc.isOrderCompletion = true
                    //                    self.pushVC(orderDetailVc)
                }
            }
        })
    }
}


extension CartViewController {
    
    func checkAddonExistAcctoTypeid(productId: String, addonId: String,typeId:String, finished: (_ isContain: Bool, _ data: [AddonsModalClass]?)-> Void) {
        
        DBManager.sharedManager.getAddonsDataFromDbAcctoTypeId(productId: productId, addonId: addonId, typeId: typeId) { (data) in
            print(data)
            finished(data.count == 0 ? false : true, data.count == 0 ? nil : data)
        }
        
    }
    
}
extension UIColor {
    static var random: UIColor {
        return .init(hue: .random(in: 0...1), saturation: 1, brightness: 1, alpha: 1)
    }
}
extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}

//MARK:- non table
extension  CartViewController {
    func getOrderDetails(orderId : String?) {
        
        let orderIdArr = orderId?.components(separatedBy:[","])
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderDetail(FormatAPIParameters.OrderDetail(orderId: orderIdArr).formatParameters())) { [weak self] (response) in
            switch response {
            case .Success(let object):
                
                self?.orderHistory =  object as? OrderHistory
                let orderDetailsArr = self?.orderHistory?.orderDetails
                let miltipleOrders = self?.orderHistory?.orderDetails?.count ?? 0 > 1
                self?.resturantNameLabel.text = self?.orderHistory?.orderDetails?.first?.supplierName
                self?.orderNumberLabel.text = orderId
                //                if miltipleOrders {
                //                    self?.constraintBottomBtnNext.constant = 0
                //                }
                // self?.btnPrevoius.isHidden = self?.orderHistory?.orderDetails?.count ?? 0 > 1 ? false : true
                
                self?.nonTableOrderDetails = orderDetailsArr?.first
                //
                if let status = self?.nonTableOrderDetails?.status {
                    //                    self?.btnCancel.isHidden = status == OrderDeliveryStatus.Pending ? false : true
                    //                    self?.btnReOrder.isHidden = (status == OrderDeliveryStatus.Delivered || status == OrderDeliveryStatus.Rejected || status == OrderDeliveryStatus.CustomerCancel) ? false : true
                }
                //self?.setupButtons()
                self?.configureOrderDetailDataSource()
                
            default: break
                
            }
        }
    }
}
extension CartViewController {
    
    func configureOrderDetailDataSource(){
        
        nontableView.estimatedRowHeight = 200.0
        var itemArray = [ProductDetailData]()
        let tax =  self.nonTableOrderDetails?.handling_admin ?? ""
        if let product = self.orderHistory?.orderDetails?.first?.product {
            for i in 0...product.count-1 {
                if let addons = product[i].productDetailAddson {
                    let grouped = Dictionary(grouping: addons) { (addon) -> Int in
                        return addon.serial_number ?? 0
                    }
                    print(grouped)
                    for (key,value) in grouped {
                        print(key,value)
                        let data = grouped[key]
                        let quant = data?[0].quantity ?? 0
                        let obj = ProductDetailData(name: product[i].name ?? "", quantity: String(quant), measuringUnit: product[i].measuringUnit, price: product[i].price ?? "", image: product[i].image ?? "", productDetailAddson: value, variants: [])
                        itemArray.append(obj)
                        //                        for data in grouped[key] ?? []{
                        //                            let obj = ProductDetailData(name: product[i].name ?? "", quantity: String(data.quantity ?? 0), measuringUnit: product[i].measuringUnit, price: product[i].price ?? "", image: product[i].image ?? "", productDetailAddson: value)
                        //                                itemArray.append(obj)
                        //                        }
                    }
                    //                    for item in grouped {
                    //                        let obj = ProductDetailData(name: product[i].name ?? "", quantity: "1", measuringUnit: product[i].measuringUnit, price: product[i].price ?? "", image: product[i].image ?? "", productDetailAddson: item.value)
                    //                        itemArray.append(obj)
                    //                    }
                } else {
                    let obj = ProductDetailData(name: product[i].name ?? "", quantity: product[i].quantity ?? "", measuringUnit: product[i].measuringUnit, price: product[i].price ?? "", image: product[i].image ?? "", productDetailAddson: nil, variants: product[i].selectedVariants)
                    itemArray.append(obj)
                }
            }
        }
        print(itemArray)
        
        nonTableDataSource = OrderDetailDataSource(items: itemArray, data: tax, height: UITableView.automaticDimension, tableView: nontableView, cellIdentifier: CellIdentifiers.OrderStatusCell, configureCellBlock: { [weak self](cell, item) in
            guard let self = self else { return }
            guard let data = item as? ([ProductDetailData],String) else {return}
            
            let array = data.0
            let tax = data.1
            self.configureOrderStatusCell(cell: cell as? OrderStatusCell, item: array, tax: tax)
            self.configureOrderDetailCell(cell: cell as? OrderDetailCell, tax: tax)
            
        }, aRowSelectedListener: { (indexPath) in
            
        }, aRowSwipeListner: { (indexPath) in
            
        })
        nontableView.delegate = nonTableDataSource
        nontableView.dataSource = nonTableDataSource
        nontableView.reloadTableViewData(inView: view)
        
    }
    
    func configureOrderDetailCell(cell : OrderDetailCell?,tax: String) {
        guard let c = cell else { return }
        Cart.totalTax = tax
        
        c.cellType = type ?? .OrderUpcoming
        c.tax = tax
        c.orderDetails = nonTableOrderDetails
        
        c.vwBgAgentH.constant = nonTableOrderDetails?.agentArray?.count == 0 ? 0 : 117
        c.cblUser = nonTableOrderDetails?.agentArray?.first
        
        let questions = nonTableOrderDetails?.questions ?? []
        c.viewQuestions.isHidden = questions.isEmpty
        
        let myViews = c.stackQuestions.subviews.filter{$0 is CartQuestionView}
        if /myViews.count > 0{
            for productView in myViews {
                productView.removeFromSuperview()
            }
        }
        
        for (i, question) in questions.enumerated() {
            let view = CartQuestionView(frame: CGRect(x: 0, y: 0, w: c.stackQuestions.size.width, h: 100))
            view.configureView(question: question, index: i, productPrice: c.subtotal)
            c.stackQuestions.addArrangedSubview(view)
        }
        cell?.layoutIfNeeded()
    }
    
    func configureOrderStatusCell(cell : OrderStatusCell?,item: [ProductDetailData]?,tax: String){
        guard let c = cell,let products = item else { return }
        c.cellType = type ?? .OrderUpcoming
        c.totalCount = products.count
        c.tax = tax
        if nonTableOrderDetails?.orderStatus == 2 || nonTableOrderDetails?.orderStatus == 5 {
            self.nonTableOrderView.removeFromSuperview()
        }else{
            c.orderDetails = nonTableOrderDetails
        }
        let myViews = c.stackView.subviews.filter{$0 is ProductView}
        if /myViews.count > 0{
            for productView in myViews {
                productView.removeFromSuperview()
            }
        }
        
        for product in products{
            let productView = ProductView(frame: CGRect(x: 0, y: 0, w: c.stackView.size.width, h: 100))
            productView.configureProductView(product: product)
            productView.labelProductmodel.text = nonTableOrderDetails?.supplierName
            c.stackView.addArrangedSubview(productView)
        }
    }
}
