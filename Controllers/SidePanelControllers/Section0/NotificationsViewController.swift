//
//  NotificationsViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class NotificationsViewController: BaseViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var menu_button: ThemeButton!{
        didSet {//Nitin
            menu_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? true : false
        }
    }
    @IBOutlet weak var back_button: ThemeButton!{
        didSet {//Nitin
            back_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? false : true
        }
    }
    @IBOutlet weak var viewPlaceholder : UIView!
    @IBOutlet var btnClearAll: UIButton!{
        didSet{
            btnClearAll?.isEnabled = false
        }
    }
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView?.estimatedRowHeight = 44.0
            tableView?.rowHeight = UITableView.automaticDimension
        }
    }
    
    //MARK:- Variables
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    var notificationListing : NotificationListing? {
        didSet{
            dataSource.items = notificationListing?.arrayNotifications
            ez.runThisInMainThread {
                weak var weakSelf : NotificationsViewController? = self
                weakSelf?.tableView.reloadTableViewData(inView: weakSelf?.view)
                guard let notifications = weakSelf?.notificationListing?.arrayNotifications, notifications.count > 0 else{
                    weakSelf?.viewPlaceholder?.isHidden = false
                    weakSelf?.btnClearAll?.isEnabled = false
                    return
                }
                weakSelf?.btnClearAll?.isEnabled = true
                weakSelf?.viewPlaceholder?.isHidden = true

            }
        }
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        webServiceGetAllNotifications()

    }
    
}

//MARK: - WebService
extension NotificationsViewController{
    
    func webServiceGetAllNotifications(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.AllNotifications(FormatAPIParameters.AllNotifications.formatParameters())) { (response) in
            
            weak var weakSelf : NotificationsViewController? = self
            switch response{
            case .Success(let listing):
                weakSelf?.notificationListing = listing as? NotificationListing
            default :
                break
            }
        }
    }
    
    func webServiceClearAllNotifications(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ClearAllNotifications(FormatAPIParameters.ClearAllNotifications.formatParameters())) { (response) in
            
            weak var weakSelf : NotificationsViewController? = self
            switch response{
            case .Success(_):
                weakSelf?.notificationListing?.arrayNotifications = []
                weakSelf?.dataSource.items = []
                weakSelf?.tableView.reloadData()
            default :
                break
            }
        }
    }
}

//MARK: - TableView configuration Methods
extension NotificationsViewController{
    
    func configureTableView (){
        
        dataSource = TableViewDataSource(items: notificationListing?.arrayNotifications , height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.NotificationsCell , configureCellBlock: { (cell, item) in
            weak var weakSelf = self
            weakSelf?.configureTableCell(cell: cell, item: item)
            }, aRowSelectedListener: { [weak self] (indexPath) in
                self?.configureCellSelection(indexPath: indexPath)
        })
        
    }
    
    func configureTableCell(cell : Any?, item : Any?){
        (cell as? NotificationsCell)?.notification = item as? NotificationClass
    }
    
    func configureCellSelection(indexPath : IndexPath) {
        guard let notifications = dataSource.items as? [NotificationClass] else { return }
        let order = OrderDetails(orderId: notifications[indexPath.row].orderId)
        tableView.beginUpdates()
        dataSource.items?.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .right)
        tableView.endUpdates()
        
        let VC = StoryboardScene.Order.instantiateOrderDetailController()
        VC.orderDetails = order
        VC.type = .OrderUpcoming
        pushVC(VC)
    }
}


//MARK: - Button Actions
extension NotificationsViewController{
    
    @IBAction func back_buttonAction(_ sender: Any) {
        popVC()
        
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionClearAll(sender: AnyObject) {
        btnClearAll.isEnabled = true
        webServiceClearAllNotifications()
    }
}
