//
//  WishListViewController.swift
//  Sneni
//
//  Created by Sandeep Kumar on 24/06/19.
//  Copyright © 2019 Taran. All rights reserved.
//

import UIKit

class WishListViewController: UIViewController {
    
    //MARK:- ======== Outlets ========
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            configCollection()
        }
    }
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var menu_button: ThemeButton!{
        didSet {//Nitin
            menu_button.isHidden = /*SKAppType.type == .gym ||*/ SKAppType.type == .food || SKAppType.type == .home || SKAppType.type == .carRental || SKAppType.type == .eCom ? true : false
        }
    }
    
    //MARK:- ======== Variables ========
    var collectionViewDataSource: SKCollectionViewDataSource!
    
//    var blockSelectBrand: ((Product) -> ())?
    
    var arrayProducts: [Product] = [] {
        didSet {
            viewPlaceholder?.isHidden = !arrayProducts.isEmpty
            collectionViewDataSource.reload(items: arrayProducts)
        }
    }
    
    //MARK:- ======== LifeCycle ========
    override func viewDidLoad() {
        super.viewDidLoad()
        initalSetup()
   
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- ======== Actions ========
    @IBAction func actionBack(sender: AnyObject) {
        FilterCategory.shared.arraySubCatIds.removeAll()
        FilterCategory.shared.arraySubCatNames.removeAll()
        NotificationCenter.default.post(name: NSNotification.Name("SubCategoryCountChanged"), object: nil)
        popVC()
    }
    
    @IBAction func actionCart(sender: AnyObject) {
        let vc = StoryboardScene.Options.instantiateCartViewController()
        pushVC(vc)
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    //MARK:- ======== Functions ========
    func initalSetup() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFavs(notification:)), name: Notification.Name("FavouritePressed"), object: nil)
        webServiceItemListing()
    }
    
    @objc func updateFavs(notification: Notification) {
           if let product = notification.object as? Product {
            if let index = arrayProducts.firstIndex(where: {$0.id == product.id}) {
                arrayProducts.remove(at: index)
                collectionView.reloadData()
            }
           }
       }
    
    private func configCollection() {
        
        let height = CGFloat(240.0)
        let width = CGFloat(collectionView.frame.size.width/2-20)
        
        if collectionViewDataSource == nil {
            collectionView.registerCells(nibNames: [HomeProductCell.identifier])
            collectionViewDataSource = SKCollectionViewDataSource(
                items: arrayProducts,
                collectionView: collectionView,
                cellIdentifier: HomeProductCell.identifier,
                cellHeight: height,
                cellWidth: width)
        }
        
        collectionViewDataSource.configureCellBlock = {
            (index, cell, item) in
            
            if let cell = cell as? HomeProductCell {
                cell.productListing = item as? Product
            }
            
        }
        
        collectionViewDataSource.aRowSelectedListener = {
            (index, cell) in
            
            if let cell = cell as? HomeProductCell {
                self.openProduct(objProduct: cell.productListing)
            }
        }
        
        collectionViewDataSource.reload(items: arrayProducts)
    }
    
    func openProduct(objProduct: Product?) {
        
        guard let objProduct = objProduct else { return }
        let productDetailVc = StoryboardScene.Main.instantiateProductVariantVC()
        productDetailVc.passedData.productId = objProduct.id
        productDetailVc.is_question = objProduct.is_question
        productDetailVc.suplierBranchId = objProduct.supplierBranchId
        self.pushVC(productDetailVc)
        
        productDetailVc.blockUpdataData = {
            [weak self] in
            guard let self = self else { return }
            self.webServiceItemListing(isLoader: false)
        }
    }
    
    func webServiceItemListing(isLoader: Bool = true) {
        
        let api = API.listAllFav
        APIManager.sharedInstance.opertationWithRequest(isLoader: isLoader, withApi: api) {
            [weak self] (response) in
            guard let self = self else { return }
            
            switch response {
            case .Success(let listing):
                self.arrayProducts = (listing as? [Product]) ?? []
            default :
                break
            }
        }
    }
    
}
