//
//  OnboardingViewController.swift
//  InTheNight
//
//  Created by Dhan Guru Nanak on 4/22/18.
//  Copyright © 2018 InTheNight. All rights reserved.
//

import UIKit

struct OnboardingModel {
    
    //Constants
    static let discover = "DISCOVER"
    static let discoverInfo = "Explore World’s top brands\nand boutique."

    static let payment = "MAKE THE PAYMENT"
    static let paymentInfo = "Choose the preferable option\nfor payment."

    static let shopping = "ENJOY YOUR SHOPPING"
    static let shoppingInfo = "Get high quality products\nfor the best prices."

    static let search = "Search"
    static let searchInfo = "Explore World’s top restaurants and food."
    static let searchInfoHome = "Explore World’s top Services."

    static let onlineOrder = "Online Order"
    static let onlineOrderInfo = "Choose the preferable option for payment."
    
    static let delivery = "Delivery"
    static let deliveryInfo = "Choose the preferable option for payment."

    static let select = "Select"
    static let selectInfo = "Choose the preferable option for service."

    static let enjoy = "Enjoy"
    static let enjoyInfo = "Get high quality service for the best price."

    //DIGIDINE ONBOARDS
    static let reserveTable = "Scan the QR Code"
    static let reserveTableInfo = ""
    
    static let joinFriends = "Order from Your Phone"
    static let joinFriendsInfo = ""
    
    static let enjoyFood = "Apply Vouchers and Discounts"
    static let enjoyFoodInfo = ""
    
    static let differentPayment = "Different Payment Options"
    static let differentPaymentInfo = "You'll have the choice to pay for your items or split the bill with your friends"
    
    var title: String
    var subtitle: String
    var image: UIImage
}

class OnboardingViewController: UIViewController {

    //MARK:- ======== Outlets ========
    @IBOutlet weak var btnLetsStart: UIButton!{
        didSet{
            btnLetsStart.isHidden = false
        }
    }
    @IBOutlet weak var stackvwBtns: UIStackView!
    @IBOutlet weak var btnSkipUpper: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnPageControl: UIPageControl! {
        didSet {
            currentIndex = 0
            btnPageControl.numberOfPages = items.count
            btnPageControl.currentPageIndicatorTintColor = SKAppType.type.color
        }
    }
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            configureCollectionView()
        }
    }

    //MARK:- ======== Properties ========
    var collectionViewDataSource: SKCollectionViewDataSource?
    
    lazy var items: [OnboardingModel] = {
        if SKAppType.type == .food || SKAppType.type.isJNJ {
            if APIConstants.defaultAgentCode == "digidine_0239" {
                return [
                    OnboardingModel.init(title: OnboardingModel.reserveTable, subtitle: "", image: #imageLiteral(resourceName: "Group 127"))
                    , OnboardingModel.init(title: OnboardingModel.joinFriends, subtitle: "", image: #imageLiteral(resourceName: "Group 126"))
                    , OnboardingModel.init(title: OnboardingModel.enjoyFood, subtitle: "", image: #imageLiteral(resourceName: "Group 125")),
                      OnboardingModel.init(title: OnboardingModel.differentPayment, subtitle: OnboardingModel.differentPaymentInfo, image: #imageLiteral(resourceName: "Group 123"))
                    
                ]
                /*OnboardingModel.init(title: OnboardingModel.reserveTable, subtitle: OnboardingModel.searchInfo, image: #imageLiteral(resourceName: "dining"))
                                   , OnboardingModel.init(title: OnboardingModel.joinFriends, subtitle: OnboardingModel.paymentInfo, image: #imageLiteral(resourceName: "friendship"))
                                   , OnboardingModel.init(title: OnboardingModel.enjoyFood, subtitle: OnboardingModel.enjoyInfo, image: #imageLiteral(resourceName: "sandwich"))*/
            }
            return [
                OnboardingModel.init(title: OnboardingModel.search, subtitle: OnboardingModel.searchInfo, image: #imageLiteral(resourceName: "search_food"))
                , OnboardingModel.init(title: OnboardingModel.payment, subtitle: OnboardingModel.paymentInfo, image: #imageLiteral(resourceName: "online_order_food"))
                , OnboardingModel.init(title: OnboardingModel.enjoy, subtitle: OnboardingModel.enjoyInfo, image: #imageLiteral(resourceName: "delivery_food"))
            ]
        } else if SKAppType.type == .home {
            return [
                OnboardingModel.init(title: OnboardingModel.search, subtitle: OnboardingModel.searchInfoHome, image: #imageLiteral(resourceName: "search_service_homeservices"))
                , OnboardingModel.init(title: OnboardingModel.select, subtitle: OnboardingModel.selectInfo, image: #imageLiteral(resourceName: "select_agent_homeservices"))
                , OnboardingModel.init(title: OnboardingModel.enjoy, subtitle: OnboardingModel.enjoyInfo, image: #imageLiteral(resourceName: "enjoy_service_homeservices"))
            ]
        }

        return [
            OnboardingModel.init(title: OnboardingModel.discover, subtitle: OnboardingModel.discoverInfo, image: #imageLiteral(resourceName: "discover_ecommerce"))
            , OnboardingModel.init(title: OnboardingModel.payment, subtitle: OnboardingModel.paymentInfo, image: #imageLiteral(resourceName: "make the payment_ecommerce"))
            , OnboardingModel.init(title: OnboardingModel.shopping, subtitle: OnboardingModel.shoppingInfo, image: #imageLiteral(resourceName: "enjoy_shopping_ecommerce"))
        ]
    }()

    var currentIndex = 0 {
        didSet {
           self.btnPageControl.currentPage = currentIndex
//            let isLast = currentIndex > 0//currentIndex == items.count - 1
//            btnLetsStart.isHidden = !isLast
//            btnSkip.isHidden = isLast
        }
    }
    
     var comingFrom : String? = ""
    
    //MARK:- ======== LifeCycle ========
    override func viewDidLoad(){
        super.viewDidLoad()
        if comingFrom == "" {
            onLoad()
            self.backBtn.isHidden = true
        }else{
            self.backBtn.isHidden = false
        }
        
       // loadLocation(loader: false)
    }
    
    //MARK:- ======== Functions ========
    func onLoad() {
        if GDataSingleton.isOnBoardingDone {
            (UIApplication.shared.delegate as? AppDelegate)?.onload()
        }
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.popVC()
    }
    
    @IBAction func didTapSkip(_ sender:UIButton) {
        
        GDataSingleton.isOnBoardingDone = true
        (UIApplication.shared.delegate as? AppDelegate)?.onload()
        
//        if LocationSingleton.sharedInstance.location == nil {
//            loadLocation(loader: true) {
//                GDataSingleton.isOnBoardingDone = true
//                (UIApplication.shared.delegate as? AppDelegate)?.onload()
//            }
//        } else {
//            GDataSingleton.isOnBoardingDone = true
//            (UIApplication.shared.delegate as? AppDelegate)?.onload()
//        }
    }
    
    func loadLocation(loader: Bool, blockDone: (() -> ())? = nil) {
        (UIApplication.shared.delegate as? AppDelegate)?.onload()
//        if LocationSingleton.sharedInstance.location != nil {
//            return
//        }
//        blockDone?()

//        LocationViewController.getFirstCity(loader: loader, location: ApplicationLocation(), types: [SelectedLocation.Country, .City, .Area], lastId: nil) {
//            (location) in
//            LocationSingleton.sharedInstance.location = location
//            blockDone?()
//        }
        
    }
}

//MARK:- ======== Handle Table View  ========
extension OnboardingViewController {
    
    func configureCollectionView() {
        
        collectionViewDataSource = SKCollectionViewDataSource(
            collectionView: collectionView,
            cellIdentifier: OnboardingCollectionViewCell.identifier,
            headerIdentifier: nil,
            cellHeight: UIScreen.main.bounds.height,
            cellWidth: UIScreen.main.bounds.width)
        
        collectionViewDataSource?.configureCellBlock = {
            (index, cell, item) in
            
            guard let cell = cell as? OnboardingCollectionViewCell else { return }
            
            if let model = item as? OnboardingModel
            {
                cell.onboardingData = model
            }
        }
        
        collectionViewDataSource?.scrollViewListener = {
            [weak self] (scrollView) in // for swipe
            
            guard let self = self else { return }
            self.currentIndex = Int(scrollView.contentOffset.x/scrollView.frame.width)
        }
        
        collectionViewDataSource?.reload(items: items)
    }
}
